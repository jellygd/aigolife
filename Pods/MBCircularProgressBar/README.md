# MBCircularProgressBar

[![Version](https://img.shields.io/cocoapods/v/MBCircularProgressBar.svg?style=flat)](http://cocoapods.org/pods/MBCircularProgressBar)
[![License](https://img.shields.io/cocoapods/l/MBCircularProgressBar.svg?style=flat)](http://cocoapods.org/pods/MBCircularProgressBar)
[![Platform](https://img.shields.io/cocoapods/p/MBCircularProgressBar.svg?style=flat)](http://cocoapods.org/pods/MBCircularProgressBar)

![](https://raw.github.com/matibot/MBCircularProgressBar/master/Readme/example.jpg)
![](https://raw.github.com/matibot/MBCircularProgressBar/master/Readme/MBCircularProgressBar.gif)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Installation

MBCircularProgressBar is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "MBCircularProgressBar"
```

## Author

Mati Bot, matibot@gmail.com, [@b0tnik](https://twitter.com/b0tnik)

## License

MBCircularProgressBar is available under the MIT license. See the LICENSE file for more info.

