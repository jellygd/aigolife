//
//  JBaseViewController.h
//  tcp
//
//  Created by Jelly on 15/7/21.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MBProgressHUD.h>
#import "MBProgressHUD+JMessage.h"
#import "UIViewController+JShake.h"
#import <FXBlurView.h>

#define VSDeviceOSVersionIsAtLeast(X) ([[[UIDevice currentDevice]systemVersion] floatValue] >= X)

#define kSearchCount 4

typedef void(^ValidateRouterBlock)(NSNumber* validate);

@interface JBaseViewController : UIViewController


@property(nonatomic, assign) BOOL showBlur;

@property(nonatomic, strong) FXBlurView* blurNewView;


@property(nonatomic,copy)ValidateRouterBlock validateRouterBlock;

@property(nonatomic, assign) BOOL canNextTask;

@property(nonatomic, assign) NSInteger searchCommad;


-(void)setHeaderBgView:(UIView*)view;
-(void)dreawLine:(UIView*)imageView color:(UIColor*)color;
-(void)dreawEllipseLine:(UIView*)view color:(UIColor*)color;


-(void)showErrorMessage:(NSString*)message;

-(void)showRouterPWDAlertView;

-(void)validateRouter:(NSString*)pwd;



-(void)commandFail;

-(void)commandSuccess;

-(void)commandNextSearch;


-(void)searchCommandStatue:(NSString*)commandIdentifier;



-(void)loadRouterData;


-(void)loadRouterDataSuccess;

-(void)loadRouterDataFail;



-(void)nextTask;

-(void)taskAction;


-(void)showBlurView;


-(BOOL)showCanBlur;

@end
