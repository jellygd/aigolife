//
//  JDeviceTableViewCell.h
//  tcp
//
//  Created by Jelly on 15/7/25.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JDeviceTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *deviceImageView;


@property (weak, nonatomic) IBOutlet UILabel *deviceNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *deviceSpeadLabel;

@property (weak, nonatomic) IBOutlet UIImageView *deviceBlackImageView;

@end
