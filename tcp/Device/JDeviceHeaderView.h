//
//  JDeviceHeaderView.h
//  tcp
//
//  Created by Jelly on 15/7/25.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JDeviceHeaderView : UIView

@property (weak, nonatomic) IBOutlet UILabel *name;
@end
