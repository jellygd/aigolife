//
//  JDeviceDetailTableViewController.m
//  tcp
//
//  Created by Jelly on 15/7/25.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JDeviceDetailTableViewController.h"
#import "JDeviceDetailTableViewCell.h"
#import "JServerClient+JRouter.h"
#import "JTimeManagerView.h"
#import "JKAlertDialog.h"
#import <LGAlertView.h>
#import "JServerClient+JCommand.h"
#import <RXCollection.h>
#import <ActionSheetDatePicker.h>


typedef void(^SetDeviceInfoBlock)(BOOL success);

@interface JDeviceDetailTableViewController ()

@property(nonatomic, strong) NSArray* detailKey;
@property(nonatomic, strong) NSArray* detailValue;
@property(nonatomic, strong) JTimeManagerView* timeManagerView;
@property(nonatomic, strong) LGAlertView* lgAlertView;

@property(nonatomic, copy) SetDeviceInfoBlock setDeviceBlock;
/**
 *  是否可以设置时间
 */
@property(nonatomic, assign) BOOL canSetting;


@property(nonatomic,strong) UITapGestureRecognizer* limitTimeLabelGestureRecognizer;
@end

@implementation JDeviceDetailTableViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.detailKey = @[@"连接类型",@"连接时长",@"总下载量",@"当前速度",@"MAC地址"];
    
    self.detailValue = @[self.deviceInfo.wifiTpye ? self.deviceInfo.wifiTpye :@"",[self timeString:self.deviceInfo.connTime ],self.deviceInfo.totalDl ? [self downString:self.deviceInfo.totalDl] :@"0 B",[self spedString:self.deviceInfo.speed],self.deviceInfo.mac ? self.deviceInfo.mac :@""];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView setTableFooterView:[[UIView alloc] init]];
    
    self.title = self.deviceInfo.remark.length > 0 ?self.deviceInfo.remark : self.deviceInfo.devName;
    
    self.addBlackSwitch.on = [self.deviceInfo.isblack boolValue];
    self.limitSwitch.on = [self.deviceInfo.limit boolValue];
    self.limitTimeLabelGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showLimit)];
    [self.limitTimeLable addGestureRecognizer:self.limitTimeLabelGestureRecognizer];
    self.limitTimeLable.userInteractionEnabled = YES;
    [self nextTask];
    
    if ([self.deviceInfo.limit boolValue] && self.deviceInfo.limitTime.start && self.deviceInfo.limitTime.end) {
        self.limitTimeLable.text = [NSString stringWithFormat:@"%@-%@",self.deviceInfo.limitTime.start,self.deviceInfo.limitTime.end];
    }else{
        self.limitTimeLable.text = @"开启后，该设备只能在该时间段内上网";
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.canNextTask = YES;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.canNextTask = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString*)spedString:(NSNumber*)speedN{
    CGFloat speed = [speedN floatValue];
    if (speed >= 1024*1024) {
        CGFloat mSpeed = speed/ (1024*1024);
        return [NSString stringWithFormat:@"%.2f M/S",mSpeed];
    }else if(speed == 0 ){
        return @"0 B/S";
    }else{
        CGFloat mSpeed = speed / 1024;
        if(mSpeed < 1){
            return [NSString stringWithFormat:@"%.2f B/S",speed];
        }
        
        return [NSString stringWithFormat:@"%.2f KB/S",mSpeed];

    }
}

-(NSString*)downString:(NSNumber*)speedN{
    CGFloat speed = [speedN floatValue];
    if (speed >= 1024*1024) {
        CGFloat mSpeed = speed/ (1024*1024);
        return [NSString stringWithFormat:@"%.2f MB",mSpeed];
    }else if(speed == 0 ){
        return @"0 B";
    }else{
        CGFloat mSpeed = speed / 1024;
        if(mSpeed < 1){
            return [NSString stringWithFormat:@"%.2f B",speed];
        }
        
        return [NSString stringWithFormat:@"%.2f KB",mSpeed];

    }
}



-(NSString*)timeString:(NSNumber*)conntime{
    CGFloat differ =[conntime floatValue];
    if (differ > 0) {
        //获取天
        NSInteger days = differ / 24 / 60 / 60  ;
        
        //获取小时
        NSInteger sub = differ - (CGFloat)days * 24 * 60 * 60 ;
        NSInteger hours = sub / 60 / 60 ;
        
        //分钟
        sub = sub - hours * 60 * 60;
        NSInteger mins = sub / 60 ;
        
        //秒钟
        sub = sub - mins * 60 ;
        NSInteger s = sub;
        NSString *remainingTimeString = @"";
        if (days > 0) {
            remainingTimeString
            = [NSString stringWithFormat:@"%ld天%02ld小时%02ld分%02ld秒",
               (long)days, (long)hours, (long)mins, (long)s] ;
        }else if (hours >0){
            remainingTimeString
            = [NSString stringWithFormat:@"%02ld小时%02ld分%02ld秒", (long)hours, (long)mins, (long)s] ;
        }else if (mins > 0){
            remainingTimeString
            = [NSString stringWithFormat:@"%02ld分%02ld秒",(long)mins, (long)s] ;
            
        }else if (s >0 ){
            remainingTimeString
            = [NSString stringWithFormat:@"%02ld秒", (long)s] ;
            
        }
        
        return remainingTimeString ;
    }
    return @"0秒";
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.detailValue count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 48.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    JDeviceDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"deviceIdentifier" forIndexPath:indexPath];
    cell.iconImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_deviceList%lD",(long)indexPath.row+1]];
    cell.nameLabel.text = self.detailKey[indexPath.row];
    cell.valueLabel.text = self.detailValue[indexPath.row];
    return cell;
}


#pragma mark Btn action
- (IBAction)reNameAction:(id)sender {
     IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = NO;
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"修改备注名称" delegate:nil cancelButtonTitle:@"确认" otherButtonTitles:@"取消", nil];
    alertView.alertViewStyle  = UIAlertViewStylePlainTextInput;
    UITextField* nameTextField = [alertView textFieldAtIndex:0];
    nameTextField.placeholder =  self.deviceInfo.remark.length > 0 ?self.deviceInfo.remark :  self.deviceInfo.devName;
    nameTextField.text = self.deviceInfo.remark;
    nameTextField.clearButtonMode =  UITextFieldViewModeAlways;
    
    [alertView show];
    @weakify(self);
    [[alertView rac_buttonClickedSignal] subscribeNext:^(id x) {
        manager.enable = YES;
        if ([x integerValue] == 0) {
            UITextField* nameTextField = [alertView textFieldAtIndex:0];
            @strongify(self);
            [self markName:nameTextField.text];
        }
    }];
}

- (IBAction)switchBlack:(id)sender {
    UISwitch* switchView = sender;
    UIAlertView* alertView =[[UIAlertView alloc] initWithTitle:@"" message:switchView.on ? @"是否拉黑此设备？" :@"取消拉黑此设备？" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
    [alertView show];
    @weakify(self);
    [[alertView rac_buttonClickedSignal ] subscribeNext:^(id x) {
        if ([x integerValue] == 0) {
            @strongify(self);
            [self setBlcak:switchView.on];
        }else{
            self.addBlackSwitch.on = !switchView.on;
        }
    }];
}

-(void)showLimit{
    if (self.limitSwitch.on) {
        [self showTimePickerView];
    }
}

- (IBAction)switchDeviceTime:(id)sender {
    UISwitch* switchView = sender;
    if (switchView.on) {
        [self showTimePickerView];
    }else{
        [self setLimitTime:@"" endHour:@""];
    }
}

-(void)markName:(NSString*)name{
    if (name.length >0  ) {
        NSString*oldMark =  self.deviceInfo.remark;
        self.deviceInfo.remark = name;
        @weakify(self);
        [self setDeviceInfo:self.deviceInfo blcok:^(BOOL success) {
            @strongify(self);
            if (!success) {
                self.deviceInfo.remark = oldMark;
            }else{
                self.title = self.deviceInfo.remark.length > 0 ?self.deviceInfo.remark : self.deviceInfo.devName;
            }
        }];
    }else{
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"请输入名称" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
        [alertView show];
    }
}


-(void)setBlcak:(BOOL)isBlack{
    self.deviceInfo.isblack = @(isBlack);
     @weakify(self);
    [self setDeviceInfo:self.deviceInfo blcok:^(BOOL success) {
        @strongify(self);
        if (!success) {
            self.deviceInfo.isblack = @(!isBlack);
            self.addBlackSwitch.on = !isBlack;
        }
    }];
}


-(void)showTimePickerView{
    self.timeManagerView = nil;
    self.timeManagerView = [[[NSBundle mainBundle] loadNibNamed:@"JTimeManagerView" owner:self options:nil] firstObject];
    @weakify(self);
    
    LGAlertView* alertView = [[LGAlertView alloc] initWithViewStyleWithTitle:@"" message:@"设置上网时间" view:self.timeManagerView buttonTitles:nil cancelButtonTitle:@"取消" destructiveButtonTitle:@"确定"];
    [alertView showAnimated:YES completionHandler:nil];
    if (self.deviceInfo.limitTime && self.deviceInfo.limitTime.start) {
        self.timeManagerView.startTime.text = self.deviceInfo.limitTime.start;
    }
    if (self.deviceInfo.limitTime && self.deviceInfo.limitTime.end) {
        self.timeManagerView.endTime.text = self.deviceInfo.limitTime.end;
    }
    
    
    self.canSetting = NO;
    alertView.didDismissHandler = ^(LGAlertView *alertView){
        @strongify(self);
        if (self.canSetting) {
            [self setLimitTime:self.timeManagerView.startTime.text endHour:self.timeManagerView.endTime.text];
        }else{
            if(![self.deviceInfo.limit boolValue]){
                self.limitSwitch.on = NO;
            }
        }
    };
    alertView.cancelHandler = ^(LGAlertView *alertView, BOOL onButton){
        @strongify(self);
        self.canSetting = NO;
    };
    alertView.destructiveHandler = ^(LGAlertView *alertView){
        @strongify(self);
        self.canSetting = YES;
    };
    
    
    [[self.timeManagerView.startTapGestureRecognizer rac_gestureSignal] subscribeNext:^(id x) {
        @strongify(self);
        [self showPickTime:YES];
    }];
    [[self.timeManagerView.endTapGestureRecognizer rac_gestureSignal] subscribeNext:^(id x) {
        @strongify(self);
        [self showPickTime:NO];
    }];

}


-(void)showPickTime:(BOOL)first{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSInteger unitFlags =  NSYearCalendarUnit |  NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    calendar.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
    calendar.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"HKT"];
    
    
    NSDateComponents *comp = [calendar components:unitFlags fromDate:[NSDate date]];
    comp.hour = 0;
    comp.minute = 0;
    
    if (first ) {
        if ([self.timeManagerView.startTime.text componentsSeparatedByString:@":"].count > 1) {
            comp.hour = [[self.timeManagerView.startTime.text componentsSeparatedByString:@":"].firstObject longLongValue];
            comp.minute = [[self.timeManagerView.startTime.text componentsSeparatedByString:@":"][1] longLongValue] ;
        }
    }else{
        
        if ([self.timeManagerView.endTime.text componentsSeparatedByString:@":"].count > 1) {
            comp.hour = [[self.timeManagerView.endTime.text componentsSeparatedByString:@":"].firstObject longLongValue];
            comp.minute = [[self.timeManagerView.endTime.text componentsSeparatedByString:@":"][1] longLongValue] ;
        }
    }
    
    
    
    
    @weakify(self);
    ActionSheetDatePicker *datePicker =[[ActionSheetDatePicker alloc] initWithTitle:@"选择时间" datePickerMode:UIDatePickerModeTime selectedDate:[calendar dateFromComponents:comp] doneBlock:^(ActionSheetDatePicker *picker, NSDate* selectedDate, id origin) {
        @strongify(self);
        
        NSDate* serverDate = selectedDate;
        NSDateComponents *comps = [calendar components:unitFlags fromDate:serverDate];
        if (first) {
            self.timeManagerView.startTime.text = [NSString stringWithFormat:@"%@%ld : %@%ld",(comps.hour > 10 ? @"" :@"0"),(long)comps.hour,(comps.minute >= 10 ? @"" :@"0"),(long)comps.minute];
        }else{
            self.timeManagerView.endTime.text = [NSString stringWithFormat:@"%@%ld : %@%ld",(comps.hour > 10 ? @"" :@"0"),(long)comps.hour,(comps.minute >= 10 ? @"" :@"0"),(long)comps.minute];
        }
    } cancelBlock:^(ActionSheetDatePicker *picker) {
        
    } origin:[UIApplication sharedApplication].keyWindow];
//    datePicker.minuteInterval = 5;
    [datePicker showActionSheetPicker];
    
}

/**
 *  设置上网时间
 *
 */
-(void)setLimitTime:(NSString*)startTime  endHour:(NSString*)endTime{
    NSNumber* oldLimit = self.deviceInfo.limit;
    JLimitTime* oldLimitTime = self.deviceInfo.limitTime;
    if (startTime.length > 0 && endTime.length > 0) {
        JLimitTime* limitTime = [[JLimitTime alloc] init];
        limitTime.start = startTime;
        limitTime.end = endTime;
        self.deviceInfo.limit = @(1);
        self.deviceInfo.limitTime = limitTime;
    }else{
        self.deviceInfo.limit = @(0);
        self.deviceInfo.limitTime = nil;
    }
    @weakify(self);
    [self setDeviceInfo:self.deviceInfo blcok:^(BOOL success) {
        @strongify(self);
        if (!success) {
            self.deviceInfo.limit = oldLimit;
            self.deviceInfo.limitTime = oldLimitTime;
            self.limitSwitch.on = !self.limitSwitch.on;
        }
    }];
}



-(void)setDeviceInfo:(JDeviceInfo*)info blcok:(SetDeviceInfoBlock)setDeviceInfoBlcok{
    
    self.canNextTask = NO;
    
    self.allDevices = [self.allDevices rx_mapWithBlock:^id(JDeviceInfo* each) {
        if ([each.mac isEqualToString:info.mac]) {
            each = info;
        }
        return each;
    }];
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    @weakify(self);
    [MBProgressHUD showMessag:@"设置中..." toView:self.view];
    [[JServerClient setDevices:self.allDevices sn:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(id x) {
        
        if ([JGlobals shareGlobal].isAigoRouter && [JGlobals shareGlobal].validateRouter) {
            if(setDeviceInfoBlcok){
                setDeviceInfoBlcok(YES);
            }
            self.canNextTask = YES;
            [self nextTask];
            return ;
        }
        
        NSString* commadIdentifier = x[@"id"];
        commadIdentifier = [NSString stringWithFormat:@"%@",commadIdentifier];
       
        if ([commadIdentifier length] > 0) {
            self.searchCommad = 0;
            if(setDeviceInfoBlcok){
                self.setDeviceBlock = setDeviceInfoBlcok;
            }
            [self searchCommandStatue:[NSString stringWithFormat:@"%@",commadIdentifier]];
        }else{
            [MBProgressHUD showError:@"设置失败，稍后重试！" toView:self.view];
        }
    } error:^(NSError *error) {
        @strongify(self);
        if(setDeviceInfoBlcok){
            setDeviceInfoBlcok(NO);
        }
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
    }];
    
    
}


-(void)commandSuccess{
    if (self.setDeviceBlock) {
        self.setDeviceBlock(YES);
        self.setDeviceBlock = nil;
        self.canNextTask = YES;
        [self nextTask];
    }
}

-(void)commandFail{
    if (self.setDeviceBlock) {
        self.setDeviceBlock(NO);
        self.setDeviceBlock = nil;
        self.canNextTask = YES;
        [self nextTask];
    }
}


-(void)taskAction{
    if (self.canNextTask) {
       [self reflash];
    }
    
}

-(void)reflash{
    
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    @weakify(self);
    [[JServerClient queryDevices:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(NSDictionary* response) {
        @strongify(self);
        if(self.canNextTask){
            NSDictionary* data = response[@"data"];
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            if (data) {
                NSArray*dev = data[@"dev"];
                self.allDevices =[MTLJSONAdapter modelsOfClass:JDeviceInfo.class fromJSONArray:dev error:nil];
                NSArray* devices =  [self.allDevices rx_filterWithBlock:^BOOL(JDeviceInfo* each) {
                    return [each.mac isEqualToString:self.deviceInfo.mac];
                }];
                if (devices.count > 0) {
                    self.deviceInfo =devices.firstObject;
                }
                self.detailValue = @[self.deviceInfo.wifiTpye ? self.deviceInfo.wifiTpye :@"",[self timeString:self.deviceInfo.connTime ],self.deviceInfo.totalDl ? [self downString:self.deviceInfo.totalDl] :@"0 B",[self spedString:self.deviceInfo.speed],self.deviceInfo.mac ? self.deviceInfo.mac :@""];
                if ([self.deviceInfo.limit boolValue] && self.deviceInfo.limitTime.start && self.deviceInfo.limitTime.end) {
                    self.limitTimeLable.text = [NSString stringWithFormat:@"%@-%@",self.deviceInfo.limitTime.start,self.deviceInfo.limitTime.end];
                }else{
                    self.limitTimeLable.text = @"开启后，该设备只能在该时间段内上网";
                }
                
                [self.tableView reloadData];
                [self nextTask];
            }
        }
    } error:^(NSError *error) {
        @strongify(self);
        [self nextTask];
    }];
}


@end
