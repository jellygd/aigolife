//
//  JDevicesViewController.m
//  tcp
//
//  Created by Jelly on 15/7/25.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JDevicesViewController.h"
#import "JDeviceHeaderView.h"
#import "JDeviceTableViewCell.h"
#import "JDeviceInfo.h"
#import "JServerClient+JRouter.h"
#import "JServerClient.h"
#import "JDeviceDetailTableViewController.h"
#import <RXCollection.h>

@interface JDevicesViewController ()

@property(nonatomic, strong) UIButton* rightBtn;


@property(nonatomic, strong) NSArray* allDevices;

@end

@implementation JDevicesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.showBlur = YES;
    // Do any additional setup after loading the view.
    self.title = @"连接设备";
    [self setHeaderBgView:self.headerView];
    self.rightBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    self.rightBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    NSString* title = @"黑名单";
    [self.rightBtn  setTitle:title forState:UIControlStateNormal];
    [self.rightBtn sizeToFit];
    UIBarButtonItem* barItem = [[UIBarButtonItem alloc] initWithCustomView:self.rightBtn];
    
    self.navigationItem.rightBarButtonItem = barItem;
    
    [self.deviceTableView setTableFooterView:[[UIView alloc] init]];
    
    self.deviceTableView.delegate = self;
    self.deviceTableView.dataSource = self;
    
    self.deviceArray = [NSMutableArray array];
    
    
    [self btnAction];
}

-(void)btnAction{
    @weakify(self);
    [[self.rightBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
       
        [self performSegueWithIdentifier:@"BlackList" sender:self];
    }];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     if(!self.showCanBlur){
         [MBProgressHUD showMessag:@"查询中..." toView:self.view];
         [self loadDevice];
     }
}


/**
 *  获取设备数据
 */
-(void)loadDevice{
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    @weakify(self);
    
    [[JServerClient queryDevices:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(NSDictionary* response) {
        @strongify(self);
        NSDictionary* data = response[@"data"];
        if (data) {
            NSArray*dev = data[@"dev"];
            self.allDevices =[MTLJSONAdapter modelsOfClass:JDeviceInfo.class fromJSONArray:dev error:nil];
            self.deviceArray = [self.allDevices rx_filterWithBlock:^BOOL(JDeviceInfo* each) {
                return ![each.isblack boolValue];
            }];
            NSInteger blackCount = self.allDevices.count - self.deviceArray.count;
            if (blackCount > 0) {
               [self.rightBtn setTitle:[NSString stringWithFormat:@"黑名单(%d)",blackCount] forState:UIControlStateNormal];
                [self.rightBtn sizeToFit];
            }else{
                [self.rightBtn setTitle:@"黑名单" forState:UIControlStateNormal];
                [self.rightBtn sizeToFit];
            }
            [self nextTask];
            [self.deviceTableView reloadData];
        }else{
            
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } error:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:NO];
         [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
    }];
}

-(void)taskAction{
    [self loadDevice];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.deviceArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 48.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
   JDeviceInfo* deviceInfo =  [self.deviceArray objectAtIndex:indexPath.row];
    JDeviceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"deviceIdentifier" forIndexPath:indexPath];
    
    if([[deviceInfo.devName lowercaseString] hasPrefix:@"iphone"] || [[deviceInfo.devName lowercaseString] hasSuffix:@"iphone"]){
        cell.deviceImageView.image = [UIImage imageNamed:@"icon_deviceClass1_normal"];
    }else if ([[deviceInfo.devName lowercaseString] hasPrefix:@"pc"] || [[deviceInfo.devName lowercaseString] hasSuffix:@"pc"]){
        cell.deviceImageView.image = [UIImage imageNamed:@"icon_deviceClass3_normal"];
    }else{
        cell.deviceImageView.image = [UIImage imageNamed:@"icon_deviceClass2_normal"];
    }
    cell.deviceNameLabel.text = deviceInfo.remark.length > 0 ? deviceInfo.remark : deviceInfo.devName;
    cell.deviceSpeadLabel.text = [NSString stringWithFormat:@"%@",[self spedString:deviceInfo.speed]];
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
        
    [self performSegueWithIdentifier:@"deviceDetail" sender:indexPath];
}

-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.showCanBlur){
        [self showBlurView];
        return NO;
    }
    
    return YES;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    JDeviceHeaderView *view= [[NSBundle mainBundle] loadNibNamed:@"JDeviceHeaderView" owner:nil options:nil].firstObject;
    if([self.deviceArray count] > 0){
        view.name.text  =[ NSString stringWithFormat:@"接入设备(%lu台)",(unsigned long)[self.deviceArray count]];
    }
    return view;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50;
}


-(NSString*)spedString:(NSNumber*)speedN{
    CGFloat speed = [speedN floatValue];
    if (speed >= 1024*1024) {
        CGFloat mSpeed = speed/ (1024*1024);
        return [NSString stringWithFormat:@"%.2f M/S",mSpeed];
    }else if (speed == 0){
     return [NSString stringWithFormat:@"%.2f B/S",speed];
    } else{
        CGFloat mSpeed = speed / 1024;
        if(mSpeed < 1){
            return [NSString stringWithFormat:@"%.2f B/S",speed];
        }
        
        return [NSString stringWithFormat:@"%.2f KB/S",mSpeed];

    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  
    if ([segue.identifier isEqualToString:@"deviceDetail"]) {
        NSIndexPath* indexPath = sender;
        JDeviceDetailTableViewController* detailViewController = segue.destinationViewController;
        JDeviceInfo* info = self.deviceArray[indexPath.row];
        detailViewController.allDevices = self.allDevices;
        detailViewController.deviceInfo = info;
    }
}

@end
