//
//  JBlacklistViewController.h
//  tcp
//
//  Created by Jelly on 15/7/25.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JBaseViewController.h"
#import "JDeviceInfo.h"

@interface JBlacklistViewController : JBaseViewController<UITableViewDelegate,UITableViewDataSource>


@property(nonatomic, strong) NSMutableArray* blackDiviceArray;
@property (weak, nonatomic) IBOutlet UITableView *blackTableView;

@end
