//
//  JBlacklistViewController.m
//  tcp
//
//  Created by Jelly on 15/7/25.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JBlacklistViewController.h"
#import <MGSwipeTableCell.h>
#import "JDeviceInfo.h"
#import <MGSwipeButton.h>
#import "JServerClient+JRouter.h"
#import "JServerClient.h"
#import "JBlackTableViewCell.h"
#import <RXCollection.h>
#import "JServerClient+JCommand.h"
#import "JDeviceDetailTableViewController.h"

@interface JBlacklistViewController ()

/**
 *  设备列表
 */

@property(nonatomic, strong) NSArray* allDevices;
@property(nonatomic, strong) JDeviceInfo* editDeviceInfo;


@end

@implementation JBlacklistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    FIX_EDGES_FOR_EXTENDEDLAYTOU;
    [self.blackTableView setTableFooterView:[[UIView alloc] init]];
    self.blackTableView.delegate = self;
    self.blackTableView.dataSource = self;
    self.blackDiviceArray = [NSMutableArray array];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self loadBlackDevice];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark  加载数据
/**
 *  获取设备数据
 */
-(void)loadBlackDevice{
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    @weakify(self);
    [MBProgressHUD showMessag:@"查询中..." toView:self.view];
    [[JServerClient queryBlackDevices:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(NSDictionary* response) {
        @strongify(self);
        NSDictionary* data = response[@"data"];
        if (data) {
            NSArray*dev = data[@"dev"];
            
            self.allDevices =[MTLJSONAdapter modelsOfClass:JDeviceInfo.class fromJSONArray:dev error:nil];
            self.blackDiviceArray =[self.allDevices rx_filterWithBlock:^BOOL(JDeviceInfo* each) {
                 return [each.isblack boolValue];
             }];
            
            [self.blackTableView reloadData];
        }else{
            
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } error:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
    }];
}



#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.blackDiviceArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 48.0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    JBlackTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"blackDevicesIdentifier" forIndexPath:indexPath];
 
    
    JDeviceInfo* device = self.blackDiviceArray[indexPath.row];
    cell.blackNameLabel.text =  device.remark.length > 0 ? device.remark : device.devName;
    
    if([[device.devName lowercaseString] hasPrefix:@"iphone"] || [[device.devName lowercaseString] hasSuffix:@"iphone"]){
        cell.blackImageView.image = [UIImage imageNamed:@"icon_deviceClass1_normal"];
    }else if ([[device.devName lowercaseString] hasPrefix:@"pc"] || [[device.devName lowercaseString] hasSuffix:@"pc"]){
        cell.blackImageView.image = [UIImage imageNamed:@"icon_deviceClass3_normal"];
    }else{
        cell.blackImageView.image = [UIImage imageNamed:@"icon_deviceClass2_normal"];
    }
    
    @weakify(self);
    MGSwipeButton* rightBtn = [MGSwipeButton buttonWithTitle:@"恢复" icon:nil backgroundColor:[UIColor colorWithRed:0.141 green:0.522 blue:0.906 alpha:1.000]  callback:^BOOL(MGSwipeTableCell *sender) {
        @strongify(self);
        [self removeBlack:device];
        return YES;
    }];
    cell.rightButtons = @[rightBtn];
    
    return cell;
}


-(void)removeBlack:(JDeviceInfo*)info{
    
    self.editDeviceInfo = info;
    
    self.allDevices = [self.allDevices rx_mapWithBlock:^id(JDeviceInfo* each) {
        if ([each.mac isEqualToString:info.mac]) {
            each.isblack = @(0);
        }
        return each;
    }];
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    @weakify(self);
    [MBProgressHUD showMessag:@"设置中..." toView:self.view];
    [[JServerClient setDevices:self.allDevices sn:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(id x) {
        if ([JGlobals shareGlobal].isAigoRouter && [JGlobals shareGlobal].validateRouter) {
            @strongify(self);
            self.blackDiviceArray =[self.allDevices rx_filterWithBlock:^BOOL(JDeviceInfo* each) {
                return [each.isblack boolValue];
            }];
            [self.blackTableView reloadData];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            return ;
        }
        
        NSString* commadIdentifier = x[@"id"];
        commadIdentifier = [NSString stringWithFormat:@"%@",commadIdentifier];
        if ([commadIdentifier length] > 0) {
            self.searchCommad = 0;
            [self searchCommandStatue:[NSString stringWithFormat:@"%@",commadIdentifier]];
        }else{
            [MBProgressHUD showError:@"设置失败，稍后重试！" toView:self.view];
            
        }
        
    } error:^(NSError *error) {
        @strongify(self);
        self.allDevices = [self.allDevices rx_mapWithBlock:^id(JDeviceInfo* each) {
            if ([each.mac isEqualToString:info.mac]) {
                each.isblack = @(1);
            }
            return each;
        }];
        self.blackDiviceArray =[self.allDevices rx_filterWithBlock:^BOOL(JDeviceInfo* each) {
            return [each.isblack boolValue];
        }];
        [self.blackTableView reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
    }];
    
    
}
-(void)commandSuccess{
    self.blackDiviceArray =[self.allDevices rx_filterWithBlock:^BOOL(JDeviceInfo* each) {
        return [each.isblack boolValue];
    }];
    [self.blackTableView reloadData];
}

-(void)commandFail{
    if(self.editDeviceInfo){
        self.allDevices = [self.allDevices rx_mapWithBlock:^id(JDeviceInfo* each) {
            if ([each.mac isEqualToString:self.editDeviceInfo.mac]) {
                each.isblack = @(1);
            }
            return each;
        }];
    }
    [self.blackTableView reloadData];
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"blcakDeviceDetail"]) {
        UITableViewCell* cell = sender;
        NSIndexPath* indexPath  = [self.blackTableView indexPathForCell:cell];
        JDeviceDetailTableViewController* detailViewController = segue.destinationViewController;
        JDeviceInfo* info = self.blackDiviceArray[indexPath.row];
        detailViewController.allDevices = self.allDevices;
        detailViewController.deviceInfo = info;
    }
}


@end
