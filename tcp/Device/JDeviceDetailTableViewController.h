//
//  JDeviceDetailTableViewController.h
//  tcp
//
//  Created by Jelly on 15/7/25.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JDeviceInfo.h"
#import "JBaseTableViewController.h"

@interface JDeviceDetailTableViewController : JBaseTableViewController
@property (weak, nonatomic) IBOutlet UILabel *limitTimeLable;

@property (weak, nonatomic) IBOutlet UISwitch *addBlackSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *limitSwitch;

/**
 *  设备信息
 */
@property(nonatomic, strong) JDeviceInfo* deviceInfo;

/**
 *  设备列表
 */
@property(nonatomic, strong) NSArray* allDevices;


@end
