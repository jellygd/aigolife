//
//  JBlackTableViewCell.h
//  tcp
//
//  Created by dong on 15/7/27.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MGSwipeTableCell.h>

@interface JBlackTableViewCell : MGSwipeTableCell

@property (weak, nonatomic) IBOutlet UIImageView *blackImageView;

@property (weak, nonatomic) IBOutlet UILabel *blackNameLabel;
@end
