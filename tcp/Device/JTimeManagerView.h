//
//  JTimeManagerView.h
//  tcp
//
//  Created by Jelly on 15/8/10.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JTimeManagerView : UIView

@property (weak, nonatomic) IBOutlet UILabel *startTime;
@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *startTapGestureRecognizer;



@property (weak, nonatomic) IBOutlet UITapGestureRecognizer *endTapGestureRecognizer;

@property (weak, nonatomic) IBOutlet UILabel *endTime;

@end
