//
//  JDevicesViewController.h
//  tcp
//
//  Created by Jelly on 15/7/25.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JBaseViewController.h"

@interface JDevicesViewController : JBaseViewController<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic, strong) NSMutableArray* deviceArray;

@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UITableView *deviceTableView;

@end
