//
//  JAddBlackViewController.h
//  tcp
//
//  Created by Jelly on 15/7/26.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JBaseViewController.h"

@interface JAddBlackViewController : JBaseViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *blackTableView;

@end
