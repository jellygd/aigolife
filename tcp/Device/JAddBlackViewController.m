//
//  JAddBlackViewController.m
//  tcp
//
//  Created by Jelly on 15/7/26.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JAddBlackViewController.h"
#import "JDeviceTableViewCell.h"
#import "JDeviceInfo.h"
#import "JServerClient+JRouter.h"
#import <RXCollection.h>
#import "JServerClient+JCommand.h"
#import "JDeviceDetailTableViewController.h"


@interface JAddBlackViewController ()

@property(nonatomic, strong) NSArray* deviceArray;

@property(nonatomic, strong) NSArray* allDevices;

@property(nonatomic, strong) JDeviceInfo* selectDeviceInfo;

@end

@implementation JAddBlackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    FIX_EDGES_FOR_EXTENDEDLAYTOU;
    
    self.blackTableView.delegate = self;
    self.blackTableView.dataSource = self;
    
    [self.blackTableView setTableFooterView:[[UIView alloc] init]];
    [self loadDeviceData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)loadDeviceData{
    
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    @weakify(self);
    [MBProgressHUD showMessag:@"查询中..." toView:self.view];
    [[JServerClient queryDevices:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(NSDictionary* response) {
        @strongify(self);
        NSDictionary* data = response[@"data"];
        if (data) {
            NSArray*dev = data[@"dev"];
            self.allDevices =[MTLJSONAdapter modelsOfClass:JDeviceInfo.class fromJSONArray:dev error:nil];
            self.deviceArray =self.allDevices ;
            //            [self.allDevices rx_filterWithBlock:^BOOL(JDeviceInfo* each) {
            //                return ![each.isblack boolValue];
            //            }];
            [self.blackTableView reloadData];
        }else{
            
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } error:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
    }];
}

#pragma mark - Table view data source



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.deviceArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    JDeviceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"deviceIdentifier" forIndexPath:indexPath];
    JDeviceInfo* device = [self.deviceArray objectAtIndex:indexPath.row];
    cell.deviceNameLabel.text = device.remark.length > 0 ? device.remark : device.devName;
    cell.deviceSpeadLabel.text = @"";
    
    if([[device.devName lowercaseString] hasPrefix:@"iphone"] || [[device.devName lowercaseString] hasSuffix:@"iphone"]){
        cell.deviceImageView.image = [UIImage imageNamed:@"icon_deviceClass1_normal"];
    }else if ([[device.devName lowercaseString] hasPrefix:@"pc"] || [[device.devName lowercaseString] hasSuffix:@"pc"]){
        cell.deviceImageView.image = [UIImage imageNamed:@"icon_deviceClass3_normal"];
    }else{
        cell.deviceImageView.image = [UIImage imageNamed:@"icon_deviceClass2_normal"];
    }
    
    
    
    cell.deviceBlackImageView.hidden = NO;
    if ([device.isblack boolValue]) {
        cell.deviceBlackImageView.image = [UIImage imageNamed:@"icon_regist_selectSex_highlight"];
    }else{
        cell.deviceBlackImageView.image = [UIImage imageNamed:@"icon_regist_selectSex_normal"];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    JDeviceInfo* device = [self.deviceArray objectAtIndex:indexPath.row];
    UIAlertView* alertView =[[UIAlertView alloc] initWithTitle:@"" message:[device.isblack boolValue]? @"是否取消拉黑当前选择设备？": @"是否拉黑当前选中设备？" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
    [alertView show];
    @weakify(self);
    [[alertView rac_buttonClickedSignal] subscribeNext:^(id x) {
        if ([x integerValue] == 0) {
            @strongify(self);
            [self addBlackDeViceInfo:device];
        }
    }];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 48.0;
}

-(void)addBlackDeViceInfo:(JDeviceInfo*)info{
    self.selectDeviceInfo = info;
    self.allDevices = [self.allDevices rx_mapWithBlock:^id(JDeviceInfo* each) {
        if ([each.mac isEqualToString:info.mac]) {
            each.isblack = @(![each.isblack boolValue]);
        }
        return each;
    }];
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    @weakify(self);
    [MBProgressHUD showMessag:@"设置中..." toView:self.view];
    [[JServerClient setDevices:self.allDevices sn:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(id x) {
        @strongify(self);
        if ([JGlobals shareGlobal].isAigoRouter && [JGlobals shareGlobal].validateRouter) {
            self.deviceArray =self.allDevices;
            //            [self.allDevices rx_filterWithBlock:^BOOL(JDeviceInfo* each) {
            //                return ![each.isblack boolValue];
            //            }];
            [self.blackTableView reloadData];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            return ;
        }
        
        NSString* commadIdentifier = x[@"id"];
        commadIdentifier = [NSString stringWithFormat:@"%@",commadIdentifier];
        if ([commadIdentifier length] > 0) {
            self.searchCommad = 0;
            [self searchCommandStatue:[NSString stringWithFormat:@"%@",commadIdentifier]];
        }else{
            [MBProgressHUD showError:@"设置失败，稍后重试！" toView:self.view];
            
        }
        
        
    } error:^(NSError *error) {
        @strongify(self);
        self.allDevices = [self.allDevices rx_mapWithBlock:^id(JDeviceInfo* each) {
            if ([each.mac isEqualToString:info.mac]) {
                each.isblack = @(0);
            }
            return each;
        }];
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
    }];
    
}

-(void)commandSuccess{
    [self.blackTableView reloadData];
}

-(void)commandFail{
    self.allDevices = [self.allDevices rx_mapWithBlock:^id(JDeviceInfo* each) {
        if ([each.mac isEqualToString:self.selectDeviceInfo.mac]) {
            each.isblack = @(0);
        }
        return each;
    }];
    
    [self.blackTableView reloadData];
}

//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//
//    if ([segue.identifier isEqualToString:@"addDeviceDetail"]) {
//        NSIndexPath* indexPath = sender;
//        JDeviceDetailTableViewController* detailViewController = segue.destinationViewController;
//        JDeviceInfo* info = self.deviceArray[indexPath.row];
//        detailViewController.allDevices = self.allDevices;
//        detailViewController.deviceInfo = info;
//    }
//}


@end
