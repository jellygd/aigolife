//
//  JRouterResetViewController.h
//  tcp
//
//  Created by Jelly on 15/8/1.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JBaseViewController.h"

@interface JRouterResetViewController : JBaseViewController

- (IBAction)resetAction:(id)sender;
@end
