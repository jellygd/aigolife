//
//  JRouterAdminPWDViewController.h
//  tcp
//
//  Created by Jelly on 15/8/1.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JBaseViewController.h"

@interface JRouterAdminPWDViewController : JBaseViewController

@property (weak, nonatomic) IBOutlet UITextField *oldPWDTextField;
@property (weak, nonatomic) IBOutlet UITextField *nPWDTextField;


@end
