//
//  JRouterNetWorkingViewController.h
//  tcp
//
//  Created by Jelly on 15/8/2.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JBaseViewController.h"

@interface JRouterNetWorkingViewController : JBaseViewController

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

@property (weak, nonatomic) IBOutlet UITextField *pwdTextField;

@property (weak, nonatomic) IBOutlet UIButton *typeChangeBtn;

- (IBAction)dhcpBtn:(id)sender;


@end
