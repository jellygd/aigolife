//
//  JRouterlimitViewController.m
//  tcp
//
//  Created by Jelly on 15/8/1.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JRouterlimitViewController.h"
#import "JLimitTableViewCell.h"
#import "JSpeedLimit.h"
#import "JServerClient+JCommand.h"
#import "JServerClient+JRouter.h"
#import "MMPickerView.h"

@interface JRouterlimitViewController ()<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong) NSArray* limitArray;
@property(nonatomic, strong) NSArray* smartModel;
@property(nonatomic, strong) NSArray* smartType;

@property(nonatomic, assign) BOOL smartLimit;
@property(nonatomic, assign) NSInteger smartModeIndex;

@property(nonatomic, strong) UIButton* rightBtn;

@end

@implementation JRouterlimitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"设备智能限速";
    self.limitSwitch.on =  NO;
    self.limitType.hidden = YES;
    self.limitTableView.hidden = YES;
    [self loadRouterData];
    
    self.smartModel = @[@"自动模式(系统判断)",@"平均模式(所有设备网速平均分配)"];
    self.smartType = @[@"1",@"2",@"3",@"4"];
    
    NSInteger model =  [[JGlobals shareGlobal].routerInfo.smartModel integerValue] - 1;
    [self setModel:model];
    @weakify(self);
    [[self.limitType rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        [self showPickModelView];
    }];
    
    self.rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.rightBtn setTitle:@"保存" forState:UIControlStateNormal];
    [self.rightBtn sizeToFit];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.rightBtn];
    [[self.rightBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        [self saveSmartLimit];
    }];
    
    [[self.limitSwitch rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        self.limitType.hidden = !self.limitSwitch.on;
        self.limitTableView.hidden = !self.limitSwitch.on;
    }];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self loadSmartLimit];
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [MMPickerView dismissWithCompletion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark save Data 
-(void)saveSmartLimit{
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    @weakify(self);
    [MBProgressHUD showMessag:@"设置中..." toView:self.view];
    [[JServerClient setSpeedLimit:self.limitSwitch.on mode:self.smartModeIndex speedLimitDevs:self.limitArray sn:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(id x) {
        
        @strongify(self);
        if ([JGlobals shareGlobal].isAigoRouter && [JGlobals shareGlobal].validateRouter) {
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            [MBProgressHUD showSuccess:@"操作成功" toView:self.view];
            return ;
        }
        
        NSString* commadIdentifier = x[@"id"];
        commadIdentifier = [NSString stringWithFormat:@"%@",commadIdentifier];
        if ([commadIdentifier length] > 0) {
            self.searchCommad = 0;
            [self searchCommandStatue:[NSString stringWithFormat:@"%@",commadIdentifier]];
        }else{
            [MBProgressHUD showError:@"设置失败，稍后重试！" toView:self.view];
        }
        
    } error:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
    }];
}


#pragma mark  loadData

-(void)loadSmartLimit{
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    @weakify(self);
    [MBProgressHUD showMessag:@"查询中..." toView:self.view];
    [[JServerClient querySmartlimit:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(NSDictionary* response) {
        @strongify(self);
        NSDictionary* data = response[@"data"];
        if (data) {
            self.smartLimit =  [data[@"smart_limit"] boolValue];
            self.limitSwitch.on =  self.smartLimit;
            if (self.limitSwitch.on) {
                self.limitTableView.hidden = NO;
                self.limitType.hidden = NO;
            }else{
                self.limitTableView.hidden = YES;
                self.limitType.hidden = YES;
            }
            self.smartModeIndex = [data[@"smart_mode"] integerValue];
            [self setModel:self.smartModeIndex];
            NSArray* speed_limit = data[@"speed_limit"];
            if ([speed_limit count] > 0) {
                self.limitArray = [MTLJSONAdapter modelsOfClass:JSpeedLimit.class fromJSONArray:speed_limit error:nil];
                [self.limitTableView reloadData];
            }
        }else{
            
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } error:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
    }];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.limitArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    JLimitTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"limitIdentifier" forIndexPath:indexPath];
    JSpeedLimit* speedLimit  = [self.limitArray objectAtIndex:indexPath.row];
    cell.limitNameLabel.text = speedLimit.remark.length > 0 ? speedLimit.remark : speedLimit.devName;
    
    if (self.smartModeIndex == 0 ) {
        cell.limitGreadLabel.text = @"";
        cell.limitNameLabel.textColor = UIColorFromRGB(0xC2C2C2);
        cell.limitGreadLabel.textColor = UIColorFromRGB(0xC2C2C2);
    }else{
        if ([speedLimit.level integerValue]-1 >= 0) {
            cell.limitGreadLabel.text = [self.smartType objectAtIndex:[speedLimit.level integerValue]-1];
        }else{
            cell.limitGreadLabel.text = [self.smartType objectAtIndex:0];
        }
        cell.limitNameLabel.textColor = [UIColor blackColor];
        cell.limitGreadLabel.textColor = [UIColor blackColor];
    }
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 48.0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(self.limitSwitch.on && self.smartModeIndex == 1){
        JSpeedLimit* speedLimit  = [self.limitArray objectAtIndex:indexPath.row];
        [self setSpeedLimit:speedLimit index:indexPath.row];
    }
}

-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    return self.limitSwitch.on;
}

-(void)setModel:(NSInteger)model{
    if (model < 0) {
        model = 0;
    }else if (model > 1){
        model = 1;
    }
    self.smartModeIndex = model;
    NSString* title = [self.smartModel objectAtIndex:model];
    [self.limitType setTitle:[NSString stringWithFormat:@"%@ >",title] forState:UIControlStateNormal];
}


-(void)setSpeedLimit:(JSpeedLimit*)speedLimit index:(NSUInteger)indexRow{
    @weakify(self);
    [MMPickerView showPickerViewInView:self.view withObjects:self.smartType withOptions:@{@"selectedObject":[NSString stringWithFormat:@"%d",[speedLimit.level integerValue]]} objectToStringConverter:nil completion:^(id selectedObject) {
        @strongify(self);
        NSUInteger index =[self.smartType indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            NSString *s = (NSString *)obj;
            if([selectedObject isEqualToString:s])
            {
                return YES;
            }
            return NO;
        }];
        speedLimit.level = @(index+1);
        JSpeedLimit* speed  = [self.limitArray objectAtIndex:indexRow];
        speed.level =  @(index+1);
        [self.limitTableView reloadData];
    }];
}


-(void)showPickModelView{
    @weakify(self);
    
    [MMPickerView showPickerViewInView:self.view withObjects:self.smartModel withOptions:@{@"selectedObject":self.smartModel[self.smartModeIndex]} objectToStringConverter:nil completion:^(id selectedObject) {
        NSUInteger index =[self.smartModel indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            NSString *s = (NSString *)obj;
            if([selectedObject isEqualToString:s])
            {
                return YES;
            }
            return NO;
        }];
        
        @strongify(self);
        if (index == 1) {
            for (JSpeedLimit* speedLimit in self.limitArray) {
                speedLimit.level = @(2);
            }
        }
        [self setModel:index];
        [self.limitTableView reloadData];
        
    }];
}



@end
