//
//  JRubIdenTableViewCell.m
//  tcp
//
//  Created by Jelly on 15/8/1.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JRubIdenTableViewCell.h"

@implementation JRubIdenTableViewCell

- (void)awakeFromNib {
   
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)switchChangeAct:(id)sender {
    if (self.switchChangeBlock) {
        self.switchChangeBlock(sender);
    }
}
@end
