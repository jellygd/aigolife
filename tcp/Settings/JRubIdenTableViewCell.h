//
//  JRubIdenTableViewCell.h
//  tcp
//
//  Created by Jelly on 15/8/1.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^SwitchChangeBlock)(UISwitch *rubSwitch);
@interface JRubIdenTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *rubTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *rubSubTitleLabel;
@property (weak, nonatomic) IBOutlet UISwitch *rubSwitch;

- (IBAction)switchChangeAct:(id)sender;

@property (copy, nonatomic ) SwitchChangeBlock switchChangeBlock;
@end
