//
//  JRouterSettingsViewController.m
//  tcp
//
//  Created by Jelly on 15/7/28.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JRouterSettingsViewController.h"
#import "JRouterInfo.h"
#import "JServerClient+JCommand.h"
#import "JRouterInfo.h"
#import "AppDelegate.h"

@interface JRouterSettingsViewController ()

@property(nonatomic, strong) NSString*commandIdentifier;
@property(nonatomic, strong) NSString* successTitle;
@end

@implementation JRouterSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"路由器设置";
    
    self.showBlur = YES;
    self.bluthSwitch.on = [[JGlobals shareGlobal].routerInfo.bleSwitch boolValue];
    [[self.bluthSwitch rac_newOnChannel] subscribeNext:^(id x) {
        [self closeBluth:[x boolValue]];
    }];
    
    if ([JGlobals shareGlobal].user) {
        self.unbindeCell.hidden = NO;
    }else{
        self.unbindeCell.hidden = YES;
    }
    
    [self.tableView  setTableFooterView:[[UIView alloc] init]];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([JGlobals shareGlobal].routerInfo) {
        self.bluthSwitch.on = [[JGlobals shareGlobal].routerInfo.bleSwitch boolValue];
        self.bluthSwitch.enabled = YES;
    }else{
        self.bluthSwitch.enabled = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if([JGlobals shareGlobal].routerInfo){
        return 8;
    }
    return 7;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    if (indexPath.row == 7){
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"确定解除该路由器的绑定吗？" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
        [alert show];
        @weakify(self);
        [[alert rac_buttonClickedSignal] subscribeNext:^(id x) {
            @strongify(self);
            if ([x integerValue] == 0) {
                [self unbundlingRouter];
            }
        }];
    }
}

-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self showCanBlur]) {
        [self showBlurView];
        return false;
    }
    
    if (![JGlobals shareGlobal].deveicRouter && [JGlobals shareGlobal].user) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"您还没有绑定路由" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
        [alertView show];
        return NO;
    }
    if ([JGlobals shareGlobal].wifiStatue == WifiStatueUndefine || [JGlobals shareGlobal].wifiStatue == WifiStatueUnKnow) {
        [self showErrorMessage:@"云平台检测不到设备"];
        return NO;
    }
    if ([JGlobals shareGlobal].wifiStatue == WifiStatueCheckRouter) {
        [self showErrorMessage:@"请等待设备连接完成"];
        return NO;
    }
    
    return YES;
}

-(void)closeBluth:(BOOL)on{
    if (![JGlobals shareGlobal].deveicRouter && [JGlobals shareGlobal].user) {
        self.bluthSwitch.on = !on;
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"您还没有绑定路由" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    if ([JGlobals shareGlobal].wifiStatue == WifiStatueUndefine || [JGlobals shareGlobal].wifiStatue == WifiStatueUnKnow) {
        [self showErrorMessage:@"云平台检测不到设备"];
        return;
    }
    if ([JGlobals shareGlobal].wifiStatue == WifiStatueCheckRouter) {
        [self showErrorMessage:@"请等待设备连接完成"];
        return;
    }
    
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    @weakify(self);
    [MBProgressHUD showMessag:on?@"正在开启蓝牙...":@"正在关闭蓝牙..." toView:self.view];
    [[JServerClient setBluetooth:on sn:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(NSDictionary* response) {
        @strongify(self);
       
        if ([JGlobals shareGlobal].isAigoRouter && [JGlobals shareGlobal].validateRouter) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showSuccess:@"操作成功" toView:self.view];
            [JGlobals shareGlobal].routerInfo.bleSwitch = @(on);
            return ;
        }
        NSString* key = [response objectForKey:@"id"];
        if (key) {
            self.commandIdentifier = key;
            [self searchCommandStatue:self.commandIdentifier];
        }else{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showError:@"操作失败！请稍后重试！" toView:self.view];
            self.bluthSwitch.on = !on;
        }
    } error:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
        self.bluthSwitch.on = !on;
    }];
}

/**
 *  解除绑定
 */
-(void)unbundlingRouter{
    if (![JGlobals shareGlobal].deveicRouter) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"您还没有绑定路由" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    if ([JGlobals shareGlobal].wifiStatue == WifiStatueUndefine || [JGlobals shareGlobal].wifiStatue == WifiStatueUnKnow) {
        [self showErrorMessage:@"云平台检测不到设备"];
        return;
    }
    if ([JGlobals shareGlobal].wifiStatue == WifiStatueCheckRouter) {
        [self showErrorMessage:@"请等待设备连接完成"];
        return;
    }
    
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    @weakify(self);
    JDeviceModule*device = [[JDeviceModule alloc] init];
    device.type = @"router";
    device.identifier = [JGlobals shareGlobal].deveicRouter.identifier;
    [MBProgressHUD showMessag:@"正在解除绑定..." toView:self.view];
    [[JServerClient UnBindUserDevice:device token:[JGlobals shareGlobal].token] subscribeNext:^(id x) {
        [[JServerClient unbindRouter:[JGlobals shareGlobal].userName sn:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(id x) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showSuccess:@"解绑成功" toView:self.view];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                AppDelegate* appdelegate = [UIApplication sharedApplication].delegate;
                [appdelegate showLoginView];
            });
        } error:^(NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showError:@"解绑失败！请稍后重试！" toView:self.view];
        }];
    } error:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        NSString* message = [error.userInfo objectForKey:@"errmessage"];
        if ( [message isEqualToString:@"user_has_not_bind_this_device"]) {
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"您还没有绑定路由" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
            [alertView show];
        }else{
            [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
        }
        
        
    }];
    return;
    
    [[JServerClient unbindRouter:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(NSDictionary* response) {
        @strongify(self);
        NSString* key = [response objectForKey:@"id"];
        if (key) {
            self.commandIdentifier = key;
            [self searchCommandStatue:self.commandIdentifier];
        }else{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showError:@"解绑失败！请稍后重试！" toView:self.view];
        }
    } error:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
    }];
}


-(void)commandFail{
    [JGlobals shareGlobal].routerInfo.bleSwitch = @(!self.bluthSwitch.on);
    self.bluthSwitch.on = !self.bluthSwitch.on;
//    [MBProgressHUD hideHUDForView:self.view animated:YES];
//    [MBProgressHUD showError:@"解绑失败！请稍后重试！" toView:self.view];
}


-(void)commandSuccess{
     [JGlobals shareGlobal].routerInfo.bleSwitch = @(self.bluthSwitch.on);
//    [MBProgressHUD hideHUDForView:self.view animated:YES];
//    [MBProgressHUD showSuccess:@"解绑c" toView:self.view];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
