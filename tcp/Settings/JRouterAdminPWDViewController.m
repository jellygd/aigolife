//
//  JRouterAdminPWDViewController.m
//  tcp
//
//  Created by Jelly on 15/8/1.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JRouterAdminPWDViewController.h"
#import "JServerClient+JCommand.h"
#import "AppDelegate.h"

@interface JRouterAdminPWDViewController ()<UITextFieldDelegate>


@property(nonatomic, strong) UIButton* rightBtn;
@end

@implementation JRouterAdminPWDViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.rightBtn setTitle:@"确定" forState:UIControlStateNormal];
    [self.rightBtn sizeToFit];
    [self.rightBtn addTarget:self action:@selector(resetPWD) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.rightBtn];
    [self.rightBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    
    self.oldPWDTextField.delegate = self;
    self.nPWDTextField.delegate = self;
    
    [self.oldPWDTextField.rac_textSignal subscribeNext:^(id x) {
        [self showRightStatue];
    }];
    [self.nPWDTextField.rac_textSignal subscribeNext:^(id x) {
        [self showRightStatue];
    }];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [IQKeyboardManager sharedManager].enable = YES;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [IQKeyboardManager sharedManager].enable = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)backAction{
    if(self.oldPWDTextField.text.length > 0 || self.nPWDTextField.text.length > 0){
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"您有新的修改没保存，是否离开？ " delegate:nil cancelButtonTitle:@"否" otherButtonTitles:@"是", nil];
        [alertView show];
        @weakify(self);
        [[alertView rac_buttonClickedSignal] subscribeNext:^(id x) {
            if ([x integerValue] == 1) {
                @strongify(self);
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
        return;
    }
        
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)showRightStatue{
    if(self.oldPWDTextField.text.length > 0 || self.nPWDTextField.text.length > 0){
        self.rightBtn.enabled = YES;
        [self.rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }else{
        self.rightBtn.enabled = NO;
        [self.rightBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


-(void)resetPWD{
    [self.oldPWDTextField resignFirstResponder];
    [self.nPWDTextField resignFirstResponder];
    
    if (self.oldPWDTextField.text.length >= 8 && self.nPWDTextField.text.length >= 8) {
        NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
        
        [MBProgressHUD showMessag:@"设置中..." toView:self.navigationController.view];
        @weakify(self);
        [[JServerClient resetAdminPasss:self.oldPWDTextField.text newPass:self.nPWDTextField.text sn:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(id x) {
            @strongify(self);
            if ([JGlobals shareGlobal].isAigoRouter && [JGlobals shareGlobal].validateRouter) {
                [MBProgressHUD hideHUDForView:self.navigationController.view animated:NO];
                [MBProgressHUD showSuccess:@"操作成功" toView:self.view];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    AppDelegate* appdelegate = [UIApplication sharedApplication].delegate;
                    [appdelegate showLoginView];
                });
                return ;
            }
            
            NSString* commadIdentifier = x[@"id"];
            commadIdentifier = [NSString stringWithFormat:@"%@",commadIdentifier];
            if ([commadIdentifier length] > 0) {
                self.searchCommad = 0;
                [self searchCommandStatue:[NSString stringWithFormat:@"%@",commadIdentifier]];
            }else{
                [MBProgressHUD showError:@"设置失败，稍后重试！" toView:self.view];
            }
        } error:^(NSError *error) {
            @strongify(self);
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:NO];
            [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
        }];
        
    }else{
        [[[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入8-16位密码" delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] show];
    }
}

-(void)commandSuccess{
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:NO];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.rightBtn.enabled = YES;
        AppDelegate* appdelegate = [UIApplication sharedApplication].delegate;
        [appdelegate showLoginView];
    });
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
    if ([string isEqualToString:@"\n"])
    {
        return YES;
    }
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (self.nPWDTextField == textField || self.oldPWDTextField == textField)
    {
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression
                                      regularExpressionWithPattern:@"([^A-Za-z0-9_.])"
                                      options:0
                                      error:&error];
        
        NSString *resString = [regex stringByReplacingMatchesInString:string options:0 range:NSMakeRange(0, string.length) withTemplate:@""];
        
        if (resString.length != string.length) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"不能输入特殊字符" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] ;
            [alert show];
            return NO;
        }
        
        if ([toBeString length] > 16) {
            textField.text = [toBeString substringToIndex:16];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"提示请输入8到16位密码" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] ;
            [alert show];
            return NO;
        }
    }
    return YES;
}

@end
