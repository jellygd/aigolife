//
//  JRouterlimitViewController.h
//  tcp
//
//  Created by Jelly on 15/8/1.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JBaseViewController.h"

@interface JRouterlimitViewController : JBaseViewController
@property (weak, nonatomic) IBOutlet UISwitch *limitSwitch;

@property (weak, nonatomic) IBOutlet UIButton *limitType;

@property (weak, nonatomic) IBOutlet UITableView *limitTableView;

@end
