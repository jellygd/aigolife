//
//  JRouterSettingsViewController.h
//  tcp
//
//  Created by Jelly on 15/7/28.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JBaseTableViewController.h"

@interface JRouterSettingsViewController : JBaseTableViewController
/**
 *  蓝牙开关
 */
@property (weak, nonatomic) IBOutlet UISwitch *bluthSwitch;
@property (weak, nonatomic) IBOutlet UITableViewCell *unbindeCell;

@end
