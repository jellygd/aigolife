//
//  JAntiRubViewController.h
//  tcp
//
//  Created by Jelly on 15/8/1.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JBaseViewController.h"

@interface JAntiRubViewController : JBaseViewController

@property (weak, nonatomic) IBOutlet UIView *headerVIew;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end
