//
//  JRouterWiFiViewController.h
//  tcp
//
//  Created by Jelly on 15/8/2.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JBaseTableViewController.h"

@interface JRouterWiFiViewController : JBaseTableViewController

@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *firstPWDTextField;
@property (weak, nonatomic) IBOutlet UIButton *firstShowPWD;
@property (weak, nonatomic) IBOutlet UILabel *firstPWDLabel;

@property (weak, nonatomic) IBOutlet UISwitch *firstSwitch;
@property (weak, nonatomic) IBOutlet UILabel *firstTypeLabel;

@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *firstTap;


@property (weak, nonatomic) IBOutlet UITextField *secondNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *secondPWDTextField;
@property (weak, nonatomic) IBOutlet UIButton *secondShowPWD;

@property (weak, nonatomic) IBOutlet UISwitch *secondSwitch;
@property (weak, nonatomic) IBOutlet UILabel *secondTypeLabel;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *secondTap;
@property (weak, nonatomic) IBOutlet UILabel *secondPWDLabel;

@end
