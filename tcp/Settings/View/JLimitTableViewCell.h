//
//  JLimitTableViewCell.h
//  tcp
//
//  Created by Jelly on 15/8/1.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JLimitTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *limitNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *limitGreadLabel;

@end
