//
//  JRouterWiFiViewController.m
//  tcp
//
//  Created by Jelly on 15/8/2.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JRouterWiFiViewController.h"
#import "MMPickerView.h"
#import "JWifiDev.h"
#import "JServerClient+JCommand.h"
#import "AppDelegate.h"
#import <Masonry.h>

@interface JRouterWiFiViewController ()<UITextFieldDelegate>

@property(nonatomic, strong) UIButton* rightBtn;

@property(nonatomic, strong) NSArray* types;


@property(nonatomic, assign) BOOL edit;


@property(nonatomic, strong) NSDictionary* typeDic;
@property(nonatomic, strong) NSDictionary* typeValueDic;

@property(nonatomic, strong) JWifiDev* wifi ;
@property(nonatomic, strong) JWifiDev* wifi5G;

@end

@implementation JRouterWiFiViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    FIX_EDGES_FOR_EXTENDEDLAYTOU;
    self.rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.rightBtn setTitle:@"保存设置" forState:UIControlStateNormal];
    [self.rightBtn sizeToFit];
    [self.rightBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    self.types = @[@"强加密（WPA2）",@"混合加密（WPAPSK/WPA2）",@"无加密（允许所有人访问）"];
    
    self.typeDic =@{@"强加密（WPA2）":@"WPA2PSK",@"混合加密（WPAPSK/WPA2）":@"WPAPSK/WPA2PSK",@"无加密（允许所有人访问）":@"OPEN"};
    
    self.typeValueDic =@{@"WPA2PSK":@"强加密（WPA2）",@"WPAPSK/WPA2PSK":@"混合加密（WPAPSK/WPA2）",@"OPEN":@"无加密（允许所有人访问）"};
    self.title = @"WiFi设置";
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.navigationItem.rightBarButtonItem = [[ UIBarButtonItem alloc] initWithCustomView:self.rightBtn];
    
    
    
    NSArray* array = [JGlobals shareGlobal].routerInfo.wifiDev;
       for (JWifiDev* dev in array) {
        if ([dev.wifiType isEqualToString:@"5G"]) {
            self.wifi5G = dev;
        }else{
            self.wifi= dev;
        }
    }
    
    if (self.wifi) {
        self.firstSwitch.on = self.wifi.wifiSwitch == 1;
        self.firstNameTextField.text = self.wifi.ssid;
        self.firstPWDTextField.text = self.wifi.password;
        self.firstTypeLabel.text = [self keyForType:self.wifi.encrypt];
    }else{
        self.firstSwitch.on = NO;
        self.firstNameTextField.text = @"";
        self.firstPWDTextField.text = @"";
        self.firstTypeLabel.text = @"无";
    }
    if (self.wifi5G) {
        self.secondSwitch.on = self.wifi5G.wifiSwitch == 1;
        self.secondNameTextField.text = self.wifi5G.ssid;
        self.secondPWDTextField.text = self.wifi5G.password;
        self.secondTypeLabel.text = [self keyForType:self.wifi5G.encrypt];
    }else{
        self.secondSwitch.on = NO;
        self.secondNameTextField.text = @"";
        self.secondPWDTextField.text = @"";
        self.secondTypeLabel.text = @"无";
    }
    self.edit = NO;
    
    
    self.firstPWDTextField.delegate = self;
    self.secondPWDTextField.delegate = self;
    [self btnAction];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enable = NO;
    [MMPickerView dismissWithCompletion:nil];
}

-(void)backAction{
    if(self.edit){
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"您的修改未保存，是否返回？" delegate:nil cancelButtonTitle:@"否" otherButtonTitles:@"是", nil];
        
        [alertView show];
        [[alertView rac_buttonClickedSignal] subscribeNext:^(id x) {
            if([x integerValue]== 1){
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
        
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}



#pragma mark btnAction
-(void)btnAction{
    @weakify(self);
    [[self.firstShowPWD rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        self.firstPWDTextField.secureTextEntry = !self.firstPWDTextField.secureTextEntry;
    }];
    
    [[self.secondShowPWD rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        self.secondPWDTextField.secureTextEntry = !self.secondPWDTextField.secureTextEntry;
    }];
    
    [[self.firstSwitch rac_newOnChannel] subscribeNext:^(id x) {
        @strongify(self);
        self.edit = YES;
        [self setFirstWifiSwitch:[x boolValue]];
    }];
    
    [[self.secondSwitch rac_newOnChannel] subscribeNext:^(id x) {
        @strongify(self);
        self.edit = YES;
        [self setSecondWifiSwitch:[x boolValue]];
    }];
    
    
    [[self.rightBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        [self.firstNameTextField resignFirstResponder];
        [self.firstPWDTextField resignFirstResponder];
        [self.secondNameTextField resignFirstResponder];
        [self.secondPWDTextField resignFirstResponder];
        
        [self setWIFI];
    }];
    
    [[self.firstNameTextField rac_textSignal] subscribeNext:^(id x) {
        @strongify(self);
        if (![self.firstNameTextField.text isEqualToString:self.wifi.ssid]) {
            self.edit = YES;
        }
        
    }];
    
    [[self.firstPWDTextField rac_textSignal] subscribeNext:^(id x) {
        @strongify(self);
        if (![self.firstPWDTextField.text isEqualToString:self.wifi.password]) {
            self.edit = YES;
        }
    }];
    [[self.secondNameTextField rac_textSignal] subscribeNext:^(id x) {
        @strongify(self);
        if (![self.secondNameTextField.text isEqualToString:self.wifi5G.ssid]) {
            self.edit = YES;
        }
    }];
    [[self.secondPWDTextField rac_textSignal] subscribeNext:^(id x) {
        @strongify(self);
        if (![self.secondPWDTextField.text isEqualToString:self.wifi5G.password]) {
            self.edit = YES;
        }
    }];
    
    
    
    [[self.firstTap rac_gestureSignal] subscribeNext:^(id x) {
        @strongify(self);
        [self showPWDType:YES];
    }];
    
    [[self.secondTap rac_gestureSignal] subscribeNext:^(id x) {
        @strongify(self);
        [self showPWDType:NO];
    }];
}

-(void)setEdit:(BOOL)edit{
    _edit = edit;
    self.rightBtn.enabled = _edit;
    
    if(edit){
     [self.rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
}


-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 180;
}


#pragma mark 设置方法
/**
 *  设置第一个wifi是否开关
 *
 *  @param on 是否开
 */
-(void)setFirstWifiSwitch:(BOOL)on{
    
}
/**
 *  设置第二个wifi是否开关
 *
 *  @param on 是否开
 */
-(void)setSecondWifiSwitch:(BOOL)on{
    
}


/**
 *  显示加密方式
 *
 *  @param first 是否是第一个
 */
-(void)showPWDType:(BOOL)first{
    @weakify(self);
    [self.firstPWDTextField resignFirstResponder];
    [self.firstNameTextField resignFirstResponder];
    [self.secondPWDTextField resignFirstResponder];
    [self.secondNameTextField resignFirstResponder];
    NSString* select = first ? self.firstTypeLabel.text :self.secondTypeLabel.text;
    
     [MMPickerView  showPickerViewInView:self.view withStrings:self.types withOptions:@{@"selectedObject":select} completion:^(NSString *selectedString) {
        @strongify(self);
        self.edit = YES;
        if (first) {
            self.firstTypeLabel.text = selectedString;
            if ([selectedString isEqualToString:@"无加密（允许所有人访问）"]) {
                self.firstPWDTextField.enabled = NO;
                self.firstPWDTextField.text = @"";
                self.firstPWDLabel.textColor = [UIColor grayColor];
            }else{
                self.firstPWDTextField.enabled = YES;
                self.firstPWDLabel.textColor = [UIColor blackColor];
            }
        }else{
            self.secondTypeLabel.text = selectedString;
            if ([selectedString isEqualToString:@"无加密（允许所有人访问）"]) {
                self.secondPWDTextField.enabled = NO;
                self.secondPWDTextField.text = @"";
                self.secondPWDLabel.textColor = [UIColor grayColor];
            }else{
                self.secondPWDTextField.enabled = YES;
                self.secondPWDLabel.textColor = [UIColor blackColor];
            }
        }
    }];
    
    
    
}



-(void)setWIFI{
    
    JWifiDev* dev1 = [[JWifiDev alloc] init];
    dev1.wifiSwitch = self.firstSwitch.on;
    dev1.wifiType = @"2.4G";
    dev1.channel = self.wifi.channel;
    NSString* encrypt1 =  self.firstTypeLabel.text;
    dev1.encrypt = [self typeForKey:encrypt1];
    dev1.ssid = self.firstNameTextField.text;
    dev1.password = self.firstPWDTextField.text;
    
    JWifiDev* dev2 = [[JWifiDev alloc] init];
    dev2.wifiSwitch = self.secondSwitch.on;
    dev2.wifiType = @"5G";
    dev2.channel = self.wifi5G.channel;
    NSString* encrypt2 =  self.secondTypeLabel.text;
    
    dev2.encrypt = [self typeForKey:encrypt2];
    dev2.ssid = self.secondNameTextField.text;
    dev2.password = self.secondPWDTextField.text;
    
    if (dev1.ssid.length <=0 || dev2.ssid.length <= 0 ) {
        [[[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入有效的无线名称" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] show];
        return;
    }
    
    
    if ( ((dev1.encrypt.length > 0 || ![dev1.encrypt isEqualToString:@"OPEN"]) && dev1.ssid.length <=0) ||((dev2.encrypt.length > 0 || ![dev2.encrypt isEqualToString:@"OPEN"]) && dev2.ssid.length <= 0) ) {
       [[[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入至少8位数密码" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] show];
        return;
    }
    
    
    
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    @weakify(self);
    [MBProgressHUD showMessag:@"设置中..." toView:self.view];
    
    [[JServerClient setWifi:dev1 dev:dev2 sn:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(id x) {
        
        if ([JGlobals shareGlobal].isAigoRouter && [JGlobals shareGlobal].validateRouter) {
            [JGlobals shareGlobal].routerInfo.wifiDev = @[dev1,dev2];
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            [MBProgressHUD showSuccess:@"设置成功，请重新连接路由。" toView:self.view];
            
            AppDelegate* appdelegate = [UIApplication sharedApplication].delegate;
            [appdelegate showLoginView];
            return ;
        }
        NSString* commadIdentifier = x[@"id"];
        commadIdentifier = [NSString stringWithFormat:@"%@",commadIdentifier];
        if ([commadIdentifier length] > 0) {
            self.searchCommad = 0;
            [self searchCommandStatue:[NSString stringWithFormat:@"%@",commadIdentifier]];
        }else{
            [MBProgressHUD showError:@"设置失败，稍后重试！" toView:self.view];
            
        }
        
    } error:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
    }];
    
    
}

-(void)commandFail{
    
}

-(void)commandSuccess{
    JWifiDev* dev1 = [[JWifiDev alloc] init];
    dev1.wifiSwitch = self.firstSwitch.on;
    dev1.wifiType = @"2.4G";
    NSString* encrypt1 =  self.firstTypeLabel.text;
    dev1.encrypt = [self typeForKey:encrypt1];
    dev1.ssid = self.firstNameTextField.text;
    dev1.password = self.firstPWDTextField.text;
    
    JWifiDev* dev2 = [[JWifiDev alloc] init];
    dev2.wifiSwitch = self.secondSwitch.on;
    dev2.wifiType = @"5G";
    NSString* encrypt2 =  self.secondTypeLabel.text;
    
    dev2.encrypt =[self typeForKey:encrypt2];
    dev2.ssid = self.secondNameTextField.text;
    dev2.password = self.secondPWDTextField.text;
    
    [JGlobals shareGlobal].routerInfo.wifiDev = @[dev1,dev2];
    
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
    if ([string isEqualToString:@"\n"])
    {
        return YES;
    }
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (self.firstPWDTextField == textField || self.secondPWDTextField == textField)
    {
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression
                                      regularExpressionWithPattern:@"([^A-Za-z0-9_.])"
                                      options:0
                                      error:&error];
        
        NSString *resString = [regex stringByReplacingMatchesInString:string options:0 range:NSMakeRange(0, string.length) withTemplate:@""];
        
        if (resString.length != string.length) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"不能输入特殊字符" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] ;
            [alert show];
            return NO;
        }
        
        if ([toBeString length] > 16) {
            textField.text = [toBeString substringToIndex:16];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"提示请输入8到16位密码" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] ;
            [alert show];
            return NO;
        }
    }
    return YES;
}


-(NSString*)typeForKey:(NSString*)key{
    if (key.length > 0) {
        NSString* value = self.typeDic[key];
        if (value.length > 0 ) {
            return value;
        }else{
            return key;
        }
    }
    return @"WPAPSK/WPA2PSK";
}


-(NSString*)keyForType:(NSString*)type{
    if (type.length <= 0) {
        type = @"WPAPSK/WPA2PSK";
    }
    return  self.typeValueDic[type];
}

@end
