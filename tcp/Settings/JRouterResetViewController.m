//
//  JRouterResetViewController.m
//  tcp
//
//  Created by Jelly on 15/8/1.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JRouterResetViewController.h"
#import "JServerClient+JCommand.h"
#import "AppDelegate.h"

@interface JRouterResetViewController ()

@end

@implementation JRouterResetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"恢复出厂设置";
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)resetAction:(id)sender {
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"恢复出厂设置后所有数据将丢失，是否恢复？" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
    
    [alertView show];
    
    [[alertView rac_buttonClickedSignal] subscribeNext:^(id x) {
        if ([x integerValue] == 0) {
            [self resetRouter];
        }
    }];
}

-(void)resetRouter{
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    @weakify(self);
    [MBProgressHUD showMessag:@"设置中..." toView:self.view];
    [[JServerClient restoreRouter:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(id x) {
        if ([JGlobals shareGlobal].isAigoRouter && [JGlobals shareGlobal].validateRouter) {
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            [MBProgressHUD showSuccess:@"设置成功" toView:self.view];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                AppDelegate* appdelegate = [UIApplication sharedApplication].delegate;
                [appdelegate showLoginView];
            });
            return ;
        }
        
        NSString* commadIdentifier = x[@"id"];
        commadIdentifier = [NSString stringWithFormat:@"%@",commadIdentifier];
        if ([commadIdentifier length] > 0) {
            self.searchCommad = 0;
            [self searchCommandStatue:[NSString stringWithFormat:@"%@",commadIdentifier]];
        }else{
            [MBProgressHUD showError:@"设置失败，稍后重试！" toView:self.view];
            
        }
    } error:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
    }];
}

-(void)commandSuccess{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        AppDelegate* appdelegate = [UIApplication sharedApplication].delegate;
        [appdelegate showLoginView];
    });
}

@end
