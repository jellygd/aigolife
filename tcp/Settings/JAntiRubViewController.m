//
//  JAntiRubViewController.m
//  tcp
//
//  Created by Jelly on 15/8/1.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JAntiRubViewController.h"
#import "JRubIdenTableViewCell.h"
#import "JServerClient+JCommand.h"

@interface JAntiRubViewController ()<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic, assign) BOOL harassment;
@property(nonatomic, assign) BOOL rubNetwork;

@end

@implementation JAntiRubViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"防蹭网";
    
    [self setHeaderBgView:self.headerVIew];
    self.rubNetwork = NO;
    self.harassment = NO;
    self.tableView.layer.masksToBounds = YES;
    [self.tableView setTableFooterView:[[UIView alloc] init]];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    
    [MBProgressHUD showMessag:@"查询中..." toView:self.view];
    [self loadRouterData];
}

-(void)loadRouterDataFail{
    [MBProgressHUD hideHUDForView:self.view animated:YES];

    [self.tableView reloadData];
}


-(void)loadRouterDataSuccess{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 3;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 48.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < 2) {
        JRubIdenTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"JRubIdentifier" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row == 0 ) {
            cell.rubTitleLabel.text = @"防蹭网机制";
            cell.rubSubTitleLabel.text = @"开启后陌生设备连接WiFi时将会提醒您";
            cell.iconImageView.image = [UIImage imageNamed:@"icon_noneWifi_list1"];
            cell.rubSwitch.on = [[JGlobals shareGlobal].routerInfo.antiRubNetwork boolValue];
            cell.rubSwitch.tag = 1;
            cell.switchChangeBlock = ^(UISwitch *rubSwitch){
                [self switchChanged:rubSwitch];
            };
           
            
        }else{
            cell.rubTitleLabel.text = @"勿扰模式";
            cell.rubSubTitleLabel.text = @"开启后晚上10点到早上8点将不会有推送提醒";
            cell.rubSwitch.on = [[JGlobals shareGlobal].routerInfo.antiHarassment boolValue];
            cell.iconImageView.image = [UIImage imageNamed:@"icon_noneWifi_list2"];
            cell.rubSwitch.tag = 0;
            cell.switchChangeBlock = ^(UISwitch *rubSwitch){
                [self switchChanged:rubSwitch];
            };
        }
        return cell;
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"blackidentifier" forIndexPath:indexPath];
    
    return cell;
}


-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 2) {
        return YES;
    }
    return NO;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 2) {
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"DeviceStoryboard" bundle:[NSBundle mainBundle]];
        UIViewController* viewController = [story instantiateViewControllerWithIdentifier:@"JBlacklistViewController"];
        [self.navigationController pushViewController:viewController animated:YES];
    }
}



-(void)switchChanged:(id)sender{
    
    UISwitch* switchView = sender;
    BOOL antiHarassment = [[JGlobals shareGlobal].routerInfo.antiHarassment boolValue];
    BOOL anitiRubNetwork = [[JGlobals shareGlobal].routerInfo.antiRubNetwork boolValue];
    if (switchView.tag == 0) {
        antiHarassment = switchView.on ;
    }else{
        anitiRubNetwork = switchView.on ;
    }
    [self setUpAnti:antiHarassment rubNetwork:anitiRubNetwork];
}

/**
 *  设置防蹭网
 *
 *  @param harassment 骚扰模式
 *  @param rubNetwork 防蹭网
 */
-(void)setUpAnti:(BOOL)harassment rubNetwork:(BOOL)rubNetwork{
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    self.harassment = harassment;
    self.rubNetwork = rubNetwork;
    
    [MBProgressHUD showMessag:@"设置中..." toView:self.view];
    @weakify(self);
    [[JServerClient Anti:rubNetwork harassment:harassment sn:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(id x) {
        @strongify(self);
        if ([JGlobals shareGlobal].isAigoRouter && [JGlobals shareGlobal].validateRouter) {
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            [MBProgressHUD showSuccess:@"操作成功" toView:self.view];
            [JGlobals shareGlobal].routerInfo.antiHarassment = @(self.harassment ? self.harassment : 0);
            [JGlobals shareGlobal].routerInfo.antiRubNetwork = @(self.rubNetwork ? self.rubNetwork : 0);
            [self.tableView reloadData];
            return ;
        }
        NSString* commadIdentifier = x[@"id"];
        commadIdentifier = [NSString stringWithFormat:@"%@",commadIdentifier];
        if ([commadIdentifier length] > 0) {
            self.searchCommad = 0;
            [self searchCommandStatue:[NSString stringWithFormat:@"%@",commadIdentifier]];
        }else{
            [MBProgressHUD showError:@"设置失败，稍后重试！" toView:self.view];
        }
    } error:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
        [self.tableView reloadData];
    }];
}

-(void)commandSuccess{
    [JGlobals shareGlobal].routerInfo.antiHarassment = @(self.harassment ? self.harassment : 0);
    [JGlobals shareGlobal].routerInfo.antiRubNetwork = @(self.rubNetwork ? self.rubNetwork : 0);
    [self.tableView reloadData];
}


@end
