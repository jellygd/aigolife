//
//  JRouterNetWorkingViewController.m
//  tcp
//
//  Created by Jelly on 15/8/2.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JRouterNetWorkingViewController.h"
#import "JServerClient+JCommand.h"

@interface JRouterNetWorkingViewController ()
@property(nonatomic, strong ) UIButton* rightBtn;
@end

@implementation JRouterNetWorkingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.nameTextField.leftViewMode = UITextFieldViewModeAlways;
    
    self.pwdTextField.leftViewMode = UITextFieldViewModeAlways;
    
    self.nameTextField.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_login_name"]];
    self.pwdTextField.leftViewMode = UITextFieldViewModeAlways;
    self.pwdTextField.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_login_psw"]];
    
     self.title = @"外网设置";
    self.rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.rightBtn setTitle:@"确定" forState:UIControlStateNormal];
    [self.rightBtn sizeToFit];
    [self.rightBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    self.rightBtn.enabled = NO;
    self.navigationItem.rightBarButtonItem = [[ UIBarButtonItem alloc] initWithCustomView:self.rightBtn];
    
    
    @weakify(self);
    [[self.rightBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        [self setpppoeType];
    }];
    
    [self.nameTextField.rac_textSignal subscribeNext:^(id x) {
        [self showRightStatue];
    }];
    [self.pwdTextField.rac_textSignal subscribeNext:^(id x) {
        [self showRightStatue];
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


-(void)backAction{
    if (self.nameTextField.text.length > 0 || self.pwdTextField.text.length > 0) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"您有新的修改没保存，是否离开？ " delegate:nil cancelButtonTitle:@"否" otherButtonTitles:@"是", nil];
        [alertView show];
        @weakify(self);
        [[alertView rac_buttonClickedSignal] subscribeNext:^(id x) {
            if ([x integerValue] == 1) {
                @strongify(self);
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)showRightStatue{
    if(self.pwdTextField.text.length > 0 || self.nameTextField.text.length > 0){
        [self.rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.rightBtn.enabled = YES;
    }else{
        [self.rightBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        self.rightBtn.enabled = NO;
    }
}


- (IBAction)dhcpBtn:(id)sender {
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    JWanNet* wannet = [[JWanNet alloc] init];
    wannet.wanWay =@"dhcp";
    wannet.wanDns = @"";
    wannet.wanIp = @"";
    wannet.pppoePwd = @"";
    wannet.pppoeId = @"";
    wannet.wanGateway = @"";
    wannet.wanNetmask = @"";
    
    @weakify(self);
    [MBProgressHUD showMessag:@"设置中..." toView:self.view];
    [[JServerClient setRouterWan:[MTLJSONAdapter JSONDictionaryFromModel:wannet error:nil] sn:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(id x) {
        @strongify(self);
        if ([JGlobals shareGlobal].isAigoRouter && [JGlobals shareGlobal].validateRouter) {
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            [MBProgressHUD showSuccess:@"设置成功" toView:self.view];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
            return ;
        }
        
        NSString* key = [x objectForKey:@"id"];
        if (key) {
            [self searchCommandStatue:key];
        }else{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [MBProgressHUD showError:@"操作失败！请稍后重试！" toView:self.view];
        }
    } error:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
    }];
}


    

-(void)setpppoeType{
    if (!(self.nameTextField.text.length > 0 && self.pwdTextField.text.length > 0)) {
        [[[UIAlertView alloc]  initWithTitle:@"" message:@"请输入账号密码" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] show];
        return;
    }
    
    [self.nameTextField resignFirstResponder];
    [self.pwdTextField resignFirstResponder];
    
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    JWanNet* wannet = [[JWanNet alloc] init];
    wannet.wanWay =@"pppoe";
    wannet.wanDns = @"";
    wannet.wanIp = @"";
    wannet.pppoePwd = self.nameTextField.text;
    wannet.pppoeId = self.pwdTextField.text;
    wannet.wanGateway = @"";
    wannet.wanNetmask = @"";
    
    @weakify(self);
    [MBProgressHUD showMessag:@"设置中..." toView:self.view];
    [[JServerClient setRouterWan:[MTLJSONAdapter JSONDictionaryFromModel:wannet error:nil] sn:[JGlobals shareGlobal].routerMAC token:[JGlobals shareGlobal].routerToken appver:version] subscribeNext:^(id x) {
        @strongify(self);
        
        if ([JGlobals shareGlobal].isAigoRouter && [JGlobals shareGlobal].validateRouter) {
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            [MBProgressHUD showSuccess:@"设置成功" toView:self.view];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
            return ;
        }
        
        
        NSString* commadIdentifier = x[@"id"];
        commadIdentifier = [NSString stringWithFormat:@"%@",commadIdentifier];
        if ([commadIdentifier length] > 0) {
            self.searchCommad = 0;
            [self searchCommandStatue:[NSString stringWithFormat:@"%@",commadIdentifier]];
        }else{
            [MBProgressHUD showError:@"设置失败，稍后重试！" toView:self.view];

        }
        
    
    } error:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
    }];
}

-(void)commandFail{

}

-(void)commandSuccess{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
    });
}

@end
