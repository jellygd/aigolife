//
//  JBlurBaseView.h
//  tcp
//
//  Created by Jelly on 15/10/25.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JBlurBaseView : UIView

@property (unsafe_unretained, nonatomic) IBOutlet UIButton *settingBtn;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@end
