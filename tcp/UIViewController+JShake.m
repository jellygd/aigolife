//
//  UIViewController+JShake.m
//  tcp
//
//  Created by Jelly on 15/8/8.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "UIViewController+JShake.h"


static void *kDisableShakeKey = (void *)@"kDisableShakeKey";

@implementation UIViewController (JShake)

-(BOOL)canBecomeFirstResponder
{
    return [JGlobals shareGlobal].canShake;
}
- (void) motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    
    if (motion == UIEventSubtypeMotionShake) {
        [self shakeView];
    }
}


-(void)shakeView{
    
    if(self.viewDisableShake){
        return;
    }
    
    if (![JGlobals shareGlobal].deveicRouter && [JGlobals shareGlobal].user) {
        return;
    }
    
    NSInteger type = [JGlobals shareGlobal].shakeid;
    switch (type) {
        case 0:
            [self restartRouter];
            break;
        case 1:
            [self wifiSettings];
            break;
        case 2:
            [self  setAccount];
            break;
        case 3:
            [self setlimit];
            break;
            
        default:
            break;
    }
    
}

-(void)restartRouter{
    NSLog(@"重启路由");
    
    if ([self isKindOfClass:NSClassFromString(@"JRouterRestarViewController")]) {
        return;
    }
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UIViewController* viewController = [story instantiateViewControllerWithIdentifier:@"restartIndetifier"];
    [self.navigationController pushViewController:viewController animated:YES];
    
}
-(void)wifiSettings{
    if ([self isKindOfClass:NSClassFromString(@"JRouterRestarViewController")]) {
        return;
    }
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"RouterStoryboard" bundle:[NSBundle mainBundle]];
    UIViewController* viewController = [story instantiateViewControllerWithIdentifier:@"JRouterWiFiViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)setAccount{
    if ([self isKindOfClass:NSClassFromString(@"JRouterRestarViewController")]) {
        return;
    }
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"RouterStoryboard" bundle:[NSBundle mainBundle]];
    UIViewController* viewController = [story instantiateViewControllerWithIdentifier:@"JRouterNetWorkingViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)setlimit{
    if ([self isKindOfClass:NSClassFromString(@"JRouterlimitViewController")]) {
        return;
    }
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"RouterStoryboard" bundle:[NSBundle mainBundle]];
    UIViewController* viewController = [story instantiateViewControllerWithIdentifier:@"JRouterlimitViewController"];
    [self.navigationController pushViewController:viewController animated:YES];
}


-(BOOL)viewDisableShake{
    return [objc_getAssociatedObject(self, kDisableShakeKey) boolValue];
}

-(void)setViewDisableShake:(BOOL)viewDisableShake{
    objc_setAssociatedObject(self, kDisableShakeKey, @(viewDisableShake), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}



@end
