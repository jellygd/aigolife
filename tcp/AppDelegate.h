//
//  AppDelegate.h
//  tcp
//
//  Created by Jelly on 15/7/19.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UIViewController* rootViewController;

/**
 *  显示首页
 */
-(void)showMainView;

/**
 *  显示登录界面
 */
-(void)showLoginView;


@end

