//
//  JShakeViewController.h
//  tcp
//
//  Created by Jelly on 15/7/27.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JBaseViewController.h"

@interface JShakeViewController : JBaseViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *shakeHeaderView;
@property (weak, nonatomic) IBOutlet UITableView *shakeTableView;

@end
