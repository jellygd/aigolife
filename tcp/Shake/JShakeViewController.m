//
//  JShakeViewController.m
//  tcp
//
//  Created by Jelly on 15/7/27.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JShakeViewController.h"
#import "JShakeTableViewCell.h"

@interface JShakeViewController ()

@property(nonatomic, strong) NSArray* shareArray;

@end

@implementation JShakeViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.showBlur = YES;
    self.title = @"摇一摇";
    [self.shakeTableView setTableFooterView:[[UIView alloc] init]];
    [self setHeaderBgView:self.shakeHeaderView];
    self.shakeTableView.delegate = self;
    self.shakeTableView.dataSource = self;
    self.shareArray = @[@"重启路由器",@"WiFi设置",@"设置拨号账号与密码",@"设备智能限速",@"关闭摇一摇"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.shareArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    JShakeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"shakeIdentifier" forIndexPath:indexPath];
    cell.shakeImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_shake%d",indexPath.row+1]];
    cell.shakeNamelabel.text = self.shareArray[indexPath.row];
    if(indexPath.row == [[JGlobals shareGlobal] shakeid]){
        [cell setShakeSelected:YES];
    }else{
         [cell setShakeSelected:NO];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 48.0;
}


-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.showCanBlur) {
        [self showBlurView];
        return NO;
    }
    if (![JGlobals shareGlobal].deveicRouter && [JGlobals shareGlobal].user) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"您还没有绑定路由" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
        [alertView show];
        return NO;
    }
    if ([JGlobals shareGlobal].wifiStatue == WifiStatueUndefine || [JGlobals shareGlobal].wifiStatue == WifiStatueUnKnow) {
        [self showErrorMessage:@"云平台检测不到设备"];
        return NO;
    }
    if ([JGlobals shareGlobal].wifiStatue == WifiStatueCheckRouter) {
        [self showErrorMessage:@"请等待设备连接完成"];
        return NO;
    }
    
    return YES;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [[JGlobals shareGlobal]  setShakeType:indexPath.row];
    [tableView reloadData];
}

@end
