//
//  JShakeTableViewCell.h
//  tcp
//
//  Created by Jelly on 15/7/27.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JShakeTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *shakeImageView;

@property (weak, nonatomic) IBOutlet UILabel *shakeNamelabel;
@property (weak, nonatomic) IBOutlet UIImageView *shakeSelectedImageView;

/**
 *  设置图片是否选中
 *
 *  @param selected 是否选中
 */
-(void)setShakeSelected:(BOOL)selected;

@end
