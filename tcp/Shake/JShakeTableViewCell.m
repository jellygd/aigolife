//
//  JShakeTableViewCell.m
//  tcp
//
//  Created by Jelly on 15/7/27.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JShakeTableViewCell.h"

@implementation JShakeTableViewCell

- (void)awakeFromNib {
    self.shakeSelectedImageView.image = [UIImage imageNamed:@"icon_regist_selectSex_normal"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

-(void)setShakeSelected:(BOOL)selected{
    UIImage* image ;
    if (selected) {
        image = [UIImage imageNamed:@"icon_regist_selectSex_highlight"];
    }else{
        image = [UIImage imageNamed:@"icon_regist_selectSex_normal"];
    }
    
    self.shakeSelectedImageView.image = image;
}

@end
