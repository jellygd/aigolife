//
//  JWelcomeViewController.m
//  tcp
//
//  Created by Jelly on 15/11/20.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JWelcomeViewController.h"
#import <Masonry.h>

@interface JWelcomeViewController ()

@end

@implementation JWelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    NSDictionary * dic = @{@"320x480" : @"LaunchImage-700", @"320x568" : @"LaunchImage-700-568h", @"375x667" : @"LaunchImage-800-667h", @"414x736" : @"LaunchImage-800-Portrait-736h"};
    
    NSString * key = [NSString stringWithFormat:@"%dx%d", (int)[UIScreen mainScreen].bounds.size.width, (int)[UIScreen mainScreen].bounds.size.height];
    
    UIImage * launchImage = [UIImage imageNamed:dic[key]];
    self.welcomeImageView.image = launchImage;
    
    [self.activiyView startAnimating];
    
    [self.activiyView startAnimating];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
