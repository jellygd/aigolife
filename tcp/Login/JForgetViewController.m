//
//  JForgetViewController.m
//  tcp
//
//  Created by Jelly on 15/7/20.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JForgetViewController.h"
#import "JServerClient.h"
#import "MMPickerView.h"
#import "AppDelegate.h"

@interface JForgetViewController ()<UITextFieldDelegate>

@property(nonatomic, strong) NSString* countDownString;
@property(nonatomic, strong) NSString* codeIdentifier;
@property(nonatomic, assign) BOOL stopCountdown;

@end

@implementation JForgetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.viewDisableShake = YES;
    // Do any additional setup after loading the view.
    
    
    [self setleftView:self.view];
    
    
    RAC(self.codeButton,enabled) =[RACSignal combineLatest:@[self.phoneTextField.rac_textSignal,RACObserve(self, countDownString)] reduce:^(NSString *code, NSString *countDownString){
        
        return @(([[self class] validateMobile:code] || [[self class] validateEmail:code]) && countDownString.length == 0);
    }];
    
    
    [[self.codeButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        [self sendVerifyCode];
    }];
    
    RAC(self.doneButton, enabled) = [RACSignal
                                     combineLatest:@[
                                                     self.phoneTextField.rac_textSignal,
                                                     self.passwordTextField.rac_textSignal,self.codeTextField.rac_textSignal] reduce:^(NSString *username, NSString *password,NSString *code) {
                                                         
                                                         return @(([[self class] validateMobile:username] || [[self class] validateEmail:username]) && username.length > 0 &&username.length < 16 && password.length >=6&&code.length >0 );
                                                     }];
    
    
    [[self.doneButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        [self resetPassword];
    }];
    
    self.passwordTextField.delegate = self;
}

-(void)resetPassword{
     @weakify(self);
     [MBProgressHUD showMessag:@"发送中..." toView:self.view];
    [[JServerClient UpdateUserPassword:self.phoneTextField.text password:self.passwordTextField.text code:self.codeTextField.text identifier:self.codeIdentifier] subscribeNext:^(id x) {
        if (x) {
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            [MBProgressHUD showSuccess:@"密码重置成功！" toView:self.view];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
            
        }else{
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            [MBProgressHUD showError:@"未知错误" toView:self.view];
        }
    } error:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
    }];
}


-(void)setleftView:(UIView*)view{
    for (id target in view.subviews) {
        if ([target isKindOfClass:[UITextField class]]) {
            UITextField* targetTextField = target;
            CGRect frame = [targetTextField frame];
            frame.size.width = 7.0f;
            UIView *leftview = [[UIView alloc] initWithFrame:frame];
            targetTextField.leftViewMode = UITextFieldViewModeAlways;
            targetTextField.leftView = leftview;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(BOOL)canBecomeFirstResponder{
    return NO;
}




+ (BOOL) validateEmail:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

//手机号码验证
+ (BOOL) validateMobile:(NSString *)mobile
{
    //手机号以13， 15，18开头，八个 \d 数字字符
    NSString *phoneRegex = @"^((13[0-9])|(15[^4,\\D])|(18[0,0-9]))\\d{8}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    return [phoneTest evaluateWithObject:mobile];
}


/**
 *  发送验证码
 */
-(void)sendVerifyCode{
    
    NSString* phoneText = self.phoneTextField.text;
    [self.phoneTextField resignFirstResponder];
    [self.codeButton resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    if ([[self class] validateEmail:phoneText]) {
        @weakify(self);
        [MBProgressHUD showMessag:@"发送中..." toView:self.view];
        [[JServerClient sendMailVerifyCode:phoneText] subscribeNext:^(id x) {
            @strongify(self);
            NSString* identifier = x[@"identifier"];
            if (identifier) {
                self.codeIdentifier = identifier;
                self.countDownString = @"180s";
                [self countDown];
                [MBProgressHUD hideHUDForView:self.view animated:NO];
                [MBProgressHUD showSuccess:@"发送成功" toView:self.view];
            }else{
                [MBProgressHUD hideHUDForView:self.view animated:NO];
                [MBProgressHUD showError:@"未知错误" toView:self.view];
            }
        } error:^(NSError *error) {
            @strongify(self);
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
        }];
    }else if([[self class] validateMobile:phoneText]){
        @weakify(self);
        [MBProgressHUD showMessag:@"发送中..." toView:self.view];
        [[JServerClient sendPhoneVerifyCode:phoneText] subscribeNext:^(NSDictionary* x) {
            @strongify(self);
            NSString* identifier = x[@"identifier"];
            if (identifier) {
                self.codeIdentifier = identifier;
                self.countDownString = @"180s";
                [self countDown];
                [MBProgressHUD hideHUDForView:self.view animated:NO];
                [MBProgressHUD showSuccess:@"发送成功" toView:self.view];
            }else{
                [MBProgressHUD hideHUDForView:self.view animated:NO];
                [MBProgressHUD showError:@"未知错误" toView:self.view];
            }
        } error:^(NSError *error) {
            @strongify(self);
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
        }];
    }else{
        [MBProgressHUD showError:@"请输入有效的邮箱或者手机号码" toView:self.view];
    }
}


/**
 *  倒计时
 */
-(void)countDown{
    if ([self.countDownString length]>0 && [self.countDownString integerValue] > 0 && !self.stopCountdown) {
        NSInteger cound = [self.countDownString integerValue];
        cound -=1;
        self.countDownString = [NSString stringWithFormat:@"%lds",(long)cound];
        if (cound != 0) {
            self.codeButton.enabled = YES;
            [self.codeButton setTitle:self.countDownString forState:UIControlStateNormal];
            self.codeButton.enabled = NO;
            @weakify(self);
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                @strongify(self);
                [self countDown];
            });
        }else{
            [self.codeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
            self.codeButton.enabled = YES;
        }
    }else{
        [self.codeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
        self.codeButton.enabled = YES;
    }
}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
    if ([string isEqualToString:@"\n"])
    {
        return YES;
    }
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (self.passwordTextField == textField)
    {
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression
                                      regularExpressionWithPattern:@"([^A-Za-z0-9_.])"
                                      options:0
                                      error:&error];
        
        NSString *resString = [regex stringByReplacingMatchesInString:string options:0 range:NSMakeRange(0, string.length) withTemplate:@""];
        
        if (resString.length != string.length) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"不能输入特殊字符" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] ;
            [alert show];
            return NO;
        }
        
        if ([toBeString length] > 16) {
            textField.text = [toBeString substringToIndex:16];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"提示请输入6到16位密码" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] ;
            [alert show];
            return NO;
        }
    }
    return YES;
}


@end
