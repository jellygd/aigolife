//
//  JLoginViewController.h
//  tcp
//
//  Created by Jelly on 15/7/20.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JBaseViewController.h"

@protocol JLoginViewControllerDelegate <NSObject>

@required
-(void)loginDidSuccess;

-(void)loginDissmess;
@end

@interface JLoginViewController : JBaseViewController

@property (nonatomic,assign) id<JLoginViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *remeberBtn;

@property (weak, nonatomic) IBOutlet UIButton *dissmessButton;
@property (weak, nonatomic) IBOutlet UIImageView *headerImageView;
@property (weak, nonatomic) IBOutlet UITextField *userTextField;
@property (weak, nonatomic) IBOutlet UITextField *passTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;

@end
