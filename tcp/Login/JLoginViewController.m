//
//  JLoginViewController.m
//  tcp
//
//  Created by Jelly on 15/7/20.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JLoginViewController.h"
#import <SVProgressHUD.h>
#import "JServerClient.h"
#import <MBProgressHUD.h>
#import "MBProgressHUD+JMessage.h"
#import "JServerClient+JRouter.h"
#import "JGuideFirstViewController.h"
#import "JNavigationController.h"
#import "AppDelegate.h"

@interface JLoginViewController ()<UITextFieldDelegate>

@end

@implementation JLoginViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [IQKeyboardManager sharedManager].enable = YES;
    //    UIColor* color = UIColorFromRGB(0x2b9aec);
    self.viewDisableShake = YES;
    
    CGRect frame = [self.userTextField frame];
    frame.size.width = 7.0f;
    UIView *leftview = [[UIView alloc] initWithFrame:frame];
    self.userTextField.leftViewMode = UITextFieldViewModeAlways;
    self.userTextField.leftView = leftview;
    
    
    CGRect passframe = [self.passTextField frame];
    passframe.size.width = 7.0f;
    UIView *leftpassview = [[UIView alloc] initWithFrame:passframe];
    self.passTextField.leftViewMode = UITextFieldViewModeAlways;
    self.passTextField.leftView = leftpassview;

    NSString* user = [[NSUserDefaults standardUserDefaults] objectForKey:@"login_user"];
    NSString* pass = [[NSUserDefaults standardUserDefaults] objectForKey:@"login_pass"];
    
    if ([user length] > 0 &&pass.length >0) {
        self.remeberBtn.selected = YES;
    }else{
        self.remeberBtn.selected = NO;
    }
    self.userTextField.text = [user length] > 0 ?user :@""; //@"13174762863";
    self.passTextField.text = [user length] > 0 && pass.length >0 ? pass: @""; //@"1112223";
    @weakify(self);
    [[self.remeberBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        self.remeberBtn.selected = !self.remeberBtn.selected;
    }];
    self.validateRouterBlock = ^(NSNumber* success){
        if (!success) {
            return ;
        }
        if ([success boolValue]) {
            @strongify(self);
            if (self.delegate) {
                [JGlobals shareGlobal].validateRouter = YES;
                [self.delegate loginDissmess];
            }
        }else{
            UIAlertView* alertView =  [[UIAlertView alloc] initWithTitle:@"" message:@"密码错误请重新输入" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] ;
            [alertView show];
            [[alertView rac_buttonClickedSignal] subscribeNext:^(id x) {
                @strongify(self);
                [self showRouterPWDAlertView];
            }];
        }
    };
   
    [[self.dissmessButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
         [MBProgressHUD showMessag:@"查询路由状态..." toView:self.view];
        [[JServerClient queryRouterSetupInfo] subscribeNext:^(id x) {
            @strongify(self);
            NSDictionary* data = x[@"data"];
            BOOL isSet = [data[@"isSet"] boolValue];
            NSString* mac = data[@"mac"];
            [JGlobals shareGlobal].isAigoRouter = YES;
            [JGlobals shareGlobal].isSet = isSet;
            [JGlobals shareGlobal].routerMAC = mac;
            
            
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            
            if (!isSet) {
                NSLog(@"AigoWIFI 没有设置过 mac 地址为：%@",mac);
                UIStoryboard *story = [UIStoryboard storyboardWithName:@"GuideStoryboard" bundle:[NSBundle mainBundle]];
                //由storyboard根据myView的storyBoardID来获取我们要切换的视图
                JGuideFirstViewController *storageViewController = [story instantiateViewControllerWithIdentifier:@"guide1"];
               AppDelegate* appdelegate =(AppDelegate*) [UIApplication sharedApplication].delegate;
                appdelegate.window.rootViewController = [[JNavigationController alloc] initWithRootViewController:storageViewController];
                return ;
            }
        
            [self showRouterPWDAlertView];
            
        } error:^(NSError *error) {
            [JGlobals shareGlobal].isAigoRouter = NO;
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            [[[UIAlertView alloc] initWithTitle:@"" message:@"请连接aigo路由器的无线wifi" delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] show];
        }];       
    }];
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden  = YES;
}

-(void)viewWillDisappear:(BOOL)animated{
    self.navigationController.navigationBarHidden = NO;
    [super viewWillDisappear:animated];
}


- (IBAction)loginAction:(id)sender {
    if (!(self.userTextField.text.length > 0 && self.passTextField.text.length >= 6)) {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"" message:@"请输入有效的账号或密码" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
        
        [alert show];
        return;
    }
    
    [MBProgressHUD showMessag:@"登录中..." toView:self.view];
    @weakify(self);
    [[JServerClient loginUser:self.userTextField.text pass:self.passTextField.text] subscribeNext:^(NSDictionary* x) {
        @strongify(self);
        NSString* token = x[@"token"];
        NSString* duration = x[@"duration"];
        NSString* expire = x[@"expire"];
        if ([token length] > 0) {
            [IQKeyboardManager sharedManager].enable = NO;
            NSLog(@"登录成功  token 为：%@",token);
            NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
            NSString* deviceToken = [userDefaults objectForKey:@"deviceToken"];
            [userDefaults setObject:self.userTextField.text forKey:@"login_user"];
            if (self.remeberBtn.selected) {
                [userDefaults setObject:self.passTextField.text forKey:@"login_pass"];
            }else{
                [userDefaults setObject:@"" forKey:@"login_pass"];
                
            }
            NSLog(@"deviceToken == %@",deviceToken);
            if (deviceToken.length > 0) {
                [[JServerClient registerPush:self.userTextField.text deviceToken:deviceToken] subscribeNext:^(id x) {}];
            }
            [userDefaults setObject:token forKey:@"token"];
            if (expire) {
                [userDefaults setObject:expire forKey:@"expire"];
            }
            if (duration) {
                [userDefaults setObject:duration forKey:@"duration"];
            }
            [userDefaults synchronize];
            [JGlobals shareGlobal].userName = self.userTextField.text;
            //需要保存token 跟有效期
           
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            [self loadRouterData];
        }else{
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            [MBProgressHUD showError:@"未知错误" toView:self.view];
        }
    } error:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
    }completed:^{
        
    }];
}


-(void)loadRouterData{
//    if ([JGlobals shareGlobal].isAigoRouter) {
//        [[RACSignal combineLatest:@[[JServerClient queryRouterSetupInfo],[JServerClient GetBindDeviceList:[JGlobals shareGlobal].token]] reduce:^(NSDictionary* localInfo,NSDictionary* x){
//            NSLog(@"获取路由信息 跟 用户的信息成功");
//            return @{@"localInfo":localInfo,@"userInfo":x};
//        } ] subscribeNext:^(id x) {
//            //如果当前用户没有绑定路由 则绑定当前的路由
//            NSDictionary* userInfo =  x[@"userInfo"];
//            NSArray* devices = [MTLJSONAdapter modelsOfClass:JDeviceModule.class fromJSONArray:userInfo[@"device"] error:nil];
//            devices = [devices rx_filterWithBlock:^BOOL(JDeviceModule* module) {
//                return [module.type isEqualToString:@"router"];
//            }];
//            //有绑定设备
//            NSDictionary* localInfo = x[@"localInfo"][@"data"];
//            BOOL bind =  [localInfo[@"bind"] boolValue];
//            BOOL isSet = [localInfo[@"isSet"] boolValue];
//            NSString* macString = localInfo[@"mac"];
//            
//            if (!isSet) {
//                NSLog(@"路由没有设置过 进行设置");
//                UIStoryboard *story = [UIStoryboard storyboardWithName:@"GuideStoryboard" bundle:[NSBundle mainBundle]];
//                //由storyboard根据myView的storyBoardID来获取我们要切换的视图
//                JGuideFirstViewController *storageViewController = [story instantiateViewControllerWithIdentifier:@"guide1"];
//                [self.navigationController pushViewController:storageViewController animated:YES];
//            }
//            if (!bind) {
//            
//            }
//        }];
//    }else
    if (self.delegate) {
        [IQKeyboardManager sharedManager].enable = NO;
        [self.delegate loginDidSuccess];
    }
}

-(BOOL)canBecomeFirstResponder{
    return NO;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
    if ([string isEqualToString:@"\n"])
    {
        return YES;
    }
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (self.passTextField == textField)
    {
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression
                                      regularExpressionWithPattern:@"([^A-Za-z0-9_.])"
                                      options:0
                                      error:&error];
        
        NSString *resString = [regex stringByReplacingMatchesInString:string options:0 range:NSMakeRange(0, string.length) withTemplate:@""];
        
        if (resString.length != string.length) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"不能输入特殊字符" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] ;
            [alert show];
            return NO;
        }
        
        if ([toBeString length] > 16) {
            textField.text = [toBeString substringToIndex:16];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"提示请输入6到16位密码" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] ;
            [alert show];
            return NO;
        }
    }
    return YES;
}


@end
