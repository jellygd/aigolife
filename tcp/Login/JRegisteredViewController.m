//
//  JRegisteredViewController.m
//  tcp
//
//  Created by Jelly on 15/7/20.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JRegisteredViewController.h"
#import "JServerClient.h"
#import "UUPhotoActionSheet.h"
#import "MMPickerView.h"
#import "AppDelegate.h"

@interface JRegisteredViewController()<UUPhotoActionSheetDelegate>

@property(nonatomic,assign) BOOL btnSelect;
@property(nonatomic, strong) NSString* countDownString;
@property(nonatomic, strong) NSString* codeIdentifier;
@property(nonatomic, assign) BOOL stopCountdown;
@property(nonatomic, strong) NSString* avatorUrl;

@property (nonatomic, strong) UUPhotoActionSheet *sheet;


@property(nonatomic, strong) UITapGestureRecognizer*ageGestureRecognizer;

@end

@implementation JRegisteredViewController


-(void)viewDidLoad{
    [super viewDidLoad];
    
    self.viewDisableShake = YES;
    
    self.countDownString = @"";
    self.registerLabel.textColor = [self AvailableColor];
    self.ziliaoLabel.textColor = [self unAvailableColor];
    [self setleftView:self.registeredView];
    
    
    self.touxiangBtn.layer.masksToBounds = YES;
    self.touxiangBtn.layer.cornerRadius = self.touxiangBtn.height / 2;
    [self setleftView:self.ziliaoView];
    self.ziliaoView.hidden = YES;
    self.sexBoy.selected = YES;
    [self btnActions];
    
    RAC(self.registeredButton, enabled) = [RACSignal
                                           combineLatest:@[
                                                           self.phoneTextField.rac_textSignal,
                                                           self.passwordTextField.rac_textSignal,self.codeTextField.rac_textSignal,RACObserve(self, btnSelect)] reduce:^(NSString *username, NSString *password,NSString *code,id selected) {
                                                               
                                                               return @(([[self class] validateMobile:username] || [[self class] validateEmail:username]) && username.length > 0 && password.length >=6&&code.length >0 && [selected boolValue]);
                                                           }];

    
    RAC(self.codeButton,enabled) =[RACSignal combineLatest:@[self.phoneTextField.rac_textSignal,RACObserve(self, countDownString)] reduce:^(NSString *code, NSString *countDownString){
        
        return @(([[self class] validateMobile:code] || [[self class] validateEmail:code]) && countDownString.length == 0);
    }];
    self.ageBtn.userInteractionEnabled = NO;
    self.ageGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showAge)];
    self.ageImageView.userInteractionEnabled = YES;
    [self.ageImageView addGestureRecognizer:self.ageGestureRecognizer];
    
    
    
   
    //    UINavigationBar *bar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
    //
    //    bar.tintColor = [UIColor darkGrayColor];
    //    [self.ageActionSheet addSubview:bar];
    //    [self.ageActionSheet.view addSubview:self.agePickerView];
    
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(void)setleftView:(UIView*)view{
    for (id target in view.subviews) {
        if ([target isKindOfClass:[UITextField class]]) {
            UITextField* targetTextField = target;
            CGRect frame = [targetTextField frame];
            frame.size.width = 7.0f;
            UIView *leftview = [[UIView alloc] initWithFrame:frame];
            targetTextField.leftViewMode = UITextFieldViewModeAlways;
            targetTextField.leftView = leftview;
        }
    }
}


-(UIColor*)AvailableColor{
    return UIColorFromRGB(0x5CA9ED);
}

-(UIColor*)unAvailableColor{
    return UIColorFromRGB(0x9FE1F9);
}

- (IBAction)showZiliao:(id)sender {
    self.ziliaoView.hidden = NO;
    self.ziliaoLabel.textColor = [self AvailableColor];
    self.registerLabel.textColor = [self unAvailableColor];
    
}
- (IBAction)showRegister:(id)sender {
    self.ziliaoView.hidden = YES;
    self.ziliaoLabel.textColor = [self unAvailableColor];
    self.registerLabel.textColor = [self AvailableColor];
}


/**
 *  所有的按钮事件
 */
-(void)btnActions{
    @weakify(self);
    [[self.userButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton* btn) {
        @strongify(self);
        self.btnSelect = !self.userButton.selected;
        self.userButton.selected = self.btnSelect;
    }];
    
    [[self.registeredButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        [self registerUser];
    }];
    
    [[self.ziliaoBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        [self updateUserInfo];
    }];
    
    [[self.codeButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        [self sendVerifyCode];
    }];
    
    [[self.sexBoy rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        self.sexBoy.selected = YES;
        self.sexGirl.selected = NO;
    }];
    
    [[self.sexGirl rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        self.sexGirl.selected = YES;
        self.sexBoy.selected = NO;
    }];
    
    [[self.touxiangBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        [self showTouxiangPicker];
    }];
}


+ (BOOL) validateEmail:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

//手机号码验证
+ (BOOL) validateMobile:(NSString *)mobile
{
    //手机号以13， 15，18开头，八个 \d 数字字符
    NSString *phoneRegex = @"^((13[0-9])|(15[^4,\\D])|(18[0,0-9]))\\d{8}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    return [phoneTest evaluateWithObject:mobile];
}


/**
 *  发送验证码
 */
-(void)sendVerifyCode{
    
    NSString* phoneText = self.phoneTextField.text;
    [self.phoneTextField resignFirstResponder];
    [self.codeButton resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    if ([[self class] validateEmail:phoneText]) {
        @weakify(self);
        [MBProgressHUD showMessag:@"发送中..." toView:self.view];
        [[JServerClient sendMailVerifyCode:phoneText] subscribeNext:^(id x) {
            @strongify(self);
            NSString* identifier = x[@"identifier"];
            if (identifier) {
                self.codeIdentifier = identifier;
                self.countDownString = @"180s";
                [self countDown];
                [MBProgressHUD hideHUDForView:self.view animated:NO];
                [MBProgressHUD showSuccess:@"发送成功" toView:self.view];
            }else{
                [MBProgressHUD hideHUDForView:self.view animated:NO];
                [MBProgressHUD showError:@"未知错误" toView:self.view];
            }
        } error:^(NSError *error) {
            @strongify(self);
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
        }];
    }else if([[self class] validateMobile:phoneText]){
        @weakify(self);
        [MBProgressHUD showMessag:@"发送中..." toView:self.view];
        [[JServerClient sendPhoneVerifyCode:phoneText] subscribeNext:^(NSDictionary* x) {
            @strongify(self);
            NSString* identifier = x[@"identifier"];
            if (identifier) {
                self.codeIdentifier = identifier;
                self.countDownString = @"180s";
                [self countDown];
                [MBProgressHUD hideHUDForView:self.view animated:NO];
                [MBProgressHUD showSuccess:@"发送成功" toView:self.view];
            }else{
                [MBProgressHUD hideHUDForView:self.view animated:NO];
                [MBProgressHUD showError:@"未知错误" toView:self.view];
            }
        } error:^(NSError *error) {
            @strongify(self);
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
        }];
    }else{
        [MBProgressHUD showError:@"请输入有效的邮箱或者手机号码" toView:self.view];
    }
}
/**
 *  注册账号
 */
-(void)registerUser{
    [MBProgressHUD showMessag:@"注册中..." toView:self.view];
    NSUInteger sex = 0;
    @weakify(self);
    [[JServerClient UserRegister:self.phoneTextField.text nickName:@"" sex:sex pass:self.passwordTextField.text code:self.codeTextField.text identifier:self.codeIdentifier] subscribeNext:^(id x) {
        if (x) {
            [JGlobals shareGlobal].userName = self.phoneTextField.text;
            [self registerUserSuccess];
        }else{
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            [MBProgressHUD showError:@"未知错误" toView:self.view];
        }
    } error:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
    }];
}

/**
 *  更新用户信息
 */
-(void)updateUserInfo{
    if([self.nameTextField.text length] <= 0){
        [MBProgressHUD showError:@"请输入呢称" toView:self.view];
        return;
    }
    if([self.ageLabel.text length] <= 0){
        [MBProgressHUD showError:@"请选择年龄" toView:self.view];
         return;
    }
    @weakify(self);
    [MBProgressHUD showMessag:@"完善资料中..." toView:self.view];
    [[JServerClient GetUserInfo:self.phoneTextField.text] subscribeNext:^(id x) {
        @strongify(self);
        
        JUserModule* user = [MTLJSONAdapter modelOfClass:[JUserModule class] fromJSONDictionary:x[@"user"] error:nil];
        if (self.avatorUrl.length > 0) {
            user.avatarUrl = self.avatorUrl;
        }
        user.age = @([self.ageLabel.text integerValue]);
        user.nickname = self.nameTextField.text;
        user.username = self.phoneTextField.text;
        user.sex = self.sexBoy.selected ?@(1):@(0);
        
        [[JServerClient UpdateUserInfo:user token:[JGlobals shareGlobal].token] subscribeNext:^(id x) {
            [MBProgressHUD hideHUDForView:self.view animated:NO];
          AppDelegate* delegate = [[UIApplication sharedApplication] delegate];
            [delegate showMainView];
        } error:^(NSError *error) {
            @strongify(self);
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
        }];
    }error:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
    }];
    
}


-(void)registerUserSuccess{
    @weakify(self);
    [[JServerClient loginUser:self.phoneTextField.text pass:self.passwordTextField.text] subscribeNext:^(id x) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSString* token = x[@"token"];
        NSString* duration = x[@"duration"];
        NSString* expire = x[@"expire"];
        if ([token length] > 0) {
            NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:token forKey:@"token"];
            if (expire) {
                [userDefaults setObject:expire forKey:@"expire"];
            }
            if (duration) {
                [userDefaults setObject:duration forKey:@"duration"];
            }
            [userDefaults synchronize];
            [self showZiliao:nil];
        }else{
            
        }
    } error:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
}

/**
 *  倒计时
 */
-(void)countDown{
    if ([self.countDownString length]>0 && [self.countDownString integerValue] > 0 && !self.stopCountdown) {
        NSInteger cound = [self.countDownString integerValue];
        cound -=1;
        self.countDownString = [NSString stringWithFormat:@"%lds",(long)cound];
        if (cound != 0) {
            self.codeButton.enabled = YES;
            [self.codeButton setTitle:self.countDownString forState:UIControlStateNormal];
            self.codeButton.enabled = NO;
            @weakify(self);
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                @strongify(self);
                [self countDown];
            });
        }else{
            [self.codeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
            self.codeButton.enabled = YES;
        }
    }else{
        [self.codeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
        self.codeButton.enabled = YES;
    }
}


/**
 *  显示年龄选择
 */
-(void)showAge{
    NSMutableArray* array = [NSMutableArray array];
    for (NSInteger i = 1; i< 150; i++) {
        [array addObject:[NSString stringWithFormat:@"%ld",(long)i]];
    }
    @weakify(self);
    NSInteger age = [self.ageLabel.text integerValue];
    if (age <= 0 ) {
        age = 25;
    }
    [MMPickerView  showPickerViewInView:self.view withStrings:array withOptions:@{@"selectedObject":[NSString stringWithFormat:@"%d",age]} completion:^(NSString *selectedString) {
        @strongify(self);
        self.ageLabel.text = selectedString;
    }];
}

-(void)showTouxiangPicker{
    if(!_sheet){
        _sheet = [[UUPhotoActionSheet alloc] initWithMaxSelected:1
                                                       weakSuper:self];
        
        _sheet.delegate = self;
        [self.navigationController.view addSubview:_sheet];
    }
    [_sheet showAnimation];
}




-(BOOL)canBecomeFirstResponder{
    return NO;
}


#pragma mark - UUPhotoActionSheetDelegate

- (void)actionSheetDidFinished:(NSArray *)obj{
    UIImage* image = [obj firstObject];
    [MBProgressHUD showMessag:@"上传中..." toView:self.view];
    [[JServerClient uploadFile:UIImageJPEGRepresentation(image, 0.5) token:[JGlobals shareGlobal].token] subscribeNext:^(id x) {
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        NSString* imageUrl = x[@"url"];
        if ([imageUrl length] > 0) {
            self.avatorUrl = imageUrl;
            self.touxiangImageView.image = image;
            self.touxiangImageView.layer.cornerRadius = self.touxiangImageView.width/2;
            self.touxiangImageView.layer.masksToBounds = YES;
        }
    } error:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:@"上传失败" toView:self.view];
    }];

}
@end
