//
//  JRegisteredViewController.h
//  tcp
//
//  Created by Jelly on 15/7/20.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JBaseViewController.h"

@interface JRegisteredViewController : JBaseViewController
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *codeTextField;
@property (weak, nonatomic) IBOutlet UIButton *codeButton;
@property (weak, nonatomic) IBOutlet UIButton *userButton;
@property (weak, nonatomic) IBOutlet UIButton *registeredButton;
@property (weak, nonatomic) IBOutlet UIButton *userUrlButton;
@property (weak, nonatomic) IBOutlet UIImageView *touxiangImageView;

@property (weak, nonatomic) IBOutlet UIView *registeredView;
@property (weak, nonatomic) IBOutlet UILabel *registerLabel;
@property (weak, nonatomic) IBOutlet UILabel *ziliaoLabel;


//完善资料

@property (weak, nonatomic) IBOutlet UIView *ziliaoView;
@property (weak, nonatomic) IBOutlet UIButton *touxiangBtn;
@property (weak, nonatomic) IBOutlet UIView *touxiangBgView;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UIButton* ageBtn;
@property (weak, nonatomic) IBOutlet UIImageView *ageImageView;
@property (weak, nonatomic) IBOutlet UIButton *ziliaoBtn;

@property (weak, nonatomic) IBOutlet UIButton *sexGirl;

@property (weak, nonatomic) IBOutlet UIButton *sexBoy;

@end
