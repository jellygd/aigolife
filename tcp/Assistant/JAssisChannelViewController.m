//
//  JAssisChannelViewController.m
//  tcp
//
//  Created by Jelly on 15/7/29.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JAssisChannelViewController.h"
#import "JChannelTableViewCell.h"
#import "JServerClient+JRouter.h"
#import "JChannel.h"
#import "JServerClient+JCommand.h"

@interface JAssisChannelViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic, strong) NSMutableArray* channelArray;
/**
 *  当前选中频道
 */
@property(nonatomic, assign) NSInteger currentChannel;
/**
 *  推荐渠道
 */
@property(nonatomic, assign) NSInteger recommendChannel;


@end

@implementation JAssisChannelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    FIX_EDGES_FOR_EXTENDEDLAYTOU;
    self.title = @"WiFi信道调节器";
//    UIView* view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 20)];
//    [self.channelTableView setTableFooterView:view];
    self.channelTableView.delegate = self;
    self.channelTableView.dataSource = self;
    
    self.channelArray = [NSMutableArray array];
    
    [self loadChannelData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadChannelData{
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    @weakify(self);
    [MBProgressHUD showMessag:@"查询中..." toView:self.view];
    [[JServerClient queryChannels:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(NSDictionary* response) {
        @strongify(self);
        NSDictionary* data = response[@"data"];
        if (data) {
            self.recommendChannel = [data[@"recommend"] integerValue];
            self.currentChannel = [data[@"current"] integerValue];
            NSArray*dev = data[@"channelList"];
            self.channelArray =  [MTLJSONAdapter modelsOfClass:JChannel.class fromJSONArray:dev error:nil];

            self.channelArray = [self.channelArray sortedArrayUsingComparator:^NSComparisonResult(JChannel *obj1, JChannel *obj2) {
                                   NSComparisonResult result = [obj1.score compare:obj2.score];
                                   
                                   return result;  
                               }];
            [self.channelTableView reloadData];
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } error:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
    }];
}

#pragma mark - Table view data source



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.channelArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 48.0;
}

 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
     JChannelTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JChannelTableViewCell" forIndexPath:indexPath];
     JChannel* channel = [self.channelArray objectAtIndex:indexPath.row];
     
     cell.channelName.text = [NSString stringWithFormat:@"%@",channel.channel];
     cell.channelProgress.progress = (100-[channel.score floatValue])/100;
     if (self.currentChannel == [channel.channel integerValue]) {
         cell.channelrightView.hidden = NO;
         cell.channelrightView.image = [UIImage imageNamed:@"icon_gou"];
     }else if(indexPath.row == 0){
         cell.channelrightView.hidden = NO;
         cell.channelrightView.image = [UIImage imageNamed:@"icon_tuijian"];
     }else{
         cell.channelrightView.hidden = YES;
     }
     
     return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    JChannel* channel = [self.channelArray objectAtIndex:indexPath.row];
    NSString* tips = [NSString stringWithFormat:@"确定要切换到信道%@",channel.channel];
    
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"" message:tips delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
    
    [alert show];
    @weakify(self);
    [[alert rac_buttonClickedSignal] subscribeNext:^(id x) {
        if ([x integerValue] == 0) {
            @strongify(self);
            [self resetChannel:indexPath];
        }
    }];
}

-(void)resetChannel:(NSIndexPath*)indexPath{
    JChannel* channel = [self.channelArray objectAtIndex:indexPath.row];
    
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    @weakify(self);
    [MBProgressHUD showMessag:@"设置中..." toView:self.view];
    
    [[JServerClient assistant:[channel.channel  integerValue] level:-1 sn:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(id x) {
        if ([JGlobals shareGlobal].isAigoRouter && [JGlobals shareGlobal].validateRouter) {
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            [MBProgressHUD showSuccess:@"设置成功" toView:self.view];
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"请重新连接路由" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
            return ;
        }
        
        NSString* commadIdentifier = x[@"id"];
        commadIdentifier = [NSString stringWithFormat:@"%@",commadIdentifier];
        if ([commadIdentifier length] > 0) {
            self.searchCommad = 0;
            [self searchCommandStatue:[NSString stringWithFormat:@"%@",commadIdentifier]];
        }else{
            [MBProgressHUD showError:@"设置失败，稍后重试！" toView:self.view];
            
        }
        
    } error:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
    }];

}

-(void)commandSuccess{
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"请重新连接路由" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alertView show];
}

@end
