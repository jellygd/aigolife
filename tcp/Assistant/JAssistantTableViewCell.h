//
//  JAssistantTableViewCell.h
//  tcp
//
//  Created by Jelly on 15/7/29.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JAssistantTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *assistantImageView;
@property (weak, nonatomic) IBOutlet UILabel *assistantNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *assistantRightImageVIew;
@property (weak, nonatomic) IBOutlet UILabel *assistantSubLabel;

@end
