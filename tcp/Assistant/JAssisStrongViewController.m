//
//  JAssisStrongViewController.m
//  tcp
//
//  Created by Jelly on 15/7/29.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JAssisStrongViewController.h"
#import "MultiplePulsingHaloLayer.h"
#import <YLProgressBar.h>
#import "JServerClient+JCommand.h"

@interface JAssisStrongViewController ()

@property (nonatomic, weak) MultiplePulsingHaloLayer *mutiHalo;
@property (nonatomic, strong)YLProgressBar* progressBar;
@property (nonatomic, strong) UIView* barIcon;
@property (nonatomic, strong) UIButton* rightBtn;
@property (nonatomic, assign) NSInteger selectValue;

@property (nonatomic, strong) NSArray* keyArray;
@end

@implementation JAssisStrongViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"WiFi信道强度调节器";
    
    
    
    self.keyArray = @[@"低辐射",@"穿墙",@"强力穿墙",@"无敌了"];
//    MultiplePulsingHaloLayer *multiLayer = [[MultiplePulsingHaloLayer alloc] initWithHaloLayerNum:4 andStartInterval:0.7];
//    self.mutiHalo = multiLayer;
//    self.mutiHalo.position = CGPointMake(self.xinhaoView.center.x -self.xinhaoView.left, self.xinhaoView.center.y - self.xinhaoView.top);
//    self.mutiHalo.useTimingFunction = NO;
//    [self.mutiHalo buildSublayers];
//    self.mutiHalo.radius = 150.0;
//    UIColor *color = [UIColor colorWithRed:0
//                                     green:0.487
//                                      blue:1.0
//                                     alpha:1.0];
//    
//    [self.mutiHalo setHaloLayerColor:color.CGColor];
//
//    [self.xinhaoView.layer insertSublayer:self.mutiHalo below:self.xinhaoView.layer];
    
    self.progressBar = [[YLProgressBar alloc] initWithFrame:CGRectMake(0, 5, self.segmentedView.frame.size.width, self.segmentedView.height)];
    self.progressBar.height = 5;
    self.progressBar.backgroundColor = [UIColor redColor];
    self.progressBar.progress = 1;
    _progressBar.type               = YLProgressBarTypeFlat;
    _progressBar.progressTintColors = @[UIColorFromRGB(0xff3006),UIColorFromRGB(0xf6f400),UIColorFromRGB(0xadadad),UIColorFromRGB(0xaeaeae)];
    _progressBar.hideStripes        = YES;
    _progressBar.hideTrack          = YES;
    _progressBar.behavior           = YLProgressBarBehaviorDefault;
    [_progressBar setProgress:1];
    [self.segmentedView addSubview:_progressBar];
    self.segmentedView.layer.masksToBounds = YES;
    
    self.barIcon = [[UIView alloc] initWithFrame:CGRectMake(0, -2.5, 10, 10)];
    self.barIcon.backgroundColor = UIColorFromRGB(0x2b9aeb);
    self.barIcon.layer.masksToBounds = YES;
    self.barIcon.layer.cornerRadius = 5;
    [self.progressBar addSubview:self.barIcon];
    
    [self.segmentedView addTarget:self action:@selector(segmentAction:) forControlEvents:UIControlEventValueChanged];
    
    
    self.rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.rightBtn setTitle:@"修改" forState:UIControlStateNormal];
    [self.rightBtn sizeToFit];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.rightBtn];
    @weakify(self);
    [[self.rightBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        if (self.segmentedView.selectedSegmentIndex != self.selectValue) {
            [self setxinhao];
        }
    }];
    
    [MBProgressHUD showMessag:@"查询中..." toView:self.view];

    [self loadRouterData];
    
   
}

-(void)loadRouterDataSuccess{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self setData];
}

-(void)loadRouterDataFail{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [self setData];
}


-(void)setData{
    NSInteger level =  [[JGlobals shareGlobal].routerInfo.signalLevel integerValue];
    if(level <= 0){
        level = 1;
    }
    if (level >= 4) {
        level = 4;
    }
    [self setselect:level-1];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)segmentAction:(UISegmentedControl *)Seg{
    
    [self setselect:Seg.selectedSegmentIndex];
}

-(void)setxinhao{
    NSInteger selectIndex = self.segmentedView.selectedSegmentIndex;
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    @weakify(self);
    [MBProgressHUD showMessag:@"设置中..." toView:self.view];
    
    [[JServerClient assistant:-1 level:selectIndex+1 sn:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(id x) {
        if ([JGlobals shareGlobal].isAigoRouter && [JGlobals shareGlobal].validateRouter) {
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            [MBProgressHUD showSuccess:@"设置成功" toView:self.view];
            
            return ;
        }
        
        NSString* commadIdentifier = x[@"id"];
        commadIdentifier = [NSString stringWithFormat:@"%@",commadIdentifier];
        if ([commadIdentifier length] > 0) {
            self.searchCommad = 0;
            [self searchCommandStatue:[NSString stringWithFormat:@"%@",commadIdentifier]];
        }else{
            [MBProgressHUD showError:@"设置失败，稍后重试！" toView:self.view];
        }
    } error:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
    }];
}

-(void)commandSuccess{
    NSInteger selectIndex = self.segmentedView.selectedSegmentIndex;
    [JGlobals shareGlobal].routerInfo.signalLevel = @(selectIndex +1);
}


-(void)setselect:(NSInteger)seletedIndex{
    NSString* title = [NSString stringWithFormat:@"当前等级：%ld级 %@",seletedIndex +1,_keyArray[seletedIndex]];
    self.titleLabel.text = title;
    CGFloat x = 0.0;
    switch (seletedIndex) {
        case 0:
            x = 0;
            break;
        case 1:
            x = self.segmentedView.width / 2;
            break;
        case 2:
            x = self.segmentedView.width - 10;
            break;
            
        default:
            break;
    }
    [UIView animateWithDuration:0.2 animations:^{
        self.barIcon.left = x;
    }];
}

@end
