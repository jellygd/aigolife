//
//  JAssisTestViewController.m
//  tcp
//
//  Created by Jelly on 15/7/29.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JAssisTestViewController.h"
#import "LMGaugeView.h"
#import "JServerClient+JCommand.h"
#import "JServerClient+JRouter.h"
#import <Reachability.h>

@interface JAssisTestViewController ()

@property(nonatomic, assign) CGFloat timeDelta;

@property(nonatomic, strong) LMGaugeView* barView;

@property(nonatomic, assign) BOOL isStopValue;

@property(nonatomic, assign) CGFloat velocity;

@property(nonatomic, assign) CGFloat acceleration;


@property(nonatomic, assign) CGFloat limitValue;

@property(nonatomic, assign) BOOL isCycle;
@end



@implementation JAssisTestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"宽带网速测试";
    self.speedLabel.hidden  = YES;
    self.speedsLabel.hidden = YES;
    
    
    self.barView = [[LMGaugeView alloc] initWithFrame:CGRectMake(-50, 10, self.speedBgImageView.width+100, self.speedBgImageView.height+20)];
    self.barView.value = 0.0;
    self.barView.minValue = 0;
    self.barView.maxValue = 300;
    self.limitValue = 180;
    //    self.barView.ringBackgroundColor = [UIColor clearColor]
    self.barView.valueFont = [UIFont systemFontOfSize:50];
    [self.speedBgImageView addSubview:self.barView];
    
    self.acceleration = 10;
    self.timeDelta = 0.04;
    self.barView.unitOfMeasurement = @"KB/S";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)updateGaugeTimer:(NSTimer *)timer
{
    
    if (self.isStopValue) {
        
        return;
    }
    
    self.velocity += self.timeDelta * self.acceleration;
    self.barView.value = self.velocity;
    
    if (self.velocity > self.limitValue) {
        self.isCycle = YES;
        self.acceleration = - 5;
    }
    if (self.velocity < 100 && self.isCycle) {
        self.acceleration = 10;
    }
    
    
    [NSTimer scheduledTimerWithTimeInterval:self.timeDelta
                                     target:self
                                   selector:@selector(updateGaugeTimer:)
                                   userInfo:nil
                                    repeats:NO];
}

- (IBAction)testAction:(id)sender {
    
    UIButton* btn = sender;
    btn.enabled = NO;
    self.isCycle = NO;
    self.barView.value = 0.0;
    self.velocity = 0.0;
    self.isStopValue = NO;
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    @weakify(self);
    
    [[JServerClient speedTest:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(id x) {
        @strongify(self);
        [self queryTestSpeed];
    } error:^(NSError *error) {
        btn.enabled = YES;
        @strongify(self);
        self.isStopValue = YES;
        self.barView.value = 0;
        [MBProgressHUD showError:@"查询失败" toView:self.view];
    }];
    
    [NSTimer scheduledTimerWithTimeInterval:self.timeDelta
                                     target:self
                                   selector:@selector(updateGaugeTimer:)
                                   userInfo:nil
                                    repeats:NO];
    
    
    
}


/**
 *  查询获取速度
 */
-(void)queryTestSpeed{
//    //如果是连接智能路由 而且 内网断开
//    if( ![JGlobals shareGlobal].user && ![[Reachability reachabilityForInternetConnection] isConnectionOnDemand]){
//    
//        
//        return;
//    }
    
    
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    @weakify(self);
    [[JServerClient querySpeed:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(id x) {
  
        
        @strongify(self);
        if(self.velocity > 100){
            [MBProgressHUD showSuccess:@"查询成功" toView:self.view];
            self.barView.value = 0;
            NSDictionary* data = x[@"data"];
            NSString* upload = data[@"upload"];
            NSString* download = data[@"download"];
            self.testBtn.enabled = YES;
            self.isStopValue = YES;
            self.uploadSpeedLabel.text = [NSString stringWithFormat:@"%@ MB/S",upload];
            self.downSpeedLabel.text = [NSString stringWithFormat:@"%@ MB/S",download];
        }else{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [MBProgressHUD showSuccess:@"查询成功" toView:self.view];
                self.barView.value = 0;
                NSDictionary* data = x[@"data"];
                NSString* upload = data[@"upload"];
                NSString* download = data[@"download"];
                self.testBtn.enabled = YES;
                self.isStopValue = YES;
                self.uploadSpeedLabel.text = [NSString stringWithFormat:@"%@ MB/S",upload];
                self.downSpeedLabel.text = [NSString stringWithFormat:@"%@ MB/S",download];
            });
        }
    } error:^(NSError *error) {
        @strongify(self);
        self.isStopValue = YES;
        self.testBtn.enabled = YES;
        self.barView.value = 0;
        [MBProgressHUD showError:@"查询失败" toView:self.view];
    }];
}

@end
