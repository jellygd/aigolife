//
//  JAssistantViewController.m
//  tcp
//
//  Created by Jelly on 15/7/28.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JAssistantViewController.h"
#import "JAssistantTableViewCell.h"

@interface JAssistantViewController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation JAssistantViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.showBlur = YES;
    [self setHeaderBgView:self.headerVIew];
    [self.assistantTableView setTableFooterView:[[UIView alloc] init]];
    self.assistantTableView.delegate = self;
    self.assistantTableView.dataSource = self;
    self.title = @"上网助手";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 68.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    JAssistantTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JAssistantTableViewCell" forIndexPath:indexPath];
    NSString* name = @"";
    NSString* sub =@"";
    UIImage* image ;
    switch (indexPath.row) {
        case 0:
        {
            name = @"WiFi信道调节器";
            sub = @"网速慢或出现卡顿时，可调试信道设置";
            image = [UIImage imageNamed:@"icon_helper1"];
        }
            break;
        case 1:
        {
            name = @"WiFi信号强度调节器";
            sub = @"信号强度弱时，可调节强度";
            image = [UIImage imageNamed:@"icon_helper2"];
        }
            break;
        case 2:
        {
            name = @"宽带网速测试";
            sub = @"测试你当前的外网及内网网速";
            image = [UIImage imageNamed:@"icon_helper3"];
        }
            break;
            
        default:
            break;
    }
    
    cell.assistantNameLabel.text = name;
    cell.assistantSubLabel.text = sub;
    cell.assistantImageView.image = image;
    cell.assistantRightImageVIew.image = [UIImage imageNamed:@"next_step"];
    return cell;
}


-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.showCanBlur){
        [self showBlurView];
        return NO;
    }
    return YES;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.showCanBlur) {
        [self showBlurView];
        return;
    }
    switch (indexPath.row) {
        case 0:
        {
            [self performSegueWithIdentifier:@"channel" sender:indexPath];
        }
            break;
        case 1:
        {
            [self performSegueWithIdentifier:@"strong" sender:indexPath];
        }
            break;
        case 2:
        {
            [self performSegueWithIdentifier:@"test" sender:indexPath];
        }
            break;
            
        default:
            break;
    }
}

@end
