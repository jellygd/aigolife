//
//  JAssisStrongViewController.h
//  tcp
//
//  Created by Jelly on 15/7/29.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JBaseViewController.h"
#import "MMCircularProgressView.h"


@interface JAssisStrongViewController : JBaseViewController


@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedView;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;



@end
