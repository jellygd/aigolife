//
//  JAssistantViewController.h
//  tcp
//
//  Created by Jelly on 15/7/28.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JBaseViewController.h"

@interface JAssistantViewController : JBaseViewController
@property (weak, nonatomic) IBOutlet UIView *headerVIew;
@property (weak, nonatomic) IBOutlet UITableView *assistantTableView;

@end
