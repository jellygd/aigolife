//
//  JAssisChannelViewController.h
//  tcp
//
//  Created by Jelly on 15/7/29.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JBaseViewController.h"

@interface JAssisChannelViewController : JBaseViewController

@property (weak, nonatomic) IBOutlet UITableView *channelTableView;
@end
