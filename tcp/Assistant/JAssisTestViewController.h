//
//  JAssisTestViewController.h
//  tcp
//
//  Created by Jelly on 15/7/29.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JBaseViewController.h"

@interface JAssisTestViewController : JBaseViewController

@property (weak, nonatomic) IBOutlet UIImageView *speedBgImageView;


@property (weak, nonatomic) IBOutlet UILabel *speedLabel;

@property (weak, nonatomic) IBOutlet UILabel *speedsLabel;

@property (weak, nonatomic) IBOutlet UILabel *downSpeedLabel;

@property (weak, nonatomic) IBOutlet UILabel *uploadSpeedLabel;


@property (weak, nonatomic) IBOutlet UIButton *testBtn;

- (IBAction)testAction:(id)sender;


@end
