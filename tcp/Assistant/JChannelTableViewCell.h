//
//  JChannelTableViewCell.h
//  tcp
//
//  Created by Jelly on 15/7/29.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JChannelTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *channelName;
@property (weak, nonatomic) IBOutlet UIProgressView *channelProgress;
@property (weak, nonatomic) IBOutlet UIImageView *channelrightView;

@end
