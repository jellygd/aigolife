//
//  AFHTTPRequestOperationManager+Combin.m
//  eim
//
//  Created by yanghui on 14-6-17.
//  Copyright (c) 2014年 Forever Open-source Software Inc. All rights reserved.
//

#import "AFHTTPRequestOperationManager+Combin.h"


@implementation AFHTTPRequestOperationManager (Combin)

- (AFHTTPRequestOperation *)CombinationHTTPRequestOperationWithRequest:(NSURLRequest *)request
                                                               success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                                               failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    __weak id weakSelf = self;
    return [weakSelf HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success(operation,responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        [self handlingExceptionsWithEsError:operation];
        failure(operation,error);
    }];
}

- (AFHTTPRequestOperation *)CombinationPOST:(NSString *)URLString
                                 parameters:(NSDictionary *)parameters
                  constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
                                    success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                    failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    __weak id weakSelf = self;
    return [weakSelf POST:URLString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        block(formData);
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self handlingExceptionsWithEsError:operation];
        success(operation,responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failure(operation,error);
    }];
}

-(void)handlingExceptionsWithError:(NSDictionary*)error{


}

-(void)handlingExceptionsWithEsError:(AFHTTPRequestOperation *)operation
{

}


@end
