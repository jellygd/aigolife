//
//  JPackage.h
//  tcp
//
//  Created by dong on 15/10/15.
//  Copyright © 2015年 Jelly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JPackage : NSObject

/**
 *  增加不上传标示
 *
 *  @param URL 文件夹路径
 *
 *  @return 是否成功
 */
+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;

@end
