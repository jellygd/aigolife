//
//  MBProgressHUD+JMessage.h
//  tcp
//
//  Created by dong on 15/7/21.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "MBProgressHUD.h"

@interface MBProgressHUD (JMessage)


+ (void)showError:(NSString *)error toView:(UIView *)view;
+ (void)showSuccess:(NSString *)success toView:(UIView *)view;

+ (MBProgressHUD *)showMessag:(NSString *)message toView:(UIView *)view;


@end
