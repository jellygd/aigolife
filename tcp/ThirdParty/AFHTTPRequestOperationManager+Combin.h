//
//  AFHTTPRequestOperationManager+Combin.h
//  eim
//
//  Created by yanghui on 14-6-17.
//  Copyright (c) 2014年 Forever Open-source Software Inc. All rights reserved.
//

#import "AFHTTPRequestOperationManager.h"

@interface AFHTTPRequestOperationManager (Combin)

- (AFHTTPRequestOperation *)CombinationHTTPRequestOperationWithRequest:(NSURLRequest *)request
                                                               success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                                               failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

- (AFHTTPRequestOperation *)CombinationPOST:(NSString *)URLString
                      parameters:(NSDictionary *)parameters
       constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
                         success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                    failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)handlingExceptionsWithError:(NSDictionary*)error;
@end
