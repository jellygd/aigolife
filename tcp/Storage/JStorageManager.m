//
//  JStorageManager.m
//  tcp
//
//  Created by Jelly on 15/8/29.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JStorageManager.h"

static JStorageManager *kStorageManager = nil;

@interface JStorageManager ()

/**
 *  文件操作类型
 */
@property(nonatomic, assign) FileOption option;


/**
 *  当前操作的文件或者文件夹路径
 */
@property(nonatomic, strong) NSString* filePath;

@end

@implementation JStorageManager


+ (JStorageManager *)shareStorageManager{
    static dispatch_once_t globalToken;
    dispatch_once(&globalToken, ^{
        if (!kStorageManager) {
            kStorageManager=[[JStorageManager alloc] init];
            [[kStorageManager.storageOptionView.closeBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
                kStorageManager.filePath = @"";
                kStorageManager.option =0;
                [kStorageManager.storageOptionView removeFromSuperview];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNoficationStorageUpdataBar object:nil];
            }];
        }
    });
    return kStorageManager;
}



-(void)setFilePath:(NSString *)filePath fileName:(NSString*)fileName fileOption:(FileOption)option{
    
    self.filePath = filePath;
    self.option = option;
    self.storageOptionView.optionLabel.text = [NSString stringWithFormat:@"%@ :",self.option == FileOptionCopy ? @"复制" : @"剪切"];
    self.storageOptionView.fileNameLabel.text = fileName;
    [self.storageOptionView removeFromSuperview];
    if (self.filePath.length > 0) {
        UIWindow* keyWindow =  [UIApplication sharedApplication].keyWindow;
        [self.storageOptionView setTop:64.0];
        [keyWindow addSubview:self.storageOptionView];
    }else if(self.storageOptionView.superview){
        [self.storageOptionView removeFromSuperview];
    }
}


-(void)removeStorageOptionView{
    [self.storageOptionView removeFromSuperview];
}

-(void)showStorageOptionView{
    [self.storageOptionView removeFromSuperview];
    if (self.filePath.length > 0) {
        UIWindow* keyWindow =  [UIApplication sharedApplication].keyWindow;
        [self.storageOptionView setTop:64.0];
        [keyWindow addSubview:self.storageOptionView];
    }
}



-(JStorageOptionView*)storageOptionView{
    if (!_storageOptionView) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"JStorageOptionView" owner:self options:nil];
        _storageOptionView = [nib objectAtIndex:0];
         UIWindow* keyWindow =  [UIApplication sharedApplication].keyWindow;
        _storageOptionView.width = keyWindow.width;
    }
    return _storageOptionView;
}



@end
