//
//  JStorageViewController.h
//  tcp
//
//  Created by Jelly on 15/7/28.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JBaseViewController.h"
#import "JFileInfo.h"

@interface JStorageViewController : JBaseViewController

@property(nonatomic, strong) JFileInfo* selectFileInfo;
@property(nonatomic, strong) NSString* filePath;

@property (weak, nonatomic) IBOutlet UIView *storageHeaderView;
@property (weak, nonatomic) IBOutlet UITableView *storageTableView;
@property (weak, nonatomic) IBOutlet UIView *headerView;

-(void)cutFileToTheFile;

-(void)copyFileToTheFile;

@end
