//
//  JStorageDetailView.h
//  tcp
//
//  Created by Jelly on 15/12/1.
//  Copyright © 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JStorageDetailView : UIView

@property (weak, nonatomic) IBOutlet UILabel *fileNaleLabel;

@property (weak, nonatomic) IBOutlet UILabel *pathLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@end
