//
//  JMountDeskView.h
//  tcp
//
//  Created by Jelly on 15/12/7.
//  Copyright © 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CATextLayer+NumberJump.h"

@interface JMountDiskView : UIView

@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

@property (weak, nonatomic) IBOutlet UIImageView *restartImageVIew;


@property (weak, nonatomic) IBOutlet UILabel *restartProgressLabel;

@property(nonatomic, strong) CATextLayer* textLayer;


-(void)startMountAnimation:(BOOL)success;


-(void)stopMountAnimation;

@end
