//
//  JStorageProgressView.h
//  tcp
//
//  Created by dong on 15/10/13.
//  Copyright © 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FXBlurView.h>


@interface JStorageProgressView : UIView

@property(nonatomic, strong) FXBlurView* blurView;

@property (weak, nonatomic) IBOutlet UILabel *progressLabel;

@property (weak, nonatomic) IBOutlet UIImageView *progressImageView;


@property(nonatomic, strong) CATextLayer* textLayer;

@property (weak, nonatomic) IBOutlet UIButton *goback;

-(void)starLoadingAnimation;
-(void)stopLoadingAnimation;
@end
