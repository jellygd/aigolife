//
//  JStorageTableViewCell.m
//  tcp
//
//  Created by Jelly on 15/8/5.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JStorageTableViewCell.h"

@implementation JStorageTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



- (IBAction)showMoreAction:(id)sender {
    if (self.showMore ) {
        self.showMore();
    }
    
}
@end
