//
//  JMountDeskView.m
//  tcp
//
//  Created by Jelly on 15/12/7.
//  Copyright © 2015年 Jelly. All rights reserved.
//

#import "JMountDiskView.h"

@implementation JMountDiskView


-(void)awakeFromNib{
    self.textLayer = [[CATextLayer alloc] init];
    self.textLayer.string = @"0";
    self.textLayer.frame = CGRectMake(0,3, self.restartProgressLabel.width-15,self.restartProgressLabel.height);
    self.textLayer.fontSize = 12;
    self.textLayer.contentsScale = 2;
    [self.textLayer setAlignmentMode:kCAAlignmentCenter];
    self.textLayer.foregroundColor = [UIColor blackColor].CGColor;
    [self.restartProgressLabel.layer addSublayer:self.textLayer];
}


-(void)startMountAnimation:(BOOL)success{
    [self starLoadingAnimation];
    self.restartProgressLabel.text = @"     %";
    if (success) {
        [self.textLayer jumpNumberWithDuration:5 fromNumber:0  toNumber:100 ];
    }else{
        [self.textLayer jumpNumberWithDuration:120 fromNumber:0  toNumber:99 ];
    }
}


-(void)stopMountAnimation{

    [self stopLoadingAnimation];
}



#pragma mark  动画设置
/**
 *  开始loading 的动画
 */
-(void)starLoadingAnimation{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 ];
    rotationAnimation.duration = 1;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = MAXFLOAT;
    
    [self.restartImageVIew.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

/**
 *  关闭动画 并隐藏动画
 */
-(void)stopLoadingAnimation{
    [self.restartImageVIew.layer removeAllAnimations];
}
@end
