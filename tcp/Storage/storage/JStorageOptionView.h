//
//  JStrageIOptionView.h
//  tcp
//
//  Created by Eileen on 15/8/30.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JStorageOptionView : UIView

@property (weak, nonatomic) IBOutlet UILabel *optionLabel;

@property (weak, nonatomic) IBOutlet UILabel *fileNameLabel;

@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@end
