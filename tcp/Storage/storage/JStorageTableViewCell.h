//
//  JStorageTableViewCell.h
//  tcp
//
//  Created by Jelly on 15/8/5.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ShowStorageMoreBlock)();

@interface JStorageTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *storageImageView;
@property (weak, nonatomic) IBOutlet UILabel *storageNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *storageMoreBtn;
@property (nonatomic, copy) ShowStorageMoreBlock showMore;

- (IBAction)showMoreAction:(id)sender;

@end
