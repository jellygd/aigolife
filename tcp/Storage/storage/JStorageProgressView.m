//
//  JStorageProgressView.m
//  tcp
//
//  Created by dong on 15/10/13.
//  Copyright © 2015年 Jelly. All rights reserved.
//

#import "JStorageProgressView.h"

@implementation JStorageProgressView


-(void)awakeFromNib{
//    self.textLayer = [[CATextLayer alloc] init];
//    self.textLayer.string = @"0";
//    self.textLayer.frame = CGRectMake(0,3, self.progressLabel.width-15,self.progressLabel.height);
//    self.textLayer.fontSize = 12;
//    self.textLayer.contentsScale = 2;
//    [self.textLayer setAlignmentMode:kCAAlignmentCenter];
//    self.textLayer.foregroundColor = [UIColor blackColor].CGColor;
//    [self.progressLabel.layer addSublayer:self.textLayer];
}


#pragma mark  动画设置
/**
 *  开始loading 的动画
 */
-(void)starLoadingAnimation{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 ];
    rotationAnimation.duration = 1;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = MAXFLOAT;
    
    [self.progressImageView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

/**
 *  关闭动画 并隐藏动画
 */
-(void)stopLoadingAnimation{
    [self.progressImageView.layer removeAllAnimations];
}

@end
