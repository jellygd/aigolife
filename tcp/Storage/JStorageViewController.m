//
//  JStorageViewController.m
//  tcp
//
//  Created by Jelly on 15/7/28.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JStorageViewController.h"
#import "JServerClient+JRouter.h"
#import "JFileInfo.h"
#import "JStorageTableViewCell.h"
#import "UUPhotoActionSheet.h"
#import "UUPhoto-Macros.h"
#import "UUPhoto-Import.h"
#import "VCFloatingActionButton.h"
#import <JGActionSheet.h>
#import "JStorageManager.h"
#import "JServerClient+JCommand.h"
#import "JStorageProgressView.h"
#import <LGAlertView.h>
#import "JStorageDetailView.h"
#import "JDiskinfo.h"
#import "JBlurBaseView.h"
#import "YLProgressBar.h"
#import <Masonry.h>
#import "JMountDiskView.h"


typedef void(^DidFileOptionBlock)(BOOL success);

@interface JStorageViewController ()<UITableViewDelegate,UITableViewDataSource,floatMenuDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property(nonatomic, strong) UIButton* downloadBtn;

@property(nonatomic, strong) NSArray* storageArray;

@property (nonatomic, strong) UUPhotoActionSheet *sheet;

@property(nonatomic , strong) VCFloatingActionButton* menuButton;

@property(nonatomic, strong) NSArray* menuKey;

@property(nonatomic, strong) UIButton* pasteBtn;

@property(nonatomic, strong) JStorageDetailView* detailView;

@property(nonatomic, strong) UIImagePickerController* imagePickerController;

@property(nonatomic, strong) JStorageProgressView* progressView;


@property(nonatomic, strong) YLProgressBar* progressBar;

@property(nonatomic, strong) UIView* barIconView;

@property(nonatomic, strong) FXBlurView* emptyView;

@property(nonatomic, strong) FXBlurView* progressBaseView;


@property(nonatomic, strong) UILabel* sizeLabel;


@property(nonatomic, strong) UILabel* usedLabel;

@property(nonatomic, strong) JMountDiskView* mountDiskView;

@property(nonatomic, strong) NSTimer* timer;

@end

@implementation JStorageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (!self.selectFileInfo) {
        self.title = @"路由储存";
        UIImage* iconImage = [UIImage imageNamed:@"icon_top_processDownload"];
        self.downloadBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.downloadBtn setImage:iconImage forState:UIControlStateNormal];
        [self.downloadBtn sizeToFit];
        self.downloadBtn.hidden = YES;
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.downloadBtn];
    }else{
        self.title = self.selectFileInfo.fileName;
    }
    [self setHeaderBgView:self.storageHeaderView];
    if ([JGlobals shareGlobal].isAigoRouter && [JGlobals shareGlobal].validateRouter) {
        self.menuKey = @[@"复制",@"剪切",@"重命名",@"删除",@"详情",@"发送至手机"];
    }else{
        self.menuKey = @[@"复制",@"剪切",@"重命名",@"删除",@"详情"];
    }
    
    
    [self.storageTableView setTableFooterView:[[UIView alloc] init]];
    self.storageTableView.dataSource = self;
    self.storageTableView.delegate = self;
    self.storageTableView.hidden = YES;
    //判断是否已经开启了存储
    
    CGRect floatFrame = CGRectMake([UIScreen mainScreen].bounds.size.width - 44 - 20, [UIScreen mainScreen].bounds.size.height - 64 - 20, 44, 44);
    
    
    self.menuButton = [[VCFloatingActionButton alloc]initWithFrame:floatFrame normalImage:[UIImage imageNamed:@"icon_bottomMenuOpen"] andPressedImage:[UIImage imageNamed:@"icon_bottomMenuClose"] withScrollview:nil];
    self.menuButton.imageArray = @[@"icon_bottomMenuVideo",@"icon_bottomMenuPicture",@"icon_bottomMenuFolder"];
    self.menuButton.labelArray = @[@"上传手机视频",@"上传手机照片",@"创建文件夹"];
    self.menuButton.hideWhileScrolling = YES;
    self.menuButton.delegate = self;
    [self.view addSubview:self.menuButton];
    
    floatFrame.origin.x -= 44;
    floatFrame.size.width += 44;
    self.pasteBtn = [[UIButton alloc] initWithFrame:floatFrame];
    [self.pasteBtn setImage:[UIImage imageNamed:@"icon_bottomMenuPaste"] forState:UIControlStateNormal];
    [self.pasteBtn setTitle:[NSString stringWithFormat:@"粘贴"] forState:UIControlStateNormal];
    [self.pasteBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.pasteBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -44, 0, 44)];
    [self.pasteBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 44, 0, 0)];
    self.pasteBtn.hidden = YES;
    
    [self.view addSubview:self.pasteBtn];
    @weakify(self);
    [[self.pasteBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        if ([JStorageManager shareStorageManager].option == FileOptionCopy) {
            [self copyFileToTheFile];
        }else if([JStorageManager shareStorageManager].option == FileOptionCut){
            [self cutFileToTheFile];
        }
    }];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadStorageOptionBar) name:kNoficationStorageUpdataBar object:nil];
    
    [self loadDiskInfo];
    
    
    self.progressBar = [[YLProgressBar alloc] initWithFrame:CGRectMake(0, 30, self.view.width - 60, 1)];
    self.progressBar.height = 1;
    self.progressBar.backgroundColor = UIColorFromRGB(0x167dca);
    self.progressBar.progress = 0;
    _progressBar.type               = YLProgressBarTypeFlat;
    _progressBar.progressTintColors = @[UIColorFromRGB(0x00fcfd),UIColorFromRGB(0x43fcb8),UIColorFromRGB(0xadfc48),UIColorFromRGB(0xe6e608)];
    _progressBar.hideStripes        = YES;
    _progressBar.hideTrack          = YES;
    _progressBar.behavior           = YLProgressBarBehaviorDefault;
    [_progressBar setProgress:0];
    _progressBar.hidden = YES;
    _progressBar.layer.masksToBounds = YES;
    _progressBar.layer.cornerRadius = 1;
    
    self.barIconView = [[UIView alloc] initWithFrame:CGRectMake(0, -2.5, 10,10)];
    self.barIconView.backgroundColor = UIColorFromRGB(0xeae613);
    self.barIconView.layer.masksToBounds = YES;
    self.barIconView.layer.cornerRadius = 5;
    
    
    self.sizeLabel = [[UILabel alloc] init];
    self.sizeLabel.textColor = [UIColor whiteColor];
    self.sizeLabel.font = [UIFont systemFontOfSize:14];
    self.usedLabel = [[UILabel alloc] init];
    self.usedLabel.textColor = [UIColor whiteColor];
    self.usedLabel.font = [UIFont systemFontOfSize:19];
    
    self.sizeLabel.hidden = YES;
    self.usedLabel.hidden = YES;
    
    [self.view addSubview:self.progressBar];
    [self.view addSubview:self.barIconView];
    [self.view addSubview:self.sizeLabel];
    [self.view addSubview:self.usedLabel];
    
    [[self.downloadBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        [self UmmountDisk];
    }];
    
    if (self.filePath.length > 0) {
       [self.timer fire];
    }
}



-(void)updateViewConstraints{
    [super updateViewConstraints];
    [self.progressBar mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(30);
        make.right.equalTo(self.view).with.offset(-30);
        
        make.height.equalTo(@(5));
        make.bottom.equalTo(self.headerView).with.offset(-50);
    }];
    
    [self.usedLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.sizeLabel);
        make.right.equalTo(self.sizeLabel.mas_left);
    }];
    [self.sizeLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.progressBar);
        make.bottom.equalTo(self.headerView).with.offset(-20);
    }];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.timer invalidate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self loadStorageData];
    [self showPasteBtn];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    
}

-(void)loadDiskInfo{
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    @weakify(self);
    [[JServerClient getRouterDiskInfo:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(NSDictionary* x) {
        @strongify(self);
        NSDictionary* data =  x[@"data"];
        NSArray* diskinfo = data[@"diskinfo"];
        NSInteger havedisk =  [data[@"havedisk"] integerValue];
        if (havedisk == 0 || diskinfo.count <= 0 ) {
            [self showEmptyView];
        }else{
            NSDictionary*dictionary =  diskinfo.firstObject;
            if ([dictionary allKeys].count <= 0) {
                [self showEmptyView];
                return ;
            }
            
            NSArray* disks = [MTLJSONAdapter modelsOfClass:[JDiskinfo class] fromJSONArray:diskinfo error:nil];
            if (disks.count > 0) {
                [self showProgress:disks.firstObject];
            }else{
                [self showEmptyView];
            }
        }
    } error:^(NSError *error) {
        @strongify(self);
        [self showEmptyView];
    }];
    
}


#pragma mark  通知处理
-(void)reloadStorageOptionBar{
    [self showPasteBtn];
    
}

-(void)showPasteBtn{
    if(self.filePath.length <= 0){
        self.menuButton.hidden = YES;
        self.pasteBtn.hidden = YES;
        return;
    }
    
    if ([JStorageManager shareStorageManager].filePath &&[JStorageManager shareStorageManager].filePath.length  > 0) {
        self.pasteBtn.hidden = NO;
    }else{
        self.pasteBtn.hidden = YES;
    }
    self.menuButton.hidden = !self.pasteBtn.hidden;
 
    [self.storageTableView reloadData];
}

-(void)hiddenMenu{
    self.menuButton.hidden = YES;
}

#pragma mark  加载数据

-(void)loadStorageData{
    NSString* identifier = [JGlobals shareGlobal].serverMAC;
    NSString* token = [JGlobals shareGlobal].serverToken;
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    @weakify(self);
    [MBProgressHUD showMessag:@"查询中..." toView:self.view];
    [[JServerClient queryRouterFilesInfo:identifier?identifier:@"" token:token?token:@"" appver:version path:self.filePath.length > 0 ? self.filePath : @"/mnt" start:0 count:0] subscribeNext:^(id x) {
        @strongify(self);
        NSArray* data = x[@"data"];
        if ([data count] > 0) {
            NSArray* fileInfoArray =  [MTLJSONAdapter modelsOfClass:JFileInfo.class fromJSONArray:data error:nil];
            [self.storageTableView reloadData];
            
            self.storageArray = [fileInfoArray rx_filterWithBlock:^BOOL(JFileInfo* each) {
                return each.fileName.length > 0;
            }];
            
            [self.storageTableView reloadData];
        }else{
            if (self.filePath.length <= 0 ) {
                
            }
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } error:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
    }];
}


/**
 *  显示空View
 */
-(void)showEmptyView{
    self.storageTableView.hidden = YES;
    [MBProgressHUD hideHUDForView:self.view animated:NO];
    if (![self.emptyView superview]) {
        [[UIApplication sharedApplication].keyWindow addSubview:self.emptyView];
    }
    self.downloadBtn.hidden = YES;
    
    self.progressBar.hidden = YES;
    self.sizeLabel.hidden = YES;
    self.usedLabel.hidden = YES;
    
}




-(void)UmmountDisk{
    @weakify(self);
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"卸载磁盘" message:@"您确定要卸载当前磁盘吗?" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alertView show];
    [[alertView rac_buttonClickedSignal] subscribeNext:^(id x) {
        if ([x integerValue] == 1) {
            @strongify(self);
            [self optionFile:@"" newFilePath:@"" option:FileOptionUnmount optionBlock:^(BOOL success) {
                
            }];
        }
    }];
    
}

-(void)didMountDesk{
    @weakify(self);
    [self optionFile:@"" newFilePath:@"" option:FileOptionMount optionBlock:^(BOOL success) {
        @strongify(self);
        [self loadDiskInfo];
        
        if (success) {
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            [self.mountDiskView startMountAnimation:YES];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.mountDiskView stopMountAnimation];
                [self.mountDiskView removeFromSuperview];
            });
        }else{
            [self.mountDiskView stopMountAnimation];
            [self.mountDiskView removeFromSuperview];
        }
    }];
    //    [[UIApplication sharedApplication].keyWindow addSubview:self.mountDiskView];
    //    [MBProgressHUD hideHUDForView:self.view animated:NO];
    [self.mountDiskView startMountAnimation:NO];
}


-(void)showProgress:(JDiskinfo*)diskInfo{
    self.storageTableView.hidden = NO;
    self.downloadBtn.hidden = NO;
    CGFloat progress = [diskInfo.used floatValue] / [diskInfo.size floatValue];
    
    self.progressBar.hidden = NO;
    [self.progressBar setProgress:progress animated:NO];
    self.barIconView.left = self.progressBar.width * progress + self.progressBar.left;
    self.barIconView.top = self.progressBar.top - self.barIconView.height / 2+2.5;
    
    self.sizeLabel.hidden = NO;
    self.usedLabel.hidden = NO;
    
    self.sizeLabel.text = [self spedString:diskInfo.size];
    self.usedLabel.text = [NSString stringWithFormat:@"%@/",[self spedString:diskInfo.used]];
    
    //    [self.sizeLabel sizeToFit];
    //    [self.usedLabel sizeToFit];
    
    [self.view setNeedsUpdateConstraints];
}

#pragma mark - uiscroll delegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(!self.filePath.length <= 0){
        [self.timer fire];
    }
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if(!self.filePath.length <= 0){
        if (self.menuButton.hidden) {
            self.menuButton.hidden = NO;
        }
        [self.timer invalidate];
    }
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.storageArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 48.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    JStorageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JStorageTableViewCell" forIndexPath:indexPath];
    
    JFileInfo* fileInfo = self.storageArray[indexPath.row];
    NSString* fileType =  [fileInfo.isFile boolValue] ? @"icon_file" : @"icon_folder";
    cell.storageImageView.image = [UIImage imageNamed:fileType];
    cell.storageNameLabel.text = fileInfo.fileName;
    NSString* filePath = [NSString stringWithFormat:@"%@/%@",(self.filePath.length > 0 ? self.filePath : @"/mnt"),fileInfo.fileName];
    if ([[JStorageManager shareStorageManager].filePath isEqualToString:filePath]) {
        cell.storageNameLabel.textColor = [UIColor grayColor];
        if (![fileInfo.isFile boolValue]) {
            cell.storageImageView.image = [UIImage imageNamed:@"ico_folder_"];
        }
    }else{
        cell.storageNameLabel.textColor = [UIColor blackColor];
        if (![fileInfo.isFile boolValue]) {
            cell.storageImageView.image = [UIImage imageNamed:@"icon_folder"];
        }
    }
    @weakify(self);
    cell.showMore = ^(){
        @strongify(self);
        [self showMoreMenuView:fileInfo];
    };
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    JFileInfo* fileInfo = [self.storageArray objectAtIndexedSubscript:indexPath.row];
    if (![fileInfo.isFile boolValue]) {
        
        NSString* fileAllPath = [NSString stringWithFormat:@"%@/%@",(self.filePath.length > 0 ? self.filePath : @"/mnt"),fileInfo.fileName];
        if ([[JStorageManager shareStorageManager].filePath isEqualToString:fileAllPath]) {
            return;
        }
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"StorageStoryboard" bundle:[NSBundle mainBundle]];
        //由storyboard根据myView的storyBoardID来获取我们要切换的视图
        JStorageViewController *storageViewController = [story instantiateViewControllerWithIdentifier:@"StorageIdentifier"];
        storageViewController.selectFileInfo = fileInfo;
        NSString* filePath = [NSString stringWithFormat:@"%@/%@",(self.filePath.length > 0 ? self.filePath : @"/mnt"),fileInfo.fileName];
        storageViewController.filePath = filePath;
        [self.navigationController pushViewController:storageViewController animated:YES];
    }
}


-(void)showMoreMenuView:(JFileInfo*)fileInfo{
    
    if ([JGlobals shareGlobal].isAigoRouter && [JGlobals shareGlobal].validateRouter) {
        self.menuKey = @[@"复制",@"剪切",@"重命名",@"删除",@"详情"];
    }else{
        self.menuKey = @[@"复制",@"剪切",@"重命名",@"删除",@"详情"];
    }
    
    
    JGActionSheetSection *section1 = [JGActionSheetSection sectionWithTitle:@"" message:@"" buttonTitles:self.menuKey buttonStyle:JGActionSheetButtonStyleDefault];
    JGActionSheetSection *cancelSection = [JGActionSheetSection sectionWithTitle:nil message:nil buttonTitles:@[@"取消"] buttonStyle:JGActionSheetButtonStyleCancel];
    
    NSArray *sections = @[section1, cancelSection];
    
    JGActionSheet *sheet = [JGActionSheet actionSheetWithSections:sections];
    
    [sheet setButtonPressedBlock:^(JGActionSheet *sheet, NSIndexPath *indexPath) {
        [sheet dismissAnimated:YES];
        if(indexPath.section == 1){
            return ;
        }
        
        switch (indexPath.row) {
            case 0:
                [self copyFile:fileInfo];
                break;
            case 1:
                [self cutFile:fileInfo];
                break;
            case 2:
                [self reNameFile:fileInfo];
                break;
            case 3:
                [self deleteFile:fileInfo];
                break;
            case 4:
                [self detailFile:fileInfo];
                break;
            case 5:
                if (fileInfo.isFile) {
                    [self downFile:fileInfo];
                }
                break;
                
            default:
                break;
        }
        
    }];
    
    [sheet showInView:self.view animated:YES];
}



-(void) didSelectMenuOptionAtIndex:(NSInteger)row
{
    if (row == 1) {
        [self showImagePick];
    }else if (row == 0){
        [self showImagePickVedio];
    }else if (row == 2){
        [self addFold];
    }
}



-(void)showImagePickVedio{
    ALAuthorizationStatus author = [ALAssetsLibrary authorizationStatus];
    
    if(author == ALAuthorizationStatusRestricted || author == ALAuthorizationStatusDenied){
        
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"没有使用相册的权限" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.videoQuality = UIImagePickerControllerQualityTypeHigh;
    picker.mediaTypes = [NSArray arrayWithObjects:@"public.movie", nil];
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:nil];
}

-(void)showImagePick{
    ALAuthorizationStatus author = [ALAssetsLibrary authorizationStatus];
    
    if(author == ALAuthorizationStatusRestricted || author == ALAuthorizationStatusDenied){
        
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"没有使用相册的权限" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:nil];
}



#pragma mark 更多操作

/**
 *  复制文件
 *
 *  @param fileInfo 文件信息
 */
-(void)copyFile:(JFileInfo*)fileInfo{
    NSString* filePath = [NSString stringWithFormat:@"%@/%@",(self.filePath.length > 0 ? self.filePath : @"/mnt"),fileInfo.fileName];
    [[JStorageManager shareStorageManager] setFilePath:filePath fileName:fileInfo.fileName fileOption:FileOptionCopy];
    [self showPasteBtn];
    [self.storageTableView reloadData];
}

/**
 *  剪切文件
 *
 *  @param fileInfo 文件信息
 */
-(void)cutFile:(JFileInfo*)fileInfo{
    NSString* filePath = [NSString stringWithFormat:@"%@/%@",(self.filePath.length > 0 ? self.filePath : @"/mnt"),fileInfo.fileName];
    [[JStorageManager shareStorageManager] setFilePath:filePath fileName:fileInfo.fileName fileOption:FileOptionCut];
    
    [self showPasteBtn];
    [self.storageTableView reloadData];
}

/**
 *  重命名
 *
 *  @param fileInfo 文件信息
 */
-(void)reNameFile:(JFileInfo*)fileInfo{
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = NO;
    
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"请输入新的名称" message:@"" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
    [alertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [[alertView textFieldAtIndex:0] setPlaceholder:@"请输入新的名称"];
    [[alertView textFieldAtIndex:0] setText:fileInfo.fileName];
    [alertView show];
    NSString* filePath = [NSString stringWithFormat:@"%@/%@",(self.filePath.length > 0 ? self.filePath : @""),fileInfo.fileName];
    @weakify(self);
    [[alertView rac_buttonClickedSignal] subscribeNext:^(id x) {
        manager.enable = YES;
        if([x integerValue] ==  0){
            @strongify(self);
            NSString* textValue = [alertView textFieldAtIndex:0].text;
            if (textValue.length > 0) {
                
                NSString* newFilePath = [NSString stringWithFormat:@"%@/%@",(self.filePath.length > 0 ? self.filePath : @""),textValue];
                [self optionFile:filePath newFilePath:newFilePath option:FileOptionEdit optionBlock:^(BOOL success) {}];
                
            }else{
                [[[UIAlertView alloc] initWithTitle:@"" message:@"请输入有效的文件名称" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] show];
            }
        }
    }];
}


-(void)addFold{
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = NO;
    
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"请输入文件夹名称" message:@"" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
    [alertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [[alertView textFieldAtIndex:0] setPlaceholder:@"请输入文件夹名称"];
    [alertView show];
    @weakify(self);
    [[alertView rac_buttonClickedSignal] subscribeNext:^(id x) {
        manager.enable = YES;
        if([x integerValue] ==  0){
            @strongify(self);
            NSString* textValue = [alertView textFieldAtIndex:0].text;
            if (textValue.length > 0) {
                
                NSString* newFilePath = [NSString stringWithFormat:@"%@/%@",(self.filePath.length > 0 ? self.filePath : @""),textValue];
                [self optionFile:nil  newFilePath:newFilePath option:FileOptionAdd optionBlock:^(BOOL success) {}];
                
            }else{
                [[[UIAlertView alloc] initWithTitle:@"" message:@"请输入有效的文件名称" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] show];
            }
        }
    }];
}

/**
 *  删除文件
 *
 *  @param fileInfo 文件信息
 */
-(void)deleteFile:(JFileInfo*)fileInfo{
    UIAlertView* alertView = [[UIAlertView alloc]  initWithTitle:@"删除" message:[NSString stringWithFormat:@"您确定要删除“%@”吗？",fileInfo.fileName] delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
    [alertView show];
    @weakify(self);
    [[alertView rac_buttonClickedSignal] subscribeNext:^(id x) {
        if ([x integerValue] == 0 ) {
            @strongify(self);
            NSString* filePath = [NSString stringWithFormat:@"%@/%@",(self.filePath.length > 0 ? self.filePath : @""),fileInfo.fileName];
            [self optionFile:filePath newFilePath:filePath option:FileOptionDelete optionBlock:^(BOOL success) {}];
        }
    }];
}

/**
 *  文件详情
 *
 *  @param fileInfo 文件信息
 */
-(void)detailFile:(JFileInfo*)fileInfo{
    self.detailView = nil;
    self.detailView = [[NSBundle mainBundle] loadNibNamed:@"JStorageDetailView" owner:self options:nil].firstObject;
    
    
    NSString* filePath = self.filePath.length > 0 ? self.filePath : @"/mnt";
    
    NSString* fileName = fileInfo.fileName;
    if (fileName.length > 18) {
        fileName = [fileName substringToIndex:18];
        fileName = [fileName stringByAppendingString:@"..."];
    }
    self.detailView.fileNaleLabel.text = [NSString stringWithFormat:@"文件名称:%@",fileName];
    self.detailView.pathLabel.text = [NSString stringWithFormat:@"路径:%@",filePath];
    if (self.filePath.length > 0) {
        if(fileInfo.modifyTime){
            NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
            [dateFormatter setDateFormat:@"yyyy.MM.dd HH:mm"];
            
            
            self.detailView.timeLabel.text = [NSString stringWithFormat:@"修改时间:%@",[dateFormatter stringFromDate:fileInfo.modifyTime]];
        }else{
            self.detailView.timeLabel.text = @"";
        }
    }else{
        self.detailView.timeLabel.text = @"";
    }
    LGAlertView* alertView = [[LGAlertView alloc] initWithViewStyleWithTitle:@"" message:@"详情" view:self.detailView buttonTitles:nil cancelButtonTitle:@"确定" destructiveButtonTitle:nil];
    [alertView showAnimated:YES completionHandler:nil];
}


-(NSString*)spedString:(NSString*)speedN{
    CGFloat speed = [speedN floatValue];
    if (speed >= 1024*1024) {
        CGFloat mSpeed = speed/ (1024*1024);
        return [NSString stringWithFormat:@"%.2f G",mSpeed];
    }else
        if (speed >= 1024) {
            CGFloat mSpeed = speed/ (1024);
            return [NSString stringWithFormat:@"%.2f M",mSpeed];
        }else if(speed == 0 ){
            return @"0 kB";
        }else{
            CGFloat mSpeed = speed ;
            if(mSpeed < 1){
                return [NSString stringWithFormat:@"%.2f B",speed];
            }
            return [NSString stringWithFormat:@"%.2f KB",mSpeed];
        }
}

/**
 *  下载文件
 *
 *  @param fileInfo  文件信息
 */
-(void)downFile:(JFileInfo*)fileInfo{
    [[JServerClient downFile:fileInfo.fileName token:[JGlobals shareGlobal].serverToken] subscribeNext:^(id x) {
        
    } error:^(NSError *error) {
        
    } completed:^{
        
    }];
}

/**
 *  复制文件到这个文件夹下
 */
-(void)copyFileToTheFile{
    NSString* filePath = self.filePath.length > 0 ? self.filePath : @"/mnt";
    NSString* oldFilePath =  [JStorageManager shareStorageManager].filePath;
    if (oldFilePath.length > 0) {
        if ([filePath isEqualToString:oldFilePath]) {
            [MBProgressHUD showError:@"不能复制到同一个目录" toView:self.view];
            return;
        }
        [self optionFile:oldFilePath newFilePath:filePath option:FileOptionCopy optionBlock:nil];
    }else{
        [MBProgressHUD showError:@"操作失败，稍后重试！" toView:self.view];
    }
}

/**
 *  剪切文件到这个文件夹下
 */
-(void)cutFileToTheFile{
    NSString* filePath = self.filePath.length > 0 ? self.filePath : @"/mnt";
    NSString* oldFilePath =  [JStorageManager shareStorageManager].filePath;
    if (oldFilePath.length > 0) {
        if ([filePath isEqualToString:oldFilePath]) {
            [MBProgressHUD showError:@"不能剪切到同一个目录" toView:self.view];
            return;
        }
        [self optionFile:oldFilePath newFilePath:filePath option:FileOptionCut optionBlock:nil];
    }else{
        [MBProgressHUD showError:@"操作失败，稍后重试！" toView:self.view];
    }
}


-(void)optionFile:(NSString*)oldFilePath newFilePath:(NSString*)newFilePath option:(FileOption)option optionBlock:(DidFileOptionBlock)block{
    NSString* fileOption = @"";
    switch (option) {
        case FileOptionDelete:
            fileOption = @"del";
            break;
        case FileOptionEdit:
            fileOption = @"rename";
            break;
        case FileOptionCopy:
            fileOption = @"copy";
            break;
        case FileOptionCut:
            fileOption = @"move";
            break;
        case FileOptionUnmount:
            fileOption = @"unmount";
            break;
        case FileOptionMount:
            fileOption = @"mount";
            break;
        case FileOptionAdd:
            fileOption = @"add";
            break;
        default:
            fileOption = @"";
            break;
    }
    
    if (fileOption.length > 0) {
        NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
        @weakify(self);
        [MBProgressHUD showMessag:@"操作中..." toView:self.view];
        
        [[JServerClient optionFile:[JGlobals shareGlobal].serverMAC oldFilePath:oldFilePath newFilePath:newFilePath token:[JGlobals shareGlobal].serverToken appver:version option:fileOption] subscribeNext:^(id x) {
            @strongify(self);
            if ([JGlobals shareGlobal].isAigoRouter && [JGlobals shareGlobal].validateRouter) {
                if (block) {
                    block(YES);
                }
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                [self loadStorageData];
                if(option != FileOptionCopy){
                    [[JStorageManager shareStorageManager] setFilePath:@"" fileName:@""  fileOption:FileOptionNone];
                }
                if (option == FileOptionUnmount) {
                    [MBProgressHUD showSuccess:@"卸载成功" toView:self.view];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self.navigationController popToRootViewControllerAnimated:YES];
                    });
                }
                [self showPasteBtn];
                return ;
            }
        } error:^(NSError *error) {
            if (block) {
                block(NO);
            }
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            [MBProgressHUD showError:@"操作失败，稍后重试！" toView:self.view];
        }];
    }else{
        if (block) {
            block(NO);
        }
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:@"操作失败，稍后重试！" toView:self.view];
    }
    
}


-(void)commandSuccess{
    [self loadStorageData];
    [MBProgressHUD hideHUDForView:self.view animated:NO];
    [[JStorageManager shareStorageManager] setFilePath:@"" fileName:@""  fileOption:FileOptionNone];
    [self showPasteBtn];
}




- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    [picker dismissViewControllerAnimated:YES completion:nil];
    NSString* mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:@"public.image"]){
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        NSData* data = UIImageJPEGRepresentation(image, 1.0);
        
        ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *myasset)
        
        {
            
            ALAssetRepresentation *representation = [myasset defaultRepresentation];
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            NSString *fileName = [representation filename];
            [self fileUpload:data fileName:fileName];
        };
        ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
        NSURL *imageURL = [info valueForKey:UIImagePickerControllerReferenceURL];
        [assetslibrary assetForURL:imageURL
         
                       resultBlock:resultblock
         
                      failureBlock:nil];
    }else if ([mediaType isEqualToString:@"public.movie"]){
        NSURL* mediaURL = [info objectForKey:UIImagePickerControllerMediaURL];
        
        NSString* st1la =  mediaURL.lastPathComponent;
        
        NSData *webData = [NSData dataWithContentsOfURL:mediaURL];
        st1la = [st1la stringByReplacingOccurrencesOfString:@"trim." withString:@""];
        [self fileUpload:webData fileName:st1la];
    }
    
    
}


-(void)fileUpload:(NSData*)fileData fileName:(NSString*)fileName{
    [[JServerClient uploadFile:fileData token:[JGlobals shareGlobal].serverToken fileName:fileName destination:self.filePath] subscribeNext:^(id x) {
        self.progressView.progressLabel.text = [NSString stringWithFormat:@"%.f %%",[x floatValue]*100];
        
        if (![self.progressBaseView superview]) {
            [self.progressView starLoadingAnimation];
            [[UIApplication sharedApplication].keyWindow addSubview:self.progressBaseView];
        }
    }error:^(NSError *error) {
        NSLog(@"上传失败");
        if ([self.progressBaseView superview]) {
            [self.progressBaseView removeFromSuperview];
        }
        [self.progressView stopLoadingAnimation];
        
        [MBProgressHUD showError:@"上传失败" toView:self.view];
    }completed:^{
        
        [self.progressView stopLoadingAnimation];
        if ([self.progressBaseView superview]) {
            [self.progressBaseView removeFromSuperview];
        }
        [MBProgressHUD showSuccess:@"上传成功" toView:self.view];
    }];
}



-(JStorageProgressView*)progressView{
    if(!_progressView){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"JStorageProgressView" owner:self options:nil];
        _progressView = [nib objectAtIndex:0];
        @weakify(self);
        [[_progressView.goback rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
            @strongify(self);
            [self.progressBaseView removeFromSuperview];
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }
    return _progressView;
}

-(FXBlurView*)progressBaseView{
    if (!_progressBaseView) {
        _progressBaseView = [[FXBlurView alloc] initWithFrame:[UIApplication sharedApplication].keyWindow.bounds];
        [_progressBaseView setDynamic:NO];
        _progressBaseView.blurEnabled = YES;
        [_progressBaseView setBlurRadius:0];
        [_progressBaseView setIterations:0];
        _progressBaseView.tintColor = [UIColor blackColor];
        UIView* blackView = [[UIView alloc] initWithFrame:_progressBaseView.bounds];
        blackView.backgroundColor = [UIColor blackColor];
        blackView.alpha = 0.7;
        [_progressBaseView addSubview:blackView];
        
        self.progressView.frame = _progressBaseView.bounds;
        [_progressBaseView addSubview:self.progressView];
        
    }
    
    return _progressBaseView;
}


-(FXBlurView*)emptyView{
    if (!_emptyView) {
        _emptyView = [[FXBlurView alloc] initWithFrame:[UIApplication sharedApplication].keyWindow.bounds];
        [_emptyView setDynamic:NO];
        _emptyView.blurEnabled = YES;
        [_emptyView setBlurRadius:0];
        [_emptyView setIterations:0];
        _emptyView.tintColor = [UIColor blackColor];
        UIView* blackView = [[UIView alloc] initWithFrame:_emptyView.bounds];
        blackView.backgroundColor = [UIColor blackColor];
        blackView.alpha = 0.7;
        [_emptyView addSubview:blackView];
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"JStorageEmpty" owner:self options:nil];
        JBlurBaseView* blurBaseView = nib.firstObject;
        blurBaseView.frame = _emptyView.bounds;
        [_emptyView addSubview:blurBaseView];
        @weakify(self);
        [[blurBaseView.backBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
            @strongify(self);
            [self.emptyView removeFromSuperview];
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [[blurBaseView.settingBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
            @strongify(self);
            [self.emptyView removeFromSuperview];
            
            [self didMountDesk];
        }];
    }
    
    return _emptyView;
}


-(JMountDiskView*)mountDiskView{
    if (!_mountDiskView) {
        _mountDiskView = [[NSBundle mainBundle] loadNibNamed:@"JMountDiskView" owner:self options:nil].firstObject;
        _mountDiskView.width = self.view.bounds.size.width;
        _mountDiskView.height = self.view.bounds.size.height;
        @weakify(self);
        [[_mountDiskView.closeBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
            @strongify(self);
            [self.mountDiskView removeFromSuperview];
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }
    
    return _mountDiskView;
}

-(NSTimer*)timer{
//    if (!_timer) {
//        _timer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(hiddenMenu) userInfo:nil repeats:NO];
//    }
    
    return _timer;
}

@end
