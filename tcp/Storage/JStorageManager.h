//
//  JStorageManager.h
//  tcp
//
//  Created by Jelly on 15/8/29.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JStorageOptionView.h"


#define kNoficationStorageUpdataBar @"kNoficationStorageUpdataBar"

typedef NS_OPTIONS(NSInteger,FileOption){
    FileOptionNone = 0,
    FileOptionDelete = 1 << 0,
    FileOptionCopy = 1 << 1,
    FileOptionEdit = 1 << 2,
    FileOptionSearch = 1<< 3,
    FileOptionCut = 1<< 4,
    
    FileOptionUnmount  = 1<< 5,
    FileOptionMount  = 1<< 6,
    FileOptionAdd  = 1<< 7,
};


@interface JStorageManager : NSObject



/**
 *  单例
 *
 *  @return JStorageManager
 */
+ (JStorageManager *)shareStorageManager;

/**
 *  文件操作类型
 */
@property(nonatomic, assign, readonly) FileOption option;


/**
 *  当前操作的文件或者文件夹路径
 */
@property(nonatomic, strong, readonly) NSString* filePath;

-(void)setFilePath:(NSString *)filePath fileName:(NSString*)fileName fileOption:(FileOption)option;

@property(nonatomic, strong) JStorageOptionView* storageOptionView;

-(void)removeStorageOptionView;
-(void)showStorageOptionView;

@end
