//
//  UIViewController+JShake.h
//  tcp
//
//  Created by Jelly on 15/8/8.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <objc/runtime.h>

@interface UIViewController (JShake)

@property(nonatomic,assign) BOOL viewDisableShake;



-(void)setAccount;

-(void)restartRouter;


-(void)wifiSettings;
@end
