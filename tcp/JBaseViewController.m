//
//  JBaseViewController.m
//  tcp
//
//  Created by Jelly on 15/7/21.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JBaseViewController.h"
#import "JServerClient+JRouter.h"
#import "JServerClient+JCommand.h"
#import "JRouterInfo.h"
#import "JBlurBaseView.h"
#import <Reachability.h>


@interface JBaseViewController ()

@end

@implementation JBaseViewController


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.layer.masksToBounds = YES;
    if ([self.navigationController.childViewControllers count] >= 2) {
        
        UIBarButtonItem* leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStyleBordered target:self action:@selector(backAction)];
        self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    }
    
    
    //TODO:缺少一个全部网络失败的接口
}


-(void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(void)setHeaderBgView:(UIView*)view{
    CGRect frame = view.bounds;
    frame.origin.y = 20;
    frame.size.height -= 20;
    frame.size.width +=130;
    frame.origin.x -=30;
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = frame;
    gradient.colors = [NSArray arrayWithObjects:(id)UIColorFromRGB(0x46aefa).CGColor,
                       (id)UIColorFromRGB(0x1d7fc9).CGColor,nil];
    [view.layer insertSublayer:gradient atIndex:0];
}

-(void)dreawLine:(UIView*)imageView color:(UIColor*)color{
    CAShapeLayer *dotteShapeLayer = [CAShapeLayer layer];
    CGMutablePathRef dotteShapePath =  CGPathCreateMutable();
    [dotteShapeLayer setFillColor:[[UIColor clearColor] CGColor]];
    [dotteShapeLayer setStrokeColor:[color CGColor]];
    dotteShapeLayer.lineWidth = 1.0f ;
    NSArray *dotteShapeArr = [[NSArray alloc] initWithObjects:[NSNumber numberWithInt:2],[NSNumber numberWithInt:1], nil];
    [dotteShapeLayer setLineDashPattern:dotteShapeArr];
    
    CGPathMoveToPoint(dotteShapePath, NULL, 10,1);
    CGPathAddLineToPoint(dotteShapePath, NULL, imageView.width-20, 1);
    [dotteShapeLayer setPath:dotteShapePath];
    CGPathRelease(dotteShapePath);
    [imageView.layer addSublayer:dotteShapeLayer];
}


-(void)dreawEllipseLine:(UIView*)view color:(UIColor*)color{
    
    CAShapeLayer *solidLine =  [CAShapeLayer layer];
    CGMutablePathRef solidPath =  CGPathCreateMutable();
    solidLine.lineWidth = 1.0f ;
    solidLine.strokeColor = color.CGColor;
    solidLine.fillColor = [UIColor clearColor].CGColor;
    
    CGPathMoveToPoint(solidPath, NULL, 0, 1);
    CGPathAddLineToPoint(solidPath, NULL, view.width, 1);
    
    //    CGPathAddEllipseInRect(solidPath, nil, CGRectMake(0,  0, view.width, view.height));
    solidLine.path = solidPath;
    CGPathRelease(solidPath);
    [view.layer addSublayer:solidLine];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self becomeFirstResponder];
    [self showBlurView];
    
}
- (void) viewWillAppear:(BOOL)animated
{
    [self resignFirstResponder];
    
    [super viewWillAppear:animated];
}

-(void)showErrorMessage:(NSString*)message{
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
    [alertView show];

}


-(void)showRouterPWDAlertView{
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = NO;
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"管理员密码" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
    alertView.alertViewStyle = UIAlertViewStyleSecureTextInput;
    UITextField* pwdTextField = [alertView textFieldAtIndex:0];
    pwdTextField.placeholder = @"请输入密码";
    pwdTextField.clearButtonMode = UITextFieldViewModeAlways;
    NSString* adminPwd = [[NSUserDefaults standardUserDefaults] objectForKey:@"adminPwd"];
    if (adminPwd.length > 0) {
        pwdTextField.text = adminPwd;
    }
    [pwdTextField becomeFirstResponder];
    @weakify(self);
    [[alertView rac_buttonClickedSignal] subscribeNext:^(id x) {
        [pwdTextField resignFirstResponder];
        if ([x integerValue] == 0) {
            @strongify(self);
            UITextField* pwdTextField = [alertView textFieldAtIndex:0];
            if(pwdTextField.text.length > 0){
                [self validateRouter:pwdTextField.text];
            }else{
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    UIAlertView* tipsAlertView = [[UIAlertView alloc] initWithTitle:@"" message:@"请输入" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                    [tipsAlertView show];
                    [[tipsAlertView rac_buttonClickedSignal] subscribeNext:^(id x) {
                        if ([x integerValue] == 1) {
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                 [self showRouterPWDAlertView];
                            });
                        }
                    }];
                });
                
                
            }
        }else{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if (self.validateRouterBlock) {
                    self.validateRouterBlock(nil);
                }
            });
        }
        
        IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
        manager.enable = YES;
    }];
    [alertView show];
}


-(void)validateRouter:(NSString*)pwd{
    
    @weakify(self);
    [[JServerClient loginRouter:pwd] subscribeNext:^(id x) {
        @strongify(self);
        NSString*token = x[@"result"][@"info"][@"token"];
        if ([token length] > 0) {
            [JGlobals shareGlobal].routerToken = token;
            [JGlobals shareGlobal].adminPWd = pwd;
            [[NSUserDefaults standardUserDefaults] setObject:pwd forKey:@"adminPwd"];
            if (self.validateRouterBlock) {
                self.validateRouterBlock(@(YES));
            }
        }else{
            UIAlertView* alertView =  [[UIAlertView alloc] initWithTitle:@"" message:@"密码错误请重新输入" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] ;
            [alertView show];
            [[alertView rac_buttonClickedSignal] subscribeNext:^(id x) {
                @strongify(self);
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                 [self showRouterPWDAlertView];
                });
                
            }];
        }
    } error:^(NSError *error) {
        @strongify(self);
        UIAlertView* alertView =  [[UIAlertView alloc] initWithTitle:@"" message:@"密码错误请重新输入" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] ;
        [alertView show];
        [[alertView rac_buttonClickedSignal] subscribeNext:^(id x) {
            @strongify(self);
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                 [self showRouterPWDAlertView];
            });
        }];
    }];
}



-(void)searchCommandStatue:(NSString*)commandIdentifier{
    self.searchCommad += 1;
    [self commandNextSearch];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10*self.searchCommad * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        @weakify(self);
        [[JServerClient queryCommandStatus:commandIdentifier sn:[JGlobals shareGlobal].serverMAC  token:[JGlobals shareGlobal].token ] subscribeNext:^(id x) {
            @strongify(self);
            
            NSDictionary* dictionary = x[@"command"];
            if (dictionary) {
                JCommad* cammand =  [MTLJSONAdapter modelOfClass:[JCommad class]  fromJSONDictionary:dictionary error:nil];
                if([cammand.status isEqualToString:@"running"]){
                    if (self.searchCommad >= 4 ) {
                        [MBProgressHUD hideHUDForView:self.view animated:NO];
                        [MBProgressHUD showError:@"操作失败" toView:self.view];
                        self.searchCommad = 0;
                        [self commandFail];
                        return ;
                    }
                    [self searchCommandStatue:commandIdentifier];
                }else if([cammand.status isEqualToString:@"success"]){
                    [MBProgressHUD hideHUDForView:self.view animated:NO];
                    [MBProgressHUD showSuccess:@"操作成功。" toView:self.view];
                    self.searchCommad = 0;
                    [self commandSuccess];
                }else{
                    [MBProgressHUD hideHUDForView:self.view animated:NO];
                    [MBProgressHUD showError:@"操作失败" toView:self.view];
                    self.searchCommad = 0;
                    [self commandFail];
                }
            }
            
        } error:^(NSError *error) {
            @strongify(self);
            if (self.searchCommad >= 4 ) {
                [MBProgressHUD hideHUDForView:self.view animated:NO];
                [MBProgressHUD showError:@"操作失败" toView:self.view];
                [self commandFail];
                return ;
            }
            [self searchCommandStatue:commandIdentifier];
            
            
        }];
    });
}



-(void)commandFail{
    
}

-(void)commandSuccess{
    
}

-(void)commandNextSearch{
    
}




/**
 *  获取路由的信息
 */
-(void)loadRouterData{
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    @weakify(self);
    [[JServerClient queryRouterStatueInfo:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(NSDictionary* x) {
        @strongify(self);
        if (x[@"data"]) {
            id value = x[@"data"];
            NSDictionary* dic = [value firstObject];
            JRouterInfo* routerInfo =  [MTLJSONAdapter modelOfClass:JRouterInfo.class fromJSONDictionary:dic error:nil];
            if(routerInfo){
                [JGlobals shareGlobal].routerInfo = routerInfo;
                //判断路由状态
                if ([routerInfo.route isEqualToString:@"on"] && [routerInfo.internet isEqualToString:@"on"]) {
                    [JGlobals shareGlobal].wifiStatue = WifiStatueNomal;
                }else if([routerInfo.route isEqualToString:@"on"]){
                    [JGlobals shareGlobal].wifiStatue = WifiStatueExtranet;
                }else{
                    [JGlobals shareGlobal].wifiStatue = WifiStatueIntranet;
                }
                [self loadRouterDataSuccess];
            }else{
                [self loadRouterDataFail];
                [JGlobals shareGlobal].wifiStatue = WifiStatueUnKnow;
                
            }
        }
    } error:^(NSError *error) {
        
        [self loadRouterDataFail];
        [JGlobals shareGlobal].wifiStatue = WifiStatueUnKnow;
    }];
}


-(void)loadRouterDataSuccess{}

-(void)loadRouterDataFail{}



-(void)nextTask{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self taskAction];
    });
    
}

-(void)taskAction{
    
}



-(BOOL)showCanBlur{
    if (![[Reachability reachabilityForInternetConnection] isReachable]) {
        if([JGlobals shareGlobal].adminPWd){
            return YES;
        }
    }
    return NO;
}

-(void)showBlurView{
    if (self.showBlur) {
        if (![[Reachability reachabilityForInternetConnection] isReachable]) {
            if([JGlobals shareGlobal].adminPWd){
                
                [[UIApplication sharedApplication].keyWindow addSubview:self.blurNewView];
            }
        }
    }
}

-(FXBlurView*)blurNewView{
    if (!_blurNewView) {
        _blurNewView = [[FXBlurView alloc] initWithFrame:[UIApplication sharedApplication].keyWindow.bounds];
        [_blurNewView setDynamic:NO];
        _blurNewView.blurEnabled = YES;
        [_blurNewView setBlurRadius:0];
        [_blurNewView setIterations:0];
        _blurNewView.tintColor = [UIColor blackColor];
        UIView* blackView = [[UIView alloc] initWithFrame:_blurNewView.bounds];
        blackView.backgroundColor = [UIColor blackColor];
        blackView.alpha = 0.7;
        [_blurNewView addSubview:blackView];
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"JBlurBaseView" owner:self options:nil];
        JBlurBaseView* blurBaseView = nib.firstObject;
        blurBaseView.frame = _blurNewView.bounds;
        [_blurNewView addSubview:blurBaseView];
        @weakify(self);
        [[blurBaseView.backBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
            @strongify(self);
            [self.blurNewView removeFromSuperview];
            [self.navigationController popViewControllerAnimated:YES];
        }];
        
        //TODO：
        [[blurBaseView.settingBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
            @strongify(self);
            [self.blurNewView removeFromSuperview];
            
            if(VSDeviceOSVersionIsAtLeast(8.0)){
                NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                if ([[UIApplication sharedApplication] canOpenURL:url]) {
                    [[UIApplication sharedApplication] openURL:url];
                }
            }else{
                NSURL*url=[NSURL URLWithString:@"prefs:root=WIFI"];
                if ([[UIApplication sharedApplication] canOpenURL:url]) {
                    [[UIApplication sharedApplication] openURL:url];
                }
            }
            
        }];
    }
    
    return _blurNewView;
}



@end
