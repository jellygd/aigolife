//
//  JMessageTableViewCell.h
//  tcp
//
//  Created by Jelly on 15/7/27.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JMessageTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@end
