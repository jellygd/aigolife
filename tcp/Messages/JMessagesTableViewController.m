//
//  JMessagesTableViewController.m
//  tcp
//
//  Created by Jelly on 15/7/21.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JMessagesTableViewController.h"
#import "JMessageTableViewCell.h"
#import "JServerClient.h"
#import "JMessageInfo.h"
#import "JRouterUpgregeViewController.h"
#import "JDevicesViewController.h"
#import "JBlacklistViewController.h"

@interface JMessagesTableViewController ()

/**
 *  消息列表
 */
@property(nonatomic, strong) NSMutableArray* messageArray;

/**
 *  消息页数
 */
@property(nonatomic, assign) NSInteger indexPage;


@property(nonatomic, assign) BOOL loading;

@end

@implementation JMessagesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.messagesTableView.dataSource = self;
    self.messagesTableView.delegate = self;
    FIX_EDGES_FOR_EXTENDEDLAYTOU;
    self.title = @"消息";
    [self.messagesTableView setTableFooterView:[[UIView alloc] init]];
    
    UIButton* clearMessage = [UIButton buttonWithType:UIButtonTypeCustom];
    [clearMessage setTitle:@"清空列表" forState:UIControlStateNormal];
    clearMessage.titleLabel.font = [UIFont systemFontOfSize:14];
    [clearMessage sizeToFit];
    @weakify(self);
    [[clearMessage rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        [self clearMessage];
    }];
    self.navigationItem.rightBarButtonItem =
    [[UIBarButtonItem alloc] initWithCustomView:clearMessage];
    self.indexPage = 0;
    self.messageArray = [NSMutableArray array];
    [self loadMessagesData:YES];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - 加载数据
/**
 *  加载消息数据
 */
-(void)loadMessagesData:(BOOL)showLoadingView{
    if (showLoadingView) {
        self.indexPage = 0;
        [self.messageArray removeAllObjects];
    }
    self.indexPage += 1;
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    @weakify(self);
    if (showLoadingView) {
        [MBProgressHUD showMessag:@"查询中..." toView:self.view];
    }
    self.loading = YES;
    
    NSString* mac = [JGlobals shareGlobal].deveicRouter.identifier ? [JGlobals shareGlobal].deveicRouter.identifier :@"";
    if (mac.length <= 0) {
        mac =  [JGlobals shareGlobal].routerMAC;
    }
    
    [[JServerClient queryMessages:mac token:[JGlobals shareGlobal].token appver:version pageIndex:self.indexPage] subscribeNext:^(id x) {
        @strongify(self);
        self.loading = NO;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSArray* data =  x[@"data"];
        if (data.count > 0) {
            NSArray* array = [MTLJSONAdapter modelsOfClass:[JMessageInfo class] fromJSONArray:data error:nil];
            [self.messageArray addObjectsFromArray:array];
            [self.messagesTableView reloadData];
            [self showMessageView];
        }else{
            self.indexPage -= 1;
            if ([self.messageArray count] <= 0 ) {
                self.emptyMessageView.hidden = NO;
            }
        }
    } error:^(NSError *error) {
        @strongify(self);
        self.loading = NO;
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:@"网络错误" toView:self.view];
        self.indexPage -= 1;
    }];
}

/**
 *  清除消息列表
 */
-(void)clearMessage{
    if (self.messageArray.count <= 0 ) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"当前列表已经是空的了" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"是否要清除消息列表？" delegate:self cancelButtonTitle:@"否" otherButtonTitles:@"是", nil];
    [alertView show];
    @weakify(self);
    [[alertView rac_buttonClickedSignal] subscribeNext:^(id x) {
        if ([x integerValue] == 1) {
            @strongify(self);
            NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
            [MBProgressHUD showMessag:@"删除消息..." toView:self.view];
            NSString* mac = [JGlobals shareGlobal].deveicRouter.identifier ? [JGlobals shareGlobal].deveicRouter.identifier :@"";
            if (mac.length <= 0) {
                mac =  [JGlobals shareGlobal].routerMAC;
            }
            [[JServerClient deleteMessage:@"" sn:mac token:@"" appver:version] subscribeNext:^(id x) {
                [MBProgressHUD hideHUDForView:self.view animated:NO];
                [MBProgressHUD showSuccess:@"删除成功！" toView:self.view];
                [self.messageArray removeAllObjects];
                [self.messagesTableView reloadData];
                self.emptyMessageView.hidden = NO;
            } error:^(NSError *error) {
                [MBProgressHUD hideHUDForView:self.view animated:NO];
                [MBProgressHUD showError:@"网络失败，请稍后重试！" toView:self.view];
            }];
        }
    }];
}

-(void)deleteMessage:(JMessageInfo*)messageInfo{
    if (messageInfo) {
         NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
        [MBProgressHUD showMessag:@"删除消息..." toView:self.view];
        NSString* mac = [JGlobals shareGlobal].deveicRouter.identifier ? [JGlobals shareGlobal].deveicRouter.identifier :@"";
        if (mac.length <= 0) {
            mac =  [JGlobals shareGlobal].routerMAC;
        }
        [[JServerClient deleteMessage:messageInfo.identifier sn:mac token:[JGlobals shareGlobal].token appver:version] subscribeNext:^(id x) {
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            [MBProgressHUD showSuccess:@"删除成功！" toView:self.view];
            [self.messageArray removeObject:messageInfo];
            [self.messagesTableView reloadData];
            if ([self.messageArray count] <= 0) {
                self.emptyMessageView.hidden = NO;
            }
        } error:^(NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            [MBProgressHUD showError:@"网络失败，请稍后重试！" toView:self.view];
            [self.messagesTableView reloadData];
        }];
    }else{
        [self.messagesTableView reloadData];
    }
}

/**
 *  显示消息View
 */
-(void)showMessageView{
    self.emptyMessageView.hidden = YES;
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.messageArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    JMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MessageIdentifier" forIndexPath:indexPath];
    JMessageInfo* messageInfo = [self.messageArray objectAtIndex:indexPath.row];
    cell.messageLabel.text = messageInfo.title;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat:@"yyyy年MM月dd日 HH:MM:SS"];
    cell.timeLabel.text =[formatter stringFromDate:messageInfo.createTime];
    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 48.0;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    JMessageInfo* messageInfo = [self.messageArray objectAtIndex:indexPath.row];
    
    if ([messageInfo.type isEqualToString:@"blacklist"]) {
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"DeviceStoryboard" bundle:[NSBundle mainBundle]];
        UIViewController* viewController = [story instantiateViewControllerWithIdentifier:@"JBlacklistViewController"];
        [self.navigationController pushViewController:viewController animated:YES];
    }else if ([messageInfo.type isEqualToString:@"update"]){
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        JRouterUpgregeViewController* viewController = [story instantiateViewControllerWithIdentifier:@"JRouterUpgregeViewController"];
        [self.navigationController pushViewController:viewController animated:YES];
    }else if ([messageInfo.type isEqualToString:@"device"]){
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"DeviceStoryboard" bundle:[NSBundle mainBundle]];
        //由storyboard根据myView的storyBoardID来获取我们要切换的视图
        JDevicesViewController *devicesViewController = [story instantiateViewControllerWithIdentifier:@"DevicesIdentifier"];
        [self.navigationController pushViewController:devicesViewController animated:YES];
    }else if ([messageInfo.type isEqualToString:@"ad"]){
       NSString* urlString =   messageInfo.url;
        NSURL* url = [NSURL URLWithString:urlString];
        
        if (url&&[[UIApplication sharedApplication] canOpenURL:url]) {
            [[UIApplication sharedApplication] openURL:url];
        }
    }
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    JMessageInfo* messageInfo = [self.messageArray objectAtIndex:indexPath.row];
    [self deleteMessage:messageInfo];
}

#pragma mark - scrollview delegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView == self.messagesTableView) {
    
        if (scrollView.contentOffset.y+5/3*scrollView.height > scrollView.contentSize.height) {
            if (!self.loading) {
                [self loadMessagesData:NO];
            }
        }
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
