//
//  JMessagesTableViewController.h
//  tcp
//
//  Created by Jelly on 15/7/21.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JBaseViewController.h"

@interface JMessagesTableViewController : JBaseViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *emptyMessageView;

@property (weak, nonatomic) IBOutlet UITableView *messagesTableView;


/**
 *  加载消息数据
 */
-(void)loadMessagesData:(BOOL)showLoadingView;
@end
