//
//  JWifiDev.m
//  tcp
//
//  Created by dong on 15/7/23.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JWifiDev.h"

@implementation JWifiDev


+(NSDictionary*)JSONKeyPathsByPropertyKey{

    return @{
             @"ssid":@"ssid",
             @"wifiType":@"wifi_type",
             @"wifiSwitch":@"wifiSwitch",
             @"encrypt":@"encrypt",
             @"password":@"password",
             @"channel":@"channel"
             };
}
@end
