//
//  JRouterInfo.h
//  tcp
//
//  Created by dong on 15/7/23.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSpeedLimit.h"
#import "JWifiDev.h"
#import "JWanNet.h"
#import <Mantle.h>
#import <MTLModel.h>

@interface JRouterInfo : MTLModel<MTLJSONSerializing>

/**
 *  更新时间串  "2015-06-01 17:00:00"
 */
@property(nonatomic, strong) NSDate* updateTime;

/**
 *  路由连接状态
 */
@property(nonatomic, strong) NSString* route;

/**
 *  互联网状态
 */
@property(nonatomic, strong) NSString* internet;

/**
 *  1-设置过 0-未设置
 */
@property(nonatomic, strong) NSNumber* isSet;

/**
 *  连接设备数
 */
@property(nonatomic, strong) NSNumber* devN;

/**
 *  //表示当前1000B
 */
@property(nonatomic, strong) NSNumber* speed;

/**
 *  wan 口 pppoe 拨号账号
 */
@property(nonatomic, strong) NSString* pppoeId;

/**
 *  wan 口 pppoe 拨号密码
 */
@property(nonatomic, strong) NSString* pppoePwd;

/**
 *  智能限速设备限速等级 JSpeedLimit
 */
@property(nonatomic, strong) NSArray* speedLimit;

/**
 *  0/1
 */
@property(nonatomic, strong) NSNumber* bleSwitch;

/**
 *  wifi 设备 数组 对应的每一个是 JWifiDev
 */
@property(nonatomic, strong) NSArray* wifiDev;

/**
 *  1/0，  1开，0关
 */
@property(nonatomic, strong) NSNumber* antiRubNetwork;

/**
 *  1/0    //防骚扰
 */
@property(nonatomic, strong) NSNumber* antiHarassment;

/**
 *  1/0   //智能限制
 */
@property(nonatomic, strong) NSNumber* smartLimit;

/**
 *  限速模式 1,2,3,4
 */
@property(nonatomic, strong) NSNumber* smartModel;
/**
 *  存储状态 "on/off"
 */
@property(nonatomic, strong) NSString* diskState;

/**
 *  信号强度
 */
@property(nonatomic, strong) NSNumber* signalLevel;


@property(nonatomic, strong) NSNumber* slow_speed;

/**
 *  wan 口状态
 */
@property(nonatomic, strong) JWanNet* wanNet;

@end
