//
//  JFileInfo.m
//  tcp
//
//  Created by dong on 15/7/23.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JFileInfo.h"

@implementation JFileInfo

+(NSDictionary*)JSONKeyPathsByPropertyKey{
    return @{@"path":@"path",@"fileName":@"fileName",@"size":@"size",@"type":@"type",@"isFile":@"isFile",@"modifyTime":@"modifyTime"};
}


+(NSValueTransformer*)modifyTimeJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        return [NSDate dateWithTimeIntervalSince1970:[value floatValue]];
    }];
}


@end
