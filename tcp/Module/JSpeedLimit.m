//
//  JSpeedLimit.m
//  tcp
//
//  Created by dong on 15/7/23.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JSpeedLimit.h"

@implementation JSpeedLimit

+(NSDictionary*)JSONKeyPathsByPropertyKey{
    return @{
             @"devName":@"dev_name",
             @"remark":@"remark",
             @"level":@"level"
             };
}

@end
