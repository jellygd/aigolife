//
//  JUserModule.h
//  tcp
//
//  Created by dong on 15/7/20.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle.h>
#import <MTLModel.h>

@interface JUserModule : MTLModel<MTLJSONSerializing>
/**
 *  自增id
 */
@property(nonatomic,strong) NSString*identifier;
/**
 *  用户账号,邮箱地址或手机号
 */
@property(nonatomic,strong) NSString*username;
/**
 *  昵称
 */
@property(nonatomic,strong) NSString*nickname;
/**
 *  性别 0-女 1-男
 */
@property(nonatomic,strong) NSNumber* sex;
/**
 *  年龄
 */
@property(nonatomic,strong) NSNumber* age;
/**
 *  头像URL
 */
@property(nonatomic,strong) NSString*avatarUrl;
/**
 *  注册来源
 */
@property(nonatomic,strong) NSString*regfrom;


@end
