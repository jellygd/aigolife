//
//  JDeviceModule.m
//  tcp
//
//  Created by dong on 15/7/23.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JDeviceModule.h"

@implementation JDeviceModule

+(NSDictionary*)JSONKeyPathsByPropertyKey{
    return @{
             @"identifier" : @"id",
             @"type" : @"type"
             };
}

@end
