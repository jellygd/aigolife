//
//  JFileInfo.h
//  tcp
//
//  Created by dong on 15/7/23.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JFileInfo : MTLModel<MTLJSONSerializing>

/**
 *  文件或文件夹所在路径
 */
@property(nonatomic, strong) NSString* path;

/**
 *  文件名
 */
@property(nonatomic, strong) NSString* fileName;

/**
 *  文件大小 KB
 */
@property(nonatomic, strong) NSString* size;

/**
 *  文件类型  根据不太的类型 获取不同的图片
 */
@property(nonatomic, strong) NSString* type;
/**
 *  是否是文件  //文件1，目录0
 */
@property(nonatomic, strong) NSNumber* isFile;
/**
 *  文件修改时间 eg:2015-01-03 14:32:21
 */
@property(nonatomic, strong) NSDate* modifyTime;


@end
