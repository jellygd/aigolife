//
//  JBleRawDataObject.h
//  tcp
//
//  Created by dong on 15/7/23.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JBleRawDataObject : NSObject

/** 
 *  数据对象ID
 */
@property(nonatomic,strong) NSString* id;
/**
 *  设备名称
 */
@property(nonatomic,strong) NSString* deviceSSID;
/**
 *  设备mac地址
 */
@property(nonatomic,strong) NSString* deviceSN;
/**
 *  PM2,5  serviceUUID
 */
@property(nonatomic,strong) NSString* serviceUUID;
/**
 *  PM2.5  characterUUID
 */
@property(nonatomic,strong) NSString* characterUUID;
/**
 *  数据更新时间
 */
@property(nonatomic,strong) NSString* updateTime;
/**
 *  PM2.5 数据
 */
@property(nonatomic,strong) NSString* data;


@end
