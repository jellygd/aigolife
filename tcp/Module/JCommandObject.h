//
//  JCommandObject.h
//  tcp
//
//  Created by dong on 15/7/23.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JCommandObject : NSObject


@property(nonatomic, strong) NSString* id;
@property(nonatomic, strong) NSString* type;
@property(nonatomic, strong) NSString* status;
@property(nonatomic, strong) NSString* data;
@property(nonatomic, strong) NSString* sn;
@property(nonatomic, strong) NSString* appver;
@property(nonatomic, strong) NSString* info;

@property(nonatomic, strong) NSString* creator;
@property(nonatomic, strong) NSString* createtime;
@property(nonatomic, strong) NSString* updatetime;


@end
