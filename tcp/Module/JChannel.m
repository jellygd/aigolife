//
//  JChannel.m
//  tcp
//
//  Created by Jelly on 15/7/31.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JChannel.h"

@implementation JChannel

+(NSDictionary*)JSONKeyPathsByPropertyKey{
    return @{
             @"band":@"band",
             @"falsecca":@"falsecca",
             @"score":@"score",
             @"ap":@"ap",
             @"busytime":@"busytime",
             @"dirty":@"dirty",
             @"channel":@"channel",
             };

}

@end
