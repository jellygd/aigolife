//
//  JDeviceInfo.m
//  tcp
//
//  Created by dong on 15/7/23.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JDeviceInfo.h"
#import <ReactiveCocoa.h>

@implementation JLimitTime

+(NSDictionary*)JSONKeyPathsByPropertyKey{
    return @{@"start":@"start",@"end":@"end"};
}

@end

@implementation JDeviceInfo


+(NSDictionary*)JSONKeyPathsByPropertyKey{
    return @{@"devName":@"dev_name",
             @"wifiTpye":@"wifi_type",
             @"connTime":@"conn_time",
             @"totalDl":@"total_dl",
             @"speed":@"speed",
             @"level":@"level",
             @"limit":@"limit",
             @"mac":@"mac",
             @"ip":@"ip",
             @"limitTime":@"limit_time",
             @"isblack":@"isblack",
             @"remark":@"remark",
             };
}

+(NSValueTransformer*)limitTimeJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSDictionary *dictionaryArray, BOOL *success, NSError *__autoreleasing *error) {
        JLimitTime* object =[MTLJSONAdapter modelOfClass:JLimitTime.class fromJSONDictionary:dictionaryArray error:error];
        return object;
    } reverseBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        if (value) {
           return [MTLJSONAdapter JSONDictionaryFromModel:value error:nil];
            
        }
        return @"";
    }];
        
    
}


//- (id)reverseTransformedValue:(id)value success:(BOOL *)success error:(NSError **)error{
//    
//    if(value){
//        return value;
//    }
//    if (value == nil || value == [NSNull null]) {
//        return @"";
//    }
//    return value;
//}

//- (void)setNilValueForKey:(NSString *)key {
//    
//    [self setValue:@"" forKey:key];  // For NSInteger/CGFloat/BOOL
//}


@end
