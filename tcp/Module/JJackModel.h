//
//  JJackModel.h
//  tcp
//
//  Created by Jelly on 15/8/5.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "MTLModel.h"

@interface JJackModel : MTLModel<MTLJSONSerializing>
/**
 *  ssid 名称
 */
@property(nonatomic, strong) NSString* routerSSID;

/**
 *  ssid 密码
 */
@property(nonatomic, strong) NSString* routerPWD;

@end
