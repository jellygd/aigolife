//
//  JDiskinfo.h
//  tcp
//
//  Created by Jelly on 15/12/6.
//  Copyright © 2015年 Jelly. All rights reserved.
//

#import "MTLModel.h"

@interface JDiskinfo : MTLModel<MTLJSONSerializing>

@property(nonatomic, strong) NSString* dev;

@property(nonatomic, strong) NSString* available;

@property(nonatomic, strong) NSString* used;

@property(nonatomic, strong) NSString* mount;

@property(nonatomic, strong) NSString* size;

@end
