//
//  JDeviceInfo.h
//  tcp
//
//  Created by dong on 15/7/23.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface JLimitTime : MTLModel<MTLJSONSerializing>
/**
 *  上网开始时间
 */
@property(nonatomic, strong) NSString*start;
/**
 *  结束时间
 */
@property(nonatomic, strong) NSString*end;


@end


@interface JDeviceInfo : MTLModel<MTLJSONSerializing>
/**
 *  连接热点名称
 */
@property(nonatomic, strong) NSString* devName;

/**
 *  备注信息
 */
@property(nonatomic, strong) NSString* remark;
/**
 *   连接热点类型
 */
@property(nonatomic, strong) NSString* wifiTpye;
/**
 *  连接时长
 */
@property(nonatomic, strong) NSNumber* connTime;
/**
 *  总下载量
 */
@property(nonatomic, strong) NSNumber* totalDl;
/**
 *  当前速度
 */
@property(nonatomic, strong) NSNumber* speed;
/**
 *  限速模式 高中低
 */
@property(nonatomic, strong) NSNumber* level;
/**
 *  限制开关
 */
@property(nonatomic, strong) NSNumber* limit;

/**
 *  是否是黑名单
 */
@property(nonatomic, strong) NSNumber* isblack;
/**
 *  mac 地址
 */
@property(nonatomic, strong) NSString* mac;
/**
 *  IP地址
 */
@property(nonatomic, strong) NSString* ip;
/**
 *  限制时间
 */
@property(nonatomic, strong) JLimitTime* limitTime;

@end



