//
//  JWanNet.h
//  tcp
//
//  Created by dong on 15/7/23.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JWanNet : MTLModel<MTLJSONSerializing>
/**
 *  网络 方式  static/dhcp/pppoe
 */
@property(nonatomic, strong) NSString* wanWay;
/**
 *  网络 IP地址
 */
@property(nonatomic, strong) NSString* wanIp;
/**
 *  网络入口
 */
@property(nonatomic, strong) NSString* wanGateway;
/**
 *  网络掩码
 */
@property(nonatomic, strong) NSString* wanNetmask;
/**
 *  网络 DNS
 */
@property(nonatomic, strong) NSString* wanDns;



@property(nonatomic, strong) NSString* pppoeId;

@property(nonatomic, strong) NSString* pppoePwd;

@end
