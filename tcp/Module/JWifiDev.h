//
//  JWifiDev.h
//  tcp
//  WIFI SSID名称
//  Created by dong on 15/7/23.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle.h>
#import <MTLModel.h>

@interface JWifiDev : MTLModel<MTLJSONSerializing>
/**
 *  ssid 名称
 */
@property(nonatomic, strong) NSString* ssid;
/**
 *  wifi类型
 */
@property(nonatomic, strong) NSString* wifiType;
/**
 *  是否开启
 */
@property(nonatomic, assign) NSInteger wifiSwitch;
/**
 *  加密方式
 */
@property(nonatomic, strong) NSString* encrypt;
/**
 *  密码
 */
@property(nonatomic, strong) NSString* password;
/**
 *  信号道
 */
@property(nonatomic, strong) NSString* channel;


@end
