//
//  JJackModel.m
//  tcp
//
//  Created by Jelly on 15/8/5.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JJackModel.h"

@implementation JJackModel

+(NSDictionary*)JSONKeyPathsByPropertyKey{
    return @{
             @"routerSSID":@"routerSSID",
             @"routerPWD":@"routerPWD"
             };
}

@end
