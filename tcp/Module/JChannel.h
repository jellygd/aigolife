//
//  JChannel.h
//  tcp
//
//  Created by Jelly on 15/7/31.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JChannel : MTLModel<MTLJSONSerializing>

@property(nonatomic, strong) NSNumber* band;
@property(nonatomic, strong) NSNumber* falsecca;
@property(nonatomic, strong) NSNumber* score;
@property(nonatomic, strong) NSNumber* ap;
@property(nonatomic, strong) NSNumber* busytime;
@property(nonatomic, strong) NSNumber* dirty;
@property(nonatomic, strong) NSNumber* channel;





@end
