//
//  JDiskinfo.m
//  tcp
//
//  Created by Jelly on 15/12/6.
//  Copyright © 2015年 Jelly. All rights reserved.
//

#import "JDiskinfo.h"

@implementation JDiskinfo

+(NSDictionary*)JSONKeyPathsByPropertyKey{
    return @{
             @"dev":@"dev",
             @"available":@"available",
             @"used":@"used",
             @"mount":@"mount",
             @"size":@"size",
             };
};

@end
