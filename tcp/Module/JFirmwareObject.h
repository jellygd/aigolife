//
//  JFirmwareObject.h
//  tcp
//
//  Created by Eileen on 15/8/30.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "MTLModel.h"

@interface JFirmwareObject : MTLModel<MTLJSONSerializing>

/**
 *  唯一ID名字
 */
@property(nonatomic, strong) NSString* packageName;

/**
 *  依赖的SDK版本信息
 */
@property(nonatomic, strong) NSString* sdkVersion;

/**
 *  版本名（显示给用户看）
 */
@property(nonatomic, strong) NSString* versionName;

/**
 *  版本号
 */
@property(nonatomic, strong) NSNumber* versionCode;

/**
 *  发布说明
 */
@property(nonatomic, strong) NSString* releaseNote;

/**
 *  发布时间，精确到秒的时间戳，10位整型
 */
@property(nonatomic, strong) NSString* releaseDate;

/**
 *  安装包地址
 */
@property(nonatomic, strong) NSString* packageUrl;

/**
 *  文件的摘要信息，md5全小写哈希编码
 */
@property(nonatomic, strong) NSString* digest;


/**
 *  是否有升级
 */
@property(nonatomic, assign) BOOL isUpdate;
@end
