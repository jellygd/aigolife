//
//  JSpeedLimit.h
//  tcp
//  智能限速设备限速等级
//  Created by dong on 15/7/23.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle.h>
#import <MTLModel.h>

@interface JSpeedLimit : MTLModel<MTLJSONSerializing>


/**
 *  设备名称
 */
@property(nonatomic, strong) NSString* devName;


/**
 *  备注
 */
@property(nonatomic, strong) NSString* remark;

/**
 *  优先级别
 */
@property(nonatomic, strong) NSNumber* level;
@end
