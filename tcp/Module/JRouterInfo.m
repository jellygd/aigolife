
//
//  JRouterInfo.m
//  tcp
//
//  Created by dong on 15/7/23.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JRouterInfo.h"

@implementation JRouterInfo

+(NSDictionary*)JSONKeyPathsByPropertyKey{
    return @{
             @"updateTime":@"updateTime",
             @"route":@"route",
             @"internet":@"internet",
             @"isSet":@"isSet",
             @"devN":@"devN",
             @"speed":@"speed",
             @"pppoeId":@"pppoe_id",
             @"pppoePwd":@"pppoe_pwd",
             @"speedLimit":@"speed_limit",
             @"bleSwitch":@"bleSwitch",
             @"wifiDev":@"wifi_dev",
             @"antiRubNetwork":@"anti_rub_network",
             @"antiHarassment":@"anti_harassment",
             @"smartLimit":@"smartLimit",
             @"smartModel":@"smartModel",
             @"diskState":@"disk_state",
             @"signalLevel":@"signal_level",
             @"slow_speed":@"slow_speed",
             @"wanNet":@"wan_net",
             
             };
}

+(NSValueTransformer*)updateTimeJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
         return [dateFormatter dateFromString:value];
    }];
}

+(NSValueTransformer*)speedLimitJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSArray *dictionaryArray, BOOL *success, NSError *__autoreleasing *error) {
       return [MTLJSONAdapter modelsOfClass:JSpeedLimit.class fromJSONArray:dictionaryArray error:error];
    }];
}

+(NSValueTransformer*)wifiDevJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSArray *dictionaryArray, BOOL *success, NSError *__autoreleasing *error) {
        return [MTLJSONAdapter modelsOfClass:JWifiDev.class fromJSONArray:dictionaryArray error:error];
    }];
}

+(NSValueTransformer*)wanNetJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        
        return [MTLJSONAdapter modelOfClass:JWanNet.class fromJSONDictionary:value error:error];
    }];
}

@end
