//
//  JDeviceModule.h
//  tcp
//
//  Created by dong on 15/7/23.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle.h>
#import <MTLModel.h>

@interface JDeviceModule : MTLModel<MTLJSONSerializing>

/**
 *  设备唯一ID或MAC地址
 */
@property(nonatomic, strong) NSString* identifier;

/**
 *  设备类型
 */
@property(nonatomic, strong) NSString* type;

@end
