//
//  JMessageInfo.h
//  tcp
//
//  Created by dong on 15/8/31.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "MTLModel.h"

@interface JMessageInfo : MTLModel<MTLJSONSerializing>

@property(nonatomic, strong) NSString* identifier;

@property(nonatomic, strong) NSDate* createTime;

@property(nonatomic, strong) NSNumber* pushFlag;

@property(nonatomic, strong) NSString* sn;

@property(nonatomic, strong) NSString* title;

@property(nonatomic, strong) NSString* source;

@property(nonatomic, strong) NSNumber* status;

@property(nonatomic, strong) NSString* data;

@property(nonatomic, strong) NSString* type;

@property(nonatomic, strong) NSString* url;


@end
