//
//  JCommad.m
//  tcp
//
//  Created by Jelly on 15/8/22.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JCommad.h"

@implementation JCommad


+(NSDictionary*)JSONKeyPathsByPropertyKey{
    return @{
             @"appver":@"appver",
             @"createtime":@"createtime",
             @"creator":@"creator",
             @"identifier":@"id",
             @"sn":@"sn",
             @"status":@"status",
             @"type":@"type",
             @"updatetime":@"updatetime",
             };
}

@end
