//
//  JUserModule.m
//  tcp
//
//  Created by dong on 15/7/20.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JUserModule.h"

@implementation JUserModule

+(NSDictionary*)JSONKeyPathsByPropertyKey{
    return @{
              @"identifier": @"id",
              @"username":@"username",
              @"nickname":@"nickname",
              @"sex":@"sex",
              @"age":@"age",
              @"avatarUrl":@"avatarUrl",
              @"regfrom":@"regfrom"
             };
}



@end
