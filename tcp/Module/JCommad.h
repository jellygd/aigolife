//
//  JCommad.h
//  tcp
//
//  Created by Jelly on 15/8/22.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "MTLModel.h"

@interface JCommad : MTLModel<MTLJSONSerializing>

@property(nonatomic, strong) NSString* appver;
@property(nonatomic, strong) NSString* creator;
@property(nonatomic, strong) NSString* identifier;
@property(nonatomic, strong) NSString* sn;
@property(nonatomic, strong) NSString* type;
@property(nonatomic, strong) NSString* updatetime;
@property(nonatomic, strong) NSString* createtime;
@property(nonatomic, strong) NSString* status;

@end
