//
//  JFirmwareObject.m
//  tcp
//
//  Created by Eileen on 15/8/30.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JFirmwareObject.h"

@implementation JFirmwareObject

+(NSDictionary*)JSONKeyPathsByPropertyKey{
    return @{
             @"packageName":@"packageName",
             @"sdkVersion":@"sdkVersion",
             @"versionName":@"versionName",
             @"versionCode":@"versionCode",
             @"releaseNote":@"releaseNote",
             @"releaseDate":@"releaseDate",
             @"packageUrl":@"packageUrl",
             @"digest":@"digest",
             };
}


//+(NSValueTransformer*)releaseDateJSONTransformer
//{
//    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
////        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
////        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
////       ;
////        
////        NSString* valueStr = @"";
////        if ([value isKindOfClass:[NSString class]]) {
////            valueStr  = value;
////        }else{
////            valueStr = [NSString stringWithFormat:@"%ld",valueStr];
////        }
//        return  [NSDate dateWithTimeIntervalSince1970:[value longValue]];
//    }];
//}


@end
