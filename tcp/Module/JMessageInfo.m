//
//  JMessageInfo.m
//  tcp
//
//  Created by dong on 15/8/31.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JMessageInfo.h"

@implementation JMessageInfo


+(NSDictionary*)JSONKeyPathsByPropertyKey{
    return @{
             @"identifier":@"id",
             @"createTime":@"createTime",
             @"pushFlag":@"pushFlag",
             @"sn":@"sn",
             @"title":@"title",
             @"source":@"source",
             @"status":@"status",
             @"data":@"data",
             @"type":@"type",
             @"url":@"url",
             };
}

+(NSValueTransformer*)createTimeJSONTransformer
{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString* valueStr = @"";
        if ([value isKindOfClass:[NSString class]]) {
            valueStr  = value;
        }else{
            valueStr = [NSString stringWithFormat:@"%ld",valueStr];
        }
        
        return [dateFormatter dateFromString:valueStr];
    }];
}


@end
