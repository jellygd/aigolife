//
//  JWelcomeViewController.h
//  tcp
//
//  Created by Jelly on 15/11/20.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JWelcomeViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *welcomeImageView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activiyView;
@end
