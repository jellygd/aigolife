//
//  JGuideSecondViewController.h
//  tcp
//
//  Created by Jelly on 15/8/2.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JBaseViewController.h"

@interface JGuideSecondViewController : JBaseViewController

@property (weak, nonatomic) IBOutlet UIButton *dhcpBtn;

@property (weak, nonatomic) IBOutlet UIButton *ppoeBtn;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIImageView *guideSecondItem;

@property (weak, nonatomic) IBOutlet UIView *bohaoView;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *pwdTextField;

@property (weak, nonatomic) IBOutlet UILabel *dhcpLabel;


@end
