//
//  JGuideFourthViewController.m
//  tcp
//
//  Created by Jelly on 15/8/2.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JGuideFourthViewController.h"
#import "JServerClient+JCommand.h"
#import "AppDelegate.h"
#import <Masonry.h>

@interface JGuideFourthViewController ()<UITextFieldDelegate>

@property(nonatomic, strong) UIView *lineView;
@end

@implementation JGuideFourthViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.lineView = [[UIView alloc] init];
    self.lineView.backgroundColor = UIColorFromRGB(0x228FE7);
    [self.headerView insertSubview:self.lineView atIndex:0];
    self.nameTextField.text = @"aigo-router";
    self.nameTextField.leftViewMode = UITextFieldViewModeAlways;
    self.nameTextField.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_WIFI_name"]];
    self.pwdTextField.leftViewMode = UITextFieldViewModeAlways;
    self.pwdTextField.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_wifi_psw"]];
    
    self.pwdTextField.delegate = self;
    self.nameTextField.delegate = self;
    
    
    [[self.settingBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        if(VSDeviceOSVersionIsAtLeast(8.0)){
            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            if ([[UIApplication sharedApplication] canOpenURL:url]) {
                [[UIApplication sharedApplication] openURL:url];
            }
        }else{
            NSURL*url=[NSURL URLWithString:@"prefs:root=WIFI"];
            if ([[UIApplication sharedApplication] canOpenURL:url]) {
                [[UIApplication sharedApplication] openURL:url];
            }
        }
        
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            AppDelegate* appdelegate = [UIApplication sharedApplication].delegate;
            [appdelegate showLoginView];
        });
    }];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enable = NO;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
}


- (void)updateViewConstraints{
    
    [super updateViewConstraints];
    [self.lineView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.right.width.equalTo(self.headerView);
        make.height.equalTo(@1);
        make.centerY.equalTo(self.headerView.mas_centerY);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)subAction:(id)sender {
    if (self.pwdTextField.text.length >= 8 && self.nameTextField.text.length > 0) {
        [self setWAN];
    }else{
        [[[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入有效的无线名称或密码" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] show];
    }
}


-(void)setNetWorking{
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    @weakify(self);
    [MBProgressHUD hideHUDForView:self.view animated:NO];
    [MBProgressHUD showMessag:@"设置中..." toView:self.view];
    [[JServerClient setWifi:self.nameTextField.text switch:YES type:@"2.4G" encrypt:@"WPA/WPA2" password:self.pwdTextField.text sn:[JGlobals shareGlobal].isAigoRouter ? [JGlobals shareGlobal].routerMAC :[JGlobals shareGlobal].deveicRouter.identifier token:[JGlobals shareGlobal].isAigoRouter ? [JGlobals shareGlobal].routerToken : [JGlobals shareGlobal].token appver:version] subscribeNext:^(id x) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showSuccess:@"设置成功" toView:self.view];
        [self showSuccessView];
    } error:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
        
        [self showErrorForType:3];
    }];
}

-(void)setWAN{
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    @weakify(self);
    [MBProgressHUD hideHUDForView:self.view animated:NO];
    [MBProgressHUD showMessag:@"设置中..." toView:self.view];
    [[JServerClient setRouterWan:[MTLJSONAdapter JSONDictionaryFromModel:[JGlobals shareGlobal].guideWannet error:nil] sn:[JGlobals shareGlobal].routerMAC token:[JGlobals shareGlobal].routerToken appver:version] subscribeNext:^(id x) {
        @strongify(self);
        [self setAdminPWD];
    } error:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
        
        [self showErrorForType:1];
    }];
}

-(void)setAdminPWD{
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    @weakify(self);
    [MBProgressHUD hideHUDForView:self.view animated:NO];
    [MBProgressHUD showMessag:@"设置中..." toView:self.view];
    NSString* oldPWD = [JGlobals shareGlobal].adminPWd;
    if (!oldPWD.length > 0) {
        oldPWD = kDefaultAdminPWD;
    }
    [[JServerClient setAdminAccount:oldPWD newPWD:[JGlobals shareGlobal].guideAdminPWD sn:[JGlobals shareGlobal].isAigoRouter ? [JGlobals shareGlobal].routerMAC :[JGlobals shareGlobal].deveicRouter.identifier token:[JGlobals shareGlobal].isAigoRouter ? [JGlobals shareGlobal].routerToken : [JGlobals shareGlobal].token appver:version ] subscribeNext:^(id x) {
        @strongify(self);
        [[NSUserDefaults standardUserDefaults] setObject:[JGlobals shareGlobal].guideAdminPWD forKey:@"adminPwd"];
        [self setNetWorking];
    } error:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
        
        [self showErrorForType:2];
    }];
}


-(void)showErrorForType:(NSInteger)type{
    
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"设置错误，请重试。" delegate:nil cancelButtonTitle:@"重试" otherButtonTitles:nil, nil];
    [alertView show];
    @weakify(self);
    [[alertView rac_buttonClickedSignal] subscribeNext:^(id x) {
        @strongify(self);
        switch (type) {
            case 1:
            {
                [self setWAN];
            }
                break;
            case 2:
            {
                [self setAdminPWD];
            }
                break;
            case 3:
            {
                [self setNetWorking];
            }
                break;
                
            default:
                break;
        }
    }];
}

-(void)showSuccessView{
    self.successView.hidden = NO;
    self.fourImageView.image = [UIImage imageNamed:@"step_complete"];
    
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
    if ([string isEqualToString:@"\n"])
    {
        return YES;
    }
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (self.pwdTextField == textField)
    {
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression
                                      regularExpressionWithPattern:@"([^A-Za-z0-9_.])"
                                      options:0
                                      error:&error];
        
        NSString *resString = [regex stringByReplacingMatchesInString:string options:0 range:NSMakeRange(0, string.length) withTemplate:@""];
        
        if (resString.length != string.length) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"不能输入特殊字符" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] ;
            [alert show];
            return NO;
        }
        
        if ([toBeString length] > 16) {
            textField.text = [toBeString substringToIndex:16];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"提示请输入6到16位密码" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] ;
            [alert show];
            return NO;
        }
    }else if(self.nameTextField == textField){
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression
                                      regularExpressionWithPattern:@"([^A-Za-z0-9_.])"
                                      options:0
                                      error:&error];
        
        NSString *resString = [regex stringByReplacingMatchesInString:string options:0 range:NSMakeRange(0, string.length) withTemplate:@""];
        
        if (resString.length != string.length) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"不能输入特殊字符" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] ;
            [alert show];
            return NO;
        }
        
        if ([toBeString length] > 16) {
            textField.text = [toBeString substringToIndex:16];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"不得16个字符" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] ;
            [alert show];
            return NO;
        }
    }
    return YES;
}

@end
