//
//  JGuideSecondViewController.m
//  tcp
//
//  Created by Jelly on 15/8/2.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JGuideSecondViewController.h"

#import "JServerClient+JCommand.h"
#import "JWanNet.h"
#import <Masonry.h>
#import "JServerClient+JRouter.h"

@interface JGuideSecondViewController ()

@property (nonatomic, strong) UIImageView *lineImageView;
@property (nonatomic, strong) UIView* lineView;

@end

@implementation JGuideSecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    [self dreawEllipseLine:self.headerView1 color:[UIColor colorWithRed:0.549 green:0.718 blue:0.922 alpha:1.000]];
//    [self dreawLine:self.headerView2 color:[UIColor colorWithWhite:0.757 alpha:1.000]];
    self.lineView = [[UIView alloc] init];
    self.lineView.backgroundColor = UIColorFromRGB(0x228FE7);
    self.lineImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bgline"]];
    [self.headerView insertSubview:self.lineView atIndex:0];
    [self.headerView insertSubview:self.lineImageView atIndex:0];
    
    
    self.dhcpLabel.hidden = YES;
    
    self.nameTextField.leftViewMode = UITextFieldViewModeAlways;
    self.nameTextField.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_WIFI_name"]];
    self.pwdTextField.leftViewMode = UITextFieldViewModeAlways;
    self.pwdTextField.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_wifi_psw"]];

    [self loadRouterWanType];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enable = NO;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
}

-(void)updateViewConstraints{
    [super updateViewConstraints];
    
    
    [self.lineView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.headerView);
        make.right.equalTo(self.guideSecondItem.mas_centerX);
        make.height.equalTo(@1);
        make.centerY.equalTo(self.headerView.mas_centerY);
    }];
    
    [self.lineImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.headerView);
        make.left.equalTo(self.guideSecondItem.mas_centerX);
        make.height.equalTo(@8);
        make.centerY.equalTo(self.headerView.mas_centerY);
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(void)loadRouterWanType{
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    @weakify(self);
    [MBProgressHUD showMessag:@"获取上网方式中..." toView:self.view];
    [[JServerClient getRouterWanType:[JGlobals shareGlobal].routerMAC token:[JGlobals shareGlobal].routerToken appver:version]  subscribeNext:^(NSDictionary* x) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        NSDictionary* data = x[@"result"][@"data"];
        if (!data || ![data objectForKey:@"wantype"]) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            [MBProgressHUD showError:@"查询失败，请手动设置" toView:self.view];
            return ;
        }
        NSString* wantType = data[@"wantype"];
        @strongify(self);
        if ([wantType isEqualToString:@"dhcp"]) {
            [self updateDHCPType:NO];
        }else if([wantType isEqualToString:@"pppoe"]){
            [self updateDHCPType:YES];
        }else{
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            [MBProgressHUD showError:@"查询失败，请手动设置" toView:self.view];
        }
    } error:^(NSError *error) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [MBProgressHUD showError:@"查询失败，请手动设置" toView:self.view];
    }];
}

- (IBAction)changeTypeAction:(id)sender {
    if (self.bohaoView.hidden) {
        [self updateDHCPType:YES];
    }else{
        [self updateDHCPType:NO];
    }
}

-(void)updateDHCPType:(BOOL)isDHCPType{
    if (isDHCPType) {
        self.bohaoView.hidden = NO;
        self.dhcpLabel.hidden = YES;
        self.ppoeBtn.hidden = NO;
        [self.dhcpBtn setTitle:@"切换为DHCP模式》" forState:UIControlStateNormal];
    }else{
        self.bohaoView.hidden = YES;
        self.dhcpLabel.hidden = NO;
        self.ppoeBtn.hidden = NO;
        [self.dhcpBtn setTitle:@"切换为拨号模式》" forState:UIControlStateNormal];
    }
}


-(void)setDHCPType{
    JWanNet* wannet = [[JWanNet alloc] init];
    wannet.wanWay =@"dhcp";
    wannet.wanDns = @"";
    wannet.wanIp = @"";
    wannet.pppoePwd = @"";
    wannet.pppoeId = @"";
    wannet.wanGateway = @"";
    wannet.wanNetmask = @"";
    [JGlobals shareGlobal].guideWannet = wannet;
     [self performSegueWithIdentifier:@"thirdlyIdentifier" sender:self];
    
//    @weakify(self);
//    [MBProgressHUD showMessag:@"设置中..." toView:self.view];
//    [[JServerClient setRouterWan:[MTLJSONAdapter JSONDictionaryFromModel:wannet error:nil] sn:[JGlobals shareGlobal].routerMAC token:[JGlobals shareGlobal].routerToken appver:version] subscribeNext:^(id x) {
//        @strongify(self);
//        [MBProgressHUD hideHUDForView:self.view animated:NO];
//        [MBProgressHUD showSuccess:@"设置成功" toView:self.view];
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            //跳入下一级
//            [self performSegueWithIdentifier:@"thirdlyIdentifier" sender:self];
//        });
//    } error:^(NSError *error) {
//        @strongify(self);
//        [MBProgressHUD hideHUDForView:self.view animated:NO];
//        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
//    }];
}

- (IBAction)setpp:(id)sender {
    if (self.bohaoView.hidden) {
        [self setDHCPType];
        return;
    }
    if (self.nameTextField.text.length > 0 && self.pwdTextField.text.length > 0) {
        [self setpppoeType];
    }else{
        [[[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入上网账号或密码" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] show];
    }
}

-(void)setpppoeType{
   
    JWanNet* wannet = [[JWanNet alloc] init];
    wannet.wanWay =@"pppoe";
    wannet.wanDns = @"";
    wannet.wanIp = @"";
    wannet.pppoePwd = self.nameTextField.text;
    wannet.pppoeId = self.pwdTextField.text;
    wannet.wanGateway = @"";
    wannet.wanNetmask = @"";
    
    [JGlobals shareGlobal].guideWannet = wannet;
     [self performSegueWithIdentifier:@"thirdlyIdentifier" sender:self];
}

-(void)setstaticType{

}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
    if ([string isEqualToString:@"\n"])
    {
        return YES;
    }
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (self.pwdTextField == textField)
    {
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression
                                      regularExpressionWithPattern:@"([^A-Za-z0-9_.])"
                                      options:0
                                      error:&error];
        
        NSString *resString = [regex stringByReplacingMatchesInString:string options:0 range:NSMakeRange(0, string.length) withTemplate:@""];
        
        if (resString.length != string.length) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"不能输入特殊字符" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] ;
            [alert show];
            return NO;
        }
        
        if ([toBeString length] > 16) {
            textField.text = [toBeString substringToIndex:16];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"提示请输入8到16位密码" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] ;
            [alert show];
            return NO;
        }
    }
    return YES;
}



@end
