//
//  JGuideFirstViewController.m
//  tcp
//
//  Created by Jelly on 15/8/2.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JGuideFirstViewController.h"
#import "JServerClient+JRouter.h"
#import "JServerClient+JCommand.h"
#import <Masonry.h>
#import "AppDelegate.h"

@interface JGuideFirstViewController ()

@property(nonatomic, strong) UIImageView* lineView;

@end

@implementation JGuideFirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    // Do any additional setup after loading the view.
   
    self.centerView.layer.borderWidth = 0.5;
    self.centerView.layer.borderColor = [UIColor colorWithWhite:0.702 alpha:1.000].CGColor;
    self.lineView = [[UIImageView alloc ] initWithImage:[UIImage imageNamed:@"bgline"]];
    [self.headerView insertSubview:self.lineView atIndex:0];
    
//    self.headerImageView.backgroundColor = [UIColor colorWithWhite:0.757 alpha:1.000];
    
    [self loginRouter:kDefaultAdminPWD];
    
    [self.textView scrollRangeToVisible:NSMakeRange(0, 0)];
    self.suberBtn.enabled = NO;
}

-(void)backAction{
    AppDelegate* delegate = [UIApplication sharedApplication].delegate;
    [delegate showLoginView];
}


-(void)updateViewConstraints{
    [super updateViewConstraints];
    
    [self.lineView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.right.width.equalTo(self.headerView);
        make.height.equalTo(@8);
        make.centerY.equalTo(self.headerView.mas_centerY);
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)showAdminPWDAlertView{

    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"输入管理员密码" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    alertView.alertViewStyle = UIAlertViewStyleSecureTextInput;
    @weakify(self);
    [[alertView rac_buttonClickedSignal] subscribeNext:^(id x) {
        if ([x integerValue] == 0) {
            @strongify(self);
            UITextField* pwdTextField = [alertView textFieldAtIndex:0];
            
            if(pwdTextField.text.length > 0){
                [self loginRouter:pwdTextField.text];
            }else{
                [self showAdminPWDAlertView];
            }
        }
    }];
    [alertView show];
}


-(void)loginRouter:(NSString*)pwd{
    @weakify(self);
    [[JServerClient loginRouter:pwd] subscribeNext:^(id x) {
        @strongify(self);
        NSString*token = x[@"result"][@"info"][@"token"];
        if ([token length] > 0) {
            [JGlobals shareGlobal].routerToken = token;
            [JGlobals shareGlobal].adminPWd = pwd;
        }else{
            [self showAdminPWDAlertView];
        }
    } error:^(NSError *error) {
        @strongify(self);
        [self showAdminPWDAlertView];
    }];
}

- (IBAction)greadBtn:(id)sender {
    
    
    UIButton* btn = sender;
    btn.selected = !btn.selected;
    if (btn.selected) {
        [btn setImage:[UIImage imageNamed:@"btn_agree_highlight"] forState:UIControlStateNormal];
    }else{
        [btn setImage:[UIImage imageNamed:@"btn_agree_normal"] forState:UIControlStateNormal];
    }
    
    self.suberBtn.enabled = btn.selected;
}
@end
