//
//  JGuideThirdlyViewController.m
//  tcp
//
//  Created by Jelly on 15/8/2.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JGuideThirdlyViewController.h"
#import "JServerClient+JCommand.h"
#import <Masonry.h>


@interface JGuideThirdlyViewController ()<UITextFieldDelegate>


@property (nonatomic, strong) UIImageView *lineImageView;
@property (nonatomic, strong) UIView* lineView;

@end

@implementation JGuideThirdlyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.lineView = [[UIView alloc] init];
    self.lineView.backgroundColor = UIColorFromRGB(0x228FE7);
    self.lineImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bgline"]];
    [self.headerView insertSubview:self.lineView atIndex:0];
    [self.headerView insertSubview:self.lineImageView atIndex:0];
    
    
    self.pwdTextField.leftViewMode = UITextFieldViewModeAlways;
    self.pwdTextField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 7, 10)];
    self.pwdTextField.delegate = self;
    @weakify(self);
    [[self.subBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        if (self.pwdTextField.text.length >= 8 && self.pwdTextField.text.length <=16) {
            [self setAdminPWd];
        }else{
            [[[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入有效的管理员密码" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] show];
        }
    }];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enable = NO;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
}

- (void)updateViewConstraints{
    [super updateViewConstraints];
    [self.lineView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.headerView);
        make.right.equalTo(self.guideThirdlyItem.mas_centerX);
        make.height.equalTo(@1);
        make.centerY.equalTo(self.headerView.mas_centerY);
    }];
    
    [self.lineImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.headerView);
        make.left.equalTo(self.guideThirdlyItem.mas_centerX);
        make.height.equalTo(@8);
        make.centerY.equalTo(self.headerView.mas_centerY);
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setAdminPWd{
    NSString* oldPWD = [JGlobals shareGlobal].adminPWd;
    if (!oldPWD.length > 0) {
        oldPWD = kDefaultAdminPWD;
    }
    
    [JGlobals shareGlobal].guideAdminPWD = self.pwdTextField.text;
    [self performSegueWithIdentifier:@"fourthIdentifier" sender:self];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
    if ([string isEqualToString:@"\n"])
    {
        return YES;
    }
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (self.pwdTextField == textField)
    {
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression
                                      regularExpressionWithPattern:@"([^A-Za-z0-9_.])"
                                      options:0
                                      error:&error];
        
        NSString *resString = [regex stringByReplacingMatchesInString:string options:0 range:NSMakeRange(0, string.length) withTemplate:@""];
        
        if (resString.length != string.length) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"不能输入特殊字符" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] ;
            [alert show];
            return NO;
        }
        
        if ([toBeString length] > 16) {
            textField.text = [toBeString substringToIndex:16];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"提示请输入8到16位密码" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] ;
            [alert show];
            return NO;
        }
    }
    return YES;
}


@end
