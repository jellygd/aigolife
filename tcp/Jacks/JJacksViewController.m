//
//  JJacksViewController.m
//  tcp
//
//  Created by Jelly on 15/8/5.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JJacksViewController.h"
#import "JJackTableViewCell.h"
#import "JJackModel.h"

@interface JJacksViewController ()<UITableViewDataSource,UITableViewDelegate>

/**
 *  插座列表
 */
@property(nonatomic, strong) NSMutableArray* jackArray;

@property(nonatomic, strong) UIButton* rightBtn;

@end

@implementation JJacksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setHeaderBgView:self.headerView];
    self.jackArray = [NSMutableArray array];
    
    
    self.rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.rightBtn setImage:[UIImage imageNamed:@"icon_top_add"] forState:UIControlStateNormal];
    [self.rightBtn sizeToFit];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.rightBtn];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.jackArray count];
}


 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 JJackTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JJackTableViewCell" forIndexPath:indexPath];
 
 
 return cell;
 }

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}



@end
