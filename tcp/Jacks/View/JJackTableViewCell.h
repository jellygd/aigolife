//
//  JJackTableViewCell.h
//  tcp
//
//  Created by Jelly on 15/8/5.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JJackTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *jackNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *jackMoreImageView;

@end
