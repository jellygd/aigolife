//
//  AppDelegate.m
//  tcp
//
//  Created by Jelly on 15/7/19.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "AppDelegate.h"
#import "JLoginViewController.h"
#import "JMainViewController.h"
#import <SVProgressHUD.h>
#import <REFrostedViewController.h>
#import "JMenuViewController.h"
#import <MMDrawerController.h>
#import "JNavigationController.h"
#import "JServerClient+JRouter.h"
#import <Reachability.h>
#import "JGuideFirstViewController.h"
#import "JPushManager.h"
#import "iConsole.h"￼
#import <Bugly/CrashReporter.h>
#import "JPackage.h"
#import "ICETutorialPage.h"
#import "ICETutorialStyle.h"
#import "ICETutorialController.h"
#import "JWelcomeViewController.h"
#import "JFoundation.h"
#import "JMessagesTableViewController.h"

@interface AppDelegate ()<JLoginViewControllerDelegate,ICETutorialControllerDelegate>

@property(nonatomic, strong)UIAlertView *mesageAlertView;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    //设置不上传iCloud
    NSString *docsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSURL * finalURL = [NSURL fileURLWithPath:docsDir];
    [JPackage addSkipBackupAttributeToItemAtURL:finalURL];
    
    
    //键盘设置
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = NO;
    manager.shouldResignOnTouchOutside = YES;
    manager.shouldToolbarUsesTextFieldTintColor = YES;
    manager.enableAutoToolbar = NO;
    
    [[CrashReporter sharedInstance] installWithAppId:@"900009098"];
    [[CrashReporter sharedInstance] enableLog:YES];
    [[CrashReporter sharedInstance] setEnableSymbolicateInProcess:YES];
    
    [[UINavigationBar appearance] setBarTintColor:UIColorFromRGB(0x469DFA)];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [[UINavigationBar appearance] setBackIndicatorImage:[UIImage imageNamed:@"back"]];
    //TODO:上线前替换
    self.window =[[iConsoleWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [iConsole sharedConsole].deviceShakeToShow = NO;
    [[JPushManager shareInstance] registerAPNS];
    
    NSDictionary* userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (userInfo != nil) {
        [JGlobals shareGlobal].push = YES;
    }
    [self showMain];
    [UIApplication sharedApplication].applicationSupportsShakeToEdit = YES;
    
    if([[[UIDevice currentDevice]systemVersion] floatValue] < 8.0//8.0以前
       || ([[UIApplication sharedApplication]  respondsToSelector:@selector(currentUserNotificationSettings)] && ([[[UIApplication sharedApplication] currentUserNotificationSettings] types] & UIUserNotificationTypeBadge) == UIUserNotificationTypeBadge)){//8.0以后需要授权UIUserNotificationTypeBadge，才能设置badge
        [UIApplication sharedApplication].applicationIconBadgeNumber = 1;
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    }
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    return YES;
}


-(void)showGuide{
    ICETutorialPage *layer1 = [[ICETutorialPage alloc] initWithTitle:@""
                                                            subTitle:@""
                                                         pictureName:@"tutorial_background_00@2x.jpg"
                                                            duration:3.0];
    ICETutorialPage *layer2 = [[ICETutorialPage alloc] initWithTitle:@""
                                                            subTitle:@""
                                                         pictureName:@"tutorial_background_01@2x.jpg"
                                                            duration:3.0];
    ICETutorialPage *layer3 = [[ICETutorialPage alloc] initWithTitle:@""
                                                            subTitle:@""
                                                         pictureName:@"tutorial_background_02@2x.jpg"
                                                            duration:3.0];
    ICETutorialPage *layer4 = [[ICETutorialPage alloc] initWithTitle:@""
                                                            subTitle:@""
                                                         pictureName:@"tutorial_background_03@2x.jpg"
                                                            duration:3.0];
    NSArray *tutorialLayers = @[layer1,layer2,layer3,layer4];
    
    // Set the common style for the title.
    ICETutorialLabelStyle *titleStyle = [[ICETutorialLabelStyle alloc] init];
    [titleStyle setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17.0f]];
    [titleStyle setTextColor:[UIColor whiteColor]];
    [titleStyle setLinesNumber:1];
    [titleStyle setOffset:180];
    [[ICETutorialStyle sharedInstance] setTitleStyle:titleStyle];
    
    // Set the subTitles style with few properties and let the others by default.
    [[ICETutorialStyle sharedInstance] setSubTitleColor:[UIColor whiteColor]];
    [[ICETutorialStyle sharedInstance] setSubTitleOffset:150];
    
    // Init tutorial.
    ICETutorialController*guideVC = [[ICETutorialController alloc] initWithPages:tutorialLayers
                                                                        delegate:self];
    
    self.rootViewController =guideVC;
    [guideVC startScrolling];
    
    self.window.rootViewController = self.rootViewController;
}



#pragma mark - ICETutorialController delegate
- (void)tutorialController:(ICETutorialController *)tutorialController scrollingFromPageIndex:(NSUInteger)fromIndex toPageIndex:(NSUInteger)toIndex {
}

- (void)tutorialControllerDidReachLastPage:(ICETutorialController *)tutorialController {
    if ([self.rootViewController isKindOfClass:[ICETutorialController class] ]) {
        ICETutorialController*guideVC = self.rootViewController;
        
        [guideVC stopScrolling];
    }
    [[NSUserDefaults standardUserDefaults] setObject:@(YES) forKey:@"First"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if ([JGlobals shareGlobal].isAigoRouter && ![JGlobals shareGlobal].isSet) {
            [self showSystemGuide];
        }else{
            [self loadInterfaceView];
        }
        
    });
}

- (void)tutorialController:(ICETutorialController *)tutorialController didClickOnLeftButton:(UIButton *)sender {
    if ([self.rootViewController isKindOfClass:[ICETutorialController class] ]) {
        ICETutorialController*guideVC = self.rootViewController;
        [guideVC stopScrolling];
    }
    [[NSUserDefaults standardUserDefaults] setObject:@(YES) forKey:@"First"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if ([JGlobals shareGlobal].isAigoRouter && ![JGlobals shareGlobal].isSet) {
        [self showSystemGuide];
    }else{
        [self loadInterfaceView];
    }
}

- (void)tutorialController:(ICETutorialController *)tutorialController didClickOnRightButton:(UIButton *)sender {
    [[NSUserDefaults standardUserDefaults] setObject:@(YES) forKey:@"First"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if ([JGlobals shareGlobal].isAigoRouter && ![JGlobals shareGlobal].isSet) {
        [self showSystemGuide];
    }else{
        [self loadInterfaceView];
    }
    
    [tutorialController stopScrolling];
}


-(void)showMain{
    JWelcomeViewController* welcomeVC = [[JWelcomeViewController alloc] initWithNibName:@"JWelcomeViewController" bundle:nil];
    self.window.rootViewController = welcomeVC;
    [self.window makeKeyAndVisible];
    
    if ([[Reachability reachabilityForLocalWiFi] isReachableViaWiFi]) {
        NSLog(@"连接的是WIFI");
        //判断当前连接的路由器是否是本地路由
        @weakify(self);
        [[JServerClient queryRouterSetupInfo] subscribeNext:^(id x) {
            
            NSLog(@"连接的是AigoWIFI");
            
            NSDictionary* data = x[@"data"];
            BOOL isSet = [data[@"isSet"] boolValue];
            NSString* mac = data[@"mac"];
            [JGlobals shareGlobal].isAigoRouter = YES;
            [JGlobals shareGlobal].isSet = isSet;
            [JGlobals shareGlobal].routerMAC = mac;
            if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"First"] boolValue]){
                [[NSUserDefaults standardUserDefaults] setObject:@(YES) forKey:@"First"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self showGuide];
                [self.window makeKeyAndVisible];
                return ;
            }
            
            
            
            if (!isSet) {
                NSLog(@"AigoWIFI 没有设置过 mac 地址为：%@",mac);
                [self showSystemGuide];
            }else{
                NSLog(@"AigoWIFI 已经设置过 mac 地址为：%@",mac);
                [self loadInterfaceView];
            }
        } error:^(NSError *error) {
            NSLog(@"连接的不是AigoWIFI");
            @strongify(self);
            [JGlobals shareGlobal].isAigoRouter = NO;
            [self loadInterfaceView];
            
        }];
    }else{
        NSLog(@"连接的不是WIFI");
        [JGlobals shareGlobal].isAigoRouter = NO;
        
        if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"First"] boolValue]){
            [[NSUserDefaults standardUserDefaults] setObject:@(YES) forKey:@"First"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self showGuide];
            [self.window makeKeyAndVisible];
        }else{
            
            [self loadInterfaceView];
        }
    }
}


-(void)showSystemGuide{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"GuideStoryboard" bundle:[NSBundle mainBundle]];
    //由storyboard根据myView的storyBoardID来获取我们要切换的视图
    JGuideFirstViewController *storageViewController = [story instantiateViewControllerWithIdentifier:@"guide1"];
    
    self.window.rootViewController = [[JNavigationController alloc] initWithRootViewController:storageViewController];
    [self.window makeKeyAndVisible];
    
}

-(void)loadInterfaceView{
    [self showLoginView];
}



- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)pushDic
{
    NSLog(@"接收到推送:%@", pushDic);
    if([[[UIDevice currentDevice]systemVersion] floatValue] < 8.0//8.0以前
       || ([[UIApplication sharedApplication]  respondsToSelector:@selector(currentUserNotificationSettings)] && ([[[UIApplication sharedApplication] currentUserNotificationSettings] types] & UIUserNotificationTypeBadge) == UIUserNotificationTypeBadge)){//8.0以后需要授权UIUserNotificationTypeBadge，才能设置badge
        [UIApplication sharedApplication].applicationIconBadgeNumber = 1;
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    }
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    if ([self.rootViewController isKindOfClass:[MMDrawerController class]]) {
        @weakify(self);
        MMDrawerController *drawerController = (MMDrawerController*)self.rootViewController;
        [JGlobals shareGlobal].push = NO;
        NSString *alterMsg = [[pushDic objectForKey:@"aps"] objectForKey:@"alert"] ;
        
        if(!self.mesageAlertView){
            self.mesageAlertView = [[UIAlertView alloc] initWithTitle:@"温馨提示"
                                                                message:alterMsg
                                                               delegate:self
                                                      cancelButtonTitle:@"忽略"
                                                      otherButtonTitles:@"去看看", nil] ;
            [self.mesageAlertView show] ;
        }
        
        
        [[self.mesageAlertView rac_buttonClickedSignal] subscribeNext:^(id x) {
            @strongify(self);
            self.mesageAlertView = nil;
            if([x integerValue] == 1){
                JNavigationController* navViewController = (JNavigationController*)drawerController.centerViewController;
//                JMainViewController
                
                if ([navViewController.viewControllers.lastObject isKindOfClass:[JMessagesTableViewController class]]) {
                    JMessagesTableViewController* messageViewController = navViewController.viewControllers.lastObject;
                    [messageViewController loadMessagesData:YES];
                }else{
                    JMainViewController* mainViewController = navViewController.viewControllers.firstObject;
                    if ([mainViewController respondsToSelector:@selector(showMessagesView)]) {
                       [mainViewController showMessagesView];
                    }
                }
            }
        }];
    }else{
        [JGlobals shareGlobal].push = YES;
    }
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DidEnterBackground" object:nil];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}



-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    [[JPushManager shareInstance] didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}

-(void)showLoginView{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:@"token"];
    [userDefaults removeObjectForKey:@"expire"];
    [userDefaults removeObjectForKey:@"duration"];
    
    [userDefaults synchronize];
    [JGlobals shareGlobal].routerToken = nil;
    [JGlobals shareGlobal].validateRouter = NO;
    [JGlobals shareGlobal].deveicRouter = nil;
    [JGlobals shareGlobal].user = nil;
    
    //判断用户是否已经登录 如果没有登录 则进入登录界面，如果登录了~ 则直接进入首页
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"JUserStoryboard" bundle:[NSBundle mainBundle]];
    //由storyboard根据myView的storyBoardID来获取我们要切换的视图
    JLoginViewController *loginViewController = [story instantiateViewControllerWithIdentifier:@"Login"];
    loginViewController.delegate = self;
    self.rootViewController = loginViewController;
    self.window.rootViewController =  [[JNavigationController alloc] initWithRootViewController:self.rootViewController  ];
    [self.window makeKeyAndVisible];
}

-(void)showMainView{
    //判断用户是否已经登录 如果没有登录 则进入登录界面，如果登录了~ 则直接进入首页
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    //由storyboard根据myView的storyBoardID来获取我们要切换的视图
    JMainViewController *mainViewController = [story instantiateViewControllerWithIdentifier:@"main"];
    JMenuViewController* menuViewController = [story instantiateViewControllerWithIdentifier:@"Menu"];
    
    MMDrawerController* drawerController =[[MMDrawerController alloc] initWithCenterViewController:[[JNavigationController alloc] initWithRootViewController:mainViewController] leftDrawerViewController:[[JNavigationController alloc] initWithRootViewController:menuViewController] ];
    //    MMDrawerController *frostedViewController = [[MMDrawerController alloc] initWithContentViewController:[[UINavigationController alloc] initWithRootViewController:mainViewController] menuViewController:menuViewController];
    //    frostedViewController.direction = REFrostedViewControllerDirectionLeft;
    [drawerController setMaximumLeftDrawerWidth:CGRectGetWidth([UIScreen mainScreen].bounds)*0.8];
    [drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModePanningCenterView | MMOpenDrawerGestureModePanningNavigationBar];
    [drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    
    self.rootViewController = drawerController;
    self.window.rootViewController =self.rootViewController;
    //    [[UINavigationController alloc] initWithRootViewController:self.rootViewController  ];
    [self.window makeKeyAndVisible];
    
    
}


#pragma mark- JLoginViewControllerDelegate
-(void)loginDidSuccess{
    [self showMainView];
}

-(void)loginDissmess{
    [self showMainView];
}

@end
