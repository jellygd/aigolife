//
//  ViewController.m
//  tcp
//
//  Created by Jelly on 15/7/19.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JMainViewController.h"
#import "JServerClient.h"
//#import <REFrostedViewController.h>
//#import "UIViewController+REFrostedViewController.h"
#import <MMDrawerController.h>
#import <UIViewController+MMDrawerController.h>
#import <Reachability.h>
#import "JServerClient+JRouter.h"
#import "JServerClient+JCommand.h"
#import "JServerClient.h"
#import "JDeviceModule.h"
#import "JRouterInfo.h"
#import "JDevicesViewController.h"
#import <FXBlurView.h>
#import "JStorageViewController.h"
#import "JAssistantViewController.h"
#import "JGuideFirstViewController.h"
#import "NSString+Encryption.h"
#import <RXCollection.h>
#import "UIViewController+JShake.h"
#import "JStorageManager.h"
#import "AppDelegate.h"


@interface JMainViewController ()

@property(nonatomic, strong) JRouterInfo* routerInfo;
@property(nonatomic, strong) FXBlurView* blurView;
@property(nonatomic, assign) BOOL isBind;
@property(nonatomic, assign) BOOL currentView;
@property(nonatomic, assign) BOOL loading;

@end

@implementation JMainViewController

#pragma mark  view 生命周期
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupLeftMenuButton];
    [self setupRightButton];
    for (UIView* view in self.navigationController.navigationBar.subviews) {
        if ([view isKindOfClass:[UIImageView class]]) {
            for (UIView* imageView in view.subviews) {
                if ([imageView isKindOfClass:[UIImageView class]]) {
                    [imageView removeFromSuperview];
                }
            }
        }
    }
    
    [self buttonAction];
    self.blurView.frame = CGRectMake(0, GETHEIGHT(self.blurView), GETWIDTH(self.blurView), GETHEIGHT(self.blurView));
    //监听wifi状态
    @weakify(self);
    [RACObserve([JGlobals shareGlobal], wifiStatue) subscribeNext:^(id x) {
        @strongify(self);
        if ([[self.navigationController.viewControllers lastObject] class] == [self class]) {
            [self showRouterStatue];
        }
        
    }];
    [JGlobals shareGlobal].wifiStatue = WifiStatueCheckRouter;
    //判断用户未登录的
    if ([JGlobals shareGlobal].token.length > 0) {
        [self getUserRouter];
    } else if ([JGlobals shareGlobal].isAigoRouter){
        [self getLocalRouter];
    }else{
        [JGlobals shareGlobal].wifiStatue = WifiStatueExtranet;
        
        [self touseChooseWifiView];
    }
    self.validateRouterBlock = ^(NSNumber* validate){
        @strongify(self);
        if (!validate  && self.isBind) {
            self.isBind = NO;
            [MBProgressHUD showError:@"绑定失败" toView:self.view];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                AppDelegate* delegate =(AppDelegate*)[UIApplication sharedApplication].delegate;
                [delegate showLoginView];
            });
            return ;
        }
        if (self.isBind &&  [validate boolValue]) {
            NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
            [[JServerClient bindRouter:[JGlobals shareGlobal].userName sn:[JGlobals shareGlobal].routerMAC token:[JGlobals shareGlobal].routerToken appver:version] subscribeNext:^(id x) {
                self.isBind = NO;
                [JGlobals shareGlobal].validateRouter = YES;
                self.validateRouterBlock(@(YES));
                [MBProgressHUD showSuccess:@"绑定成功" toView:self.view];
            } error:^(NSError *error) {
                self.isBind = NO;
                [JGlobals shareGlobal].validateRouter = YES;
                self.validateRouterBlock(@(YES));
                [MBProgressHUD showSuccess:@"绑定成功" toView:self.view];
            }];
        }else{
             [JGlobals shareGlobal].wifiStatue = WifiStatueCheckRouter;
            [self loadRouterData];
        }
        
    };
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didWillEnterBackgroud) name:UIApplicationWillEnterForegroundNotification object:nil];
    
    [[self.shakeButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        if (![JGlobals shareGlobal].deveicRouter && [JGlobals shareGlobal].user) {
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"您还没有绑定路由" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
            [alertView show];
            return ;
        }else{
            [self performSegueWithIdentifier:@"shakeIdentifier" sender:nil];
        }
    }];
    
    if ([JGlobals shareGlobal].push) {
        NSLog(@"====== tuis");
    }
    
    if ([JGlobals shareGlobal].push) {
        [JGlobals shareGlobal].push = NO;
        [self showMessagesView];
    }
}


-(void)didBecomeActive{
    self.loading = NO;
    [JGlobals shareGlobal].wifiStatue = WifiStatueCheckRouter;
    [self.blurView removeFromSuperview];
    [self loadRouterData];
}

-(void)didWillEnterBackgroud{
    self.loading = YES;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setShadowImage:nil];
    [self.mm_drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeNone];
    [self.mm_drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeNone];
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0x469DFA)];
    self.currentView = NO;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setShadowImage:nil];
    [self setUpMainview];
    [self.mm_drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    
    [self.mm_drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    [self setNavBackgroundColor];
    [self loadUserInfo];
    
    self.currentView = YES;
    [[JStorageManager shareStorageManager] setFilePath:nil fileName:nil fileOption:FileOptionNone];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark 导航栏按钮初始化

-(void)setupLeftMenuButton{
    UIBarButtonItem* leftBtnItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_top_menu"] style:UIBarButtonItemStylePlain target:self action:@selector(leftDrawerButtonPress:)];
    [self.navigationItem setLeftBarButtonItem:leftBtnItem animated:YES];
}

-(void)setupRightButton{
    UIBarButtonItem* rightBtnItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_top_message"] style:UIBarButtonItemStylePlain target:self action:@selector(rightDrawerButtonPress:)];
    [self.navigationItem setRightBarButtonItem:rightBtnItem animated:YES];
}

- (IBAction)reloadData:(id)sender {
    if ([JGlobals shareGlobal].wifiStatue == WifiStatueCheckRouter) {
        return;
    }
    [JGlobals shareGlobal].wifiStatue = WifiStatueCheckRouter;
    
    [self loadRouterData];
    
}

-(void)buttonAction{
    @weakify(self);
    [[self.deviceButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        if (![JGlobals shareGlobal].deveicRouter && [JGlobals shareGlobal].user) {
            [self showErrorMessage:@"您还没有绑定路由"];
            return ;
        }
        if ([JGlobals shareGlobal].wifiStatue == WifiStatueUndefine || [JGlobals shareGlobal].wifiStatue == WifiStatueUnKnow) {
            [self showErrorMessage:@"云平台检测不到设备"];
            return;
        }
        if ([JGlobals shareGlobal].wifiStatue == WifiStatueCheckRouter) {
            [self showErrorMessage:@"请等待设备连接完成"];
            return;
        }
        @strongify(self);
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"DeviceStoryboard" bundle:[NSBundle mainBundle]];
        //由storyboard根据myView的storyBoardID来获取我们要切换的视图
        JDevicesViewController *devicesViewController = [story instantiateViewControllerWithIdentifier:@"DevicesIdentifier"];
        
        [self.navigationController pushViewController:devicesViewController animated:YES];
    }];
    
    [[self.storageButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        if([JGlobals shareGlobal].routerToken.length <= 0)
        {
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"不支持远程访问" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
            [alertView show];
            return ;
        }
        @strongify(self);
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"StorageStoryboard" bundle:[NSBundle mainBundle]];
        //由storyboard根据myView的storyBoardID来获取我们要切换的视图
        JStorageViewController *storageViewController = [story instantiateViewControllerWithIdentifier:@"StorageIdentifier"];
        
        [self.navigationController pushViewController:storageViewController animated:YES];
        
    }];
    [[self.JacksButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        //        if (![JGlobals shareGlobal].deveicRouter && [JGlobals shareGlobal].user) {
        //            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"您还没有绑定路由" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
        //            [alertView show];
        //            return ;
        //        }
        return ;
        [[[UIAlertView alloc] initWithTitle:@"" message:@"敬请期待" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] show];
        return ;
        @strongify(self);
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"JacksStoryboard" bundle:[NSBundle mainBundle]];
        //由storyboard根据myView的storyBoardID来获取我们要切换的视图
        UIViewController *viewController = [story instantiateViewControllerWithIdentifier:@"JJacksViewController"];
        [self.navigationController pushViewController:viewController animated:YES];
        
    }];
    
    [[self.netWorkButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        if (![JGlobals shareGlobal].deveicRouter && [JGlobals shareGlobal].user) {
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"您还没有绑定路由" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
            [alertView show];
            return ;
        }
        if ([JGlobals shareGlobal].wifiStatue == WifiStatueUndefine || [JGlobals shareGlobal].wifiStatue == WifiStatueUnKnow) {
            [self showErrorMessage:@"云平台检测不到设备"];
            return;
        }
        if ([JGlobals shareGlobal].wifiStatue == WifiStatueCheckRouter) {
            [self showErrorMessage:@"请等待设备连接完成"];
            return;
        }
        @strongify(self);
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"AssistantStoryboard" bundle:[NSBundle mainBundle]];
        //由storyboard根据myView的storyBoardID来获取我们要切换的视图
        JStorageViewController *storageViewController = [story instantiateViewControllerWithIdentifier:@"JAssistantViewController"];
        
        [self.navigationController pushViewController:storageViewController animated:YES];
        
    }];
}

#pragma mark - 导航栏设置
-(void)leftDrawerButtonPress:(id)sender{
    
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}


-(void)rightDrawerButtonPress:(id)sender{
    [self showMessagesView];
}


- (UIImage *)buttonImageFromColor:(UIColor *)color{
    CGRect rect = CGRectMake(0, 0, self.view.frame.size.width, 0.5);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext(); return img;
}

#pragma mark -菜单栏

-(void)setUpMainview{
    [self setMainMenuView:self.mainMenu1];
    [self setMainMenuView:self.mainMenu2];
    [self setMainMenuView:self.mainMenu3];
    [self setMainMenuView:self.mainMenu4];
}

-(void)setMainMenuView:(UIView*)menuView{
    menuView.transform = CGAffineTransformScale(CGAffineTransformIdentity, [JGlobals shareGlobal].scale, [JGlobals shareGlobal].scale);
}

#pragma mark  -获取用户信息

-(void)loadUserInfo{
    if ([JGlobals shareGlobal].user || [JGlobals shareGlobal].token.length <= 0 ) {
        return;
    }
    [[JServerClient GetUserInfo:[JGlobals shareGlobal].userName] subscribeNext:^(id x) {
        JUserModule* user = [MTLJSONAdapter modelOfClass:[JUserModule class] fromJSONDictionary:x[@"user"] error:nil];
        [JGlobals shareGlobal].user = user;
    } error:^(NSError *error) {
        
    }];
}


-(void)taskAction{
    if ([self.navigationController.viewControllers.firstObject class] == self.class) {
        [self loadRouterData];
    }
    
}

/**
 *  获取路由的信息
 */
-(void)loadRouterData{
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    
    if (![[JGlobals shareGlobal] isAigoRouter] && [JGlobals shareGlobal].user && ![JGlobals shareGlobal].deveicRouter) {
        //弹出选择WiFi界面
        [self touseChooseWifiView];
        return;
    }
    @weakify(self);
    [[JServerClient queryRouterStatueInfo:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(NSDictionary* x) {
        @strongify(self);
        if (x[@"data"]) {
            id value = x[@"data"];
            NSDictionary* dic = [value firstObject];
            self.routerInfo = [MTLJSONAdapter modelOfClass:JRouterInfo.class fromJSONDictionary:dic error:nil];
            if (self.routerInfo) {
                //                self.title = self.routerInfo.;
                
                if ([self.routerInfo.isSet integerValue] == 0 && [JGlobals shareGlobal].isAigoRouter) {
                    //进行路由设置界面
                    UIStoryboard *story = [UIStoryboard storyboardWithName:@"GuideStoryboard" bundle:[NSBundle mainBundle]];
                    //由storyboard根据myView的storyBoardID来获取我们要切换的视图
                    JGuideFirstViewController *storageViewController = [story instantiateViewControllerWithIdentifier:@"guide1"];
                    [self.navigationController pushViewController:storageViewController animated:YES];
                    
                    return ;
                }
                [JGlobals shareGlobal].routerInfo = self.routerInfo;
                //判断路由状态
                if ([self.routerInfo.route isEqualToString:@"on"] && [self.routerInfo.internet isEqualToString:@"on"]) {
                    if ([self.routerInfo.slow_speed boolValue]) {
                        [JGlobals shareGlobal].wifiStatue = WifiStatueSlow;
                    }else{
                        [JGlobals shareGlobal].wifiStatue = WifiStatueNomal;
                    }
                }else if([self.routerInfo.route isEqualToString:@"on"]){
                    [JGlobals shareGlobal].wifiStatue = WifiStatueExtranet;
                }else{
                    [JGlobals shareGlobal].wifiStatue = WifiStatueIntranet;
                }
                [self nextTask];
            }else{
                [self nextTask];
                [JGlobals shareGlobal].wifiStatue = WifiStatueUnKnow;
                [MBProgressHUD showError:@"获取信息失败，请重新检查！" toView:self.view];
            }
        }
    } error:^(NSError *error) {
        if (self.loading) {
            return;
        }
        
        if ([[UIApplication sharedApplication] applicationState] != UIApplicationStateBackground) {
            //        @strongify(self);
            if ([[Reachability reachabilityForInternetConnection] isReachable]) {
                if([JGlobals shareGlobal].user){
                    [self nextTask];
                    [JGlobals shareGlobal].wifiStatue = WifiStatueUnKnow;
                }else{
                    if([JGlobals shareGlobal].wifiStatue != WifiStatueIntranet){
                        [JGlobals shareGlobal].wifiStatue = WifiStatueIntranet;
                        [self touseChooseWifiView];
                    }
                }
            }else{
                if (![JGlobals shareGlobal].user) {
                    [self nextTask];
                    if([JGlobals shareGlobal].wifiStatue != WifiStatueIntranet){
                        [JGlobals shareGlobal].wifiStatue = WifiStatueIntranet;
                        [self touseChooseWifiView];
                    }
                    
                    return ;
                }
                [JGlobals shareGlobal].wifiStatue = WifiStatueUnKnow;
            }
        }
    }];
}

/**
 *  获取用户绑定的路由信息
 */
-(void)getUserRouter{
    @weakify(self);
    if ([JGlobals shareGlobal].isAigoRouter) {
        
        [[RACSignal combineLatest:@[[JServerClient queryRouterSetupInfo],[JServerClient GetBindDeviceList:[JGlobals shareGlobal].token]] reduce:^(NSDictionary* localInfo,NSDictionary* x){
            NSLog(@"获取路由信息 跟 用户的信息成功");
            return @{@"localInfo":localInfo,@"userInfo":x};
        } ] subscribeNext:^(id x) {
            //如果当前用户没有绑定路由 则绑定当前的路由
            NSDictionary* userInfo =  x[@"userInfo"];
            NSArray* devices = [MTLJSONAdapter modelsOfClass:JDeviceModule.class fromJSONArray:userInfo[@"device"] error:nil];
            devices = [devices rx_filterWithBlock:^BOOL(JDeviceModule* module) {
                return [module.type isEqualToString:@"router"];
            }];
            NSDictionary* localInfo = x[@"localInfo"][@"data"];
            if (!localInfo && devices.count <= 0 ) {
                [JGlobals shareGlobal].wifiStatue = WifiStatueExtranet;
                return ;
            }
            BOOL bind =  [localInfo[@"bind"] boolValue];
            BOOL isSet = [localInfo[@"isSet"] boolValue];
            NSString* macString = localInfo[@"mac"];
            
            if (!isSet) {
                NSLog(@"路由没有设置过 进行设置");
                UIStoryboard *story = [UIStoryboard storyboardWithName:@"GuideStoryboard" bundle:[NSBundle mainBundle]];
                //由storyboard根据myView的storyBoardID来获取我们要切换的视图
                JGuideFirstViewController *storageViewController = [story instantiateViewControllerWithIdentifier:@"guide1"];
                [self.navigationController pushViewController:storageViewController animated:YES];
            }
            
            //如果当前的路由没有绑定 则绑定当前的路由
            if (bind) {
                NSLog(@"路由已经绑定");
                if ([devices count] > 0) {
                    JDeviceModule* module ;
                    
                    for (JDeviceModule* device in devices) {
                        if ([device.identifier isEqualToString:macString]) {
                            module = device;
                        }
                    }
                    
                    if (!module) {
                        module = devices.firstObject;
                    }
                    
                    [JGlobals shareGlobal].deveicRouter = module;
                    //如果绑定的当前路由是当前的路由 则发送绑定的接口 不然则走云端
                    if ([module.identifier isEqualToString:macString]) {
                        NSLog(@"用户绑定的路由跟当前的路由一致");
                        [JGlobals shareGlobal].routerToken = [[JGlobals shareGlobal].userName md5String];
                        NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
                        [[JServerClient bindRouter:[JGlobals shareGlobal].userName sn:macString token:[[JGlobals shareGlobal].userName md5String] appver:version] subscribeNext:^(id x) {
                            NSLog(@"用户绑定的路由跟当前的路由一致  绑定本地路由成功");
                            [JGlobals shareGlobal].validateRouter = YES;
                            self.validateRouterBlock(@(YES));
                        } error:^(NSError *error) {
                            [JGlobals shareGlobal].validateRouter = NO;
                            NSLog(@"用户绑定的路由跟当前的路由一致  绑定本地路由失败");
                            self.validateRouterBlock(@(YES));
                        }];
                    }
                    
                }
                return ;
            }
            NSLog(@"路由没有绑定");
            [JGlobals shareGlobal].aigoRouterMac = macString;
            
            JDeviceModule*device = [[JDeviceModule alloc] init];
            device.type = @"router";
            device.identifier = macString;
            
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"是否绑定当前路由？" delegate:nil cancelButtonTitle:@"否" otherButtonTitles:@"是", nil];
            [alertView show];
            [[alertView rac_buttonClickedSignal] subscribeNext:^(id x) {
                if ([x integerValue] == 1) {
                    [[JServerClient BindUserDevice:device token:[JGlobals shareGlobal].token] subscribeNext:^(id x) {
                        @strongify(self);
                        [JGlobals shareGlobal].deveicRouter = device;
                        self.isBind = YES;
                        [self showRouterPWDAlertView];
                    } error:^(NSError *error) {
                        if ([error.userInfo[@"errmessage"] isEqualToString:@"user_has_been_bind_this_device"]) {
                            [JGlobals shareGlobal].deveicRouter = device;
                            self.isBind = YES;
                            [self showRouterPWDAlertView];
                        }else{
                           NSString* errorMessage = error.userInfo[@"errmessage"];
                            if ([errorMessage isEqualToString:@"someone_has_been_binded_this_device"] || [errorMessage isEqualToString:@"user_has_been_bind_other_device"]) {
                                NSString* alertMessage = @"当前路由已被绑定";
                                if ([errorMessage isEqualToString:@"user_has_been_bind_other_device"]  ) {
                                    alertMessage = @"您已绑定其他的路由";
                                }
                                UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:alertMessage delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
                                [alertView show];
                                
                                [[alertView rac_buttonClickedSignal] subscribeNext:^(id x) {
                                    if ([x integerValue] == 0) {
                                        AppDelegate* delegate = [UIApplication sharedApplication].delegate;
                                        [delegate showLoginView];
                                    }else{
                                        if (devices.count > 0) {
                                            [JGlobals shareGlobal].deveicRouter = devices.firstObject;
                                        }
                                        [JGlobals shareGlobal].isAigoRouter = NO;
                                        self.validateRouterBlock(nil);
                                    }
                                }];
                            }else{
                                if (devices.count > 0) {
                                    [JGlobals shareGlobal].deveicRouter = devices.firstObject;
                                    [JGlobals shareGlobal].isAigoRouter = NO;
                                }
                                self.validateRouterBlock(nil);
                            }
                        }
                    }];
                }else{
                    if (devices.count > 0) {
                        [JGlobals shareGlobal].deveicRouter = devices.firstObject;
                        [JGlobals shareGlobal].isAigoRouter = NO;
                        self.validateRouterBlock(nil);
                    }else{
                        [self touseChooseWifiView];
                    }
                    
                }
            }];
        } error:^(NSError *error) {
            @strongify(self);
            [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
        }];
        return;
    }
    
    
    
    [[JServerClient GetBindDeviceList:[JGlobals shareGlobal].token] subscribeNext:^(NSDictionary* x) {
        if (x[@"device"]) {
            NSArray* devices = [MTLJSONAdapter modelsOfClass:JDeviceModule.class fromJSONArray:x[@"device"] error:nil];
            
            devices = [devices rx_filterWithBlock:^BOOL(JDeviceModule* module) {
                
                return [module.type isEqualToString:@"router"];
            }];
            @strongify(self);
            if([devices count] > 0){
                JDeviceModule* module = devices.firstObject;
                [JGlobals shareGlobal].deveicRouter = module;
                [self loadRouterData];
                return;
            }else if (![[JGlobals shareGlobal] isAigoRouter]) {
                //弹出选择WiFi界面
                [self touseChooseWifiView];
            }
        }
        //如果当前用户没有绑定AigoWiFi
        if (![[JGlobals shareGlobal] isAigoRouter]) {
            //弹出选择WiFi界面
            [self touseChooseWifiView];
        }
    } error:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
    }];
}

/**
 *  获取本地路由信息
 */
-(void)getLocalRouter{
    @weakify(self);
    [[JServerClient queryRouterSetupInfo] subscribeNext:^(id x) {
        @strongify(self);
        if (x[@"data"]) {
            BOOL isSet = [x[@"data"][@"isSet"] boolValue];
            NSString* macString = x[@"data"][@"mac"];
            [JGlobals shareGlobal].aigoRouterMac = macString;
            if (!isSet) {
                UIStoryboard *story = [UIStoryboard storyboardWithName:@"GuideStoryboard" bundle:[NSBundle mainBundle]];
                //由storyboard根据myView的storyBoardID来获取我们要切换的视图
                JGuideFirstViewController *storageViewController = [story instantiateViewControllerWithIdentifier:@"guide1"];
                [self.navigationController pushViewController:storageViewController animated:YES];
            }else{
                [self loadRouterData];
            }
        }else{
            [JGlobals shareGlobal].wifiStatue = WifiStatueUnKnow;
            [MBProgressHUD showError:@"获取信息失败，请重新检查！" toView:self.view];
        }
    } error:^(NSError *error) {
        @strongify(self);
        [JGlobals shareGlobal].wifiStatue = WifiStatueUnKnow;
        [MBProgressHUD showError:@"获取信息失败，请重新检查！" toView:self.view];
    }];
}
/**
 *  显示提示选择AigoWIfi
 */
-(void)touseChooseWifiView{
    
    if([JGlobals shareGlobal].wifiStatue != WifiStatueIntranet){
        [JGlobals shareGlobal].wifiStatue = WifiStatueUndefine;
    }
    
    
    if (!self.currentView) {
        return;
    }
    UIWindow* keyWindow =[[UIApplication sharedApplication] keyWindow];
    [keyWindow addSubview:self.blurView];
    self.blurView.frame = CGRectMake(0, 0, GETWIDTH(self.blurView), GETHEIGHT(self.blurView));
}

/**
 *  显示路由状态
 */
-(void)showRouterStatue{
    
    if (self.loading) {
        return;
    }
    for (JWifiDev* dev in self.routerInfo.wifiDev) {
        if ([dev.wifiType isEqualToString:@"2.4G"]) {
            self.title = dev.ssid;
        }
    }
    
    if (!self.title.length > 0) {
        JWifiDev* dev = self.routerInfo.wifiDev.firstObject;
        self.title = dev.ssid;
    }
    //如果APP进入后台
    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground) {
        return;
    }
    
    
    WifiStatue statue = [JGlobals shareGlobal].wifiStatue;
    self.headerView.statue = statue;
    [self setNavBackgroundColor];
    
    NSString* headerTitle = @"";
    NSString* headerTip = @"";
    NSString* buttomTitle = @"";
    NSString* menu1 = @"0";
    NSString* menu2 = @"0";
    NSString* menu3 = @"0";
    NSString* menu4 = @"0";
    UIImage* headerImage ;
    self.redView.hidden = YES;
    menu1 = [NSString stringWithFormat:@"%@",self.routerInfo.devN];
    menu2 = [NSString stringWithFormat:@"%lu",(unsigned long)[self.routerInfo.wifiDev count]];
    if (statue == WifiStatueCheckRouter) {
        [self starLoadingAnimation];
        headerTitle = @"检测路由连接";
        headerTip = @"";
        buttomTitle = @"检查中...";
        headerImage = [UIImage imageNamed:@"home_circle_blue"];
    }else{
        
        [self stopLoadingAnimation:YES];
        switch (statue) {
            case WifiStatueExtranet:
            {
                //外网断
                self.redView.hidden = NO;
                headerTitle = @"当前外网中断";
                headerTip = @"请检查网线或拨打运营商电话";
                self.redleftLabel.text = @"重设上网账号";
                self.redRightLabel.text = @"重启路由器";
                headerImage = [UIImage imageNamed:@"home_circle_red_pic"];
            }
                break;
            case WifiStatueIntranet:
            {
                self.redView.hidden = NO;
                //内网断
                menu1 = @"";
                menu2 = @"";
                headerTitle = @"当前内网中断";
                headerTip = @"请检查网线或查看WiFi连接正常";
                self.redleftLabel.text = @"WiFi连接";
                self.redRightLabel.text = @"WiFi设置";
                headerImage = [UIImage imageNamed:@"home_circle_red_pic"];
            }
                break;
            case WifiStatueNomal:
            {
                //正常状态
                headerTitle = @"当前网络畅通";
                headerTip = @"";
                ;
                
                buttomTitle = [NSString stringWithFormat:@"当前网速：%@",[self spedString:@( [self.routerInfo.speed floatValue])]];
                headerImage = [UIImage imageNamed:@"home_circle_blue"];
                menu1 = [NSString stringWithFormat:@"%@",self.routerInfo.devN];
                menu2 = [NSString stringWithFormat:@"%lu",(unsigned long)[self.routerInfo.wifiDev count]];
                headerImage = [UIImage imageNamed:@"home_circle_blue_pic"];
            }
                break;
                
                
            case WifiStatueSlow:
            {
                //正常状态
                headerTitle = @"";
                headerTip = @"您可尝试一下方式解决问题";
                buttomTitle =@"一键加速";
                headerImage = [UIImage imageNamed:@"home_circle_slow"];
                headerImage = [UIImage imageNamed:@"home_circle_slow"];
            }
                break;
            case WifiStatueUnKnow:{
                self.redView.hidden = NO;
                headerTitle = @"找不到路由器";
                headerTip = @"云平台检查不到您的路由器";
                self.redleftLabel.text = @"在同一网络下重新连接";
                self.redRightLabel.text = @"点击重新获取路由器";
                headerImage = [UIImage imageNamed:@"home_circle_red_pic"];
            }
                break;
                
            case WifiStatueUndefine:{
                self.redView.hidden = NO;
                //内网断
                headerTitle = @"找不到路由器";
                headerTip = @"云平台检查不到您的路由器";
                self.redleftLabel.text = @"在同一网络下重新连接";
                self.redRightLabel.text = @"点击重新获取路由器";
                headerImage = [UIImage imageNamed:@"home_circle_red_pic"];
            }
                break;
                
                
            default:
                break;
        }
    }
    
    if([menu1 isEqualToString:@"(null)"]){
        menu1 = @"0";
    }
    
    self.mainMenuNum1.text = menu1;
    self.mainMenuNum2.text = menu2;
    self.mainMenuNum3.text = menu3;
    self.mainMenuNum4.text = menu4;
    self.routerStatueImageVIew.image = headerImage;
    self.headerTitleLabel.text = headerTitle;
    self.headerTipsLabel.text = headerTip;
    self.buttomTipsLabel.text = buttomTitle;
}

- (IBAction)slow:(id)sender {
    
    if ([JGlobals shareGlobal].wifiStatue == WifiStatueSlow) {
        
        NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
        @weakify(self);
        
        [MBProgressHUD showMessag:@"优化中..." toView:self.navigationController.view.superview];
        
        [[JServerClient routerOptimization:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(id x) {
            @strongify(self);
            [MBProgressHUD hideAllHUDsForView:self.navigationController.view.superview animated:YES];
            [JGlobals shareGlobal].wifiStatue = WifiStatueCheckRouter;
            [self loadRouterData];
            [MBProgressHUD showSuccess:@"优化成功" toView:self.view];
        } error:^(NSError *error) {
            [MBProgressHUD hideAllHUDsForView:self.navigationController.view.superview animated:YES];
            
            [MBProgressHUD showError:@"优化出错，请稍后重试！" toView:self.view];
        }];
    }
}




-(NSString*)spedString:(NSNumber*)speedN{
    CGFloat speed = [speedN floatValue];
    if (speed >= 1024*1024) {
        CGFloat mSpeed = speed/ (1024*1024);
        mSpeed = mSpeed > 2000 ? 0 :mSpeed;
        return [NSString stringWithFormat:@"%.2f M/S",mSpeed];
    }else if(speed == 0 ){
        return @"0 B/S";
    }else{
        CGFloat mSpeed = speed / 1024;
        if(mSpeed < 1){
            return [NSString stringWithFormat:@"%.2f B/S",speed];
        }
        
        return [NSString stringWithFormat:@"%.2f KB/S",mSpeed];
    }
}


-(void)setNavBackgroundColor{
    if (self.headerView.headerColor) {
        self.nomarlView.backgroundColor = self.headerView.headerColor;
        [self.navigationController.navigationBar setBarTintColor:self.headerView.headerColor];
    }else{
        self.nomarlView.backgroundColor = kNomalColor;
        [self.navigationController.navigationBar setBarTintColor:kNomalColor];
    }
}

#pragma mark  路由状态设置


#pragma mark  动画设置
/**
 *  开始loading 的动画
 */
-(void)starLoadingAnimation{
    self.loadingView.hidden = NO;
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 ];
    rotationAnimation.duration = 1;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = MAXFLOAT;
    
    [self.loadingView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

/**
 *  关闭动画 并隐藏动画
 */
-(void)stopLoadingAnimation:(BOOL)hiddenView{
    self.loadingView.hidden = hiddenView;
    [self.loadingView.layer removeAllAnimations];
}


-(FXBlurView*)blurView{
    if (!_blurView) {
        _blurView = [[FXBlurView alloc] initWithFrame:self.view.bounds];
        [_blurView setDynamic:YES];
        _blurView.iterations = 0;
        UIImage* alertTip = [UIImage imageNamed:@"alertPic1"];
        UIImageView* imageView = [[UIImageView alloc] initWithImage:alertTip];
        imageView.frame = CGRectMake(GETWIDTH(_blurView)/2 -alertTip.size.width/2 , GETHEIGHT(_blurView)/2 - alertTip.size.height / 2, alertTip.size.width, alertTip.size.height);
        [_blurView addSubview:imageView];
        UIImage* close = [UIImage imageNamed:@"alertClose"];
        UIButton* btn = [[UIButton alloc] initWithFrame:CGRectMake(GETWIDTH(imageView) - close.size.width - 10, 10, close.size.width, close.size.height)];
        [btn setImage:close forState:UIControlStateNormal];
        [imageView addSubview:btn];
        imageView.userInteractionEnabled = YES;
        @weakify(self);
        [[btn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
            @strongify(self);
            if([JGlobals shareGlobal].wifiStatue != WifiStatueIntranet){
                [JGlobals shareGlobal].wifiStatue = WifiStatueUndefine;
            }
            [self.blurView removeFromSuperview];
            
            
        }];
    }
    return _blurView;
}


- (IBAction)tipsOptionAct:(id)sender {
    if([JGlobals shareGlobal].wifiStatue == WifiStatueSlow){
        //一键加速
        
    }
}
- (IBAction)rightOptionAct:(id)sender {
    
    if([JGlobals shareGlobal].wifiStatue == WifiStatueExtranet){
        //外网断  重启路由
        [self restartRouter];
    }else if([JGlobals shareGlobal].wifiStatue == WifiStatueIntranet){
        //内网断
        
        [self wifiSettings];
    }else if([JGlobals shareGlobal].wifiStatue == WifiStatueUndefine || [JGlobals shareGlobal].wifiStatue == WifiStatueUnKnow){
        //内网断
        [self loadRouterData];
        [JGlobals shareGlobal].wifiStatue = WifiStatueCheckRouter;
    }
}

- (IBAction)leftOptionAct:(id)sender {
    if([JGlobals shareGlobal].wifiStatue == WifiStatueExtranet){
        //外网断 重新设置上网账号
        [self setAccount];
    }else if([JGlobals shareGlobal].wifiStatue == WifiStatueIntranet){
        //内网断
        if(VSDeviceOSVersionIsAtLeast(8.0)){
            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            if ([[UIApplication sharedApplication] canOpenURL:url]) {
                [[UIApplication sharedApplication] openURL:url];
            }
        }else{
            NSURL*url=[NSURL URLWithString:@"prefs:root=WIFI"];
            if ([[UIApplication sharedApplication] canOpenURL:url]) {
                [[UIApplication sharedApplication] openURL:url];
            }
        }
    }else if([JGlobals shareGlobal].wifiStatue == WifiStatueUndefine){
        //找不到路由
        [self restartRouter];
    }
}



-(void)showMessagesView{
    [self performSegueWithIdentifier:@"Messages" sender:self];
}


@end
