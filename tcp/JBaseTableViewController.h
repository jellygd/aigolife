//
//  JBaseTableViewController.h
//  tcp
//
//  Created by Jelly on 15/7/26.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+JShake.h"
#import <MBProgressHUD.h>
#import "MBProgressHUD+JMessage.h"
#import <FXBlurView.h>

#define VSDeviceOSVersionIsAtLeast(X) ([[[UIDevice currentDevice]systemVersion] floatValue] >= X)


@interface JBaseTableViewController : UITableViewController



@property(nonatomic, assign) BOOL showBlur;

@property(nonatomic, strong) FXBlurView* blurNewView;


@property(nonatomic, assign) NSInteger searchCommad;

@property(nonatomic, assign) BOOL canNextTask;

-(void)commandFail;

-(void)commandSuccess;

-(void)commandNextSearch;


-(void)searchCommandStatue:(NSString*)commandIdentifier;

-(void)showErrorMessage:(NSString*)message;

-(void)loadRouterData;


-(void)loadRouterDataSuccess;

-(void)loadRouterDataFail;



-(void)nextTask;

-(void)taskAction;



-(void)showBlurView;


-(BOOL)showCanBlur;

@end
