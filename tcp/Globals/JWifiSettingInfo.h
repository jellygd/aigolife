//
//  JWifiSettingInfo.h
//  tcp
//
//  Created by Jelly on 15/7/23.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JWifiSettingInfo : NSObject

+(NSString *) routerIp;


+ (NSString *) routerAddressIp ;

+ (NSString *) localWiFiIPAddress;
@end
