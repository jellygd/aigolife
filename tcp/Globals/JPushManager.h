//
//  JPushManager.h
//  tcp
//
//  Created by Jelly on 15/8/30.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JPushManager : NSObject


// 单例
+ (instancetype) shareInstance ;


/**
 *  注册苹果推送
 */
-(void)registerAPNS;


-(void)didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;


@end
