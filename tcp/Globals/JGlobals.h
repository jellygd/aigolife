//
//  JGlobals.h
//  tcp
//
//  Created by Jelly on 15/7/21.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "JRouterInfo.h"
#import "JUserModule.h"
#import "JDeviceModule.h"

@interface JGlobals : NSObject

/**
 *  用户数据
 */
@property(nonatomic, strong) JUserModule* user;

/**
 *  用户账号
 */
@property(nonatomic, strong) NSString* userName;
/**
 *  用户token
 */
@property(nonatomic, strong) NSString* token;

/**
 *  连接token
 */
@property(nonatomic, strong, readonly) NSString* serverToken;

/**
 *  连接的MAC地址
 */
@property(nonatomic, strong, readonly) NSString* serverMAC;

/**
 *  路由token
 */
@property(nonatomic, strong) NSString* routerToken;


/**
 *  路由验证
 */
@property(nonatomic, assign) BOOL validateRouter;

/**
 *  有效期
 */
@property(nonatomic, strong) NSString*expire;


/**
 *  判断是否是Aigo路由
 */
@property(nonatomic, assign) BOOL isAigoRouter;

/**
 *  当前连接路由的MAC地址
 */
@property(nonatomic,strong) NSString* aigoRouterMac;

/**
 *  服务器URL 
 */
@property(nonatomic, strong, readonly) NSString* serverURL;

/**
 *  获取路由的url
 */
@property(nonatomic, strong, readonly) NSString* routerUrl;

/**
 *  获取用户绑定的路由
 */
@property(nonatomic,strong) JDeviceModule* deveicRouter;

/**
 *  view的缩小倍速
 */
@property(nonatomic, assign, readonly) CGFloat scale;

/**
 *  wifi状态
 */
@property(nonatomic, assign) WifiStatue wifiStatue;


@property(nonatomic, strong) JRouterInfo* routerInfo;


@property(nonatomic, assign,readonly) BOOL canShake;


@property(nonatomic, assign,readonly) NSInteger shakeid;


@property(nonatomic, copy) NSString* adminPWd;

@property(nonatomic, assign) BOOL isSet;

@property(nonatomic, strong) NSString* routerMAC;


@property(nonatomic, assign) BOOL push;


/**
 *  引导界面
 */
@property(nonatomic, strong) JWanNet* guideWannet;
/**
 *  引导界面
 */
@property(nonatomic, strong) NSString* guideAdminPWD;

/**
 *  连接的URL
 */
@property(nonatomic, strong,readonly) NSString* url;

/**
 *  获取当前wifiSSID
 *
 *  @return SSID
 */
- (NSString *)currentWifiSSID;

/**
 *  设置摇一摇类型
 *
 *  @param type 类型id
 */
-(void)setShakeType:(NSInteger)type;

/**
 *  单例
 *
 *  @return JGlobals
 */
+ (JGlobals *)shareGlobal;








@end
