//
//  JFoundation.h
//  tcp
//  宏的定义 跟 枚举的定义
//  Created by Jelly on 15/7/20.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#ifndef tcp_JFoundation_h
#define tcp_JFoundation_h


#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]





///获取居中的CGRect
#define CGRectCenter(superRect,subRect) \
CGRectMake((superRect.size.width-subRect.size.width)/2, (superRect.size.height-subRect.size.height)/2,  subRect.size.width, subRect.size.height)


///设置视图frame的x
#define CGRectSetX(viewRect,x) \
CGRectMake(x, view.origin.y, view.size.width, view.size.height)


///设置视图frame的y
#define CGRectSetY(viewRect,y) \
CGRectMake(viewRect.origin.x, y, viewRect.size.width, viewRect.size.height)

#define GETHEIGHT(view) CGRectGetHeight(view.frame)
#define GETWIDTH(view) CGRectGetWidth(view.frame)
#define GETORIGIN_X(view) view.frame.origin.x
#define GETORIGIN_Y(view) view.frame.origin.y
#define GETRIGHTORIGIN_X(view) view.frame.origin.x + CGRectGetWidth(view.frame)
#define GETBOTTOMORIGIN_Y(view) view.frame.origin.y + CGRectGetHeight(view.frame)


#pragma mark - 系统 部分

#define IOS_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]   //当前设备的系统版本
#define CurrentSystemVersion ([[UIDevice currentDevice] systemVersion])     //当前的系统版本
#define IS_IOS6() [[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0


#pragma mark - Path
#define kHomePath        NSHomeDirectory()
#define kCachePath      [NSHomeDirectory() stringByAppendingPathComponent:@"Library/Caches"]
#define kDocumentFolder [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]


#pragma mark - 功能部分
#ifdef DEBUG
#   define DLog(...) NSLog((@"%s [Line %d] %@"), __PRETTY_FUNCTION__, __LINE__, [NSString stringWithFormat:__VA_ARGS__])
#   define SLog(...) NSLog(__VA_ARGS__)
#else
#   define DLog(...)
#   define SLog(...)
#endif


#define iPhone4 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)
#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
#define iPhone6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO)
#define iPhone6p ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size) : NO)



#define FIX_EDGES_FOR_EXTENDEDLAYTOU if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) \
{ \
self.edgesForExtendedLayout = UIRectEdgeNone;\
} \




typedef enum : NSUInteger {
    WifiStatueCheckRouter, //检测路由
    WifiStatueNomal, //正常
    WifiStatueSlow, //慢
    WifiStatueExtranet, //外网断
    WifiStatueIntranet, //内网断
    WifiStatueUnKnow, //未知错误
    WifiStatueUndefine, //未发现路由
} WifiStatue;

#define kDefaultAdminPWD @"admin"



#import "iConsole.h"
//TODO: 上线前关掉
#define NSLog(...) {NSLog(__VA_ARGS__); [iConsole log:__VA_ARGS__];}


#endif
