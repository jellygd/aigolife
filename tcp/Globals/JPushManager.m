//
//  JPushManager.m
//  tcp
//
//  Created by Jelly on 15/8/30.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JPushManager.h"

#define VSDeviceOSVersionIsAtLeast(X) ([[[UIDevice currentDevice]systemVersion] floatValue] >= X)


static JPushManager *shareInstance = nil ;

@implementation JPushManager

+ (instancetype) shareInstance {
    if (!shareInstance) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            if (!shareInstance) {
                shareInstance = [[JPushManager alloc] init] ;
            }
        });
    }
    
    return shareInstance ;
}

-(void)registerAPNS{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
    if(VSDeviceOSVersionIsAtLeast(8.0)){
        UIUserNotificationSettings *uns = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound) categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:uns];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
    }else{
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert];
    }
#else
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert];
#endif
}


-(void)didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    NSLog(@"DeviceToken: {%@}",deviceToken);
    NSString *deviceTokenString = [deviceToken description];
    if ([deviceTokenString characterAtIndex:0] == '<') {
        NSRange range = {1, [deviceTokenString length]-2};
        deviceTokenString =[deviceTokenString substringWithRange:range];
    }
    deviceTokenString  = [deviceTokenString stringByReplacingOccurrencesOfString:@" " withString:@""];
    [[NSUserDefaults standardUserDefaults] setObject:deviceTokenString forKey:@"deviceToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
