//
//  VSAvailability.h
//  Common
//
//  Created by linwaiwai on 2/16/15.
//  Copyright (c) 2015 Vipshop Holdings Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

//判断是否运行在模拟器中
#define isSimulator (NSNotFound != [[[UIDevice currentDevice] model] rangeOfString:@"Simulator"].location)

//判断iOS系统版本
#ifndef VSDeviceOSVersionIsAtLeast
#define VSDeviceOSVersionIsAtLeast(X) ([[[UIDevice currentDevice]systemVersion] floatValue] >= X)
#endif


#ifndef VSDeviceOSVersionIsLessThan
#define VSDeviceOSVersionIsLessThan(X)  ([[[UIDevice currentDevice]systemVersion] floatValue] < X)
#endif


#define VSOSVersion ([[[UIDevice currentDevice]systemVersion] floatValue])


// 判断iOS硬件
#define VSIsPad UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad
#define VSIsPhone [[UIDevice currentDevice].model isEqualToString:@"iPhone"]


#define VSIsIPhone4S (([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO) || ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(960, 640), [[UIScreen mainScreen] currentMode].size) : NO))


//判断iphone5/5S
#define VSIsIPhone5 (([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO) || ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1136, 640), [[UIScreen mainScreen] currentMode].size) : NO))


//判断iphone6
#define VSIsIPhone6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) || CGSizeEqualToSize(CGSizeMake(1334, 750), [[UIScreen mainScreen] currentMode].size)) : NO)


//判断iphone6+
#define VSIsIPhone6Plus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size) ||  CGSizeEqualToSize(CGSizeMake(2208, 1242), [[UIScreen mainScreen] currentMode].size) || CGSizeEqualToSize(CGSizeMake(1125, 2001), [[UIScreen mainScreen] currentMode].size) ||CGSizeEqualToSize(CGSizeMake(2001, 1125), [[UIScreen mainScreen] currentMode].size)): NO)


