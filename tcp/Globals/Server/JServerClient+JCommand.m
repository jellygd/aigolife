//
//  JServerClient+JCommand.m
//  tcp
//  指令的控制
//  Created by Jelly on 15/7/28.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JServerClient+JCommand.h"
#import "NSString+JURL.h"

@implementation JServerClient (JCommand)


+(RACSignal*)optionFile:(NSString*)sn oldFilePath:(NSString*)oldFilePath newFilePath:(NSString*)newFilePath token:(NSString*)token appver:(NSString*)version  option:(NSString*)option{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token?token:@"" forKey:@"token"];
    [userDictionary setObject:sn?sn:@"" forKey:@"sn"];
    [userDictionary setObject:version?version:@"" forKey:@"appver"];
    [userDictionary setObject:@"files" forKey:@"type"];
    NSMutableDictionary* data = [NSMutableDictionary dictionary];
    [data setObject:option forKey:@"type"];
    if (newFilePath.length > 0) {
      [data setObject:newFilePath  forKey:@"newFilePath"];
    }
    if (oldFilePath.length > 0) {
        [data setObject:oldFilePath forKey:@"oldFilePath"];
    }
    
    //    [userDictionary setObject:[self dicToString:data] forKey:@"data"];
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString =@"";
    if ([jsonData length] > 0 ){
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"JSON String = %@", jsonString);
    }
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    [userDictionary setObject:jsonString forKey:@"data"];
    
    [userDictionary setObject:@"files" forKey:@"type"];
    NSString* server = [NSString stringWithFormat:@"%@/router/sendCommand.json",[JGlobals shareGlobal].isAigoRouter ? [JGlobals shareGlobal].routerUrl : [JGlobals shareGlobal].serverURL];
    
    return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@""];
}

+(RACSignal*)bindRouter:(NSString*)account sn:(NSString*)sn token:(NSString*)token appver:(NSString*)version{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token?token:@"" forKey:@"token"];
    [userDictionary setObject:sn?sn:@"" forKey:@"sn"];
    [userDictionary setObject:version?version:@"" forKey:@"appver"];
    [userDictionary setObject:@"control" forKey:@"type"];
    
    NSMutableDictionary* data = [NSMutableDictionary dictionary];
    [data setObject:account forKey:@"aigoid"];
    [data setObject:@"bind" forKey:@"type"];
    //
    //    [userDictionary setObject:[self dicToString:data] forKey:@"data"];
    //
    //    return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/router/sendCommand.json"] parameter:userDictionary action:@""];
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString =@"";
    if ([jsonData length] > 0 ){
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"JSON String = %@", jsonString);
    }
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    [userDictionary setObject:jsonString forKey:@"data"];
    
    NSString* server = [NSString stringWithFormat:@"%@/router/sendCommand.json",[JGlobals shareGlobal].isAigoRouter ? [JGlobals shareGlobal].routerUrl : [JGlobals shareGlobal].serverURL];
    
    return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@""];
}


+(RACSignal*)unbindRouter:(NSString*)account sn:(NSString*)sn token:(NSString*)token appver:(NSString*)version{

    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token?token:@"" forKey:@"token"];
    [userDictionary setObject:sn?sn:@"" forKey:@"sn"];
    [userDictionary setObject:version?version:@"" forKey:@"appver"];
    
    NSMutableDictionary* data = [NSMutableDictionary dictionary];
    [data setObject:account forKey:@"aigoid"];
    [data setObject:@"unbind" forKey:@"type"];
    //
    //    [userDictionary setObject:[self dicToString:data] forKey:@"data"];
    //
    //    return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/router/sendCommand.json"] parameter:userDictionary action:@""];
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString =@"";
    if ([jsonData length] > 0 ){
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"JSON String = %@", jsonString);
    }
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    [userDictionary setObject:jsonString forKey:@"data"];
    
    [userDictionary setObject:@"control" forKey:@"type"];
    NSString* server = [NSString stringWithFormat:@"%@/router/sendCommand.json",[JGlobals shareGlobal].isAigoRouter ? [JGlobals shareGlobal].routerUrl : [JGlobals shareGlobal].serverURL];
    
    return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@""];
}

+(RACSignal*)assistant:(NSInteger)channel level:(NSInteger)signal_level sn:(NSString*)sn token:(NSString*)token appver:(NSString*)version{

    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token?token:@"" forKey:@"token"];
    [userDictionary setObject:sn?sn:@"" forKey:@"sn"];
    [userDictionary setObject:version?version:@"" forKey:@"appver"];
    [userDictionary setObject:@"configure_net_assistant" forKey:@"type"];
    
    NSMutableDictionary* data = [NSMutableDictionary dictionary];
    [data setObject:@(channel) forKey:@"channel"];
    [data setObject:@(signal_level) forKey:@"signal_level"];
//    
//    [userDictionary setObject:[self dicToString:data] forKey:@"data"];
//    
//    return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/router/sendCommand.json"] parameter:userDictionary action:@""];
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString =@"";
    if ([jsonData length] > 0 ){
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"JSON String = %@", jsonString);
    }
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    [userDictionary setObject:jsonString forKey:@"data"];
    
    [userDictionary setObject:@"configure_net_assistant" forKey:@"type"];
    NSString* server = [NSString stringWithFormat:@"%@/router/sendCommand.json",[JGlobals shareGlobal].isAigoRouter ? [JGlobals shareGlobal].routerUrl : [JGlobals shareGlobal].serverURL];
    
    return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@""];
}


+(RACSignal*)resetAdminPasss:(NSString*)oldPass newPass:(NSString*)newPass sn:(NSString*)sn token:(NSString*)token appver:(NSString*)version{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token?token:@"" forKey:@"token"];
    [userDictionary setObject:sn?sn:@"" forKey:@"sn"];
    [userDictionary setObject:version?version:@"" forKey:@"appver"];
    NSMutableDictionary* data = [NSMutableDictionary dictionary];
    [data setObject:oldPass forKey:@"old_pwd"];
    [data setObject:newPass forKey:@"new_pwd"];
    
    
    
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString =@"";
    if ([jsonData length] > 0 ){
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"JSON String = %@", jsonString);
    }
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    [userDictionary setObject:jsonString forKey:@"data"];
    
    [userDictionary setObject:@"configure_admin_account" forKey:@"type"];
    NSString* server = [NSString stringWithFormat:@"%@/router/sendCommand.json",[JGlobals shareGlobal].isAigoRouter ? [JGlobals shareGlobal].routerUrl : [JGlobals shareGlobal].serverURL];
    
    return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@""];

}



+(RACSignal*)setBluetooth:(BOOL)on sn:(NSString*)sn token:(NSString*)token appver:(NSString*)version{

    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token?token:@"" forKey:@"token"];
    [userDictionary setObject:sn?sn:@"" forKey:@"sn"];
    [userDictionary setObject:version?version:@"" forKey:@"appver"];
    [userDictionary setObject:@"configure_bluetooth" forKey:@"type"];
    
    NSMutableDictionary* data = [NSMutableDictionary dictionary];
    [data setObject:@(on ? 1 : 0) forKey:@"bleSwitch"];
//     
//    [userDictionary setObject:[NSString stringWithFormat:@"{\"bleSwitch\":%@}",@(on)] forKey:@"data"];
//   return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/router/sendCommand.json"] parameter:userDictionary action:@""];
    
    
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString =@"";
    if ([jsonData length] > 0 ){
        NSLog(@"Successfully serialized the dictionary into data.");
        //NSData转换为String
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"JSON String = %@", jsonString);
    }
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    [userDictionary setObject:jsonString forKey:@"data"];
    NSString* server = [NSString stringWithFormat:@"%@/router/sendCommand.json",[JGlobals shareGlobal].url];
    return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@""];
}

+(RACSignal*)queryCommandStatus:(NSString*)identifier sn:(NSString*)sn token:(NSString*)token{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token?token:@"" forKey:@"token"];
    [userDictionary setObject:sn?sn:@"" forKey:@"sn"];
    [userDictionary setObject:identifier forKey:@"id"];
    return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/router/queryCommandStatus.json"] parameter:userDictionary action:@""];
}


+(RACSignal*)setWifi:(JWifiDev*)dev1 dev:(JWifiDev*)dev2 sn:(NSString*)sn token:(NSString*)token appver:(NSString*)version{
    
    
    
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token?token:@"" forKey:@"token"];
    [userDictionary setObject:sn?sn:@"" forKey:@"sn"];
    [userDictionary setObject:version?version:@"" forKey:@"appver"];
    
    
    
    NSDictionary* wifiDev = @{@"wifi_dev":@[[MTLJSONAdapter JSONDictionaryFromModel:dev1 error:nil],[MTLJSONAdapter JSONDictionaryFromModel:dev2 error:nil]]};
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:wifiDev options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString =@"";
    if ([jsonData length] > 0 ){
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"JSON String = %@", jsonString);
    }
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    //    jsonString = [NSString stringWithFormat:@"%@",[NSArray arrayWithObject:jsonString]];
    //    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    //    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    [userDictionary setObject:jsonString forKey:@"data"];
    [userDictionary setObject:@"configure_wifi" forKey:@"type"];
    NSString* server = [NSString stringWithFormat:@"%@/router/sendCommand.json",[JGlobals shareGlobal].isAigoRouter ? [JGlobals shareGlobal].routerUrl : [JGlobals shareGlobal].serverURL];
    
    return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@""];
}

+(RACSignal*)setWifi:(NSString*)ssid switch:(BOOL)wifiSwitch type:(NSString*)wifitype encrypt:(NSString*)encrypt password:(NSString*)password sn:(NSString*)sn token:(NSString*)token appver:(NSString*)version{

    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token?token:@"" forKey:@"token"];
    [userDictionary setObject:sn?sn:@"" forKey:@"sn"];
    [userDictionary setObject:version?version:@"" forKey:@"appver"];
 
    NSMutableDictionary* data = [NSMutableDictionary dictionary];
    [data setObject:wifiSwitch?@(1):@(0) forKey:@"wifiSwitch"];
    [data setObject:ssid forKey:@"ssid"];
    [data setObject:@"2.4G" forKey:@"wifi_type"];
    [data setObject:@"WPAPSK/WPA2PSK" forKey:@"encrypt"];
    [data setObject:password forKey:@"password"];
    
    NSMutableDictionary* data5G = [NSMutableDictionary dictionary];
    [data5G setObject:wifiSwitch?@(1):@(0) forKey:@"wifiSwitch"];
    [data5G setObject:[NSString stringWithFormat:@"%@_5G",ssid] forKey:@"ssid"];
    [data5G setObject:@"5G" forKey:@"wifi_type"];
    [data5G setObject:@"WPAPSK/WPA2PSK" forKey:@"encrypt"];
    [data5G setObject:password forKey:@"password"];
    
    NSDictionary* wifiDev = @{@"wifi_dev":@[data,data5G]};
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:wifiDev options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString =@"";
    if ([jsonData length] > 0 ){
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"JSON String = %@", jsonString);
    }
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
//    jsonString = [NSString stringWithFormat:@"%@",[NSArray arrayWithObject:jsonString]];
//    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
//    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    [userDictionary setObject:jsonString forKey:@"data"];
    [userDictionary setObject:@"configure_wifi" forKey:@"type"];
    NSString* server = [NSString stringWithFormat:@"%@/router/sendCommand.json",[JGlobals shareGlobal].isAigoRouter ? [JGlobals shareGlobal].routerUrl : [JGlobals shareGlobal].serverURL];
    
    return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@""];
}

+(RACSignal*)setJeck:(NSString*)routerSSID pwd:(NSString*)routerPWD sn:(NSString*)sn token:(NSString*)token appver:(NSString*)version{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token?token:@"" forKey:@"token"];
    [userDictionary setObject:sn?sn:@"" forKey:@"sn"];
    [userDictionary setObject:version?version:@"" forKey:@"appver"];
    NSMutableDictionary* data = [NSMutableDictionary dictionary];
    
    [data setObject:routerSSID forKey:@"routerSSID"];
    [data setObject:routerPWD forKey:@"routerPWD"];
//    [userDictionary setObject:[self dicToString:data] forKey:@"data"];
    [userDictionary setObject:@"configure_wifi" forKey:@"type"];
//    return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/router/sendCommand.json"] parameter:userDictionary action:@""];
    
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString =@"";
    if ([jsonData length] > 0 ){
        NSLog(@"Successfully serialized the dictionary into data.");
        //NSData转换为String
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"JSON String = %@", jsonString);
    }
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    [userDictionary setObject:jsonString forKey:@"data"];
    NSString* server = [NSString stringWithFormat:@"%@/router/sendCommand.json",[JGlobals shareGlobal].isAigoRouter ? [JGlobals shareGlobal].routerUrl : [JGlobals shareGlobal].serverURL];
    return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@""];
    
}

+(RACSignal*)restoreRouter:(NSString*)sn token:(NSString*)token appver:(NSString*)version{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token?token:@"" forKey:@"token"];
    [userDictionary setObject:sn?sn:@"" forKey:@"sn"];
    [userDictionary setObject:version?version:@"" forKey:@"appver"];
    NSMutableDictionary* data = [NSMutableDictionary dictionary];
    [userDictionary setObject:@"control" forKey:@"type"];
    
    [data setObject:@"restore" forKey:@"type"];
//    [userDictionary setObject:[self dicToString:data] forKey:@"data"];
//    
//    return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/router/sendCommand.json"] parameter:userDictionary action:@""];

    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString =@"";
    if ([jsonData length] > 0 ){
        NSLog(@"Successfully serialized the dictionary into data.");
        //NSData转换为String
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"JSON String = %@", jsonString);
    }
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    [userDictionary setObject:jsonString forKey:@"data"];
    NSString* server = [NSString stringWithFormat:@"%@/router/sendCommand.json",[JGlobals shareGlobal].isAigoRouter ? [JGlobals shareGlobal].routerUrl : [JGlobals shareGlobal].serverURL];
    return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@""];
}


+(RACSignal*)restartRouter:(NSString*)sn token:(NSString*)token appver:(NSString*)version{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token forKey:@"token"];
    [userDictionary setObject:sn?sn:@"" forKey:@"sn"];
    [userDictionary setObject:version?version:@"" forKey:@"appver"];
    [userDictionary setObject:@"control" forKey:@"type"];
    
    NSMutableDictionary* data = [NSMutableDictionary dictionary];
    [data setObject:@"restart" forKey:@"type"];
//    [userDictionary setObject:[self dicToString:data] forKey:@"data"];
//    
//    return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/router/sendCommand.json"] parameter:userDictionary action:@""];
//    
    
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString =@"";
    if ([jsonData length] > 0 ){
        NSLog(@"Successfully serialized the dictionary into data.");
        //NSData转换为String
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"JSON String = %@", jsonString);
    }
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    [userDictionary setObject:jsonString forKey:@"data"];
    NSString* server = [NSString stringWithFormat:@"%@/router/sendCommand.json",[JGlobals shareGlobal].isAigoRouter ? [JGlobals shareGlobal].routerUrl : [JGlobals shareGlobal].serverURL];
    return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@""];
    
}


+(RACSignal*)unbindRouter:(NSString*)sn token:(NSString*)token appver:(NSString*)version{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token?token:@"" forKey:@"token"];
    [userDictionary setObject:sn?sn:@"" forKey:@"sn"];
    [userDictionary setObject:version?version:@"" forKey:@"appver"];
    [userDictionary setObject:@"control" forKey:@"type"];
    
    NSMutableDictionary* data = [NSMutableDictionary dictionary];
    [data setObject:@"unbind" forKey:@"type"];
    
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString =@"";
    if ([jsonData length] > 0 ){
        NSLog(@"Successfully serialized the dictionary into data.");
        //NSData转换为String
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"JSON String = %@", jsonString);
    }
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    [userDictionary setObject:jsonString forKey:@"data"];
    NSString* server = [NSString stringWithFormat:@"%@/router/sendCommand.json",[JGlobals shareGlobal].url];
    return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@""];
    
}

+(RACSignal*)bindRouter:(NSString*)sn token:(NSString*)token appver:(NSString*)version{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token?token:@"" forKey:@"token"];
    [userDictionary setObject:sn?sn:@"" forKey:@"sn"];
    [userDictionary setObject:version?version:@"" forKey:@"appver"];
    NSMutableDictionary* data = [NSMutableDictionary dictionary];
    [data setObject:@"bind" forKey:@"type"];
    [userDictionary setObject:@"control" forKey:@"type"];
    
//    [userDictionary setObject:[self dicToString:data] forKey:@"data"];
//    
//    return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/router/sendCommand.json"] parameter:userDictionary action:@""];
    
    
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString =@"";
    if ([jsonData length] > 0 ){
        NSLog(@"Successfully serialized the dictionary into data.");
        //NSData转换为String
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"JSON String = %@", jsonString);
    }
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    [userDictionary setObject:jsonString forKey:@"data"];
    NSString* server = [NSString stringWithFormat:@"%@/router/sendCommand.json",[JGlobals shareGlobal].isAigoRouter ? [JGlobals shareGlobal].routerUrl : [JGlobals shareGlobal].serverURL];
    return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@""];
}

+(RACSignal*)BindAigoAccount:(NSString*)UserName sn:(NSString*)sn token:(NSString*)token appver:(NSString*)appver{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token?token:@"" forKey:@"token"];
    [userDictionary setObject:sn?sn:@"" forKey:@"sn"];
    [userDictionary setObject:appver?appver:@"" forKey:@"appver"];
    [userDictionary setObject:@"bind_aigoaccount" forKey:@"type"];
    NSMutableDictionary* data = [NSMutableDictionary dictionary];
    [data setObject:UserName forKey:@"user_name"];
    
    
    //    [userDictionary setObject:[self dicToString:data] forKey:@"data"];
    //
    //    return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/router/sendCommand.json"] parameter:userDictionary action:@""];
    
    
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString =@"";
    if ([jsonData length] > 0 ){
        NSLog(@"Successfully serialized the dictionary into data.");
        //NSData转换为String
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"JSON String = %@", jsonString);
    }
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    [userDictionary setObject:jsonString forKey:@"data"];
    NSString* server = [NSString stringWithFormat:@"%@/router/sendCommand.json",[JGlobals shareGlobal].isAigoRouter ? [JGlobals shareGlobal].routerUrl : [JGlobals shareGlobal].serverURL];
    return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@""];
}



+(RACSignal*)unBindAigoAccount:(NSString*)UserName sn:(NSString*)sn token:(NSString*)token appver:(NSString*)appver{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token?token:@"" forKey:@"token"];
    [userDictionary setObject:sn?sn:@"" forKey:@"sn"];
    [userDictionary setObject:appver?appver:@"" forKey:@"appver"];
    [userDictionary setObject:@"unbind_aigoaccount" forKey:@"type"];
    NSMutableDictionary* data = [NSMutableDictionary dictionary];
    [data setObject:UserName forKey:@"user_name"];
    
    
    //    [userDictionary setObject:[self dicToString:data] forKey:@"data"];
    //
    //    return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/router/sendCommand.json"] parameter:userDictionary action:@""];
    
    
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString =@"";
    if ([jsonData length] > 0 ){
        NSLog(@"Successfully serialized the dictionary into data.");
        //NSData转换为String
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"JSON String = %@", jsonString);
    }
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    [userDictionary setObject:jsonString forKey:@"data"];
    NSString* server = [NSString stringWithFormat:@"%@/router/sendCommand.json",[JGlobals shareGlobal].isAigoRouter ? [JGlobals shareGlobal].routerUrl : [JGlobals shareGlobal].serverURL];
    return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@""];
}


+(RACSignal*)upgradeRouter:(NSString*)sn token:(NSString*)token appver:(NSString*)version{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token?token:@"" forKey:@"token"];
    [userDictionary setObject:sn?sn:@"" forKey:@"sn"];
    [userDictionary setObject:version?version:@"" forKey:@"appver"];
    NSMutableDictionary* data = [NSMutableDictionary dictionary];
    [data setObject:@"upgrade" forKey:@"type"];
    [userDictionary setObject:[self dicToString:data] forKey:@"data"];
    [userDictionary setObject:@"control" forKey:@"type"];
    return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/router/sendCommand.json"] parameter:userDictionary action:@""];
}


+(RACSignal*)Anti:(BOOL)network harassment:(BOOL)harassment  sn:(NSString*)sn token:(NSString*)token appver:(NSString*)version{
    NSMutableDictionary* data = [NSMutableDictionary dictionary];
    [data setObject:@(network) forKey:@"anti_rub_network"];
    [data setObject:@(harassment) forKey:@"anti_harassment"];
    
    
     NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:@"configure_anti_rub" forKey:@"type"];
    [userDictionary setObject:token?token:@"" forKey:@"token"];
    [userDictionary setObject:sn?sn:@"" forKey:@"sn"];
    [userDictionary setObject:version forKey:@"appver"];
    
    
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString =@"";
    if ([jsonData length] > 0 ){
        NSLog(@"Successfully serialized the dictionary into data.");
        //NSData转换为String
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"JSON String = %@", jsonString);
    }
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    [userDictionary setObject:jsonString forKey:@"data"];
    NSString* server = [NSString stringWithFormat:@"%@/router/sendCommand.json",[JGlobals shareGlobal].isAigoRouter ? [JGlobals shareGlobal].routerUrl : [JGlobals shareGlobal].serverURL];
    return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@""];
    
}



+(RACSignal*)setRouterWan:(NSDictionary*)json sn:(NSString*)sn token:(NSString*)token appver:(NSString*)version{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token?token:@"" forKey:@"token"];
    [userDictionary setObject:sn?sn:@"" forKey:@"sn"];
    [userDictionary setObject:version?version:@"" forKey:@"appver"];
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:json options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString =@"";
    if ([jsonData length] > 0 ){
        NSLog(@"Successfully serialized the dictionary into data.");
        //NSData转换为String
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"JSON String = %@", jsonString);
    }
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    [userDictionary setObject:jsonString forKey:@"data"];
    [userDictionary setObject:@"configure_wan" forKey:@"type"];

    NSString* server = [NSString stringWithFormat:@"%@/router/sendCommand.json",[JGlobals shareGlobal].isAigoRouter ? [JGlobals shareGlobal].routerUrl : [JGlobals shareGlobal].serverURL];
    return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@""];
}

+(RACSignal*)setAdminAccount:(NSString*)oldPWD newPWD:(NSString*)newPWD sn:(NSString*)sn token:(NSString*)token appver:(NSString*)version{

    NSMutableDictionary* data = [NSMutableDictionary dictionary];
    [data setObject:oldPWD forKey:@"old_pwd"];
    [data setObject:newPWD forKey:@"new_pwd"];
    
    
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:@"configure_admin_account" forKey:@"type"];
    [userDictionary setObject:token?token:@"" forKey:@"token"];
    [userDictionary setObject:sn?sn:@"" forKey:@"sn"];
    [userDictionary setObject:version forKey:@"appver"];
    
    
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString =@"";
    if ([jsonData length] > 0 ){
        NSLog(@"Successfully serialized the dictionary into data.");
        //NSData转换为String
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"JSON String = %@", jsonString);
    }
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    [userDictionary setObject:jsonString forKey:@"data"];
    NSString* server = [NSString stringWithFormat:@"%@/router/sendCommand.json",[JGlobals shareGlobal].isAigoRouter ? [JGlobals shareGlobal].routerUrl : [JGlobals shareGlobal].serverURL];
    return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@""];
}


+(RACSignal*)setDevices:(NSArray*)devices sn:(NSString*)sn token:(NSString*)token appver:(NSString*)version{

    NSArray* jsons = [MTLJSONAdapter JSONArrayFromModels:devices error:nil];
    NSDictionary* dic = @{@"dev":jsons,@"black_list":@(1)};
    
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:@"devices" forKey:@"type"];
    [userDictionary setObject:token?token:@"" forKey:@"token"];
    [userDictionary setObject:sn?sn:@"" forKey:@"sn"];
    [userDictionary setObject:version forKey:@"appver"];

    
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString =@"";
    if ([jsonData length] > 0 ){
        NSLog(@"Successfully serialized the dictionary into data.");
        //NSData转换为String
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"JSON String = %@", jsonString);
    }
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"null" withString:@"\"\""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"true" withString:@"1"];
    [userDictionary setObject:jsonString forKey:@"data"];
    NSString* server = [NSString stringWithFormat:@"%@/router/sendCommand.json",[JGlobals shareGlobal].isAigoRouter ? [JGlobals shareGlobal].routerUrl : [JGlobals shareGlobal].serverURL];
    return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@""];
}


+(NSString*)dicToString:(NSDictionary*)dic{
    NSString* dataString = @"{";
    for (NSString* key  in [dic allKeys]) {
        id value = dic[key];
        if ([value isKindOfClass:[NSString class]]) {
            dataString = [dataString stringByAppendingString: [NSString stringWithFormat:@"\"%@\":\"%@\"",key,value]];
        }else{
            dataString = [dataString stringByAppendingString:  [NSString stringWithFormat:@"\"%@\":%@",key,value]];
        }
        dataString = [dataString stringByAppendingString:@","];
    }
    if ([dataString length] > 1) {
        dataString = [dataString substringToIndex:[dataString length] -1];
    }
    dataString = [dataString stringByAppendingString:@"}"];
    return dataString;
}


+(RACSignal*)speedTest:(NSString*)sn token:(NSString*)token appver:(NSString*)version{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:@"speed_test" forKey:@"type"];
    [userDictionary setObject:token?token:@"" forKey:@"token"];
    [userDictionary setObject:sn?sn:@"" forKey:@"sn"];
    [userDictionary setObject:version forKey:@"appver"];
    NSString* server = [NSString stringWithFormat:@"%@/router/sendCommand.json",[JGlobals shareGlobal].url];
    return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@""];
}



+(RACSignal*)setSpeedLimit:(BOOL)limit mode:(NSInteger)model speedLimitDevs:(NSArray*)devs sn:(NSString*)sn token:(NSString*)token appver:(NSString*)version{

    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:@"speed_test" forKey:@"type"];
    [userDictionary setObject:token?token:@"" forKey:@"token"];
    [userDictionary setObject:sn?sn:@"" forKey:@"sn"];
    [userDictionary setObject:version forKey:@"appver"];
    
    NSMutableDictionary* dic = [NSMutableDictionary dictionary];
    [dic setObject:limit?@(1):@(0) forKey:@"smart_limit"];
    [dic setObject:@(model) forKey:@"smart_mode"];
    [dic setObject:[MTLJSONAdapter JSONArrayFromModels:devs error:nil] forKey:@"speed_limit"];
    
    
    
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString =@"";
    if ([jsonData length] > 0 ){
        NSLog(@"Successfully serialized the dictionary into data.");
        //NSData转换为String
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"JSON String = %@", jsonString);
    }
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"null" withString:@"\"\""];
    
    NSString* server = [NSString stringWithFormat:@"%@/router/sendCommand.json",[JGlobals shareGlobal].url];
    return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@""];
}



+(RACSignal*)routerUpgrade:(JFirmwareObject*)firware sn:(NSString*)sn token:(NSString*)token appver:(NSString*)appver{

    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:@"control_upgrade" forKey:@"type"];
    [userDictionary setObject:token?token:@"" forKey:@"token"];
    [userDictionary setObject:sn?sn:@"" forKey:@"sn"];
    [userDictionary setObject:appver forKey:@"appver"];
    
    NSMutableDictionary* dic = [NSMutableDictionary dictionary];
  
    NSDictionary* fir = [MTLJSONAdapter JSONDictionaryFromModel:firware error:nil];
    [dic setObject:@"0" forKey:@"force"];
    [dic setObject:fir forKey:@"info"];
    
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString =@"";
    if ([jsonData length] > 0 ){
        NSLog(@"Successfully serialized the dictionary into data.");
        //NSData转换为String
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"JSON String = %@", jsonString);
    }
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"null" withString:@"\"\""];
    
    [userDictionary setObject:jsonString forKey:@"data"];
    
    NSString* server = [NSString stringWithFormat:@"%@/router/sendCommand.json",[JGlobals shareGlobal].url];
    return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@""];
}


+(RACSignal*)routerOptimization:(NSString*)sn token:(NSString*)token appver:(NSString*)appver{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:@"network_optimization" forKey:@"type"];
    [userDictionary setObject:token?token:@"" forKey:@"token"];
    [userDictionary setObject:sn?sn:@"" forKey:@"sn"];
    [userDictionary setObject:appver forKey:@"appver"];
    
    
    NSString* server = [NSString stringWithFormat:@"%@/router/sendCommand.json",[JGlobals shareGlobal].url];
    return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@""];
}

@end
