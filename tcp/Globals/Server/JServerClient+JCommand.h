//
//  JServerClient+JCommand.h
//  tcp
//
//  Created by Jelly on 15/7/28.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JServerClient.h"
#import "JCommad.h"
#import "JFirmwareObject.h"

@interface JServerClient (JCommand)


/**
 *  路由存储操作
 *
 *  @param sn          mac地址
 *  @param oldFilePath 旧路径
 *  @param newFilepath 新路径
 *  @param token       token
 *  @param version     版本号
 *  @param option      操作符
 *
 *  @return 信号
 */
+(RACSignal*)optionFile:(NSString*)sn oldFilePath:(NSString*)oldFilePath newFilePath:(NSString*)newFilepath token:(NSString*)token appver:(NSString*)version  option:(NSString*)option;




/**
 *  绑定路由
 *
 *  @param account 账号
 *  @param sn      解绑
 *  @param token   token
 *  @param appver  版本号
 *
 *  @return 信号
 */
+(RACSignal*)bindRouter:(NSString*)account sn:(NSString*)sn token:(NSString*)token appver:(NSString*)appver;

/**
 *  解除绑定路由
 *
 *  @param account 账号
 *  @param sn      解绑
 *  @param token   token
 *  @param appver  版本号
 *
 *  @return 信号
 */
+(RACSignal*)unbindRouter:(NSString*)account sn:(NSString*)sn token:(NSString*)token appver:(NSString*)appver;

/**
 *  路由WIFI设置接口
 *
 *  @param ssid       ssid 设置
 *  @param wifiSwitch wifi开关  1开，0关
 *  @param wifitype    wifi 节点 "2.4G/5G"
 *  @param encrypt    wifi 加密方式  WPA/WPA2
 *  @param password   wifi 密码
 *  @param sn           mac地址
 *  @param token        token
 *  @param version      版本号
 *
 *  @return 信号
 */
+(RACSignal*)setWifi:(NSString*)ssid switch:(BOOL)wifiSwitch type:(NSString*)wifitype encrypt:(NSString*)encrypt password:(NSString*)password sn:(NSString*)sn token:(NSString*)token appver:(NSString*)version;



+(RACSignal*)setWifi:(JWifiDev*)dev1 dev:(JWifiDev*)dev2 sn:(NSString*)sn token:(NSString*)token appver:(NSString*)version;

/**
 *  上网助手设置
 *
 *  @param channel      信道
 *  @param signal_level 信号强度
 *  @param sn           mac地址
 *  @param token        token
 *  @param version      版本号
 *
 *  @return 信号
 */
+(RACSignal*)assistant:(NSInteger)channel level:(NSInteger)signal_level sn:(NSString*)sn token:(NSString*)token appver:(NSString*)version;

/**
 *  路由管理员密码设置接口
 *
 *  @param oldPass 旧密码
 *  @param newPass 新密码
 *  @param sn           mac地址
 *  @param token        token
 *  @param version      版本号
 *
 *  @return 信号
 */
+(RACSignal*)resetAdminPasss:(NSString*)oldPass newPass:(NSString*)newPass sn:(NSString*)sn token:(NSString*)token appver:(NSString*)version;

/**
 *  设置路由蓝牙设置接口
 *
 *  @param on      是否开
 *  @param sn           mac地址
 *  @param token        token
 *  @param version      版本号
 *
 *  @return 信号
 */
+(RACSignal*)setBluetooth:(BOOL)on sn:(NSString*)sn token:(NSString*)token appver:(NSString*)version;

/**
 *  查询指令状态接口
 *
 *  @param identifier 指令identity
 *  @param sn           mac地址
 *  @param token        token
 *
 *  @return 信号
 */
+(RACSignal*)queryCommandStatus:(NSString*)identifier sn:(NSString*)sn token:(NSString*)token;


/**
 *  插座第一次使用配置接口	把路由SSID，密码传给插座。
 *
 *  @param routerSSID ssid
 *  @param routerPWD  密码
 *  @param token        token
 *  @param version      版本号
 *
 *  @return 信号
 */
+(RACSignal*)setJeck:(NSString*)routerSSID pwd:(NSString*)routerPWD sn:(NSString*)sn token:(NSString*)token appver:(NSString*)version;



/**
 *  恢复出出厂设置
 *
 *  @param sn           mac地址
 *  @param token        token
 *  @param version      版本号
 *
 *  @return 信号
 */
+(RACSignal*)restoreRouter:(NSString*)sn token:(NSString*)token appver:(NSString*)version;

/**
 *  重启路由
 *
 *  @param sn           mac地址
 *  @param token        token
 *  @param version      版本号
 *
 *  @return 信号
 */
+(RACSignal*)restartRouter:(NSString*)sn token:(NSString*)token appver:(NSString*)version;

/**
 *  解绑
 *
 *  @param sn           mac地址
 *  @param token        token
 *  @param version      版本号
 *
 *  @return 信号
 */
+(RACSignal*)unbindRouter:(NSString*)sn token:(NSString*)token appver:(NSString*)version;



/**
 *  绑定
 *
 *  @param sn           mac地址
 *  @param token        token
 *  @param version      版本号
 *
 *  @return 信号
 */
+(RACSignal*)bindRouter:(NSString*)sn token:(NSString*)token appver:(NSString*)version;


/**
 *  绑定路由
 *
 *  @param UserName 用户名
 *  @param sn       mac地址
 *  @param token    token
 *  @param appver   版本号
 *
 *  @return 信号
 */
+(RACSignal*)BindAigoAccount:(NSString*)UserName sn:(NSString*)sn token:(NSString*)token appver:(NSString*)appver;



/**
 *  解除绑定路由
 *
 *  @param UserName 用户名
 *  @param sn       mac地址
 *  @param token    token
 *  @param appver   版本号
 *
 *  @return 信号
 */
+(RACSignal*)unBindAigoAccount:(NSString*)UserName sn:(NSString*)sn token:(NSString*)token appver:(NSString*)appver;


/**
 *  升级
 *
 *  @param sn           mac地址
 *  @param token        token
 *  @param version      版本号
 *
 *  @return 信号
 */
+(RACSignal*)upgradeRouter:(NSString*)sn token:(NSString*)token appver:(NSString*)version;




/**
 *  路由防蹭网接口
 *
 *  @param network    1开，0关
 *  @param harassment 防骚扰
 *
 *  @return 信号
 */
+(RACSignal*)Anti:(BOOL)network harassment:(BOOL)harassment  sn:(NSString*)sn token:(NSString*)token appver:(NSString*)version;



/**
 *  设置外网方式
 *
 *  @param json    jwan json
 *  @param sn      设备mac
 *  @param token   token
 *  @param version 版本号
 *
 *  @return 信号
 */
+(RACSignal*)setRouterWan:(NSDictionary*)json sn:(NSString*)sn token:(NSString*)token appver:(NSString*)version;

/**
 *  设置管理员密码
 *
 *  @param oldPWD  旧密码
 *  @param newPWD  新密码
 *  @param sn      mac地址
 *  @param token   token
 *  @param version 版本号
 *
 *  @return 信号
 */
+(RACSignal*)setAdminAccount:(NSString*)oldPWD newPWD:(NSString*)newPWD sn:(NSString*)sn token:(NSString*)token appver:(NSString*)version;



/**
 *  设置设备
 *
 *  @param devices 设备数组
 *  @param sn      mac地址
 *  @param token   token
 *  @param version 版本号
 *
 *  @return 信号
 */
+(RACSignal*)setDevices:(NSArray*)devices sn:(NSString*)sn token:(NSString*)token appver:(NSString*)version;


/**
 *  网络测速
 *
 *  @param sn      mac地址
 *  @param token   token
 *  @param version 版本号
 *
 *  @return 信号
 */
+(RACSignal*)speedTest:(NSString*)sn token:(NSString*)token appver:(NSString*)version;


/**
 *  设置速度限制
 *
 *  @param limit   是否开启速度限制
 *  @param model   模式
 *  @param devs    设备
 *  @param sn      mac地址
 *  @param token   token
 *  @param version 版本号
 *
 *  @return 信号
 */
+(RACSignal*)setSpeedLimit:(BOOL)limit mode:(NSInteger)model speedLimitDevs:(NSArray*)devs sn:(NSString*)sn token:(NSString*)token appver:(NSString*)version;



+(RACSignal*)routerUpgrade:(JFirmwareObject*)firware sn:(NSString*)sn token:(NSString*)token appver:(NSString*)appver;


+(RACSignal*)routerOptimization:(NSString*)sn token:(NSString*)token appver:(NSString*)appver;


@end
