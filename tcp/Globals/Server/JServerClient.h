//
//  JServerClient.h
//  tcp
//  用户体系API
//  Created by Jelly on 15/7/20.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <Foundation/Foundation.h>


@class JUserModule;
@class JDeviceModule;

@interface JServerClient : NSObject

/**
 *  查询消息列表
 *
 *  @param sn     mac
 *  @param token  token
 *  @param appver 版本号
 *  @param index  第几页
 *
 *  @return 信号
 */
+(RACSignal*)queryMessages:(NSString*)sn token:(NSString*)token appver:(NSString*)appver pageIndex:(NSInteger)index;

/**
 *  删除消息
 *
 *  @param messageId 消息ID 如果有消息ID 则删除当前这条消息，无 则删除所有消息
 *  @param sn     mac
 *  @param token  token
 *  @param appver 版本号
 *
 *  @return 信号
 */
+(RACSignal*)deleteMessage:(NSString*)messageId sn:(NSString*)sn token:(NSString*)token appver:(NSString*)appver;
/**
 *  绑定设备
 *
 *  @param deviceModule 设备
 *  @param token        token
 *
 *  @return 信号
 */
+(RACSignal*)BindUserDevice:(JDeviceModule*)deviceModule token:(NSString*)token;

/**
 *  用户和设备解绑
 *
 *  @param deviceModule 解绑设备
 *  @param token        token
 *
 *  @return 信号
 */
+(RACSignal*)UnBindUserDevice:(JDeviceModule*)deviceModule token:(NSString*)token;
/**
 *  获取绑定的列表
 *
 *  @param token token
 *
 *  @return 信号
 */
+(RACSignal*)GetBindDeviceList:(NSString*)token;

/**
 *  更新用户信息
 *
 *  @param user  用户信息
 *  @param token token
 *
 *  @return 信号
 */
+(RACSignal*)UpdateUserInfo:(JUserModule*)user token:(NSString*)token;

/**
 *  更新用户密码
 *
 *  @param password    用户新密码
 *  @param oldpassword 用户旧密码
 *  @param token       token
 *
 *  @return 信号
 */
+(RACSignal*)UpdateUserPassword:(NSString*)password oldpassword:(NSString*)oldpassword token:(NSString*)token;

/**
 *  更新用户头像
 *
 *  @param avatarUrl 头像URL
 *  @param token     token
 *
 *  @return 信号
 */
+(RACSignal*)UpdateUserAvatar:(NSString*)avatarUrl token:(NSString*)token;

/**
 *  获取用户信息
 *
 *  @param username 用户id
 *
 *  @return 信号
 */
+(RACSignal*)GetUserInfo:(NSString*)username;

/**
 *  重置密码
 *
 *  @param username 用户名称
 *
 *  @return 信号
 */
+(RACSignal*)ResetPassword:(NSString*)username;

/**
 *  重置密码
 *  通过密码找回给用户邮箱发送的重置密码页面中提交新密码到本接口即可实现重置密码
 *  @param username 用户id
 *  @param password 密码
 *
 *  @return 信号
 */
+(RACSignal*)DoResetPassword:(NSString*)username password:(NSString*)password;

/**
 *  检测用户名是否存在
 *
 *  @param userName 用户名
 *
 *  @return 信号
 */
+(RACSignal*)UserNameExist:(NSString*)userName;

/**
 *  检测昵称是否被占用
 *
 *  @param nickName 呢称
 *
 *  @return 信号
 */
+(RACSignal*)NickNameExist:(NSString*)nickName;

/**
 *  修改密码
 *
 *  @param username   用户名称
 *  @param password   新密码
 *  @param code       用户验证码
 *  @param identifier 验证码id
 *
 *  @return 信号
 */
+(RACSignal*)UpdateUserPassword:(NSString*)username password:(NSString*)password code:(NSString*)code identifier:(NSString*)identifier;
/**
 *  用户注册界面
 *
 *  @param user       用户名称
 *  @param nickName   呢称
 *  @param sex        性别
 *  @param passWord   密码
 *  @param code       验证码
 *  @param identifier 验证码id
 *
 *  @return 信号
 */
+(RACSignal*)UserRegister:(NSString*)user nickName:(NSString*)nickName sex:(NSInteger)sex pass:(NSString*)passWord code:(NSString*)code identifier:(NSString*)identifier;

/**
 *  登录接口
 *
 *  @param user     用户名称
 *  @param passWord 用户密码
 *
 *  @return 信号
 */
+(RACSignal*)loginUser:(NSString*)user pass:(NSString*)passWord;

/**
 *  发送邮箱验证码
 *
 *  @param mail 邮箱号码
 *
 *  @return 信号
 */
+(RACSignal*)sendMailVerifyCode:(NSString*)mail;

/**
 *  发送手机验证码
 *
 *  @param phone 手机号码
 *
 *  @return 信号
 */
+(RACSignal*)sendPhoneVerifyCode:(NSString*)phone;

/**
 *  注册推送token服务
 *
 *  @param tags  标签
 *  @param token 设备token
 *
 *  @return 信号
 */
+(RACSignal*)registerPush:(NSString*)tags deviceToken:(NSString*)token;


/**
 *  用户信息反馈界面
 *
 *  @param sn     MAC地址：路由器唯一标识
 *  @param token  登陆Aigolife用户体系时分配的Token
 *  @param appver App版本号
 *  @param appid  app identifier
 *
 *  @return 信号
 */
+(RACSignal*)feedback:(NSString*)content sn:(NSString*)sn token:(NSString*)token appver:(NSString*)appver appId:(NSString*)appid;

/**
 *  文件上传
 *
 *  @param fileData 文件的Data
 *  @param token    token
 *
 *  @return 信号
 */
+(RACSignal*)uploadFile:(NSData*)fileData token:(NSString*)token;

/**
 *  文件上传
 *
 *  @param fileData 文件的Data
 *  @param token    token
 *  @param fileName 文件名称
 *
 *  @return 信号
 */
+(RACSignal*)uploadFile:(NSData*)fileData token:(NSString*)token fileName:(NSString*)fileName destination:(NSString*)destination;


+(RACSignal*)downFile:(NSString*)filePath token:(NSString*)token;

+ (RACSignal*)updateJsonFromUrl:(NSURL*)aUrl parameter:(NSDictionary*)dictionary action:(NSString*)action;

+(RACSignal*)getJSONFromURL:(NSURL*)url action:(NSString*)action;

/**
 *  不带签名的get方法
 *
 *  @param url    url
 *
 *  @return 信号
 */
+(RACSignal*)getJSONNoSignFromURL:(NSURL*)url;



@end
