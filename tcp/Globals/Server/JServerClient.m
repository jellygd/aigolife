//
//  JServerClient.m
//  tcp
//
//  Created by Jelly on 15/7/20.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JServerClient.h"
#import <AFNetworking.h>
#import "StringUtil.h"
#import "JUserModule.h"
#import <OpenUDID.h>
#import <AFDownloadRequestOperation.h>
#import <AFURLSessionManager.h>
#import "AppDelegate.h"

#define app_secret @"5d8d32ba-4c55-43d0-b772-9607b1f3e109"

@implementation JServerClient

+(RACSignal*)queryMessages:(NSString*)sn token:(NSString*)token appver:(NSString*)appver pageIndex:(NSInteger)index{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    if(sn.length > 0){
        [userDictionary setObject:sn ? sn:@"" forKey:@"sn"];
    }
    [userDictionary setObject:appver forKey:@"version"];
    [userDictionary setObject:@(index) forKey:@"start"];
    [userDictionary setObject:@(10) forKey:@"count"];
    
    NSString* server = @"http://api.aigolife.com/router/queryMessageList.json";
    return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@""];
}


+(RACSignal*)deleteMessage:(NSString*)messageId sn:(NSString*)sn token:(NSString*)token appver:(NSString*)appver{
    
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    if(sn.length > 0){
        [userDictionary setObject:sn ? sn:@"" forKey:@"sn"];
    }
        
    
    //如果消息Id大于0 则需要单独删除某个消息ID 否则则全部删除
    if (messageId.length > 0 ) {
        [userDictionary setObject:messageId forKey:@"id"];
        [userDictionary setObject:@"delete" forKey:@"op"];
    }else{
        [userDictionary setObject:@"deleteAll" forKey:@"op"];
    }
//    [userDictionary setObject:token forKey:@"token"];
    [userDictionary setObject:appver forKey:@"version"];
    NSString* server = @"http://api.aigolife.com/router/operateMessage.json";
    return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@""];
    
    
}



+(RACSignal*)BindUserDevice:(JDeviceModule*)deviceModule token:(NSString*)token{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    NSDictionary* userJson = [MTLJSONAdapter JSONDictionaryFromModel:deviceModule error:nil];
    NSString* json = [self dicToString:userJson];
    [userDictionary setObject:json forKey:@"device"];
    [userDictionary setObject:token forKey:@"token"];
    [userDictionary setObject:@"BindUserDevice" forKey:@"action"];
    return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/if/account.jhtml"] parameter:userDictionary action:@"BindUserDevice"];
}

+(RACSignal*)UnBindUserDevice:(JDeviceModule*)deviceModule token:(NSString*)token{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token forKey:@"token"];
    NSDictionary* userJson = [MTLJSONAdapter JSONDictionaryFromModel:deviceModule error:nil];
    NSString* json = [self dicToString:userJson];
    [userDictionary setObject:json forKey:@"device"];
    [userDictionary setObject:@"UnBindUserDevice" forKey:@"action"];
    return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/if/account.jhtml"] parameter:userDictionary action:@"UnBindUserDevice"];
}

+(RACSignal*)GetBindDeviceList:(NSString*)token{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token forKey:@"token"];
    [userDictionary setObject:@"GetBindDeviceList" forKey:@"action"];
    return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/if/account.jhtml"] parameter:userDictionary action:@"GetBindDeviceList"];
}


+(RACSignal*)UpdateUserInfo:(JUserModule*)user token:(NSString*)token{
    
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token forKey:@"token"];
    NSDictionary* userJson = [MTLJSONAdapter JSONDictionaryFromModel:user error:nil];
    
    NSString* json = [self dicToString:userJson];
    
    [userDictionary setObject:json forKey:@"user"];
    [userDictionary setObject:@"UpdateUserInfo" forKey:@"action"];
    return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/if/account.jhtml"] parameter:userDictionary action:@"UpdateUserInfo"];
}


+(RACSignal*)UpdateUserPassword:(NSString*)password oldpassword:(NSString*)oldpassword token:(NSString*)token{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:password forKey:@"password"];
    [userDictionary setObject:oldpassword forKey:@"oldpassword"];
    [userDictionary setObject:token forKey:@"token"];
    [userDictionary setObject:@"UpdateUserPassword" forKey:@"action"];
    return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/if/account.jhtml"] parameter:userDictionary action:@"UpdateUserPassword"];
}


+(RACSignal*)UpdateUserAvatar:(NSString*)avatarUrl token:(NSString*)token{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:avatarUrl forKey:@"avatarUrl"];
    [userDictionary setObject:token forKey:@"token"];
    [userDictionary setObject:@"UpdateUserAvatar" forKey:@"action"];
    return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/if/account.jhtml"] parameter:userDictionary action:@"UpdateUserAvatar"];
}

+(RACSignal*)GetUserInfo:(NSString*)username{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:username forKey:@"username"];
    [userDictionary setObject:@"GetUserInfo" forKey:@"action"];
    return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/if/account.jhtml"] parameter:userDictionary action:@"GetUserInfo"];
    
}

+(RACSignal*)ResetPassword:(NSString *)username{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:username forKey:@"username"];
    [userDictionary setObject:@"ResetPassword" forKey:@"action"];
    return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/if/account.jhtml"] parameter:userDictionary action:@"ResetPassword"];
}

+(RACSignal*)DoResetPassword:(NSString*)username password:(NSString*)password{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:username forKey:@"username"];
    [userDictionary setObject:[password MD5String] forKey:@"password"];
    [userDictionary setObject:@"DoResetPassword" forKey:@"action"];
    return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/if/account.jhtml"] parameter:userDictionary action:@"DoResetPassword"];
}

+(RACSignal*)UserNameExist:(NSString*)userName{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:userName forKey:@"username"];
    [userDictionary setObject:@"UserNameExist" forKey:@"action"];
    return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/if/account.jhtml"] parameter:userDictionary action:@"UserNameExist"];
}

+(RACSignal*)NickNameExist:(NSString*)nickName{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:nickName forKey:@"nickname"];
    [userDictionary setObject:@"NickNameExist" forKey:@"action"];
    return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/if/account.jhtml"] parameter:userDictionary action:@"NickNameExist"];
}

+(RACSignal*)UpdateUserPassword:(NSString*)username password:(NSString*)password code:(NSString*)code identifier:(NSString*)identifier{
    
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:username forKey:@"username"];
    [userDictionary setObject:[password MD5String] forKey:@"password"];
    [userDictionary setObject:code forKey:@"verifyCode"];
    [userDictionary setObject:identifier forKey:@"identifier"];
    [userDictionary setObject:@"UpdateUserPassword2" forKey:@"action"];
    
    return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/if/account.jhtml"] parameter:userDictionary action:@"UpdateUserPassword2"];
}

+(RACSignal*)UserRegister:(NSString*)user nickName:(NSString*)nickName sex:(NSInteger)sex pass:(NSString*)passWord code:(NSString*)code identifier:(NSString*)identifier{
    
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:user forKey:@"username"];
    [userDictionary setObject:@(sex) forKey:@"sex"];
    if ([nickName length] > 0) {
        [userDictionary setObject:nickName forKey:@"nickname"];
    }
    
    
    return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/if/account.jhtml"] parameter:@{@"user":[self dicToString:userDictionary],@"password":[passWord MD5String],@"action":@"UserRegister2",@"verifyCode":code,@"identifier":identifier} action:@"UserRegister2"];
    
    
    return [self getJSONFromURL:[NSURL URLWithString: [NSString stringWithFormat:@"http://api.aigolife.com/if/account.jhtml?action=UserRegister2&user={\"username\"=\"%@\",\"nickname\":\"%@\",\"sex\":%ld}&password=%@&identifier=%@&verifyCode=%@",user,nickName,(long)sex,[passWord MD5String],identifier,code]] action:@"UserRegister2"];
}

+(RACSignal*)feedback:(NSString*)content sn:(NSString*)sn token:(NSString*)token appver:(NSString*)appver appId:(NSString*)appid{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:content forKey:@"content"];
    [userDictionary setObject:sn forKey:@"sn"];
    [userDictionary setObject:appver forKey:@"appver"];
    [userDictionary setObject:appid forKey:@"appid"];
    return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/router/feedback.json"] parameter:userDictionary action:@""];
}

+(RACSignal*)registerPush:(NSString*)tags deviceToken:(NSString*)token{
    
    NSDictionary *dic =[[NSBundle mainBundle] infoDictionary];//获取info－plist
    NSString *appName  =   [dic objectForKey:@"CFBundleIdentifier"];//获取Bundle identifier
    NSString *appVersion   =   [dic valueForKey:@"CFBundleVersion"];//获取Bundle Version
    
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:appName forKey:@"bundleId"];
    [userDictionary setObject:token forKey:@"token"];
    [userDictionary setObject:appVersion forKey:@"gv"];
    [userDictionary setObject:@"put" forKey:@"method"];
    if (tags.length > 0 ) {
        [userDictionary setObject:tags forKey:@"tags"];
    }
    
    NSString* server = @"http://ipush-i.aigolife.com/register";
    
    
    server = [server stringByAppendingString:@"?1=1"];
    
    for (NSString* key in [userDictionary allKeys]) {
        server = [NSString stringWithFormat:@"%@&%@=%@",server,key,userDictionary[key]];
    }
    server = [server stringByReplacingOccurrencesOfString:@"1=1&" withString:@""];
    
    
    NSURL* url = [NSURL URLWithString:server];
    return [self getJSONNoSignFromURL:url];
}


+(RACSignal*)sendPhoneVerifyCode:(NSString*)phone{
    return [self getJSONFromURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://api.aigolife.com/sendVerifyCode.html?phone=%@&action=sendVerifyCode",phone]] action:@"sendVerifyCode"];
    
    return  [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/sendVerifyCode.html"] parameter:@{@"phone":phone,@"action":@"sendVerifyCode"} action:@"sendVerifyCode"];
}

+(RACSignal*)sendMailVerifyCode:(NSString*)mail{
    return [self getJSONFromURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://api.aigolife.com/sendVerifyCode.html?mail=%@&action=sendVerifyCode",mail]] action:@"sendVerifyCode"];
    
    return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/sendVerifyCode.html"] parameter:@{@"mail":mail,@"action":@"sendVerifyCode"} action:@"sendVerifyCode"];
}



+(RACSignal*)uploadFile:(NSData*)fileData token:(NSString*)token{
    
    return [self uploadAvatar:token _imageData:fileData];
}

+(RACSignal*)uploadFile:(NSData*)fileData token:(NSString*)token fileName:(NSString*)fileName destination:(NSString*)destination{
    
    return  [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.securityPolicy.allowInvalidCertificates = YES;
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html",@"text/json",@"text/javascript",@"text/plain", nil];
        NSString*url = [NSString stringWithFormat:@"%@/storage/upload",[JGlobals shareGlobal].routerUrl];
        NSString* filePath =[NSString stringWithFormat:@"%@",destination.length > 0? destination : @"/mnt"];
//        filePath = [filePath stringByReplacingOccurrencesOfString:@"/mnt" withString:@""];
//        [filePath stringsByAppendingPaths:@"/"];
        AFHTTPRequestOperation *op =[manager POST:url parameters:@{@"type":@"upload",@"token":token,@"target":filePath} constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            [formData appendPartWithFileData:fileData name:@"destination" fileName:fileName mimeType:@"*/*"];
        } success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if (!responseObject) {
                [subscriber sendError:[NSError errorWithDomain:@"eim" code:0 userInfo:@{@"errmessage": @"解析错误"}]];
            } else {
                
                [subscriber sendCompleted];
                [op cancel];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [subscriber sendError:[NSError errorWithDomain:@"jellyD" code:0 userInfo:@{@"errmessage": @"网络错误"}]];
            [op cancel];
        }];
        [op setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
            float progress = (totalBytesWritten / (totalBytesExpectedToWrite * 1.0f));
            [subscriber sendNext:@(progress)];
        }];
        
        return [RACDisposable disposableWithBlock:^{
//
        }];
    }];
    
}


+(RACSignal*)downFile:(NSString*)filePath token:(NSString*)token{
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
    
       
        NSString*url = [NSString stringWithFormat:@"%@/storage/%@",[JGlobals shareGlobal].routerUrl,filePath];
    
//        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        NSMutableURLRequest *mutableRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
        [mutableRequest setTimeoutInterval:50];
//        NSProgress *progress = nil;
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:mutableRequest];
        
        
        [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
            NSLog(@"已经接收到响应数据，数据长度为%lld字节...", totalBytesRead);
            
            float progress = (totalBytesRead / (totalBytesExpectedToRead * 1.0f));
            [subscriber sendNext:@(progress)];
        }];
         [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSData *data = (NSData *)responseObject;
             NSString *docsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
             NSString *savePath =[docsDir stringByAppendingString: [[NSURL URLWithString:url] relativePath]];
             [data writeToFile:savePath atomically:YES]; //responseObject 的对象类型是 NSData
             
            [subscriber sendCompleted];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [subscriber sendError:error];
        }];
        [operation start];
        
        return [RACDisposable disposableWithBlock:^{
            [operation cancel];
        }];
    }];

}



+ (NSURL *)saveURL:(NSURLResponse *)response deleteExistFile:(BOOL)deleteExistFile {
    NSString *fileName = [response suggestedFilename];
    
    NSURL *saveURL = [[NSFileManager defaultManager] URLForDirectory:NSCachesDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
    saveURL = [saveURL URLByAppendingPathComponent:fileName];
    NSString *savePath = [saveURL path];
    
    if (deleteExistFile) {
        NSError *saveError;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        //判断是否存在旧的目标文件，如果存在就先移除；避免无法复制问题
        if ([fileManager fileExistsAtPath:savePath]) {
            [fileManager removeItemAtPath:savePath error:&saveError];
            if (saveError) {
                NSLog(@"移除旧的目标文件失败，错误信息：%@", saveError.localizedDescription);
            }
        }
    }
    
    return saveURL;
}


+(RACSignal*)loginUser:(NSString*)user pass:(NSString*)passWord{
    
    return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/if/account.jhtml"] parameter:@{@"username":user,@"password":[passWord MD5String],@"deviceId":[OpenUDID value],@"action":@"UserLogin"} action:@"UserLogin"];
}

+(RACSignal*)UserRegister:(NSString*)password user:(JUserModule*)user{
    
    return [self updateJsonFromUrl:[NSURL URLWithString:@"http://api.aigolife.com/if/account.jhtml"] parameter:@{@"action":@"UserRegister",@"password":[password MD5String],@"user":user} action:@"UserRegister"];
}


#pragma mark - 上传头像



+ (RACSignal*)uploadAvatar:(NSString*)token _imageData:(NSData*)imageData{
    
    return  [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.securityPolicy.allowInvalidCertificates = YES;
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html",@"text/json",@"text/javascript", nil];
        
        AFHTTPRequestOperation *op =[manager POST:@"http://api.aigolife.com/upload" parameters:@{@"token":token} constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            [formData appendPartWithFileData:imageData name:@"file" fileName:@"avatar.jpg" mimeType:@"image/*"];
        } success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if (!responseObject) {
                [subscriber sendError:[NSError errorWithDomain:@"eim" code:0 userInfo:@{@"errmessage": @"解析错误"}]];
            } else {
                NSDictionary* result = responseObject[@"result"];
                NSInteger code = [result[@"result"] integerValue];
                if (code == 0) {
                    NSString* errorKey = result[@"reason"];
                    
                    NSString* errorMessage = [self errorMessageDictionary][errorKey];
                    NSLog(@"============================================");
                    NSLog(@"网络请求出错 === 原因%@",errorMessage);
                    NSLog(@"============================================");
                    if (!errorMessage) {
                        errorMessage = @"未知错误";
                    }
                    
                    [subscriber sendError:[NSError errorWithDomain:@"jellyD" code:0 userInfo:@{@"errmessage": errorMessage}]];
                    [subscriber sendCompleted];
                }else{
                    [subscriber sendNext:responseObject];
                    [subscriber sendCompleted];
                }
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            if (error.code == 401) {
                AppDelegate* delegate = [UIApplication sharedApplication].delegate;
                
                [delegate showLoginView];
                return ;
            };
            NSLog(@"============================================");
            NSLog(@"网络请求出错 === 原因%@",error);
            NSLog(@"============================================");
            if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground) {
                [subscriber sendError:[NSError errorWithDomain:@"jellyD" code:0 userInfo:@{}]];
            }else{
                [subscriber sendError:[NSError errorWithDomain:@"jellyD" code:0 userInfo:@{@"errmessage": @"网络错误"}]];
            }
        }];
        
        return [RACDisposable disposableWithBlock:^{
            [op cancel];
        }];
    }];
    
    
    
}


#pragma mark - POST 请求
+ (RACSignal*)updateJsonFromUrl:(NSURL*)aUrl parameter:(NSDictionary*)dictionary action:(NSString*)action
{
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSArray *keysArray = [dictionary allKeys];
        NSArray *resultArray = [keysArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            
            return [obj1 compare:obj2 options:NSNumericSearch];
        }];
        NSString* query =@"";
        for (NSString* Key in resultArray) {
            NSString * value = [NSString stringWithFormat:@"%@=%@&",Key,dictionary[Key]];
            query = [query stringByAppendingString:value];
        }
        if ([query length] > 0) {
            query = [query substringToIndex:[query length] -1];
        }
        //        query =[query stringByReplacingOccurrencesOfString:@"\\" withString:@""];
        NSMutableDictionary* dic = [dictionary mutableCopy];
        [dic setObject:[self signPost:query] forKey:@"sign"];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html",@"text/json",@"text/plain", nil];
        
        NSString* token = [dictionary objectForKey:@"token"] ;
        
        //特殊处理 增加表头TOken
        if (token.length > 0) {
            [manager.requestSerializer setValue:[dictionary objectForKey:@"token"] forHTTPHeaderField:@"Token"];
        }
       
        
        AFHTTPRequestOperation *op = [manager POST:[NSString stringWithFormat:@"%@",aUrl] parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if (!responseObject) {
                [subscriber sendError:[NSError errorWithDomain:@"eim" code:0 userInfo:@{@"errmessage": @"解析错误"}]];
            } else {
            
                NSDictionary* result = responseObject[@"result"];
                NSInteger code = 0;
                if(result[@"result"]){
                    code = [result[@"result"] integerValue];
                }else{
                    code = [result[@"ret"] integerValue];
                }
                if (code != 1){
                    
                    NSString* errorKey =@"" ;
                    if (result[@"reason"]) {
                        errorKey = result[@"reason"];
                    }else{
                        errorKey = result[@"info"];
                    }
                    
                    NSString* errorMessage = [self errorMessageDictionary][errorKey];
                    if (!errorMessage) {
                        errorMessage = errorKey;
                    }
                    NSLog(@"============================================");
                    NSLog(@"网络请求出错 === 原因%@",errorMessage);
                    NSLog(@"============================================");
                    [subscriber sendError:[NSError errorWithDomain:@"jellyD" code:0 userInfo:@{@"errmessage": errorMessage}]];
                    [subscriber sendCompleted];
                }else{
                    [subscriber sendNext:responseObject];
                    [subscriber sendCompleted];
                }
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            if (error.code == -1011) {
                AppDelegate* delegate = [UIApplication sharedApplication].delegate;
                
                [delegate showLoginView];
                return ;
            };
            NSLog(@"============================================");
            NSLog(@"网络请求出错 === 原因%@",error);
            NSLog(@"============================================");
            if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground) {
                [subscriber sendError:[NSError errorWithDomain:@"jellyD" code:0 userInfo:@{}]];
            }else{
                [subscriber sendError:[NSError errorWithDomain:@"jellyD" code:0 userInfo:@{@"errmessage": @"网络错误"}]];
            }
            //            [subscriber sendError:error];
        }];
        return [RACDisposable disposableWithBlock:^{
            [op cancel];
        }];
    }];
    
}


#pragma mark -Get请求

+(RACSignal*)getJSONNoSignFromURL:(NSURL*)url{
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber)
            {
                NSMutableURLRequest *mutableRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15];
                [mutableRequest setHTTPMethod:@"GET"];
                AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                manager.securityPolicy.allowInvalidCertificates = YES;
                manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain",@"application/json", @"text/html",@"text/json",@"text/javascript",@"text/plain", nil];
                AFHTTPRequestOperation *op = [manager HTTPRequestOperationWithRequest:mutableRequest success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    if (!responseObject) {
                        [subscriber sendError:[NSError errorWithDomain:@"jellyD" code:0 userInfo:@{@"errmessage": @"解析错误"}]];
                    } else {
                        //                        NSDictionary* result = responseObject[@"result"];
                        //                        NSInteger code = 0;
                        //                        if(result[@"result"]){
                        //                            code = [result[@"result"] integerValue];
                        //                        }else{
                        //                            code = [result[@"ret"] integerValue];
                        //                        }
                        //                        if (code != 1){
                        //
                        //                            NSString* errorKey =@"" ;
                        //                            if (result[@"reason"]) {
                        //                                errorKey = result[@"reason"];
                        //                            }else{
                        //                                errorKey = result[@"info"];
                        //                            }
                        //                            NSString* errorMessage = @"未知错误";
                        //                            if ([errorKey length] > 0) {
                        //                                errorMessage = [self errorMessageDictionary][errorKey];
                        //                                if (!errorMessage) {
                        //                                    errorMessage = errorKey;
                        //                                }
                        //                            }
                        //                            [subscriber sendError:[NSError errorWithDomain:@"jellyD" code:0 userInfo:@{@"errmessage": errorMessage}]];
                        //                            [subscriber sendCompleted];
                        //                        }else{
                        [subscriber sendNext:responseObject];
                        [subscriber sendCompleted];
                        //                        }
                    }
                    
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    if ([[operation.response.URL absoluteString] rangeOfString:@"router/login?username=admin"].location != NSNotFound) {
                          [subscriber sendError:[NSError errorWithDomain:@"jellyD" code:0 userInfo:@{}]];
                        [subscriber sendCompleted];
                        return ;
                        
                    }
                    if (error.code == -1011) {
                        AppDelegate* delegate = [UIApplication sharedApplication].delegate;
                        
                        [delegate showLoginView];
                        return ;
                    };
                    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground) {
                        [subscriber sendError:[NSError errorWithDomain:@"jellyD" code:0 userInfo:@{}]];
                    }else{
                        [subscriber sendError:[NSError errorWithDomain:@"jellyD" code:0 userInfo:@{@"errmessage": @"网络错误"}]];
                    }
                    
                    
                    [subscriber sendCompleted];
                }];
                [manager.operationQueue addOperation:op];
                return [RACDisposable disposableWithBlock:^{
                    [op cancel];
                }];//return disposable
            }];
}

+(RACSignal*)getJSONFromURL:(NSURL*)url action:(NSString*)action{
    //自动签名
    
    NSLog(@" 请求URL = %@",url);
    
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber)
            {
                NSString* sign = [self sign:[NSString stringWithFormat:@"%@",url] action:action];
                NSString* newUrlString = [NSString stringWithFormat:@"%@&sign=%@",url,sign];
                NSURL* signUrl = [NSURL URLWithString:newUrlString];
                NSMutableURLRequest *mutableRequest = [NSMutableURLRequest requestWithURL:signUrl cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:50];
                [mutableRequest setHTTPMethod:@"GET"];
                AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                manager.securityPolicy.allowInvalidCertificates = YES;
                manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html",@"text/json",@"text/javascript",@"text/plain", nil];
                AFHTTPRequestOperation *op = [manager HTTPRequestOperationWithRequest:mutableRequest success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    if (!responseObject) {
                        [subscriber sendError:[NSError errorWithDomain:@"jellyD" code:0 userInfo:@{@"errmessage": @"解析错误"}]];
                    } else {
                        NSDictionary* result = responseObject[@"result"];
                        NSInteger code = 0;
                        if(result[@"result"]){
                            code = [result[@"result"] integerValue];
                        }else{
                            code = [result[@"ret"] integerValue];
                        }
                        if (code != 1){
                            
                            NSString* errorKey =@"" ;
                            if (result[@"reason"]) {
                                errorKey = result[@"reason"];
                            }else{
                                errorKey = result[@"info"];
                            }
                            
                            NSString* errorMessage = [self errorMessageDictionary][errorKey];
                            if (!errorMessage) {
                                errorMessage = errorKey;
                            }
                            NSLog(@"============================================");
                            NSLog(@"网络请求出错 === 原因%@",errorMessage);
                            NSLog(@"============================================");
                            [subscriber sendError:[NSError errorWithDomain:@"jellyD" code:0 userInfo:@{@"errmessage": errorMessage}]];
                            [subscriber sendCompleted];
                        }else{
                            [subscriber sendNext:responseObject];
                            [subscriber sendCompleted];
                        }
                    }
                    
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    NSLog(@"============================================");
                    NSLog(@"网络请求出错 === 原因%@",error);
                    NSLog(@"============================================");
                    if (error.code == -1011) {
                       AppDelegate* delegate = [UIApplication sharedApplication].delegate;
                        
                        [delegate showLoginView];
                        return ;
                    };
                    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground) {
                        [subscriber sendError:[NSError errorWithDomain:@"jellyD" code:0 userInfo:@{}]];
                    }else{
                        [subscriber sendError:[NSError errorWithDomain:@"jellyD" code:0 userInfo:@{@"errmessage": @"网络错误"}]];
                    }                 [subscriber sendCompleted];
                }];
                [manager.operationQueue addOperation:op];
                return [RACDisposable disposableWithBlock:^{
                    [op cancel];
                }];//return disposable
            }];
}


+(NSString*)signPost:(NSString*)query{
    return [[query stringByAppendingString:app_secret] MD5String];
}

+(NSString*)sign:(NSString*)query action:(NSString*)action{
    NSString* key = [[query stringByAppendingString:@"2013-03-29"] stringByAppendingString:action];
    return [key MD5String];
}


+(NSDictionary*)errorMessageDictionary{
    return  @{@"Illegal_request":@"非法调用",@"action_non_existent":@"action不存在",@"interface_exception":@"接口出现异常",@"dyn_exception":@"动态前端脚本异常",@"username_empty":@"用户名不能为空",@"password_incorrect":@"密码不正确",@"user_info_empty":@"用户信息不能为空",@"user_exist":@"该用户已存在",@"user_non_exist":@"该用户不存在",@"device_info_empty":@"设备信息不能为空",@"response_empty":@"接口返回为空",@"template_render_exception":@"模板渲染异常"
              ,@"original_password_incorrect":@"原密码不正确",@"token_invalid_or_expired":@"token不合法或token过期",@"link_invalid_or_expired":@"非法链接或链接已失效",@"data_invalid":@"提交非法数据",@"verify_code_invalid_or_expired":@"验证码不正确或已过期"};
    
}


+(NSString*)dicToString:(NSDictionary*)dic{
    NSString* dataString = @"{";
    for (NSString* key  in [dic allKeys]) {
        id value = dic[key];
        if ([value isKindOfClass:[NSString class]]) {
            dataString = [NSString stringWithFormat:@"\"%@\":\"%@\"",key,value];
        }else{
            dataString = [NSString stringWithFormat:@"\"%@\":%@",key,value];
        }
        dataString = [dataString stringByAppendingString:@","];
    }
    if ([dataString length] > 1) {
        dataString = [dataString substringToIndex:[dataString length] -1];
    }
    dataString = [dataString stringByAppendingString:@"}"];
    return dataString;
}





@end
