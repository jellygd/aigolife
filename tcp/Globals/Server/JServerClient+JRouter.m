//
//  JServerClient+JRouter.m
//  tcp
//  路由方面设置的API
//  Created by dong on 15/7/23.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JServerClient+JRouter.h"
#import "NSString+JURL.h"

@implementation JServerClient (JRouter)


+(RACSignal*)querySmartlimit:(NSString*)sn token:(NSString*)token appver:(NSString*)appver{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token forKey:@"token"];
    [userDictionary setObject:sn forKey:@"sn"];
    [userDictionary setObject:appver forKey:@"appver"];
    [userDictionary setObject:@"smartlimit" forKey:@"type"];
    NSString* server = [NSString stringWithFormat:@"%@/router/queryRouterInfo.json",[JGlobals shareGlobal].url];
    if (!([JGlobals shareGlobal].isAigoRouter && [JGlobals shareGlobal].validateRouter)) {
        return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@"blacklist"];
    }
    
    server = [server stringByAppendingString:@"?1=1"];
    
    for (NSString* key in [userDictionary allKeys]) {
        server = [NSString stringWithFormat:@"%@&%@=%@",server,key,[userDictionary[key] URLEncodedString]];
    }
    server = [server stringByReplacingOccurrencesOfString:@"1=1&" withString:@""];
    //    return[self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@""];
    return[self getJSONFromURL:[NSURL URLWithString:server] action:@"smartlimit"];
    

}
+(RACSignal*)queryBlackDevices:(NSString*)sn token:(NSString*)token appver:(NSString*)appver{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token forKey:@"token"];
    [userDictionary setObject:sn forKey:@"sn"];
    [userDictionary setObject:appver forKey:@"appver"];
    [userDictionary setObject:@"blacklist" forKey:@"type"];
    NSString* server = [NSString stringWithFormat:@"%@/router/queryRouterInfo.json",[JGlobals shareGlobal].url];
    if (!([JGlobals shareGlobal].isAigoRouter && [JGlobals shareGlobal].validateRouter)) {
        return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@"blacklist"];
    }
    server = [server stringByAppendingString:@"?1=1"];
    
    for (NSString* key in [userDictionary allKeys]) {
        server = [NSString stringWithFormat:@"%@&%@=%@",server,key,[userDictionary[key] URLEncodedString]];
    }
    server = [server stringByReplacingOccurrencesOfString:@"1=1&" withString:@""];
    
 return [self getJSONFromURL:[NSURL URLWithString:server] action:@"blacklist"];
}


+(RACSignal*)queryDevices:(NSString*)sn token:(NSString*)token appver:(NSString*)appver{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token forKey:@"token"];
    [userDictionary setObject:sn forKey:@"sn"];
    [userDictionary setObject:appver forKey:@"appver"];
    [userDictionary setObject:@"devices" forKey:@"type"];
    NSString* server = [NSString stringWithFormat:@"%@/router/queryRouterInfo.json",[JGlobals shareGlobal].url];
    
    if (!([JGlobals shareGlobal].isAigoRouter && [JGlobals shareGlobal].validateRouter)) {
        return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@"devices"];
    }
    
    server = [server stringByAppendingString:@"?1=1"];
    
    for (NSString* key in [userDictionary allKeys]) {
        server = [NSString stringWithFormat:@"%@&%@=%@",server,key,[userDictionary[key] URLEncodedString]];
    }
    server = [server stringByReplacingOccurrencesOfString:@"1=1&" withString:@""];
//    return[self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@""];
    return[self getJSONFromURL:[NSURL URLWithString:server] action:@"devices"];
}

+(RACSignal*)queryRouterStatueInfo:(NSString*)sn token:(NSString*)token appver:(NSString*)appver{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token forKey:@"token"];
    [userDictionary setObject:sn forKey:@"sn"];
    [userDictionary setObject:appver forKey:@"appver"];
    [userDictionary setObject:@"status" forKey:@"type"];
    NSString* server = [NSString stringWithFormat:@"%@/router/queryRouterInfo.json",[JGlobals shareGlobal].url];
    if (!([JGlobals shareGlobal].isAigoRouter && [JGlobals shareGlobal].validateRouter)) {
        return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@"status"];
        
    }
    server = [server stringByAppendingString:@"?1=1"];
    for (NSString* key in [userDictionary allKeys]) {
      server = [NSString stringWithFormat:@"%@&%@=%@",server,key,[userDictionary[key] URLEncodedString]];
    }
    server = [server stringByReplacingOccurrencesOfString:@"1=1&" withString:@""];
//    server = [server URLEncodedString];
    return [self getJSONFromURL:[NSURL URLWithString:server]  action:@"status"];
}


+(RACSignal*)queryChannels:(NSString*)sn token:(NSString*)token appver:(NSString*)appver{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token forKey:@"token"];
    [userDictionary setObject:sn forKey:@"sn"];
    [userDictionary setObject:appver forKey:@"appver"];
    [userDictionary setObject:@"channels" forKey:@"type"];
    
    NSString* server = [NSString stringWithFormat:@"%@/router/queryRouterInfo.json",[JGlobals shareGlobal].url];
    
    if (!([JGlobals shareGlobal].isAigoRouter && [JGlobals shareGlobal].validateRouter)) {
        return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@"channels"];
        
    }
    
    server = [server stringByAppendingString:@"?1=1"];
    
    for (NSString* key in [userDictionary allKeys]) {
        server = [NSString stringWithFormat:@"%@&%@=%@",server,key,userDictionary[key]];
    }
    server = [server stringByReplacingOccurrencesOfString:@"1=1&" withString:@""];
    return [self getJSONFromURL:[NSURL URLWithString:server]  action:@"channels"];
}



+(RACSignal*)queryRouterSetupInfo{
    NSString* serverUrl = [NSString stringWithFormat:@"%@/router/queryRouterInfo.json?type=setup",[JGlobals shareGlobal].routerUrl];
    return [self getJSONNoSignFromURL:[NSURL URLWithString:serverUrl]];
}


+(RACSignal*)loginRouter:(NSString*)pwd{
    NSString* serverUrl = [NSString stringWithFormat:@"%@/router/login?username=admin&password=%@",[JGlobals shareGlobal].routerUrl,pwd];
    return [self getJSONNoSignFromURL:[NSURL URLWithString:serverUrl]];
}







+(RACSignal*)queryRouterFilesInfo:(NSString*)sn token:(NSString*)token appver:(NSString*)appver path:(NSString*)path start:(NSUInteger)startPage count:(NSUInteger)count{

    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token forKey:@"token"];
    [userDictionary setObject:sn forKey:@"sn"];
    [userDictionary setObject:appver forKey:@"appver"];
    [userDictionary setObject:@"files" forKey:@"type"];
    if (path.length > 0) {
       [userDictionary setObject:path forKey:@"path"];
    }else{
        [userDictionary setObject:@"/" forKey:@"path"];
    }
    if (startPage > 0) {
         [userDictionary setObject:@(startPage) forKey:@"start"];
    }
    if (count > 0) {
         [userDictionary setObject:@(count) forKey:@"count"];
    }
    NSString* server = [NSString stringWithFormat:@"%@/router/queryRouterInfo.json",[JGlobals shareGlobal].url];
    if (!([JGlobals shareGlobal].isAigoRouter && [JGlobals shareGlobal].validateRouter)) {
        return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@"files"];
        
    }
    server = [server stringByAppendingString:@"?1=1"];
    
    for (NSString* key in [userDictionary allKeys]) {
        server = [NSString stringWithFormat:@"%@&%@=%@",server,key,userDictionary[key]];
    }
    server = [server stringByReplacingOccurrencesOfString:@"1=1&" withString:@""];
    return [self getJSONFromURL:[NSURL URLWithString:server]  action:@"channels"];
}

+(RACSignal*)querySpeed:(NSString*)sn token:(NSString*)token appver:(NSString*)appver{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token forKey:@"token"];
    [userDictionary setObject:sn forKey:@"sn"];
    [userDictionary setObject:appver forKey:@"appver"];
    [userDictionary setObject:@"speed" forKey:@"type"];
   
    NSString* server = [NSString stringWithFormat:@"%@/router/queryRouterInfo.json",[JGlobals shareGlobal].url];
    if (!([JGlobals shareGlobal].isAigoRouter && [JGlobals shareGlobal].validateRouter)) {
        return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@"speed"];
        
    }
    server = [server stringByAppendingString:@"?1=1"];
    
    for (NSString* key in [userDictionary allKeys]) {
        server = [NSString stringWithFormat:@"%@&%@=%@",server,key,userDictionary[key]];
    }
    server = [server stringByReplacingOccurrencesOfString:@"1=1&" withString:@""];
    return [self getJSONFromURL:[NSURL URLWithString:server]  action:@"speed"];
}




+(RACSignal*)getRouterVersion{
    return [self getJSONNoSignFromURL:[NSURL URLWithString:@"http://api.aigolife.com/device/GetfirmwareInfo?packageName=aigo.router.fw"]];
}

+(RACSignal*)getRouterWanType:(NSString*)sn token:(NSString*)token appver:(NSString*)appver{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:@"wantype" forKey:@"type"];
    if (token.length > 0) {
       [userDictionary setObject:token forKey:@"token"];
    }
    [userDictionary setObject:appver forKey:@"appver"];
   
    NSString* server = [NSString stringWithFormat:@"%@/router/queryRouterInfo.json",[JGlobals shareGlobal].routerUrl];
    server = [server stringByAppendingString:@"?1=1"];
    for (NSString* key in [userDictionary allKeys]) {
        server = [NSString stringWithFormat:@"%@&%@=%@",server,key,userDictionary[key]];
    }
    server = [server stringByReplacingOccurrencesOfString:@"1=1&" withString:@""];
    return [self getJSONFromURL:[NSURL URLWithString:server]  action:@"wantype"];
}


+(RACSignal*)getRouterDiskInfo:(NSString*)sn token:(NSString*)token appver:(NSString*)appver{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token forKey:@"token"];
    [userDictionary setObject:@"disk" forKey:@"type"];
    
    NSString* server = [NSString stringWithFormat:@"%@/router/queryRouterInfo.json",[JGlobals shareGlobal].url];
    if (!([JGlobals shareGlobal].isAigoRouter && [JGlobals shareGlobal].validateRouter)) {
        return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@"speed"];
        
    }
    server = [server stringByAppendingString:@"?1=1"];
    
    for (NSString* key in [userDictionary allKeys]) {
        server = [NSString stringWithFormat:@"%@&%@=%@",server,key,userDictionary[key]];
    }
    server = [server stringByReplacingOccurrencesOfString:@"1=1&" withString:@""];
    return [self getJSONFromURL:[NSURL URLWithString:server]  action:@"firmware"];
}

+(RACSignal*)getCurrentRouterInfo:(NSString*)sn token:(NSString*)token appver:(NSString*)appver{
    NSMutableDictionary* userDictionary  = [NSMutableDictionary dictionary];
    [userDictionary setObject:token forKey:@"token"];
    [userDictionary setObject:sn forKey:@"sn"];
    [userDictionary setObject:appver forKey:@"appver"];
    [userDictionary setObject:@"firmware" forKey:@"type"];
    
    NSString* server = [NSString stringWithFormat:@"%@/router/queryRouterInfo.json",[JGlobals shareGlobal].url];
    if (!([JGlobals shareGlobal].isAigoRouter && [JGlobals shareGlobal].validateRouter)) {
        return [self updateJsonFromUrl:[NSURL URLWithString:server] parameter:userDictionary action:@"speed"];
        
    }
    server = [server stringByAppendingString:@"?1=1"];
    
    for (NSString* key in [userDictionary allKeys]) {
        server = [NSString stringWithFormat:@"%@&%@=%@",server,key,userDictionary[key]];
    }
    server = [server stringByReplacingOccurrencesOfString:@"1=1&" withString:@""];
    return [self getJSONFromURL:[NSURL URLWithString:server]  action:@"firmware"];
}

@end
