//
//  JServerClient+JRouter.h
//  tcp
//
//  Created by dong on 15/7/23.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JServerClient.h"
#import "JFirmwareObject.h"

@interface JServerClient (JRouter)


/**
 *  获取路由器限速设备列表
 *
 *  @param sn     MAC地址：路由器唯一标识
 *  @param token  登陆Aigolife用户体系时分配的Token
 *  @param appver  App版本号
 *
 *  @return 信号
 */
+(RACSignal*)querySmartlimit:(NSString*)sn token:(NSString*)token appver:(NSString*)appver;

/**
 *  查询黑名单设备的接口
 *
 *  @param sn     MAC地址：路由器唯一标识
 *  @param token  登陆Aigolife用户体系时分配的Token
 *  @param appver  App版本号
 *
 *  @return 信号
 */
+(RACSignal*)queryBlackDevices:(NSString*)sn token:(NSString*)token appver:(NSString*)appver;

/**
 *  查询连接设备的接口
 *
 *  @param sn     MAC地址：路由器唯一标识
 *  @param token  登陆Aigolife用户体系时分配的Token
 *  @param appver  App版本号
 *
 *  @return 信号
 */
+(RACSignal*)queryDevices:(NSString*)sn token:(NSString*)token appver:(NSString*)appver;

/**
 *  路由器信息查询接口
 *  该接口用于App查询路由器状态。
 *  信息包含： 连接设备数，路由连接状态、互联网状态、WLAN状态、存储状态、当前网速、WIFI SSID名称等
 *  @param sn     MAC地址：路由器唯一标识
 *  @param token  登陆Aigolife用户体系时分配的Token
 *  @param appver App版本号
 *
 *  @return 信号
 */
+(RACSignal*)queryRouterStatueInfo:(NSString*)sn token:(NSString*)token appver:(NSString*)appver;


/**
 *  路由器存储文件查询接口，	根据当前PATH返回子目录下面的文件及文件夹
 *
 *  @param sn     MAC地址：路由器唯一标识
 *  @param token  登陆Aigolife用户体系时分配的Token
 *  @param appver App版本号
 *  @param path   "路径：  /usr/local 如为空，默认值为”/”"
 *  @param startPage 查询的数据起始位置，默认为0（从第一条开始查询）
 *  @param count     查询的数据条数，默认为1
 *
 *  @return 信号
 */
+(RACSignal*)queryRouterFilesInfo:(NSString*)sn token:(NSString*)token appver:(NSString*)appver path:(NSString*)path start:(NSUInteger)startPage count:(NSUInteger)count;


/**
 *  3.4.2.6	上网助手信道扫描查询
 *
 *  @param sn     MAC地址：路由器唯一标识
 *  @param token  登陆Aigolife用户体系时分配的Token
 *  @param appver App版本号
 *
 *  @return 信号
 */
+(RACSignal*)queryChannels:(NSString*)sn token:(NSString*)token appver:(NSString*)appver;


/**
 *  获取路由的初始信息
 *
 *  @return 信号
 */
+(RACSignal*)queryRouterSetupInfo;


/**
 *  登录路由 获取路由token
 *
 *  @param pwd 密码
 *
 *  @return 信号
 */
+(RACSignal*)loginRouter:(NSString*)pwd;


/**
 *  查询速度
 *
 *  @param sn     MAC地址：路由器唯一标识
 *  @param token  登陆Aigolife用户体系时分配的Token
 *  @param appver App版本号
 *
 *  @return 信号
 */
+(RACSignal*)querySpeed:(NSString*)sn token:(NSString*)token appver:(NSString*)appver;

/**
 *  获取路由版本
 *
 *  @return 信号
 */
+(RACSignal*)getRouterVersion;

/**
 *  查询当前路由信息
 *
 *  @param sn     MAC地址：路由器唯一标识
 *  @param token  登陆Aigolife用户体系时分配的Token
 *  @param appver App版本号
 *
 *  @return 信号
 */
+(RACSignal*)getCurrentRouterInfo:(NSString*)sn token:(NSString*)token appver:(NSString*)appver;

/**
 *  查询固件硬盘信息
 *
 *  @param sn     MAC地址：路由器唯一标识
 *  @param token  登陆Aigolife用户体系时分配的Token
 *  @param appver App版本号
 *
 *  @return 信号
 */
+(RACSignal*)getRouterDiskInfo:(NSString*)sn token:(NSString*)token appver:(NSString*)appver;



/**
 *  查询路由wan口连接类型
 *
 *  @param sn     MAC地址：路由器唯一标识
 *  @param token  登陆Aigolife用户体系时分配的Token
 *  @param appver App版本号
 *
 *  @return 信号
 */
+(RACSignal*)getRouterWanType:(NSString*)sn token:(NSString*)token appver:(NSString*)appver;


@end
