//
//  JRouterUtil.h
//  tcp
//
//  Created by dong on 15/7/27.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JRouterUtil : NSObject



/**
 *  获取路由器网管地址
 *
 *  @return 网关地址
 */
+(NSString*)routerIpAdderss;

@end
