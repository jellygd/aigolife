//
//  NSString+JURL.h
//  tcp
//
//  Created by Jelly on 15/8/22.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (JURL)
- (NSString *)URLEncodedString; 
@end
