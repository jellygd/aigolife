//
//  NSString+Encryption.h
//  Network
//
//  Created by William Zhao on 13-10-8.
//  Copyright (c) 2013年 Vipshop Holdings Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonCryptor.h>
#import <Security/Security.h>

/**
 *  字符串加密扩展类
 */
@interface NSString (Encryption)

/**
 *  md5算法加密
 *
 *  @return 加密后字符串
 */
- (NSString *)md5String;

/**
 *  sha1算法加密
 *
 *  @return 加密后字符串
 */
- (NSString *)sha1String;

/**
 *  des算法加密
 *
 *  @param key 加密需要的key
 *
 *  @return 加密后字符串
 */
- (NSString *)desStringWithKey:(NSString *)key;




@end

NSString *TripleDESEncode(NSString *plainText);

