//
//  StringUtil.h
//  TwitterFon
//
//  Created by kaz on 7/20/08.
//  Copyright 2008 naan studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSString (NSStringUtils)

- (NSString*)encodeAsURIComponent;
- (NSString*)escapeHTML;
- (NSString*)unescapeHTML;
+ (NSString*)localizedString:(NSString*)key;
+ (NSString*)base64encode:(NSString*)str;
+ (NSString *)dateWithTimeInterval:(NSTimeInterval)timeInterval formatter:(NSString *)formatter;
+ (NSString *)dateWithDate:(NSDate *)date formatter:(NSString *)formatter;

- (NSString *)MD5String;
- (NSString *)flattenHTMLWithImgStyle;
- (NSString *)flattenHTMLWhiteSpace:(BOOL)trim;
- (NSString *)stringByDecodingHTMLEntitiesInString;
- (NSString *)stringByStrippingHTML;
- (NSString *)stringByStrippingSpace;
- (NSString *)flattenImgHTMLWithWihtString:(NSString *)str;

+ (NSString*)encodeBase64String:(NSString *)input;
+ (NSString*)decodeBase64String:(NSString *)input;
+ (NSString*)encodeBase64Data:(NSData *)data;
+ (NSString*)decodeBase64Data:(NSData *)data;

+ (NSString*)walletPasswordSign:(NSString*)pwd;
+ (NSString*)guid;

@end


