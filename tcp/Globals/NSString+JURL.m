//
//  NSString+JURL.m
//  tcp
//
//  Created by Jelly on 15/8/22.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "NSString+JURL.h"

@implementation NSString (JURL)


- (NSString *)URLEncodedString
{
    return (__bridge_transfer  NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (__bridge CFStringRef)self, NULL, (__bridge CFStringRef)@":/?&=;+!@#$()',*", CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding));
}


@end
