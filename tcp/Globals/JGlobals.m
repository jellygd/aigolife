//
//  JGlobals.m
//  tcp
//
//  Created by Jelly on 15/7/21.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JGlobals.h"
#import <SystemConfiguration/CaptiveNetwork.h>
#import <Reachability.h>
#import "JWifiSettingInfo.h"

static JGlobals *kGlobal = nil;


@implementation JGlobals

+ (JGlobals *)shareGlobal{
    static dispatch_once_t globalToken;
    dispatch_once(&globalToken, ^{
        if (!kGlobal) {
            kGlobal=[[JGlobals alloc] init];
        }
    });
    return kGlobal;
}

-(NSString*)userName{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
}

-(void)setUserName:(NSString *)userName{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:userName forKey:@"username"];
    [userDefaults synchronize];
}


-(NSString*)token{
    NSString* value = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    if (value) {
        return value;
    }
    return  @"";
}


-(NSString*)serverToken{
    if (self.isAigoRouter&&self.validateRouter) {
        if (!self.user) {
            return self.routerToken.length > 0 ? self.routerToken : @"";
        }else{
            if ([self.routerMAC isEqualToString:self.deveicRouter.identifier]) {
                return self.routerToken.length > 0 ? self.routerToken : @"";
            }
        }
    }
    
    return self.token.length > 0 ? self.token : @"";
}



-(NSString*)serverMAC{
    if (self.isAigoRouter&&self.validateRouter) {
        if (!self.user) {
          return   self.routerMAC.length > 0 ? self.routerMAC : @"";
        }else{
            if ([self.routerMAC isEqualToString:self.deveicRouter.identifier]) {
                return   self.routerMAC.length > 0 ? self.routerMAC : @"";
            }
        }
    }
    
    return   self.deveicRouter.identifier.length > 0 ? self.deveicRouter.identifier : @"";
    
}
-(NSInteger)shakeid{
    if(!self.canShake){
        return 4;
    }else
    {
        return [[[NSUserDefaults standardUserDefaults] objectForKey:@"ShakeType"] integerValue];
    }
}

-(void)setShakeType:(NSInteger)type{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:@(type) forKey:@"ShakeType"];
    if(type  == 4){
       [userDefaults setValue:@(NO) forKey:@"Shake"];
    }else
    {
        [userDefaults setValue:@(YES) forKey:@"Shake"];
    }
    [userDefaults synchronize];
}

-(BOOL)canShake{
     return [[[NSUserDefaults standardUserDefaults] objectForKey:@"Shake"] boolValue];
}

-(void)setToken:(NSString *)token{
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
}


-(NSString*)expire{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"expire"];
}

-(void)setExpire:(NSString *)expire{
    [[NSUserDefaults standardUserDefaults] setObject:expire forKey:@"expire"];
}


- (id)fetchSSIDInfo {
    NSArray *ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
    id info = nil;
    for (NSString *ifnam in ifs) {
        info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        if (info && [info count]) { break; }
    }
    return info;
}

- (NSString *)currentWifiSSID {
    // Does not work on the simulator.
    NSString *ssid = nil;
    NSArray *ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
    for (NSString *ifnam in ifs) {
        NSDictionary *info = (__bridge id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        if (info[@"SSID"]) {
            ssid = info[@"SSID"]; 
        }
    }
    return ssid;
}

//-(BOOL)isAigoRouter{
//    //判断是否是wifi
//    if ([[Reachability reachabilityForLocalWiFi] isReachableViaWiFi]) {
//        NSString* currentSSID =[self currentWifiSSID];
//        return YES;
//        if ([currentSSID.lowercaseString hasPrefix:@"aigo"]) {
//            return YES;
//        }
//    }
//    return NO;
//}

-(NSString*)serverURL{

    return @"http://api.aigolife.com";
}


-(NSString*)routerUrl{
    return [NSString stringWithFormat:@"http://%@:8888",[JWifiSettingInfo routerAddressIp]];
}


-(NSString*)url{
    if (self.isAigoRouter&&self.validateRouter) {
        if (!self.user) {
            return self.routerUrl;
        }else{
            if ([self.routerMAC isEqualToString:self.deveicRouter.identifier]) {
                return self.routerUrl;
            }
        }
    }
    
    return self.serverURL;
}

-(CGFloat)scale{
    if (iPhone4) {
        return 0.7;
    }else if (iPhone5){
        return 0.8;
    }
    
    return 1;
}

@end
