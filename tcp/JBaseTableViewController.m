//
//  JBaseTableViewController.m
//  tcp
//
//  Created by Jelly on 15/7/26.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JBaseTableViewController.h"
#import "JServerClient+JRouter.h"
#import "JServerClient+JCommand.h"
#import "JRouterInfo.h"
#import "JBlurBaseView.h"
#import <Reachability.h>



@interface JBaseTableViewController ()

@end

@implementation JBaseTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.layer.masksToBounds = YES;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    if ([self.navigationController.childViewControllers count] >= 2) {
        
        UIBarButtonItem* leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStyleBordered target:self action:@selector(backAction)];
        self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    }
    
}

- (void)viewWillDisappear: (BOOL)animated
{
    [super viewWillDisappear: animated];
    if (![[self.navigationController viewControllers] containsObject: self])
    {
       
    }
}


-(void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self becomeFirstResponder];
    [self showBlurView];
}



- (void) viewWillAppear:(BOOL)animated
{
    [self resignFirstResponder];
    [super viewWillAppear:animated];
}



-(void)searchCommandStatue:(NSString*)commandIdentifier{
    self.searchCommad += 1;
    [self commandNextSearch];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10*self.searchCommad * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        @weakify(self);
        [[JServerClient queryCommandStatus:commandIdentifier sn:[JGlobals shareGlobal].serverMAC  token:[JGlobals shareGlobal].token ] subscribeNext:^(id x) {
            @strongify(self);
            
            NSDictionary* dictionary = x[@"command"];
            if (dictionary) {
                JCommad* cammand =  [MTLJSONAdapter modelOfClass:[JCommad class]  fromJSONDictionary:dictionary error:nil];
                if([cammand.status isEqualToString:@"running"]){
                    if (self.searchCommad >= 4 ) {
                        [MBProgressHUD hideHUDForView:self.view animated:NO];
                        [MBProgressHUD showError:@"操作失败" toView:self.view];
                        self.searchCommad = 0;
                        [self commandSuccess];
                    }
                    [self searchCommandStatue:commandIdentifier];
                }else if([cammand.status isEqualToString:@"success"]){
                    [MBProgressHUD hideHUDForView:self.view animated:NO];
                    [MBProgressHUD showSuccess:@"操作成功。" toView:self.view];
                    self.searchCommad = 0;
                    [self commandSuccess];
                }else{
                    [MBProgressHUD hideHUDForView:self.view animated:NO];
                    [MBProgressHUD showError:@"操作失败" toView:self.view];
                    self.searchCommad = 0;
                    [self commandFail];
                }
            }
            
        } error:^(NSError *error) {
            @strongify(self);
            if (self.searchCommad >= 4 ) {
                [MBProgressHUD hideHUDForView:self.view animated:NO];
                [MBProgressHUD showError:@"操作失败" toView:self.view];
                [self commandFail];
                return ;
            }
            [self searchCommandStatue:commandIdentifier];
            
            
        }];
    });
}



-(void)commandFail{
    
}

-(void)commandSuccess{
    
}

-(void)commandNextSearch{
    
}




/**
 *  获取路由的信息
 */
-(void)loadRouterData{
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    @weakify(self);
    [[JServerClient queryRouterStatueInfo:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(NSDictionary* x) {
        @strongify(self);
        if (x[@"data"]) {
            id value = x[@"data"];
            NSDictionary* dic = [value firstObject];
            JRouterInfo* routerInfo =  [MTLJSONAdapter modelOfClass:JRouterInfo.class fromJSONDictionary:dic error:nil];
            if(routerInfo){
                [JGlobals shareGlobal].routerInfo = routerInfo;
                //判断路由状态
                if ([routerInfo.route isEqualToString:@"on"] && [routerInfo.internet isEqualToString:@"on"]) {
                    [JGlobals shareGlobal].wifiStatue = WifiStatueNomal;
                }else if([routerInfo.route isEqualToString:@"on"]){
                    [JGlobals shareGlobal].wifiStatue = WifiStatueExtranet;
                }else{
                    [JGlobals shareGlobal].wifiStatue = WifiStatueIntranet;
                }
                [self loadRouterDataSuccess];
            }else{
                [self loadRouterDataFail];
                [JGlobals shareGlobal].wifiStatue = WifiStatueUnKnow;
                
            }
        }
    } error:^(NSError *error) {
        
        [self loadRouterDataFail];
        [JGlobals shareGlobal].wifiStatue = WifiStatueUnKnow;
    }];
}

-(void)showErrorMessage:(NSString*)message{
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
    [alertView show];
    
}

-(void)loadRouterDataSuccess{}

-(void)loadRouterDataFail{}


-(void)nextTask{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self taskAction];
    });
    
}

-(void)taskAction{
    
}


-(BOOL)showCanBlur{
    if (![[Reachability reachabilityForInternetConnection] isReachable]) {
        if([JGlobals shareGlobal].adminPWd){
            return YES;
        }
    }
    return NO;
}

-(void)showBlurView{
    if (self.showBlur) {
        if (![[Reachability reachabilityForInternetConnection] isReachable]) {
            if([JGlobals shareGlobal].adminPWd){
                [[UIApplication sharedApplication].keyWindow addSubview:self.blurNewView];
            }
        }
    }
}

-(FXBlurView*)blurNewView{
    if (!_blurNewView) {
        _blurNewView = [[FXBlurView alloc] initWithFrame:[UIApplication sharedApplication].keyWindow.bounds];
        [_blurNewView setDynamic:NO];
        _blurNewView.blurEnabled = YES;
        [_blurNewView setBlurRadius:0];
        [_blurNewView setIterations:0];
        _blurNewView.tintColor = [UIColor blackColor];
        UIView* blackView = [[UIView alloc] initWithFrame:_blurNewView.bounds];
        blackView.backgroundColor = [UIColor blackColor];
        blackView.alpha = 0.7;
        [_blurNewView addSubview:blackView];
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"JBlurBaseView" owner:self options:nil];
        JBlurBaseView* blurBaseView = nib.firstObject;
        blurBaseView.frame = _blurNewView.bounds;
        [_blurNewView addSubview:blurBaseView];
        @weakify(self);
        [[blurBaseView.backBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
            @strongify(self);
            [self.blurNewView removeFromSuperview];
            [self.navigationController popViewControllerAnimated:YES];
        }];
        
        //TODO：
        [[blurBaseView.settingBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
            @strongify(self);
            [self.blurNewView removeFromSuperview];
            
            if(VSDeviceOSVersionIsAtLeast(8.0)){
                NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                if ([[UIApplication sharedApplication] canOpenURL:url]) {
                    [[UIApplication sharedApplication] openURL:url];
                }
            }else{
                NSURL*url=[NSURL URLWithString:@"prefs:root=WIFI"];
                if ([[UIApplication sharedApplication] canOpenURL:url]) {
                    [[UIApplication sharedApplication] openURL:url];
                }
            }
            
        }];
    }
    
    return _blurNewView;
}

@end
