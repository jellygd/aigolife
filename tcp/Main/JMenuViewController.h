//
//  JMenuViewController.h
//  tcp
//
//  Created by dong on 15/7/21.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JBaseTableViewController.h"
#import "JLoginViewController.h"

@interface JMenuViewController : JBaseTableViewController<JLoginViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *faceBGView;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIButton *faceBtn;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIImageView *avtorImageView;
@property (weak, nonatomic) IBOutlet UIImageView *avtorBgImageView;

@property (weak, nonatomic) IBOutlet UITableViewCell *logoutCell;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@end
