//
//  JVersionDetailViewController.m
//  tcp
//
//  Created by Jelly on 15/8/3.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JVersionDetailViewController.h"
#import "JVersionTableViewCell.h"

@interface JVersionDetailViewController ()

@end

@implementation JVersionDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"版本信息";
    
    FIX_EDGES_FOR_EXTENDEDLAYTOU;
    
    self.versionInfoLabel.text = self.firmware.versionName;
    self.detailTextView.text = self.firmware.releaseNote;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//#pragma mark - Table view data source
//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return 3;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return 4;
//}
//
//
//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    return 60;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 48.0;
//}
//
//-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    UIView* view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.versionDetailTableView.width, 60)];
//    view.backgroundColor = [UIColor whiteColor];
//    UILabel* lable = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, self.versionDetailTableView.width- 20, 60)];
//    lable.font = [UIFont systemFontOfSize:14];
//    [view addSubview:lable];
//    switch (section) {
//        case 0:
//            lable.text = @"新增功能";
//            break;
//        case 1:
//            lable.text = @"优化改进";
//            break;
//        case 2:
//            lable.text = @"问题修复";
//            break;
//            
//        default:
//            break;
//    }
//    return view;
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    JVersionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JVersionTableViewCell" forIndexPath:indexPath];
//    
//    return cell;
//}
//
//
//-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
//    
//    return NO;
//}

@end
