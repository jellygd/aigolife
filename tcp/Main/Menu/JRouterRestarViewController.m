//
//  JRouterRestarViewController.m
//  tcp
//
//  Created by Jelly on 15/8/2.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JRouterRestarViewController.h"
#import "JServerClient+JCommand.h"
#import <MTLJSONAdapter.h>
#import "JCommad.h"
//#import "UILabel+BezierAnimation.h"
#import "CATextLayer+NumberJump.h"

@interface JRouterRestarViewController ()


@property(nonatomic, strong) CATextLayer* textLayer;

@end

@implementation JRouterRestarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.showBlur = YES;
    // Do any additional setup after loading the view.
    FIX_EDGES_FOR_EXTENDEDLAYTOU;
    self.title = @"重启路由";
    
    [self btnAction];
    self.textLayer = [[CATextLayer alloc] init];
    self.textLayer.string = @"0";
    self.textLayer.frame = CGRectMake(0,3, self.restartProgressLabel.width-15,self.restartProgressLabel.height);
    self.textLayer.fontSize = 12;
    self.textLayer.contentsScale = 2;
    [self.textLayer setAlignmentMode:kCAAlignmentCenter];
    self.textLayer.foregroundColor = [UIColor blackColor].CGColor;
    [self.restartProgressLabel.layer addSublayer:self.textLayer];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)backAction{
    if (self.restartView.hidden) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark btnAction
-(void)btnAction{
    @weakify(self);
    [[self.restartBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        [self restartRouter];
    }];
}

#pragma mark  事件处理

-(void)restartRouter{
    self.restartProgressLabel.text = @"   %";
    self.restartView.hidden = NO;
    [self starLoadingAnimation];
    [self sendRestartCommand];
}


-(void)sendRestartCommand{
     NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    
    @weakify(self);
    [[JServerClient restartRouter:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(id x) {
        
        if ([JGlobals shareGlobal].isAigoRouter && [JGlobals shareGlobal].validateRouter) {
            [self showRestarAnimation];
           
            return ;
        }
        NSString* commadIdentifier = x[@"id"];
       commadIdentifier = [NSString stringWithFormat:@"%@",commadIdentifier];
        if ([commadIdentifier length] > 0) {
            self.searchCommad = 0;
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self searchCommandStatue:[NSString stringWithFormat:@"%@",commadIdentifier]];
        }else{
            [MBProgressHUD showError:@"重启失败，稍后重试！" toView:self.view];
            self.restartView.hidden = YES;
        }
            
    } error:^(NSError *error) {
        @strongify(self);
         [MBProgressHUD showError:@"重启失败，稍后重试！" toView:self.view];
        self.restartView.hidden = YES;
    }];
}


-(void)commandNextSearch{
     [self.textLayer jumpNumberWithDuration:10*self.searchCommad fromNumber:(self.searchCommad-1)*(100/kSearchCount)  toNumber:self.searchCommad*(100/kSearchCount) >=100 ? 99 : self.searchCommad*(100/kSearchCount) ];
}


-(void)commandFail{
    self.restartView.hidden = YES;
}

-(void)commandSuccess:(NSDictionary*)commadInfo{
    self.restartView.hidden = YES;
}


-(void)showRestarAnimation{
    
    [self.textLayer jumpNumberWithDuration:10*4 fromNumber:0  toNumber:99 ];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(40 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [MBProgressHUD showSuccess:@"操作成功" toView:self.view];
        [self stopLoadingAnimation];
        self.restartProgressLabel.hidden = YES;
        self.restartLabel.text = @"路由器已经成功重启";
        self.restartImageVIew.image = [UIImage imageNamed:@"restartComplete"];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.navigationController popViewControllerAnimated:YES];
        });
    });
    
}

#pragma mark  动画设置
/**
 *  开始loading 的动画
 */
-(void)starLoadingAnimation{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 ];
    rotationAnimation.duration = 1;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = MAXFLOAT;
    
    [self.restartImageVIew.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

/**
 *  关闭动画 并隐藏动画
 */
-(void)stopLoadingAnimation{
    [self.restartImageVIew.layer removeAllAnimations];
}

@end
