//
//  JVersionTableViewCell.h
//  tcp
//
//  Created by Jelly on 15/8/3.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JVersionTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *versionDetail;
@end
