//
//  JRouterUpgregeViewController.m
//  tcp
//
//  Created by Jelly on 15/8/2.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JRouterUpgregeViewController.h"
#import "JUpgradeTableViewCell.h"
#import "JServerClient.h"
#import "JFirmwareObject.h"
#import "JServerClient+JRouter.h"
#import "JServerClient+JCommand.h"
#import "JVersionDetailViewController.h"
#import <APSSIDInfoObserver.h>

@interface JRouterUpgregeViewController ()<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic, strong) JFirmwareObject* routerInfo;
@property(nonatomic, strong) NSString* versionInfo;
@property(nonatomic, strong) APSSIDInfoObserver* observer;
@property(nonatomic, strong) NSString* currentSSID;

@property(nonatomic, strong) UIButton* refBtn;

@end

@implementation JRouterUpgregeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    FIX_EDGES_FOR_EXTENDEDLAYTOU;
    
    self.title = @"版本升级";
    self.showBlur = YES;
    
    
    self.observer = [APSSIDInfoObserver new];
    self.currentSSID = self.observer.currentSSID.ssid;
    @weakify(self);
    self.observer.SSIDChangedBlock = ^(APSSIDModel *model){
        @strongify(self);
       NSString* ssid = [NSString stringWithFormat:@"Network is: %@", model.ssid];
        if (!self.currentSSID && model.ssid) {
            self.currentSSID = model.ssid;
        }
       if (!model) {
            
        }else if ([model.ssid isEqualToString:self.currentSSID]) {
            [self loadCurrentRouterInfo];
        }else{
            [MBProgressHUD hideAllHUDsForView:self.navigationController.view.superview animated:NO];
            [MBProgressHUD showError:[NSString stringWithFormat:@"请连接%@路由进行查看升级结果",self.currentSSID] toView:self.view];
        }
    };
    
    
    self.refBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.refBtn setImage:[UIImage imageNamed:@"icon_top_refresh"] forState:UIControlStateNormal];
    [self.refBtn sizeToFit];
    [[self.refBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        [self loadRouterInfo];
    }];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.refBtn];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (!self.showCanBlur) {
        [self loadRouterInfo];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark loadData


-(void)loadRouterInfo{
    NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
    @weakify(self);
    [MBProgressHUD showMessag:@"查询中" toView:self.view];
    [[RACSignal combineLatest:@[[JServerClient getRouterVersion],[JServerClient getCurrentRouterInfo:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version]] reduce:^id(NSDictionary* versionInfo, NSDictionary*curretnInfo){
        
        return @{@"version":versionInfo,@"curretnInfo":curretnInfo};
    }] subscribeNext:^(id x) {
        NSDictionary* version = x[@"version"];
        NSDictionary* currentInfo = x[@"curretnInfo"];
        NSDictionary*fwInfo =  version[@"app"];
        if (fwInfo) {
            self.routerInfo = [MTLJSONAdapter modelOfClass:[JFirmwareObject class] fromJSONDictionary:fwInfo error:nil];
        }
        
        NSString* currentVersion =  currentInfo[@"data"][@"version"];
        NSArray *_arr = [currentVersion componentsSeparatedByString:NSLocalizedString(@".", nil)];
        float currentCode =0.0;
        if (_arr.count > 3) {
            currentCode =  [_arr[0] floatValue] * 100 +  [_arr[1] floatValue] * 10 +  [_arr[2] floatValue];
        }
        
        if(self.routerInfo){
            
            NSArray *_versionArr = [self.routerInfo.versionName componentsSeparatedByString:NSLocalizedString(@".", nil)];
            float versionCode =0.0;
            if (_versionArr.count > 3) {
                versionCode =  [_versionArr[0] floatValue] * 100 +  [_versionArr[1] floatValue] * 10 +  [_versionArr[2] floatValue];
            }
            if (versionCode > currentCode) {
                self.routerInfo.isUpdate = YES;
            }
        }
        self.versionInfo = currentVersion;
        
        [self.upgradeTableView reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:NO];
    } error:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
    }];
}


#pragma mark - Table view data source



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.routerInfo ? 1 : 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    JUpgradeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JUpgradeTableViewCell" forIndexPath:indexPath];
    cell.versionTagImageView.hidden =  !self.routerInfo.isUpdate;
    NSString* message = @"";
    if (self.routerInfo.isUpdate) {
        message = [NSString stringWithFormat:@"当前版本：%@ 最新版本%@",self.versionInfo,self.routerInfo.versionName];
    }else{
        message = [NSString stringWithFormat:@"%@ 已是最新的系统",self.versionInfo];
    }
    cell.versionDetaileLabel.text = message;
    
    cell.versionUpgradeBlock = ^(){
        [self upgradeRouter];
    };
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 48.0;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //    versionDetail
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"versionDetail"]) {
        JVersionDetailViewController* detailViewController = segue.destinationViewController;
        detailViewController.firmware = self.routerInfo;
    }
}


-(void)upgradeRouter{
    
//    if ([JGlobals shareGlobal].isAigoRouter&&[JGlobals shareGlobal].validateRouter) {
        NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
        @weakify(self);
        
        [MBProgressHUD showMessag:@"升级中..." toView:self.navigationController.view.superview];
        
        [[JServerClient routerUpgrade:self.routerInfo sn:[JGlobals shareGlobal].serverMAC token:[JGlobals shareGlobal].serverToken appver:version] subscribeNext:^(id x) {
//            [self.observer startObserving];
            [MBProgressHUD hideHUDForView:self.navigationController.view.superview animated:NO];
            
            [MBProgressHUD showError:@"升级成功！" toView:self.view];
        } error:^(NSError *error) {
            @strongify(self);
            [MBProgressHUD hideAllHUDsForView:self.navigationController.view.superview animated:NO];
            [MBProgressHUD showError:@"升级失败，稍后重试！" toView:self.view];
            
        }];
//    }else{
//        [MBProgressHUD showError:@"请连接路由进行升级！" toView:self.view];
//    }
    
    
}


-(void)loadCurrentRouterInfo{
    
    [[JServerClient getRouterVersion] subscribeNext:^(id x) {
        [self.observer stopObserving];
        [MBProgressHUD hideHUDForView:self.navigationController.view.superview animated:NO];
        NSString* currentVersion =  x[@"app"][@"versionName"];
        if ([currentVersion isEqualToString:self.routerInfo.versionName]) {
            [MBProgressHUD showError:@"升级成功！" toView:self.view];
            [self loadRouterInfo];
        }else{
           [MBProgressHUD showError:@"升级失败，稍后重试！" toView:self.view];
        }
    } error:^(NSError *error) {
        [self.observer stopObserving];
        [self.observer startObserving];
    }];
}

@end
