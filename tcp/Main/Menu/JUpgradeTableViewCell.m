//
//  JUpgradeTableViewCell.m
//  tcp
//
//  Created by Jelly on 15/8/2.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JUpgradeTableViewCell.h"

@implementation JUpgradeTableViewCell

- (void)awakeFromNib {
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)upgradeAction:(id)sender {
    if (self.versionUpgradeBlock) {
        self.versionUpgradeBlock();
    }
}

@end
