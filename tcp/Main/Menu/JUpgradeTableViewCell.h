//
//  JUpgradeTableViewCell.h
//  tcp
//
//  Created by Jelly on 15/8/2.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^VersionUpgradeBlock)();

@interface JUpgradeTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *versionImageView;
@property (weak, nonatomic) IBOutlet UILabel *versionTitle;
@property (weak, nonatomic) IBOutlet UILabel *versionDetaileLabel;
@property (weak, nonatomic) IBOutlet UIButton *versionTagImageView;

@property (copy, nonatomic) VersionUpgradeBlock versionUpgradeBlock;


@end
