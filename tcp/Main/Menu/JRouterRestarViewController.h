//
//  JRouterRestarViewController.h
//  tcp
//
//  Created by Jelly on 15/8/2.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JBaseViewController.h"

@interface JRouterRestarViewController : JBaseViewController


@property (weak, nonatomic) IBOutlet UIButton *restartBtn;

@property (weak, nonatomic) IBOutlet UIView *restartView;

@property (weak, nonatomic) IBOutlet UIImageView *restartImageVIew;


@property (weak, nonatomic) IBOutlet UILabel *restartProgressLabel;

@property (weak, nonatomic) IBOutlet UILabel *restartLabel;
@end
