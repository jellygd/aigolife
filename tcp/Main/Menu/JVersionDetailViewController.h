//
//  JVersionDetailViewController.h
//  tcp
//
//  Created by Jelly on 15/8/3.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JBaseViewController.h"
#import "JFirmwareObject.h"

@interface JVersionDetailViewController : JBaseViewController


@property(nonatomic, strong) JFirmwareObject* firmware;

@property (weak, nonatomic) IBOutlet UILabel *versionInfoLabel;
@property (weak, nonatomic) IBOutlet UITextView *detailTextView;

@property (weak, nonatomic) IBOutlet UITableView *versionDetailTableView;
@end
