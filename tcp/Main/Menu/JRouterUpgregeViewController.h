//
//  JRouterUpgregeViewController.h
//  tcp
//
//  Created by Jelly on 15/8/2.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JBaseViewController.h"

@interface JRouterUpgregeViewController : JBaseViewController

@property (weak, nonatomic) IBOutlet UITableView *upgradeTableView;

@end
