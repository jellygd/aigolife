//
//  JFeedBackViewController.m
//  tcp
//
//  Created by Jelly on 15/7/31.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JFeedBackViewController.h"
#import <UITextView+Placeholder.h>
#import "JServerClient.h"

@interface JFeedBackViewController ()<UITextViewDelegate>

@property(nonatomic ,strong) UIButton* sendFeedBackBtn;

@end

@implementation JFeedBackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"用户反馈";
    self.feedbackTextView.placeholder = @"问题描述 （必填）";
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.feedbackTextView.selectedRange = NSMakeRange(0, 0);
    self.sendFeedBackBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.sendFeedBackBtn setTitle:@"提交" forState:UIControlStateNormal];
    [self.sendFeedBackBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.sendFeedBackBtn sizeToFit];
    self.sendFeedBackBtn.enabled = NO;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.sendFeedBackBtn];
    
    [[self.sendFeedBackBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        if([self.feedbackTextView.text length] > 0){
            [self sendFeedBack];
        }
    }];
    
    self.feedbackTextView.delegate = self;
    [[self.feedbackTextView rac_textSignal] subscribeNext:^(NSString* x) {
        if (x.length > 0) {
            [self.sendFeedBackBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }else{
           [self.sendFeedBackBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        }
    }];
    self.feedbackTextView.layer.borderWidth = 0.5;
    self.feedbackTextView.layer.borderColor = UIColorFromRGB(0xc6c6c6).CGColor;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = NO;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)sendFeedBack{
    if([self.feedbackTextView.text length] > 0){
        if (self.feedbackTextView.text.length > 400) {
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"不能超过400个字符" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
            [alertView show];
            return;
        }
        @weakify(self);
        [MBProgressHUD showMessag:@"反馈中..." toView:self.view];
        NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];

        // app名称
        NSString *app_id = [infoDictionary objectForKey:@"CFBundleIdentifier"];
        
         NSString* version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
        NSString* mac = [JGlobals shareGlobal].deveicRouter.identifier ? [JGlobals shareGlobal].deveicRouter.identifier :@"";
        if (mac.length <= 0) {
            mac =  [JGlobals shareGlobal].routerMAC;
        }
        [[JServerClient feedback:self.feedbackTextView.text sn:mac token: [JGlobals shareGlobal].token.length > 0 ? [JGlobals shareGlobal].token :@"" appver:version appId:app_id] subscribeNext:^(id x) {
            @strongify(self);
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            [MBProgressHUD showSuccess:@"反馈成功！" toView:self.view];
            self.feedbackTextView.text = @"";
        } error:^(NSError *error) {
            @strongify(self);
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
        }];
    }else{
       UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"" message:@"请输入您的宝贵意见！" delegate:nil cancelButtonTitle:@"好的" otherButtonTitles:nil, nil] ;
        [alert show];
    }
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    if (self.feedbackTextView.text.length >= 400 ) {
        if (text.length == 0) {
            return YES;
        }
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"不能超过400个字符" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
        [alertView show];
        return NO;
    }
    return YES;
}


@end
