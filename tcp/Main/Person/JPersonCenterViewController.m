//
//  JPersonCenterViewController.m
//  tcp
//
//  Created by Jelly on 15/8/9.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JPersonCenterViewController.h"
#import <UIImageView+WebCache.h>
#import "UUPhotoActionSheet.h"
#import "MMPickerView.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVFoundation.h>


@interface JPersonCenterViewController()<UITextFieldDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property(nonatomic,strong)JUserModule* userInfo;
@property (nonatomic, strong) UIActionSheet *sheet;
@property(nonatomic,strong) UIButton* rightBtn;
@property(nonatomic, strong) UIImage* image;
@property(nonatomic, assign) BOOL haveChange;
@end

@implementation JPersonCenterViewController


-(void)viewDidLoad{
    [super viewDidLoad];
    self.title = @"个人资料";
    self.haveChange = NO;
    self.userInfo = [[JGlobals shareGlobal].user copy];
    if(self.userInfo){
        [self.avtorImageView sd_setImageWithURL:[NSURL URLWithString:self.userInfo.avatarUrl] placeholderImage:[UIImage imageNamed:@"icon_regist_normalFace"]];
        
        if ([self.userInfo.sex integerValue] == 0) {
            self.girlBtn.selected = YES;
            self.boyBtn.selected = NO;
        }else{
            self.boyBtn.selected = YES;
            self.girlBtn.selected = NO;
        }
        self.ageLabel.text = [NSString stringWithFormat:@"%@",[self.userInfo.age integerValue] <= 0 ? @"25" : self.userInfo.age];
        self.nickNameTextField.text = self.userInfo.nickname.length > 0 ? self.userInfo.nickname : @"AigoLife";
    }
    self.nickNameTextField.delegate = self;
    [self setNavBar];
    
    [self btnAction];
    
    self.avtorImageView.layer.cornerRadius = self.avtorImageView.width/2;
    self.avtorImageView.layer.masksToBounds = YES;
    
    
    UITextField* targetTextField = self.nickNameTextField;
    CGRect frame = [targetTextField frame];
    frame.size.width = 7.0f;
    UIView *leftview = [[UIView alloc] initWithFrame:frame];
    targetTextField.leftViewMode = UITextFieldViewModeAlways;
    targetTextField.leftView = leftview;
    
    @weakify(self);
    [[self.nickNameTextField rac_textSignal] subscribeNext:^(id x) {
        @strongify(self);
        NSString* name = self.userInfo.nickname.length > 0 ? self.userInfo.nickname : @"AigoLife";
        if (![x isEqualToString:name]) {
            self.haveChange = YES;
            
            [self.rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
        
    }];
    
    
}



-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

#pragma mark 导航栏

-(void)setNavBar{
    UIButton* btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"保存" forState:UIControlStateNormal];
    [btn sizeToFit];
    self.rightBtn = btn;
    [self.rightBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem =[[UIBarButtonItem alloc] initWithCustomView:self.rightBtn];
}


#pragma mark  btnAction
-(void)btnAction{
    @weakify(self);
    [[self.rightBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        [self saveUserInfo];
    }];
    
    [[self.boyBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        self.boyBtn.selected = YES;
        self.girlBtn.selected = NO;
        self.haveChange = YES;
        [self.rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }];
    
    [[self.girlBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        self.girlBtn.selected = YES;
        self.boyBtn.selected = NO;
        self.haveChange = YES;
        [self.rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }];
    
    
}



-(void)saveUserInfo{
    if([self.nickNameTextField.text length] <= 0){
        [MBProgressHUD showError:@"请输入呢称" toView:self.view];
        return;
    }
    if([self.ageLabel.text length] <= 0){
        [MBProgressHUD showError:@"请选择年龄" toView:self.view];
        return;
    }
    if(!self.haveChange){
        
        return;
    }
    @weakify(self);
    [MBProgressHUD showMessag:@"更新资料中..." toView:self.view];
    
    if (self.image) {
        [[JServerClient uploadFile:UIImageJPEGRepresentation(self.image, 0.5) token:[JGlobals shareGlobal].token] subscribeNext:^(id x) {
            @strongify(self);
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            NSString* imageUrl = x[@"url"];
            if ([imageUrl length] > 0) {
                self.userInfo.avatarUrl = imageUrl;
                [self updateuser];
            }
        } error:^(NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:NO];
            [MBProgressHUD showError:@"更新失败" toView:self.view];
        }];
    }else{
        [self updateuser];
    }
    
    
}


-(void)updateuser{
    @weakify(self);
    self.userInfo.age = @([self.ageLabel.text integerValue]);
    self.userInfo.nickname = self.nickNameTextField.text;
    self.userInfo.sex = self.boyBtn.selected ?@(1):@(0);
    [[JServerClient UpdateUserInfo:self.userInfo token:[JGlobals shareGlobal].token] subscribeNext:^(id x) {
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showSuccess:@"更新成功" toView:self.view];
        self.haveChange = NO;
        [JGlobals shareGlobal].user = self.userInfo;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.navigationController popViewControllerAnimated:YES];
        });
    } error:^(NSError *error) {
        @strongify(self);
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:[error.userInfo objectForKey:@"errmessage"] toView:self.view];
        
        self.userInfo.age = [JGlobals shareGlobal].user.age;
        self.userInfo.nickname = [JGlobals shareGlobal].user.nickname;
        self.userInfo.sex = [JGlobals shareGlobal].user.sex;
    }];
}
/**
 *  更新用户头衔
 *
 *  @param sender btn
 */
- (IBAction)updateAvtor:(id)sender {
    [self showTouxiangPicker];
}


- (IBAction)showAge:(id)sender {
    NSMutableArray* array = [NSMutableArray array];
    for (NSInteger i = 1; i<=99; i++) {
        [array addObject:[NSString stringWithFormat:@"%d",i]];
    }
    
    NSInteger age = [self.ageLabel.text integerValue];
    if (age <= 0 ) {
        age = 25;
    }
    @weakify(self);
    [MMPickerView  showPickerViewInView:self.view withStrings:array withOptions:@{@"selectedObject":[NSString stringWithFormat:@"%d",age]} completion:^(NSString *selectedString) {
        @strongify(self);
        self.haveChange = YES;
        
        [self.rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.ageLabel.text = selectedString;
    }];
}


-(BOOL)haveChange{
    if (_haveChange == YES) {
        return YES;
    }
    if ([self.ageLabel.text integerValue] != ([self.userInfo.age integerValue] <= 0 ? 25 : [self.userInfo.age integerValue]) || ![self.nickNameTextField.text isEqualToString:self.userInfo.nickname.length > 0 ? self.userInfo.nickname : @"AigoLife"] || [self.userInfo.sex integerValue] != (self.boyBtn.selected ?1:0)) {
        return YES;
    }
    return NO;
}

-(void)backAction{
    if(self.haveChange){
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"您有新的修改没保存，是否离开？ " delegate:nil cancelButtonTitle:@"否" otherButtonTitles:@"是", nil];
        [alertView show];
        @weakify(self);
        [[alertView rac_buttonClickedSignal] subscribeNext:^(id x) {
            if ([x integerValue] == 1) {
                @strongify(self);
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

/**
 *  显示头像
 */
-(void)showTouxiangPicker{
    self.sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles: @"打开照相机", @"从手机相册获取",nil];
     [self.sheet showInView:self.view];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == self.sheet.cancelButtonIndex)
    {
        return;
    }
    switch (buttonIndex)
    {
        case 0:  //打开照相机拍照
            [self takePhoto];
            break;
        case 1:  //打开本地相册
            [self LocalPhoto];
            break;
    }
}

-(void)takePhoto
{
    NSString *mediaType = AVMediaTypeVideo;
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
    if(authStatus == ALAuthorizationStatusRestricted || authStatus == ALAuthorizationStatusDenied){
        
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"没有打开摄像头的权限" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = sourceType;
        [self presentViewController:picker animated:YES completion:nil];
    }
}


-(void)LocalPhoto
{
    ALAuthorizationStatus author = [ALAssetsLibrary authorizationStatus];
    
    if(author == ALAuthorizationStatusRestricted || author == ALAuthorizationStatusDenied){
        
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"没有使用相册的权限" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.delegate = self;
    //设置选择后的图片可被编辑
    picker.allowsEditing = YES;
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    self.haveChange = YES;
    [self.rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.image = image;
    self.avtorImageView.image = self.image;
    self.avtorImageView.layer.cornerRadius = self.avtorImageView.width/2;
    self.avtorImageView.layer.masksToBounds = YES;
}


-(void)loadUserInfo{
    if ([JGlobals shareGlobal].user || [JGlobals shareGlobal].token.length <= 0 ) {
        return;
    }
    [[JServerClient GetUserInfo:[JGlobals shareGlobal].userName] subscribeNext:^(id x) {
        JUserModule* user = [MTLJSONAdapter modelOfClass:[JUserModule class] fromJSONDictionary:x[@"user"] error:nil];
        [JGlobals shareGlobal].user = user;
    } error:^(NSError *error) {
        
    }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
    if ([string isEqualToString:@"\n"])
    {
        return YES;
    }
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (self.nickNameTextField == textField)
    {
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression
                                      regularExpressionWithPattern:@"([^A-Za-z0-9_.])"
                                      options:0
                                      error:&error];
        
        NSString *resString = [regex stringByReplacingMatchesInString:string options:0 range:NSMakeRange(0, string.length) withTemplate:@""];
        
        if (resString.length != string.length) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"不能输入特殊字符" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] ;
            [alert show];
            return NO;
        }
        
        if ([toBeString length] > 16) {
            textField.text = [toBeString substringToIndex:16];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"不得输入超过8个汉字或16个字符" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil] ;
            [alert show];
            return NO;
        }
    }
    return YES;
}

@end
