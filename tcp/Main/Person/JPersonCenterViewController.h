//
//  JPersonCenterViewController.h
//  tcp
//
//  Created by Jelly on 15/8/9.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JBaseViewController.h"
#import "JServerClient.h"
@interface JPersonCenterViewController : JBaseViewController

/**
 *  头像ImageView
 */
@property (weak, nonatomic) IBOutlet UIImageView *avtorImageView;

/**
 *  呢称
 */
@property (weak, nonatomic) IBOutlet UITextField *nickNameTextField;
/**
 *  年龄
 */
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;

@property (weak, nonatomic) IBOutlet UIButton *ageBtn;

@property (weak, nonatomic) IBOutlet UIButton *boyBtn;

@property (weak, nonatomic) IBOutlet UIButton *girlBtn;

- (IBAction)showAge:(id)sender;
@end
