//
//  JMainHeaderView.h
//  tcp
//
//  Created by Jelly on 15/7/21.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#define kNomalColor UIColorFromRGB(0x46aefa)
#define kErrorColor UIColorFromRGB(0xeb2f2f)
#define kSlowColor UIColorFromRGB(0xfeb519)

@interface JMainHeaderView : UIView


@property(nonatomic, strong) CAGradientLayer*gradient;

@property(nonatomic,assign) WifiStatue statue;

@property(nonatomic, strong) UIColor* headerColor;
@end
