//
//  JMainHeaderView.m
//  tcp
//
//  Created by Jelly on 15/7/21.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JMainHeaderView.h"

#define kHeight 280


#define kButtomNomal UIColorFromRGB(0x1d7fc9)
#define kButtomError UIColorFromRGB(0xe01010)
#define kButtomSlow UIColorFromRGB(0xffae00)

@implementation JMainHeaderView

-(void)setStatue:(WifiStatue)statue{
    _statue = statue;
    UIColor* headerColor;
    UIColor* buttomColor;
    if (statue == WifiStatueExtranet || statue == WifiStatueIntranet || statue == WifiStatueUndefine || statue == WifiStatueUnKnow) {
        headerColor = kErrorColor;
        buttomColor = kButtomError;
        
        self.headerColor = UIColorFromRGB(0xE04110);
    }else if(statue == WifiStatueSlow){
        headerColor = kSlowColor;
        buttomColor = kButtomSlow;
        
        self.headerColor = kSlowColor;
    }else{
        self.headerColor = UIColorFromRGB(0x469DFA);
        headerColor = kNomalColor;
        buttomColor = kButtomNomal;
    }
    
    [self drewBackgroundColor:headerColor buttom:buttomColor];
}


-(void)drewBackgroundColor:(UIColor*)headerColor buttom:(UIColor*)buttomColor{
    CGRect frame = self.bounds;
    frame.origin.y = 20;
    frame.size.height -= 20;
    frame.size.width +=120;
    frame.origin.x -=30;
    if (!self.gradient) {
      self.gradient = [CAGradientLayer layer];
    }
    if ([self.gradient superlayer]) {
        [self.gradient removeFromSuperlayer];
    }
    self.gradient.frame = frame;
    self.gradient.colors = [NSArray arrayWithObjects:(id)headerColor.CGColor,
                       (id)buttomColor.CGColor,nil];
    
    [self.layer insertSublayer:self.gradient atIndex:0];
    [self.layer setNeedsDisplay];
    self.layer.masksToBounds = YES;
}

@end
