//
//  JMenuViewController.m
//  tcp
//
//  Created by dong on 15/7/21.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import "JMenuViewController.h"
#import "JMainViewController.h"
//#import <REFrostedViewController.h>
//#import "UIViewController+REFrostedViewController.h"
#import <MMDrawerController.h>
#import <UIViewController+MMDrawerController.h>
#import "AppDelegate.h"
#import "JRouterSettingsViewController.h"
#import "JFeedBackViewController.h"
#import <UIImageView+WebCache.h>

@implementation JMenuViewController

-(void)viewDidLoad{
    [super viewDidLoad];
//    FIX_EDGES_FOR_EXTENDEDLAYTOU;
    
    self.navigationController.navigationBarHidden = YES;
    [self.tableView setTableFooterView:[[UIView alloc] init]];
    self.view.backgroundColor = UIColorFromRGB(0x606060);
    self.topView.backgroundColor = UIColorFromRGB(0x606060);
    
    [self buttonAction];
    self.faceBGView.layer.masksToBounds = YES;
    self.faceBGView.layer.cornerRadius = self.faceBGView.width/2;
    self.avtorImageView.layer.masksToBounds = YES;
    self.avtorImageView.layer.cornerRadius = self.avtorImageView.width/2;
    self.avtorBgImageView.layer.masksToBounds = YES;
    self.avtorBgImageView.layer.cornerRadius = self.avtorBgImageView.width/2;

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   
    
//    if([[JGlobals shareGlobal].token length] > 0){
        self.logoutCell.hidden = NO;
        self.logoutCell.textLabel.text = @"注销账号";
//    }else{
//        self.logoutCell.hidden = YES;
//    }
    if([JGlobals shareGlobal].user){
        self.nameLabel.hidden = NO;
        NSString* name = [JGlobals shareGlobal].user.nickname;
        if (name.length <= 0) {
            name = @"AigoLife";
        }
        self.nameLabel.text = name;
    }else{
        self.nameLabel.hidden = NO;
        self.nameLabel.text = @"AigoLife";
    }
    
    
    if ([JGlobals shareGlobal].user) {
        [self.avtorImageView sd_setImageWithURL:[NSURL URLWithString:[JGlobals shareGlobal].user.avatarUrl] placeholderImage:[UIImage imageNamed:@"icon_regist_normalFace"]];
    }
}

#pragma mark 按钮事件
-(void)buttonAction{
    @weakify(self);
    [[self.backBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        [self.mm_drawerController closeDrawerAnimated:YES completion:nil];
    }];
    
    
    [[self.faceBtn rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self);
        if ([JGlobals shareGlobal].token.length > 0 && [JGlobals shareGlobal].user) {
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            //由storyboard根据myView的storyBoardID来获取我们要切换的视图
            UIViewController *settingViewController = [story instantiateViewControllerWithIdentifier:@"JPersonCenterViewController"];
            UINavigationController* nav = (UINavigationController*)self.mm_drawerController.centerViewController;
            [nav pushViewController:settingViewController animated:YES];
            [self.mm_drawerController closeDrawerAnimated:NO completion:^(BOOL finished) {
               
            }];
            
        }
    }];
}

#pragma mark tableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row != 4 && indexPath.row != 3) {
        if (![JGlobals shareGlobal].deveicRouter && [JGlobals shareGlobal].user) {
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"您还没有绑定路由" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
            [alertView show];
            return ;
        }
        if ([JGlobals shareGlobal].wifiStatue == WifiStatueUndefine || [JGlobals shareGlobal].wifiStatue == WifiStatueUnKnow) {
            [self showErrorMessage:@"云平台检测不到设备"];
            return;
        }
        if ([JGlobals shareGlobal].wifiStatue == WifiStatueCheckRouter) {
            [self showErrorMessage:@"请等待设备连接完成"];
            return;
        }
        
        
    }
    
    switch (indexPath.row) {
        case 0:{

            UIStoryboard *story = [UIStoryboard storyboardWithName:@"RouterStoryboard" bundle:[NSBundle mainBundle]];
            //由storyboard根据myView的storyBoardID来获取我们要切换的视图
            JRouterSettingsViewController *settingViewController = [story instantiateViewControllerWithIdentifier:@"routerSetting"];
            UINavigationController* nav = (UINavigationController*)self.mm_drawerController.centerViewController;
            [nav pushViewController:settingViewController animated:YES];
            [self.mm_drawerController closeDrawerAnimated:NO completion:^(BOOL finished) {
               
            }];
            
        }break;
        case 1:{
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            UIViewController* viewController = [story instantiateViewControllerWithIdentifier:@"restartIndetifier"];
            UINavigationController* nav = (UINavigationController*)self.mm_drawerController.centerViewController;
            [nav pushViewController:viewController animated:YES];
            [self.mm_drawerController closeDrawerAnimated:NO completion:^(BOOL finished) {
                
            }];
        }break;
        case 2:{
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            UIViewController* viewController = [story instantiateViewControllerWithIdentifier:@"JRouterUpgregeViewController"];
            UINavigationController* nav = (UINavigationController*)self.mm_drawerController.centerViewController;
            [nav pushViewController:viewController animated:YES];
            [self.mm_drawerController closeDrawerAnimated:NO completion:^(BOOL finished) {
                
            }];
        }break;
        case 3:{
            UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            //由storyboard根据myView的storyBoardID来获取我们要切换的视图
            JFeedBackViewController *settingViewController = [story instantiateViewControllerWithIdentifier:@"feedBack"];
            UINavigationController* nav = (UINavigationController*)self.mm_drawerController.centerViewController;
            [nav pushViewController:settingViewController animated:YES];

            [self.mm_drawerController closeDrawerAnimated:NO completion:^(BOOL finished) {
                            }];
        }break;
             
        case 4:{
            [self loginAction];
        }break;
            
        default:
            break;
    }
}

/**
 *  登录退出
 */
-(void)loginAction{
    
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"" message:@"是否要注销当前登录？" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
        
        [[alert rac_buttonClickedSignal] subscribeNext:^(id x) {
            if ([x integerValue] == 0 ) {
                AppDelegate* appdelegate = [UIApplication sharedApplication].delegate;
                [appdelegate showLoginView];
            }
        }];
        [alert show];

}

#pragma mark loginviewDelegate

-(void)loginDidSuccess{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)loginDissmess{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

@end
