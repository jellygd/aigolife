//
//  JFeedBackViewController.h
//  tcp
//
//  Created by Jelly on 15/7/31.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JBaseViewController.h"

@interface JFeedBackViewController : JBaseViewController


@property (weak, nonatomic) IBOutlet UITextView *feedbackTextView;

@end
