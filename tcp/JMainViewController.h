//
//  ViewController.h
//  tcp
//
//  Created by Jelly on 15/7/19.
//  Copyright (c) 2015年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JMainHeaderView.h"
#import "JBaseViewController.h"

@interface JMainViewController : JBaseViewController


@property (weak, nonatomic) IBOutlet UIButton *shakeButton;

@property (weak, nonatomic) IBOutlet UIImageView *mainMenu1;
@property (weak, nonatomic) IBOutlet UIImageView *mainMenu2;
@property (weak, nonatomic) IBOutlet UIImageView *mainMenu3;
@property (weak, nonatomic) IBOutlet UIImageView *mainMenu4;

@property (weak, nonatomic) IBOutlet UILabel *mainMenuNum1;
@property (weak, nonatomic) IBOutlet UILabel *mainMenuNum2;
@property (weak, nonatomic) IBOutlet UILabel *mainMenuNum3;
@property (weak, nonatomic) IBOutlet UILabel *mainMenuNum4;

//头部View
/**
 *  路由状态图片View 有三种状态  1.蓝色  2.黄色 3.红色
 */
@property (weak, nonatomic) IBOutlet UIImageView *routerStatueImageVIew;

/**
 *  头部View 设置背景色 渐变色
 */
@property (weak, nonatomic) IBOutlet JMainHeaderView *headerView;

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
/**
 *  loading  检查View
 */
@property (weak, nonatomic) IBOutlet UIImageView *loadingView;
@property (weak, nonatomic) IBOutlet UILabel *headerTipsLabel;
@property (weak, nonatomic) IBOutlet UILabel *buttomTipsLabel;

@property (weak, nonatomic) IBOutlet UIView *nomarlView;

@property (weak, nonatomic) IBOutlet UIButton *netWorkButton;

@property (weak, nonatomic) IBOutlet UIButton *deviceButton;
@property (weak, nonatomic) IBOutlet UIButton *storageButton;
@property (weak, nonatomic) IBOutlet UIButton *JacksButton;


@property (weak, nonatomic) IBOutlet UIView *redView;
@property (weak, nonatomic) IBOutlet UILabel *redleftLabel;
@property (weak, nonatomic) IBOutlet UILabel *redRightLabel;


/**
 *  显示消息界面
 */
-(void)showMessagesView;

@end

